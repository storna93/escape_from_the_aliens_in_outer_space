- Server:
To start the server you have to run an instance
of Server.

- Client:
To start a client you have to run an instance
of ClientCLI or ClientGUI and then you have 
to select the desired type of connection.

- Match:
To begin a new match you have to start the server
and at least two client (then the match will
starts in 15 seconds).

- ClientCLI commands:
If you send "help" then you could display the 
commands that the server can recognize as well
formed commands.

- ClientGUI exit command:
You can disconnect and exit the game through the
menu and the button or by closing the game window.

- ClientGUI move/fake_noise/activate_spotlight commands:
You can send these commands clicking
on a certain Sector of the zone image.

- ClientGUI zoom:
You can zoom the zone image using the mouse wheel and
move the zoomed zone image clicking the mouse wheel.

- Setting the match zone:
A specific client can set the zone before
starting match, only if he is the administrator of the
match.

- SedativeItem and AttackItem:
A client who has a human character can avoid drawing
a card or attack, only if he has already activated
SedativeItem or AttackItem respectively and then he use 
"use_sedatives" command or "use_attack" command respectively.

- Reconnection:
A client can reconnect (and then continue playing) only if he has already been
disconnected (by user himself or by the end of timeout or because
missing network) sending the command "reconnect".