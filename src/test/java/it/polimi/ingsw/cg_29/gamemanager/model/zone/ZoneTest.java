package it.polimi.ingsw.cg_29.gamemanager.model.zone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;

import org.junit.Before;
import org.junit.Test;

public class ZoneTest {

    Zone z1, z2, z3;

    @Before
    public void init() {
        z1 = new GalileiZoneCreator().createZone();
        z2 = new GalvaniZoneCreator().createZone();
        z3 = new FermiZoneCreator().createZone();
    }

    @Test
    public void testConstructor() {
        assertTrue(z1 instanceof GalileiZone);
        assertTrue(z2 instanceof GalvaniZone);
        assertTrue(z3 instanceof FermiZone);
        assertTrue(z3.getSectorByString("J", "05") instanceof EscapeHatchSector);
        assertTrue(z3.getSectorByString("M", "02") instanceof DangerousSector);
        assertTrue(z3.getSectorByString("L", "09") instanceof AlienSector);
    }

    @Test
    public void testToString() {
        String expected = "Sector[[H10][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[H10{9,7}]]]]]\n" + "Sector[[H11][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[H11{10,7}]]]]]\n" + "Sector[[I06][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[I06{5,8}]]]]]\n" + "Sector[[I07][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[I07{6,8}]]]]]\n"
                + "Sector[[I09][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[I09{8,8}]]]]]\n" + "Sector[[I10][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[I10{9,8}]]]]]\n" + "Sector[[I12][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[I12{11,8}]]]]]\n" + "Sector[[J01][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[J01{0,9}]]]]](usable=true)\n"
                + "Sector[[J03][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[J03{2,9}]]]]]\n" + "Sector[[J04][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[J04{3,9}]]]]]\n" + "Sector[[J05][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[J05{4,9}]]]]](usable=true)\n" + "Sector[[J07][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[J07{6,9}]]]]]\n"
                + "Sector[[J08][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[J08{7,9}]]]]]\n" + "Sector[[J10][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[J10{9,9}]]]]]\n" + "Sector[[J11][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[J11{10,9}]]]]]\n" + "Sector[[J12][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[J12{11,9}]]]]]\n"
                + "Sector[[K02][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[K02{1,10}]]]]]\n" + "Sector[[K03][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[K03{2,10}]]]]]\n" + "Sector[[K04][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[K04{3,10}]]]]]\n" + "Sector[[K08][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[K08{7,10}]]]]]\n"
                + "Sector[[K11][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[K11{10,10}]]]]]\n" + "Sector[[K13][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[K13{12,10}]]]]]\n" + "Sector[[L03][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[L03{2,11}]]]]]\n" + "Sector[[L04][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[L04{3,11}]]]]]\n"
                + "Sector[[L05][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[L05{4,11}]]]]]\n" + "Sector[[L06][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[L06{5,11}]]]]]\n" + "Sector[[L07][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[L07{6,11}]]]]]\n" + "Sector[[L08][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[L08{7,11}]]]]]\n"
                + "Sector[[L09][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[L09{8,11}]]]]]\n" + "Sector[[L10][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[L10{9,11}]]]]]\n" + "Sector[[L11][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[L11{10,11}]]]]]\n" + "Sector[[L12][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[L12{11,11}]]]]]\n"
                + "Sector[[M02][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[M02{1,12}]]]]]\n" + "Sector[[M03][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[M03{2,12}]]]]]\n" + "Sector[[M04][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[M04{3,12}]]]]]\n" + "Sector[[M08][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[M08{7,12}]]]]]\n"
                + "Sector[[M11][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[M11{10,12}]]]]]\n" + "Sector[[M13][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[M13{12,12}]]]]]\n" + "Sector[[N01][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[N01{0,13}]]]]](usable=true)\n" + "Sector[[N03][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[N03{2,13}]]]]]\n"
                + "Sector[[N04][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[N04{3,13}]]]]]\n" + "Sector[[N05][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[N05{4,13}]]]]](usable=true)\n" + "Sector[[N07][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[N07{6,13}]]]]]\n" + "Sector[[N08][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[N08{7,13}]]]]]\n"
                + "Sector[[N10][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[N10{9,13}]]]]]\n" + "Sector[[N11][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[N11{10,13}]]]]]\n" + "Sector[[N12][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[N12{11,13}]]]]]\n" + "Sector[[O06][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[O06{5,14}]]]]]\n"
                + "Sector[[O07][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[O07{6,14}]]]]]\n" + "Sector[[O09][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[O09{8,14}]]]]]\n" + "Sector[[O10][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[O10{9,14}]]]]]\n" + "Sector[[O12][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[O12{11,14}]]]]]\n"
                + "Sector[[P10][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[P10{9,15}]]]]]\n" + "Sector[[P11][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[P11{10,15}]]]]]\n";
        assertEquals(expected, z3.toString());
    }

    @Test
    public void testPrintToString() {
        String expected = "========================================================================================================================================================================================\n" + "                                                                                                                                                                                        \n" + "                                                                        <J01(E)>                        <N01(E)>                                                                        \n" + "                                                                                <K02(S)>        <M02(D)>                                                                                \n"
                + "                                                                                                                                                                                        \n" + "                                                                                <K03(S)>        <M03(S)>                                                                                \n" + "                                                                        <J03(D)>        <L03(D)>        <N03(D)>                                                                        \n" + "                                                                                <K04(D)>        <M04(D)>                                                                                \n"
                + "                                                                        <J04(S)>        <L04(D)>        <N04(S)>                                                                        \n" + "                                                                                                                                                                                        \n" + "                                                                        <J05(E)>        <L05(S)>        <N05(E)>                                                                        \n" + "                                                                <I06(S)>                                        <O06(S)>                                                                \n"
                + "                                                                                        <L06(S)>                                                                                        \n" + "                                                                <I07(D)>                                        <O07(S)>                                                                \n" + "                                                                        <J07(S)>        <L07(S)>        <N07(D)>                                                                        \n" + "                                                                                <K08(S)>        <M08(S)>                                                                                \n"
                + "                                                                        <J08(S)>        <L08(S)>        <N08(S)>                                                                        \n" + "                                                                <I09(D)>                                        <O09(S)>                                                                \n" + "                                                                                        <L09(A)>                                                                                        \n" + "                                                                <I10(S)>                                        <O10(D)>                                                                \n"
                + "                                                        <H10(S)>        <J10(S)>        <L10(H)>        <N10(S)>        <P10(S)>                                                        \n" + "                                                                                <K11(S)>        <M11(S)>                                                                                \n" + "                                                        <H11(S)>        <J11(S)>        <L11(S)>        <N11(S)>        <P11(D)>                                                        \n" + "                                                                <I12(D)>                                        <O12(S)>                                                                \n"
                + "                                                                        <J12(S)>        <L12(S)>        <N12(D)>                                                                        \n" + "                                                                                <K13(D)>        <M13(S)>                                                                                \n" + "                                                                                                                                                                                        \n" + "                                                                                                                                                                                        \n"
                + "                                                                                                                                                                                        \n" + "========================================================================================================================================================================================";
        assertEquals(expected, z3.printToString());
    }

    @Test
    public void testException() {

        boolean exception = false;
        try {
            z1.addSectorByString(0, 0, null);
        } catch (NullPointerException e) {
            LoggerClass.getLogger().warn(e);
            exception = true;
        }
        assertTrue(exception);

    }

    @Test
    public void testSpawnSector() throws NotWellFormedZoneException {
        Coordinates humanSectorCoordinates = new Coordinates("L", "10");
        Coordinates alienSectorCoordinates = new Coordinates("L", "9");

        assertEquals(humanSectorCoordinates, z3.getHumanSectorCoordinates());
        assertEquals(alienSectorCoordinates, z3.getAlienSectorCoordinates());

        Zone z4 = new FermiZone();
        z4.addSectorByString(0, 0, "V");
    }
}
