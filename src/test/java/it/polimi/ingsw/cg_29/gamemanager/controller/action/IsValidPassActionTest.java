package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Alien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class IsValidPassActionTest {
    Player player1, player2;
    Match currentMatch;

    @Before
    public void initMatch() throws NotWellFormedZoneException {
        List<String> clientIdList = new ArrayList<String>();
        clientIdList.add("pippo");
        clientIdList.add("pluto");
        ZoneCreator currentZoneCreator = new FermiZoneCreator();
        currentMatch = new Match(clientIdList, currentZoneCreator);

        player1 = currentMatch.getSpecificPlayerById("pippo");
        player2 = currentMatch.getSpecificPlayerById("pluto");

    }

    @Test
    public void TestValidPassFromBeginningState() {

        Action passAction = new PassAction("pippo");

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, passAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(false, passAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidPassFromMoveSecureState() {

        Action passAction = new PassAction("pippo");

        // change player1 RoundState
        player1.setRoundState(RoundState.MOVED_SECURE);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(true, passAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(true, passAction.isValid(currentMatch));

        }
    }

    @Test
    public void TestValidPassFromEndingState() {

        Action passAction = new PassAction("pippo");

        // change player1 RoundState
        player1.setRoundState(RoundState.ENDING);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(true, passAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(true, passAction.isValid(currentMatch));

        }
    }

}
