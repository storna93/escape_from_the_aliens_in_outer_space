package it.polimi.ingsw.cg_29.gamemanager.model.deck;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class DeckTest {

    @Test
    public void testConstructorDeck() {

        DangerousSectorDeckCreator creatorD = new DangerousSectorDeckCreator();
        DangerousSectorDeck deckD = creatorD.createDeck();

        EscapeHatchDeckCreator creatorE = new EscapeHatchDeckCreator();
        EscapeHatchDeck deckE = creatorE.createDeck();

        ItemDeckCreator creatorI = new ItemDeckCreator();
        ItemDeck deckI = creatorI.createDeck();

        CharacterDeckCreator creatorC = new CharacterDeckCreator();
        CharacterDeck deckC = creatorC.createDeck();

        CharacterDeckCreator creatorN = new CharacterDeckCreator();
        CharacterDeck deckN = creatorN.createDeck(7);

        Card c1 = deckD.getCardList().get(0);
        assertTrue(c1 instanceof Silence || c1 instanceof NoiseInYourSector || c1 instanceof NoiseInYourSectorIcon || c1 instanceof NoiseInAnySector || c1 instanceof NoiseInAnySectorIcon);

        c1 = deckE.getCardList().get(0);
        assertTrue(c1 instanceof EscapeGreen || c1 instanceof EscapeRed);

        c1 = deckI.getCardList().get(0);
        assertTrue(c1 instanceof SpotlightItem || c1 instanceof TeleportItem || c1 instanceof AttackItem || c1 instanceof DefenseItem || c1 instanceof SedativesItem || c1 instanceof AdrenalineItem);

        c1 = deckC.getCardList().get(0);
        assertTrue(c1 instanceof HumanCard || c1 instanceof AlienCard);

        c1 = deckN.getCardList().get(0);
        assertTrue(c1 instanceof HumanCard || c1 instanceof AlienCard);

        for (Card card : deckD.getCardList()) {
            assertTrue(card instanceof DangerousSectorCard);
        }
        for (Card card : deckE.getCardList()) {
            assertTrue(card instanceof EscapeHatchCard);
        }
        for (Card card : deckI.getCardList()) {
            assertTrue(card instanceof ItemCard);
        }
        for (Card card : deckC.getCardList()) {
            assertTrue(card instanceof CharacterCard);
        }
        for (Card card : deckN.getCardList()) {
            assertTrue(card instanceof CharacterCard);
        }

    }

    @Test
    public void testConstructorCharacterDeck() {

        int numAlien = 0;
        int numHuman = 0;

        // testing the CharacterDesk_constructor in case of a generic number n
        // of player
        // between 2 and 8
        for (int n = 2; n < 8; n++) {
            CharacterDeckCreator creatorC_nPlayer = new CharacterDeckCreator();
            CharacterDeck deckC_nPlayer = creatorC_nPlayer.createDeck(n);

            for (Card card : deckC_nPlayer.getCardList()) {
                if (card instanceof AlienCard)
                    numAlien++;
                if (card instanceof HumanCard)
                    numHuman++;
            }
            assertTrue(n == (numAlien + numHuman));
            numAlien = 0;
            numHuman = 0;
        }
    }

    @Test
    public void testDrawAndDiscardCard() {

        DangerousSectorDeckCreator creatorD = new DangerousSectorDeckCreator();
        DangerousSectorDeck deckD = creatorD.createDeck();

        EscapeHatchDeckCreator creatorE = new EscapeHatchDeckCreator();
        EscapeHatchDeck deckE = creatorE.createDeck();

        ItemDeckCreator creatorI = new ItemDeckCreator();
        ItemDeck deckI = creatorI.createDeck();

        CharacterDeckCreator creatorC = new CharacterDeckCreator();
        CharacterDeck deckC = creatorC.createDeck();

        Card c1;
        c1 = deckD.drawCard();
        deckD.discardCard(c1);
        assertTrue(!(deckD.getCardList().contains(c1)) && (deckD.getDiscardPile().contains(c1)));

        c1 = deckE.drawCard();
        deckE.discardCard(c1);
        assertTrue(!(deckE.getCardList().contains(c1)) && (deckE.getDiscardPile().contains(c1)));

        c1 = deckI.drawCard();
        deckI.discardCard(c1);
        assertTrue(!(deckI.getCardList().contains(c1)) && (deckI.getDiscardPile().contains(c1)));

        c1 = deckC.drawCard();
        deckC.discardCard(c1);
        assertTrue(!(deckC.getCardList().contains(c1)) && (deckC.getDiscardPile().contains(c1)));

    }

    @Test
    public void emptyAndReloadDeck() {
        DangerousSectorDeckCreator creatorD = new DangerousSectorDeckCreator();
        DangerousSectorDeck deckD = creatorD.createDeck();

        Card c1;
        while (!(deckD.isEmpty())) {
            c1 = deckD.drawCard();
            deckD.discardCard(c1);
        }

        assertEquals(null, deckD.drawCard());

        assertTrue(deckD.getCardList().size() == 0 && deckD.getDiscardPile().size() == 25);

        deckD.reloadDeck();

        assertTrue(deckD.getCardList().size() == 25 && deckD.getDiscardPile().size() == 0);

    }

}
