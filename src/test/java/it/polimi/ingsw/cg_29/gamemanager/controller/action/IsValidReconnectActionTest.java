package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.MatchState;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.PlayerState;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class IsValidReconnectActionTest {
    Player player1, player2;
    Match currentMatch;

    @Before
    public void initMatch() throws NotWellFormedZoneException {
        List<String> clientIdList = new ArrayList<String>();
        clientIdList.add("pippo");
        clientIdList.add("pluto");
        ZoneCreator currentZoneCreator = new FermiZoneCreator();
        currentMatch = new Match(clientIdList, currentZoneCreator);

        player1 = currentMatch.getSpecificPlayerById("pippo");
        player2 = currentMatch.getSpecificPlayerById("pluto");

    }

    @Test
    public void TestValidReconnectActionInRunningMatchStateOnADisconnectedPlayer() {

        player1.setPlayerState(PlayerState.DISCONNECTED);
        player1.setRoundState(RoundState.DISCONNECTED);
        Action reconnectAction = new ReconnectAction("pippo");

        assertEquals(true, reconnectAction.isValid(currentMatch));

    }

    @Test
    public void TestValidReconnectActionInFinishedMatchState() {

        currentMatch.setMatchState(MatchState.FINISHED);

        Action reconnectAction = new ReconnectAction("pippo");

        assertEquals(false, reconnectAction.isValid(currentMatch));

    }

    @Test
    public void TestValidReconnectActionInRunningMatchStateOnAConnectedPlayer() {

        Action reconnectAction = new ReconnectAction("pippo");

        assertEquals(false, reconnectAction.isValid(currentMatch));

    }
}
