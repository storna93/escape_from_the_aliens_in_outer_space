package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.SpotlightItem;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Alien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class DoActionActivateSpotlightActionTest {

    Player player1, player2, player3, player4;
    Match currentMatch;
    String startPlayer2 = ("M04");
    Coordinates startPlayer2Coord = new Coordinates(startPlayer2.substring(0, 1), startPlayer2.substring(1));
    String startPlayer3 = ("K04");
    Coordinates startPlayer3Coord = new Coordinates(startPlayer3.substring(0, 1), startPlayer3.substring(1));
    String startPlayer4 = ("L04");
    Coordinates startPlayer4Coord = new Coordinates(startPlayer4.substring(0, 1), startPlayer4.substring(1));

    @Before
    public void initMatch() throws NotWellFormedZoneException {
        List<String> clientIdList = new ArrayList<String>();
        clientIdList.add("pippo");
        clientIdList.add("pluto");
        clientIdList.add("paperino");
        clientIdList.add("topolino");
        ZoneCreator currentZoneCreator = new FermiZoneCreator();
        currentMatch = new Match(clientIdList, currentZoneCreator);

        player1 = currentMatch.getSpecificPlayerById("pippo");
        player2 = currentMatch.getSpecificPlayerById("pluto");
        player3 = currentMatch.getSpecificPlayerById("paperino");
        player4 = currentMatch.getSpecificPlayerById("topolino");

        player2.getMovementRecord().replaceCurrentCoordinates(startPlayer2Coord);

        player3.getMovementRecord().replaceCurrentCoordinates(startPlayer3Coord);

        player4.getMovementRecord().replaceCurrentCoordinates(startPlayer4Coord);

    }

    @Test
    public void TestDoHumanActivateSpotlightNoSpotlightResults() {

        String litCoordinates = "N11";
        Coordinates litCoord = new Coordinates(litCoordinates.substring(0, 1), litCoordinates.substring(1));

        Action activateSpotlightAction = new ActivateSpotlightAction("pippo", litCoord, new SpotlightItem());

        if (player1.getCharacter() instanceof Human) {

            activateSpotlightAction.doAction(currentMatch);
            assertEquals("", ((ActivateSpotlightAction) activateSpotlightAction).getSpotlightResultForTesting());
            assertEquals(RoundState.BEGINNING, player1.getRoundState());

        }

        else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));
            assertEquals(RoundState.BEGINNING, player1.getRoundState());

        }

    }

    @Test
    public void TestDoHumanActivateSpotlightOneSpotlightResult() {

        String litCoordinates = "L10";
        Coordinates litCoord = new Coordinates(litCoordinates.substring(0, 1), litCoordinates.substring(1));

        Action activateSpotlightAction = new ActivateSpotlightAction("pippo", litCoord, new SpotlightItem());

        if (player1.getCharacter() instanceof Human) {

            activateSpotlightAction.doAction(currentMatch);
            assertEquals("pippo has been revealed in " + litCoord.printToString() + "; ", ((ActivateSpotlightAction) activateSpotlightAction).getSpotlightResultForTesting());
            assertEquals(RoundState.BEGINNING, player1.getRoundState());

        }

        else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));
            assertEquals(RoundState.BEGINNING, player1.getRoundState());

        }

    }

    @Test
    public void TestDoHumanActivateSpotlightThreeSpotlightResult() {

        String litCoordinates = "L03";
        Coordinates litCoord = new Coordinates(litCoordinates.substring(0, 1), litCoordinates.substring(1));

        Action activateSpotlightAction = new ActivateSpotlightAction("pippo", litCoord, new SpotlightItem());

        if (player1.getCharacter() instanceof Human) {

            activateSpotlightAction.doAction(currentMatch);
            assertEquals("pluto has been revealed in " + this.startPlayer2Coord.printToString() + "; " + "topolino has been revealed in " + startPlayer4Coord.printToString() + "; " + "paperino has been revealed in " + startPlayer3Coord.printToString() + "; ", ((ActivateSpotlightAction) activateSpotlightAction).getSpotlightResultForTesting());
            assertEquals(RoundState.BEGINNING, player1.getRoundState());

        }

        else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));
            assertEquals(RoundState.BEGINNING, player1.getRoundState());

        }

    }

}
