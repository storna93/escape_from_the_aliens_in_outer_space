package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Alien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class DoActionPassActionTest {

    Player player1, player2;
    Match currentMatch;

    @Before
    public void initMatch() throws NotWellFormedZoneException {
        List<String> clientIdList = new ArrayList<String>();
        clientIdList.add("pippo");
        clientIdList.add("pluto");
        ZoneCreator currentZoneCreator = new FermiZoneCreator();
        currentMatch = new Match(clientIdList, currentZoneCreator);

        player1 = currentMatch.getSpecificPlayerById("pippo");
        player2 = currentMatch.getSpecificPlayerById("pluto");

    }

    @Test
    public void TestDoPassFromMoveSecureStateOnNormalHuman() {

        player1.setRoundState(RoundState.MOVED_SECURE);

        Action passAction = new PassAction("pippo");

        if (player1.getCharacter() instanceof Human) {

            passAction.doAction(currentMatch);
            assertEquals(RoundState.STANDBY, player1.getRoundState());

        }

        else if (player1.getCharacter() instanceof Alien) {

            passAction.doAction(currentMatch);
            assertEquals(RoundState.STANDBY, player1.getRoundState());

        }

    }

    @Test
    public void TestDoPassFromMoveSecureStateOnSedatedAndAggressiveHuman() {

        player1.setRoundState(RoundState.MOVED_SECURE);

        Action passAction = new PassAction("pippo");

        if (player1.getCharacter() instanceof Human) {

            player1.getCharacter().setBeSedate(true);
            player1.getCharacter().setCanAttack(true);

            passAction.doAction(currentMatch);
            assertEquals(RoundState.STANDBY, player1.getRoundState());
            assertEquals(false, player1.getCharacter().getBeSedate());
            assertEquals(false, player1.getCharacter().getCanAttack());

        }

        else if (player1.getCharacter() instanceof Alien) {

            passAction.doAction(currentMatch);
            assertEquals(RoundState.STANDBY, player1.getRoundState());

        }

    }
}
