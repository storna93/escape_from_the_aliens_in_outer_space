package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class DoActionFakeNoiseActionTest {
    Player player1, player2;
    Match currentMatch;

    @Before
    public void initMatch() throws NotWellFormedZoneException {
        List<String> clientIdList = new ArrayList<String>();
        clientIdList.add("pippo");
        clientIdList.add("pluto");

        ZoneCreator currentZoneCreator = new FermiZoneCreator();
        currentMatch = new Match(clientIdList, currentZoneCreator);

        player1 = currentMatch.getSpecificPlayerById("pippo");
        player2 = currentMatch.getSpecificPlayerById("pluto");

    }

    @Test
    public void TestDoFakeNoiseCall() {

        String fakeNoiseCoordinates = "N11";
        Coordinates fakeNoiseCoord = new Coordinates(fakeNoiseCoordinates.substring(0, 1), fakeNoiseCoordinates.substring(1));

        FakeNoiseCallAction fakeNoiseCallAction = new FakeNoiseCallAction("pippo", fakeNoiseCoord);

        player1.setRoundState(RoundState.FAKE_NOISE);

        fakeNoiseCallAction.doAction(currentMatch);
        assertEquals("[NOISE] " + player1.getClientId() + ": NOISE IN SECTOR " + fakeNoiseCoord.printToString(), fakeNoiseCallAction.getFakeNoisetResultForTesting());

        assertEquals(RoundState.ENDING, player1.getRoundState());

    }
}