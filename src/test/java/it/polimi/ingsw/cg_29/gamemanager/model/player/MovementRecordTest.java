package it.polimi.ingsw.cg_29.gamemanager.model.player;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Card;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.CharacterDeckCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Deck;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.DeckCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Zone;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import org.junit.Test;

public class MovementRecordTest {

    @Test
    public void testMovementRecord() throws NotWellFormedZoneException {
        // create a player to testing its movement record

        // create a clientId
        String clientId = new String("127.0.0.1");

        // create a characterDeck and then draw a character Card from it
        DeckCreator characterDeckCreator = new CharacterDeckCreator();
        Deck characterDeck = characterDeckCreator.createDeck();
        Card characterCard = characterDeck.drawCard();

        // create a zone
        ZoneCreator zoneCreator = new FermiZoneCreator();
        Zone zone = zoneCreator.createZone();

        // create a player

        Player player1 = new Player(clientId, characterCard, zone);

        Coordinates coordinates1 = new Coordinates(3, 7);
        player1.getMovementRecord().setNewCurrentCoordinates(coordinates1);
        Coordinates coordinates2 = new Coordinates(8, 10);
        player1.getMovementRecord().setNewCurrentCoordinates(coordinates2);

        assertEquals(3, player1.getMovementRecord().getCoordinatesList().size());
        assertEquals(coordinates2, player1.getMovementRecord().getCurrentCoordinates());

        Coordinates coordinates3 = new Coordinates(5, 7);
        player1.setCurrentPosition(coordinates3);
        assertEquals(coordinates3, player1.getCurrentPosition());
    }

}
