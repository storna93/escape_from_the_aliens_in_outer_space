package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.SedativesItem;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Alien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class DoActionUseSedativeActionTest {
    Player player1, player2;
    Match currentMatch;

    @Before
    public void initMatch() throws NotWellFormedZoneException {
        List<String> clientIdList = new ArrayList<String>();
        clientIdList.add("pippo");
        clientIdList.add("pluto");
        ZoneCreator currentZoneCreator = new FermiZoneCreator();
        currentMatch = new Match(clientIdList, currentZoneCreator);

        player1 = currentMatch.getSpecificPlayerById("pippo");
        player2 = currentMatch.getSpecificPlayerById("pluto");

    }

    @Test
    public void TestDoUseSedativeFromMoveDangerous() {

        Action useSedativeAction = new UseSedativeAction("pippo");
        Action activateSedativesAction = new ActivateSedativesAction("pippo", new SedativesItem());

        // change Player RoundState
        player1.setRoundState(RoundState.MOVED_DANGEROUS);
        // change Player beSedate attribute
        player1.getCharacter().setBeSedate(true);

        if (player1.getCharacter() instanceof Human) {

            useSedativeAction.doAction(currentMatch);
            assertEquals(false, player1.getCharacter().getBeSedate());
            assertEquals(RoundState.ENDING, player1.getRoundState());

        }

        else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(RoundState.MOVED_DANGEROUS, player1.getRoundState());

        }
    }
}
