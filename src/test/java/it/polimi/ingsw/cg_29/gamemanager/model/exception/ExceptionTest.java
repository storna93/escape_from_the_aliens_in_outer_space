package it.polimi.ingsw.cg_29.gamemanager.model.exception;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

public class ExceptionTest {

    @Test
    public void testNotWellFormedZoneException() {

        boolean exceptionDone = false;

        ZoneCreator zc1 = new FermiZoneCreator() {
            @Override
            public FermiZone createZone() {
                FermiZone zone = new FermiZone();
                zone.addSectorByString(0, 0, "V");
                return zone;
            }

        };

        try {
            new Match(new ArrayList<String>(Arrays.asList("123", "456", "789")), zc1);
        } catch (NotWellFormedZoneException e) {
            LoggerClass.getLogger().warn(e);
            exceptionDone = true;
        }

        assertTrue(exceptionDone);
    }

}
