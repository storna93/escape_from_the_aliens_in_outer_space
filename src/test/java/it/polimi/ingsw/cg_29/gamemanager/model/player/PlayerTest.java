package it.polimi.ingsw.cg_29.gamemanager.model.player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.AlienCard;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Card;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.CharacterDeckCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Deck;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.DeckCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.DefenseItem;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.ItemDeckCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Zone;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import org.junit.Test;

public class PlayerTest {

    @Test
    public void testConstructorPlayer() throws NotWellFormedZoneException {

        // create a clientId
        String clientId = new String("127.0.0.1");

        // create a characterDeck and then draw a character Card from it
        DeckCreator characterDeckCreator = new CharacterDeckCreator();
        Deck characterDeck = characterDeckCreator.createDeck();
        Card characterCard = characterDeck.drawCard();

        // create a zone
        ZoneCreator zoneCreator = new FermiZoneCreator();
        Zone zone = zoneCreator.createZone();

        // create a player
        Player player1 = new Player(clientId, characterCard, zone);

        // setting player name
        player1.setName("pippo");

        // testing the created player
        if (characterCard instanceof AlienCard) {
            assertEquals("127.0.0.1", player1.getClientId());
            assertTrue(player1.getCharacter().getBehaviour() instanceof NormalAlien);
            assertEquals(characterCard, player1.getCharacterCard());
            assertTrue(player1.getCharacter() instanceof Alien);
            assertEquals(0, player1.getItemCardList().size());
            assertEquals(PlayerState.PLAYING, player1.getPlayerState());
            assertEquals(RoundState.STANDBY, player1.getRoundState());
            assertEquals(zone.getAlienSectorCoordinates(), player1.getMovementRecord().getCurrentCoordinates());
            assertEquals(1, player1.getMovementRecord().getCoordinatesList().size());
            assertEquals("pippo", player1.getName());

        } else {
            assertEquals("127.0.0.1", player1.getClientId());
            assertTrue(player1.getCharacter().getBehaviour() instanceof NormalHuman);
            assertEquals(characterCard, player1.getCharacterCard());
            assertTrue(player1.getCharacter() instanceof Human);
            assertEquals(0, player1.getItemCardList().size());
            assertEquals(PlayerState.PLAYING, player1.getPlayerState());
            assertEquals(RoundState.STANDBY, player1.getRoundState());
            assertEquals(zone.getHumanSectorCoordinates(), player1.getMovementRecord().getCurrentCoordinates());
            assertEquals(1, player1.getMovementRecord().getCoordinatesList().size());
            assertEquals("pippo", player1.getName());

        }

        // create a itemDeck and then testing the player without item cards
        DeckCreator itemDeckCreator = new ItemDeckCreator();
        Deck itemDeck = itemDeckCreator.createDeck();

        Card card3 = itemDeck.drawCard();
        assertEquals(player1.hasSpecificItemCard(card3), false);

        // testing the player whit item Cards
        Card card1 = itemDeck.drawCard();
        player1.addItemCard(card1);

        assertEquals(1, player1.getItemCardList().size());
        assertEquals(true, player1.hasSpecificItemCard(card1));
        assertEquals(false, player1.hasSpecificItemCard(card1));
        assertEquals(0, player1.getItemCardList().size());

        player1.addItemCard(card1);
        Card card2 = itemDeck.drawCard();
        player1.addItemCard(card2);

        assertEquals(2, player1.getItemCardList().size());
        assertEquals(true, player1.hasSpecificItemCard(card1));
        assertEquals(1, player1.getItemCardList().size());
        assertEquals(true, player1.hasSpecificItemCard(card2));
        assertEquals(0, player1.getItemCardList().size());
        assertEquals(false, player1.hasSpecificItemCard(card2));
        assertEquals(0, player1.getItemCardList().size());

        Card card5 = itemDeck.drawCard();
        player1.addItemCard(card5);
        Card card6 = itemDeck.drawCard();

        if (card6 instanceof DefenseItem) {
            assertEquals(false, player1.hasSpecificItemCard(new DefenseItem()));
        }
    }

    @Test
    public void testRoundStateAndPlayerStatePlayer() throws NotWellFormedZoneException {

        // create a clientId
        String clientId = new String("127.0.0.1");

        // create a characterDeck and then draw a character Card from it
        DeckCreator characterDeckCreator = new CharacterDeckCreator();
        Deck characterDeck = characterDeckCreator.createDeck();
        Card characterCard = characterDeck.drawCard();

        // create a zone
        ZoneCreator zoneCreator = new FermiZoneCreator();
        Zone zone = zoneCreator.createZone();

        // create a player

        Player player1 = new Player(clientId, characterCard, zone);
        // testing the roundState and playerState of a player

        player1.setRoundState(RoundState.ENDING);
        assertEquals(RoundState.ENDING, player1.getRoundState());
        assertEquals(PlayerState.PLAYING, player1.getPlayerState());

        // create a clientId
        String clientId2 = new String("127.0.0.10");

        // create a characterDeck and then draw a character Card from it
        DeckCreator characterDeckCreator2 = new CharacterDeckCreator();
        Deck characterDeck2 = characterDeckCreator2.createDeck();
        Card characterCard2 = characterDeck2.drawCard();

        // create a player

        Player player2 = new Player(clientId2, characterCard2, zone);
        // testing the roundState of a player

        // testing the roundState and playerState of a player

        player2.setPlayerState(PlayerState.WINNER);
        assertEquals(PlayerState.WINNER, player2.getPlayerState());
        assertEquals(RoundState.STANDBY, player2.getRoundState());
    }
}
