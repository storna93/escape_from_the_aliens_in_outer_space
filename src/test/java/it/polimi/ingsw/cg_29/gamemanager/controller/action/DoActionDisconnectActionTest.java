package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.AggressiveHuman;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Alien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.FastHuman;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.PlayerState;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class DoActionDisconnectActionTest {
    Player player1, player2, player3, player4;
    Match currentMatch;

    @Before
    public void initMatch() throws NotWellFormedZoneException {
        List<String> clientIdList = new ArrayList<String>();
        clientIdList.add("pippo");
        clientIdList.add("pluto");
        clientIdList.add("paperino");
        clientIdList.add("topolino");

        ZoneCreator currentZoneCreator = new FermiZoneCreator();
        currentMatch = new Match(clientIdList, currentZoneCreator);

        player1 = currentMatch.getSpecificPlayerById("pippo");
        player2 = currentMatch.getSpecificPlayerById("pluto");
        player3 = currentMatch.getSpecificPlayerById("paperino");
        player4 = currentMatch.getSpecificPlayerById("topolino");

    }

    @Test
    public void TestDoDisconnectAction() {

        Action disconnectAction = new DisconnectAction("pippo");
        if (player1.getCharacter() instanceof Human) {
            player1.getCharacter().activateStrategy(new FastHuman());
            player1.getCharacter().activateStrategy(new AggressiveHuman());
        }

        disconnectAction.doAction(currentMatch);

        if (player1.getCharacter() instanceof Human) {

            assertEquals(1, player1.getCharacter().getRangeOfMove());
            assertEquals(false, player1.getCharacter().getCanAttack());
            assertEquals(RoundState.DISCONNECTED, player1.getRoundState());
            assertEquals(PlayerState.DISCONNECTED, player1.getPlayerState());

        }

        else if (player1.getCharacter() instanceof Alien) {

            assertEquals(2, player1.getCharacter().getRangeOfMove());
            assertEquals(true, player1.getCharacter().getCanAttack());
            assertEquals(RoundState.DISCONNECTED, player1.getRoundState());
            assertEquals(PlayerState.DISCONNECTED, player1.getPlayerState());

        }

    }

}
