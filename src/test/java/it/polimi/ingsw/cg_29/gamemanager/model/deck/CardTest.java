package it.polimi.ingsw.cg_29.gamemanager.model.deck;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CardTest {

    @Test
    public void testConstructor() {

        Card c1 = new EscapeGreen();
        assertTrue(c1 instanceof EscapeGreen);

        c1 = new EscapeRed();
        assertTrue(c1 instanceof EscapeRed);

        c1 = new Silence();
        assertTrue(c1 instanceof Silence);

        c1 = new NoiseInAnySector();
        assertTrue(c1 instanceof NoiseInAnySector);

        c1 = new NoiseInAnySectorIcon();
        assertTrue(c1 instanceof NoiseInAnySectorIcon);

        c1 = new NoiseInYourSector();
        assertTrue(c1 instanceof NoiseInYourSector);

        c1 = new NoiseInYourSectorIcon();
        assertTrue(c1 instanceof NoiseInYourSectorIcon);

        c1 = new HumanCard();
        assertTrue(c1 instanceof HumanCard);

        c1 = new AlienCard();
        assertTrue(c1 instanceof AlienCard);

        c1 = new SedativesItem();
        assertTrue(c1 instanceof SedativesItem);

        c1 = new DefenseItem();
        assertTrue(c1 instanceof DefenseItem);

        c1 = new SpotlightItem();
        assertTrue(c1 instanceof SpotlightItem);

        c1 = new TeleportItem();
        assertTrue(c1 instanceof TeleportItem);

        c1 = new AdrenalineItem();
        assertTrue(c1 instanceof AdrenalineItem);

        c1 = new AttackItem();
        assertTrue(c1 instanceof AttackItem);

    }

}
