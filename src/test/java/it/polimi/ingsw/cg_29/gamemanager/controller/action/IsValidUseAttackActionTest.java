package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.AttackItem;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Alien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class IsValidUseAttackActionTest {
    Player player1, player2;
    Match currentMatch;

    @Before
    public void initMatch() throws NotWellFormedZoneException {
        List<String> clientIdList = new ArrayList<String>();
        clientIdList.add("pippo");
        clientIdList.add("pluto");
        ZoneCreator currentZoneCreator = new FermiZoneCreator();
        currentMatch = new Match(clientIdList, currentZoneCreator);

        player1 = currentMatch.getSpecificPlayerById("pippo");
        player2 = currentMatch.getSpecificPlayerById("pluto");

    }

    @Test
    public void TestValidUseAttackFromBeginningState() {

        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        Action useAttackAction = new UseAttackAction("pippo");

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, useAttackAction.isValid(currentMatch));

            // add AttackItem card
            player1.addItemCard(new AttackItem());

            // testing a player with AttackItem Card
            assertEquals(true, activateAttackAction.isValid(currentMatch));
            activateAttackAction.doAction(currentMatch);
            assertEquals(false, useAttackAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(false, useAttackAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidUseAttackFromMoveSecureStateOnHumanSector() {

        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        Action useAttackAction = new UseAttackAction("pippo");

        // change player1 RoundState
        player1.setRoundState(RoundState.MOVED_SECURE);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, useAttackAction.isValid(currentMatch));

            // add AttackItem card
            player1.addItemCard(new AttackItem());

            // testing a player with AttackItem Card
            assertEquals(true, activateAttackAction.isValid(currentMatch));
            activateAttackAction.doAction(currentMatch);
            assertEquals(false, useAttackAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(false, useAttackAction.isValid(currentMatch));

        }
    }

    @Test
    public void TestValidUseAttackFromMoveDangerousStateOnAlienSector() {

        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        Action useAttackAction = new UseAttackAction("pippo");

        // change player1 RoundState
        player1.setRoundState(RoundState.MOVED_DANGEROUS);

        // change player1 currentposition into AlienSector
        String start = ("L09");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, useAttackAction.isValid(currentMatch));

            // add AttackItem card
            player1.addItemCard(new AttackItem());

            // testing a player with AttackItem Card
            assertEquals(true, activateAttackAction.isValid(currentMatch));
            activateAttackAction.doAction(currentMatch);
            assertEquals(false, useAttackAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(false, useAttackAction.isValid(currentMatch));

        }
    }

    @Test
    public void TestValidUseAttackFromMoveDangerousStateOnEscapeSector() {

        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        Action useAttackAction = new UseAttackAction("pippo");

        // change player1 RoundState
        player1.setRoundState(RoundState.MOVED_DANGEROUS);

        // change player1 currentPosition into EscapeSector
        String start = ("N01");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, useAttackAction.isValid(currentMatch));

            // add AttackItem card
            player1.addItemCard(new AttackItem());

            // testing a player with AttackItem Card
            assertEquals(true, activateAttackAction.isValid(currentMatch));
            activateAttackAction.doAction(currentMatch);
            assertEquals(false, useAttackAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(false, useAttackAction.isValid(currentMatch));

        }
    }

    @Test
    public void TestValidUseAttackFromMoveDangerousStateOnDangerousSector() {

        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        Action useAttackAction = new UseAttackAction("pippo");

        // change player1 RoundState
        player1.setRoundState(RoundState.MOVED_DANGEROUS);

        // change player1 currentPosition into DangerousSector
        String start = ("O10");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, useAttackAction.isValid(currentMatch));

            // add AttackItem card
            player1.addItemCard(new AttackItem());

            // testing a player with AttackItem Card
            assertEquals(true, activateAttackAction.isValid(currentMatch));
            activateAttackAction.doAction(currentMatch);
            assertEquals(true, useAttackAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(false, useAttackAction.isValid(currentMatch));

        }
    }

}
