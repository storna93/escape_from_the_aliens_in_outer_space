package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.AttackItem;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Card;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.DefenseItem;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.NoiseInAnySector;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.NoiseInAnySectorIcon;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.NoiseInYourSector;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.NoiseInYourSectorIcon;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Silence;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.TeleportItem;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class DoActionDrawActionTest {
    Player player1, player2;
    Match currentMatch;

    @Before
    public void initMatch() throws NotWellFormedZoneException {
        List<String> clientIdList = new ArrayList<String>();
        clientIdList.add("pippo");
        clientIdList.add("pluto");
        ZoneCreator currentZoneCreator = new FermiZoneCreator();
        currentMatch = new Match(clientIdList, currentZoneCreator);

        player1 = currentMatch.getSpecificPlayerById("pippo");
        player2 = currentMatch.getSpecificPlayerById("pluto");

    }

    @Test
    public void TestDoDrawDangerousSectorCard() {

        String start = ("N07");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);
        player1.setRoundState(RoundState.MOVED_DANGEROUS);

        DrawAction drawAction = new DrawAction("pippo");

        drawAction.doAction(currentMatch);

        if (drawAction.getDrawnCardForTesting() instanceof Silence) {

            assertEquals(RoundState.ENDING, player1.getRoundState());
            assertEquals("[NOISE] " + player1.getClientId() + ": SILENCE", drawAction.getDrawResultForTesting());
        }

        else if (drawAction.getDrawnCardForTesting() instanceof NoiseInYourSector || drawAction.getDrawnCardForTesting() instanceof NoiseInYourSectorIcon) {

            assertEquals(RoundState.ENDING, player1.getRoundState());
            assertEquals("[NOISE] " + player1.getClientId() + ": NOISE IN SECTOR " + startCoord.printToString(), drawAction.getDrawResultForTesting());
        }

        else if (drawAction.getDrawnCardForTesting() instanceof NoiseInAnySector || drawAction.getDrawnCardForTesting() instanceof NoiseInAnySectorIcon) {

            assertEquals(RoundState.FAKE_NOISE, player1.getRoundState());

        }

    }

    @Test
    public void TestDoDrawWithAFullItemHand() {

        String start = ("N07");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);
        player1.setRoundState(RoundState.MOVED_DANGEROUS);

        DrawAction drawAction = new DrawAction("pippo");
        player1.addItemCard(new AttackItem());
        player1.addItemCard(new DefenseItem());
        player1.addItemCard(new TeleportItem());

        drawAction.doAction(currentMatch);

        if (drawAction.getDrawnCardForTesting() instanceof NoiseInYourSectorIcon || drawAction.getDrawnCardForTesting() instanceof NoiseInAnySectorIcon) {

            assertEquals(11, currentMatch.getItemDeck().getCardList().size());
            assertEquals(1, currentMatch.getItemDeck().getDiscardPile().size());
            assertEquals(3, player1.getItemCardList().size());
        }

    }

    @Test
    public void TestDoDrawWithANonFullItemHand() {

        String start = ("N07");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);
        player1.setRoundState(RoundState.MOVED_DANGEROUS);

        DrawAction drawAction = new DrawAction("pippo");
        player1.addItemCard(new AttackItem());
        player1.addItemCard(new DefenseItem());

        drawAction.doAction(currentMatch);

        if (drawAction.getDrawnCardForTesting() instanceof NoiseInYourSectorIcon || drawAction.getDrawnCardForTesting() instanceof NoiseInAnySectorIcon) {

            assertEquals(11, currentMatch.getItemDeck().getCardList().size());
            assertEquals(0, currentMatch.getItemDeck().getDiscardPile().size());
            assertEquals(3, player1.getItemCardList().size());
        }

    }

    @Test
    public void TestDoDrawWithAnEmptyItemDeck() {

        String start = ("N07");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);
        player1.setRoundState(RoundState.MOVED_DANGEROUS);

        DrawAction drawAction = new DrawAction("pippo");
        player1.addItemCard(new AttackItem());
        player1.addItemCard(new DefenseItem());

        Card drawnItemCard;
        for (int i = 0; i < 12; i++) {
            drawnItemCard = currentMatch.getItemDeck().drawCard();
            currentMatch.getItemDeck().discardCard(drawnItemCard);
        }

        drawAction.doAction(currentMatch);

        if (drawAction.getDrawnCardForTesting() instanceof NoiseInYourSectorIcon || drawAction.getDrawnCardForTesting() instanceof NoiseInAnySectorIcon) {

            assertEquals(11, currentMatch.getItemDeck().getCardList().size());
            assertEquals(0, currentMatch.getItemDeck().getDiscardPile().size());
            assertEquals(3, player1.getItemCardList().size());
        }

    }

    @Test
    public void TestDoDrawWithAnEmptyDangerousSectorDeck() {

        String start = ("N07");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);
        player1.setRoundState(RoundState.MOVED_DANGEROUS);

        DrawAction drawAction = new DrawAction("pippo");
        player1.addItemCard(new AttackItem());
        player1.addItemCard(new DefenseItem());

        Card drawnDangerousSectorCard;
        for (int i = 0; i < 25; i++) {
            drawnDangerousSectorCard = currentMatch.getDangerousSectorDeck().drawCard();
            currentMatch.getDangerousSectorDeck().discardCard(drawnDangerousSectorCard);
        }

        drawAction.doAction(currentMatch);

        assertEquals(24, currentMatch.getDangerousSectorDeck().getCardList().size());
        assertEquals(1, currentMatch.getDangerousSectorDeck().getDiscardPile().size());
    }

}
