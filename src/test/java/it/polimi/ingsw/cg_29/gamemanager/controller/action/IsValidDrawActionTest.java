package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.SedativesItem;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Alien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class IsValidDrawActionTest {
    Player player1, player2;
    Match currentMatch;

    @Before
    public void initMatch() throws NotWellFormedZoneException {
        List<String> clientIdList = new ArrayList<String>();
        clientIdList.add("pippo");
        clientIdList.add("pluto");
        ZoneCreator currentZoneCreator = new FermiZoneCreator();
        currentMatch = new Match(clientIdList, currentZoneCreator);

        player1 = currentMatch.getSpecificPlayerById("pippo");
        player2 = currentMatch.getSpecificPlayerById("pluto");

    }

    @Test
    public void TestValidDrawFromBeginningState() {

        Action activateSedativesAction = new ActivateSedativesAction("pippo", new SedativesItem());
        Action drawAction = new DrawAction("pippo");

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, drawAction.isValid(currentMatch));

            // add SedativesItem card
            player1.addItemCard(new SedativesItem());

            // testing a player with SedativesItem Card
            assertEquals(true, activateSedativesAction.isValid(currentMatch));
            activateSedativesAction.doAction(currentMatch);
            assertEquals(false, drawAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(false, drawAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidDrawFromMoveDangerousSector() {

        Action activateSedativesAction = new ActivateSedativesAction("pippo", new SedativesItem());
        Action drawAction = new DrawAction("pippo");

        // change player1 RoundState
        player1.setRoundState(RoundState.MOVED_DANGEROUS);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(true, drawAction.isValid(currentMatch));

            // add SedativesItem card
            player1.addItemCard(new SedativesItem());

            // testing a player with SedativesItem Card
            assertEquals(true, activateSedativesAction.isValid(currentMatch));
            activateSedativesAction.doAction(currentMatch);
            assertEquals(false, drawAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(true, drawAction.isValid(currentMatch));

        }

    }
}
