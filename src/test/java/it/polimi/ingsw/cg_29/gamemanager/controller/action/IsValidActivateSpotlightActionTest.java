package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.SpotlightItem;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Alien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class IsValidActivateSpotlightActionTest {
    Player player1, player2;
    Match currentMatch;

    @Before
    public void initMatch() throws NotWellFormedZoneException {
        List<String> clientIdList = new ArrayList<String>();
        clientIdList.add("pippo");
        clientIdList.add("pluto");
        ZoneCreator currentZoneCreator = new FermiZoneCreator();
        currentMatch = new Match(clientIdList, currentZoneCreator);

        player1 = currentMatch.getSpecificPlayerById("pippo");
        player2 = currentMatch.getSpecificPlayerById("pluto");

    }

    @Test
    public void TestValidSpotlightActionFromBeginningState() {

        String litSector = "L05";
        Coordinates litCoord = new Coordinates(litSector.substring(0, 1), litSector.substring(1));

        Action activateSpotlightAction = new ActivateSpotlightAction("pippo", litCoord, new SpotlightItem());

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));

            // add SpotlightItem
            player1.addItemCard(new SpotlightItem());

            // testing a player with SpotlightItem

            assertEquals(true, activateSpotlightAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidSpotlightActionFromMoveDangerousState() {
        String litSector = "I06";
        Coordinates litCoord = new Coordinates(litSector.substring(0, 1), litSector.substring(1));

        Action activateSpotlightAction = new ActivateSpotlightAction("pippo", litCoord, new SpotlightItem());

        // change player1 RoundState
        player1.setRoundState(RoundState.MOVED_DANGEROUS);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));

            // add SpotlightItem
            player1.addItemCard(new SpotlightItem());

            // testing a player with SpotlightItem

            assertEquals(true, activateSpotlightAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));

        }
    }

    @Test
    public void TestValidSpotlightActionFromMoveSecureState() {
        String litSector = "J05";
        Coordinates litCoord = new Coordinates(litSector.substring(0, 1), litSector.substring(1));

        Action activateSpotlightAction = new ActivateSpotlightAction("pippo", litCoord, new SpotlightItem());

        // change player1 RoundState
        player1.setRoundState(RoundState.MOVED_SECURE);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));

            // add SpotlightItem
            player1.addItemCard(new SpotlightItem());

            // testing a player with SpotlightItem

            assertEquals(true, activateSpotlightAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));

        }
    }

    @Test
    public void TestValidSpotlightActionFromDrawnState() {
        String litSector = "I09";
        Coordinates litCoord = new Coordinates(litSector.substring(0, 1), litSector.substring(1));

        Action activateSpotlightAction = new ActivateSpotlightAction("pippo", litCoord, new SpotlightItem());

        // change player1 RoundState
        player1.setRoundState(RoundState.FAKE_NOISE);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));

            // add SpotlightItem
            player1.addItemCard(new SpotlightItem());

            // testing a player with SpotlightItem

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));

        }
    }

    @Test
    public void TestValidSpotlightActionFromStandbyState() {
        String litSector = "L09";
        Coordinates litCoord = new Coordinates(litSector.substring(0, 1), litSector.substring(1));

        Action activateSpotlightAction = new ActivateSpotlightAction("pippo", litCoord, new SpotlightItem());

        // change player1 RoundState
        player1.setRoundState(RoundState.STANDBY);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));

            // add SpotlightItem
            player1.addItemCard(new SpotlightItem());

            // testing a player with SpotlightItem

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));

        }
    }

    @Test
    public void TestValidSpotlightActionOnVoidSector() {
        String litSector = "J06";
        Coordinates litCoord = new Coordinates(litSector.substring(0, 1), litSector.substring(1));

        Action activateSpotlightAction = new ActivateSpotlightAction("pippo", litCoord, new SpotlightItem());

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));

            // add SpotlightItem
            player1.addItemCard(new SpotlightItem());

            // testing a player with SpotlightItem

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));

        }
    }

    @Test
    public void TestValidSpotlightActionOnNonExistingSector() {
        String litSector = "J66";
        Coordinates litCoord = new Coordinates(litSector.substring(0, 1), litSector.substring(1));

        Action activateSpotlightAction = new ActivateSpotlightAction("pippo", litCoord, new SpotlightItem());

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));

            // add SpotlightItem
            player1.addItemCard(new SpotlightItem());

            // testing a player with SpotlightItem

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateSpotlightAction.isValid(currentMatch));

        }
    }
}
