package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.AdrenalineItem;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Alien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.FastAlien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class IsValidMoveActionTest {

    Player player1, player2;
    Match currentMatch;

    @Before
    public void initMatch() throws NotWellFormedZoneException {
        List<String> clientIdList = new ArrayList<String>();
        clientIdList.add("pippo");
        clientIdList.add("pluto");
        ZoneCreator currentZoneCreator = new FermiZoneCreator();
        currentMatch = new Match(clientIdList, currentZoneCreator);

        player1 = currentMatch.getSpecificPlayerById("pippo");
        player2 = currentMatch.getSpecificPlayerById("pluto");

    }

    @Test
    public void TestValidHumanMoveRange1() {
        String destination = "L11";
        Coordinates destinationCoord = new Coordinates(destination.substring(0, 1), destination.substring(1));
        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());
        Action moveAction = new MoveAction("pippo", destinationCoord);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(true, moveAction.isValid(currentMatch));

            // add AdrenalineItem card.
            player1.addItemCard(new AdrenalineItem());

            // testing a player with AdrenalineItem Card
            assertEquals(true, activateAdrenalineAction.isValid(currentMatch));

            assertEquals(true, moveAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(false, moveAction.isValid(currentMatch));

            // add AdrenalineItem card.
            player1.addItemCard(new AdrenalineItem());

            // testing a player with AdrenalineItem Card
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, moveAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidHumanMoveRange2() {
        String destination = "L12";
        Coordinates destinationCoord = new Coordinates(destination.substring(0, 1), destination.substring(1));
        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());
        Action moveAction = new MoveAction("pippo", destinationCoord);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, moveAction.isValid(currentMatch));

            // add AdrenalineItem card.
            player1.addItemCard(new AdrenalineItem());

            // testing a player with AdrenalineItem Card
            assertEquals(true, activateAdrenalineAction.isValid(currentMatch));
            activateAdrenalineAction.doAction(currentMatch);
            assertEquals(true, moveAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(false, moveAction.isValid(currentMatch));

            // add AdrenalineItem card.
            player1.addItemCard(new AdrenalineItem());

            // testing a player with AdrenalineItem Card
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, moveAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidHumanMoveRange3() {
        String destination = "N12";
        Coordinates destinationCoord = new Coordinates(destination.substring(0, 1), destination.substring(1));
        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());
        Action moveAction = new MoveAction("pippo", destinationCoord);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, moveAction.isValid(currentMatch));

            // add AdrenalineItem card.
            player1.addItemCard(new AdrenalineItem());

            // testing a player with AdrenalineItem Card
            assertEquals(true, activateAdrenalineAction.isValid(currentMatch));
            activateAdrenalineAction.doAction(currentMatch);
            assertEquals(false, moveAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(false, moveAction.isValid(currentMatch));

            // add AdrenalineItem card.
            player1.addItemCard(new AdrenalineItem());

            // testing a player with AdrenalineItem Card
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, moveAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidAlienMoveRange1() {
        String destination = "L08";
        Coordinates destinationCoord = new Coordinates(destination.substring(0, 1), destination.substring(1));
        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());
        Action moveAction = new MoveAction("pippo", destinationCoord);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, moveAction.isValid(currentMatch));

            // add AdrenalineItem card.
            player1.addItemCard(new AdrenalineItem());

            // testing a player with AdrenalineItem Card
            assertEquals(true, activateAdrenalineAction.isValid(currentMatch));
            activateAdrenalineAction.doAction(currentMatch);
            assertEquals(false, moveAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(true, moveAction.isValid(currentMatch));

            // add FastAlien strategy
            player1.getCharacter().activateStrategy(new FastAlien());

            // testing a player with FastAlien strategy
            assertEquals(true, moveAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidAlienMoveRange2() {
        String destination = "M08";
        Coordinates destinationCoord = new Coordinates(destination.substring(0, 1), destination.substring(1));
        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());
        Action moveAction = new MoveAction("pippo", destinationCoord);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, moveAction.isValid(currentMatch));

            // add AdrenalineItem card.
            player1.addItemCard(new AdrenalineItem());

            // testing a player with AdrenalineItem Card
            assertEquals(true, activateAdrenalineAction.isValid(currentMatch));
            activateAdrenalineAction.doAction(currentMatch);
            assertEquals(false, moveAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(true, moveAction.isValid(currentMatch));

            // add FastAlien strategy
            player1.getCharacter().activateStrategy(new FastAlien());

            // testing a player with FastAlien strategy
            assertEquals(true, moveAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidAlienMoveRange3() {
        String destination = "N08";
        Coordinates destinationCoord = new Coordinates(destination.substring(0, 1), destination.substring(1));
        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());
        Action moveAction = new MoveAction("pippo", destinationCoord);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, moveAction.isValid(currentMatch));

            // add AdrenalineItem card.
            player1.addItemCard(new AdrenalineItem());

            // testing a player with AdrenalineItem Card
            assertEquals(true, activateAdrenalineAction.isValid(currentMatch));
            activateAdrenalineAction.doAction(currentMatch);

            assertEquals(false, moveAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(false, moveAction.isValid(currentMatch));

            // add FastAlien strategy
            player1.getCharacter().activateStrategy(new FastAlien());

            // testing a player with FastAlien strategy
            assertEquals(true, moveAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidMoveFromNonBeginningState() {

        player1.setRoundState(RoundState.MOVED_DANGEROUS);
        String destination = "M08";
        Coordinates destinationCoord = new Coordinates(destination.substring(0, 1), destination.substring(1));
        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());
        Action moveAction = new MoveAction("pippo", destinationCoord);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, moveAction.isValid(currentMatch));

            // add AdrenalineItem card.
            player1.addItemCard(new AdrenalineItem());

            // testing a player with AdrenalineItem Card
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            activateAdrenalineAction.doAction(currentMatch);
            assertEquals(false, moveAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(false, moveAction.isValid(currentMatch));

            // add FastAlien strategy
            player1.getCharacter().activateStrategy(new FastAlien());
            // testing a player with FastAlien strategy
            assertEquals(false, moveAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidMoveToNonExistingSector() {

        String destination = "D05";
        Coordinates destinationCoord = new Coordinates(destination.substring(0, 1), destination.substring(1));
        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());
        Action moveAction = new MoveAction("pippo", destinationCoord);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, moveAction.isValid(currentMatch));

            // add AdrenalineItem card.
            player1.addItemCard(new AdrenalineItem());

            // testing a player with AdrenalineItem Card
            assertEquals(true, activateAdrenalineAction.isValid(currentMatch));
            activateAdrenalineAction.doAction(currentMatch);
            assertEquals(false, moveAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(false, moveAction.isValid(currentMatch));

            // add FastAlien strategy
            player1.getCharacter().activateStrategy(new FastAlien());
            // testing a player with FastAlien strategy
            assertEquals(false, moveAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidMoveToEscapeSector() {

        String start = ("I07");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);
        String destination = "J05";
        Coordinates destinationCoord = new Coordinates(destination.substring(0, 1), destination.substring(1));
        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());
        Action moveAction = new MoveAction("pippo", destinationCoord);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, moveAction.isValid(currentMatch));

            // add AdrenalineItem card.
            player1.addItemCard(new AdrenalineItem());

            // testing a player with AdrenalineItem Card
            assertEquals(true, activateAdrenalineAction.isValid(currentMatch));
            activateAdrenalineAction.doAction(currentMatch);
            assertEquals(true, moveAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            // assertEquals(false, moveAction.isValid(currentMatch));

            // add FastAlien strategy
            player1.getCharacter().activateStrategy(new FastAlien());

            // testing a player with FastAlien strategy
            assertEquals(false, moveAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidMoveToHumanSector() {

        String start = ("M11");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);
        String destination = "L10";
        Coordinates destinationCoord = new Coordinates(destination.substring(0, 1), destination.substring(1));
        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());
        Action moveAction = new MoveAction("pippo", destinationCoord);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, moveAction.isValid(currentMatch));

            // add AdrenalineItem card.
            player1.addItemCard(new AdrenalineItem());

            // testing a player with AdrenalineItem Card
            assertEquals(true, activateAdrenalineAction.isValid(currentMatch));
            activateAdrenalineAction.doAction(currentMatch);
            assertEquals(false, moveAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            // assertEquals(false, moveAction.isValid(currentMatch));

            // add FastAlien strategy
            player1.getCharacter().activateStrategy(new FastAlien());

            // testing a player with FastAlien strategy
            assertEquals(false, moveAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidMoveToAlienSector() {

        String start = ("M08");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);
        String destination = "L09";
        Coordinates destinationCoord = new Coordinates(destination.substring(0, 1), destination.substring(1));
        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());
        Action moveAction = new MoveAction("pippo", destinationCoord);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, moveAction.isValid(currentMatch));

            // add AdrenalineItem card.
            player1.addItemCard(new AdrenalineItem());

            // testing a player with AdrenalineItem Card
            assertEquals(true, activateAdrenalineAction.isValid(currentMatch));
            activateAdrenalineAction.doAction(currentMatch);
            assertEquals(false, moveAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            // assertEquals(false, moveAction.isValid(currentMatch));

            // add FastAlien strategy
            player1.getCharacter().activateStrategy(new FastAlien());

            // testing a player with FastAlien strategy
            assertEquals(false, moveAction.isValid(currentMatch));

        }

    }
}
