package it.polimi.ingsw.cg_29.gamemanager.model.zone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_29.LoggerClass;

import org.junit.Before;
import org.junit.Test;

public class CoordinatesTest {

    Coordinates c1;

    @Before
    public void init() {
        c1 = new Coordinates(10, 20);
    }

    @Test
    public void testConstructor() {
        assertTrue(c1 instanceof Coordinates);

        boolean exception = false;
        try {
            c1 = new Coordinates(null, "01");
        } catch (NullPointerException e) {
            LoggerClass.getLogger().warn(e);
            exception = true;
        }
        assertTrue(exception);

        exception = false;
        try {
            c1 = new Coordinates("A", null);
        } catch (NumberFormatException e) {
            LoggerClass.getLogger().warn(e);
            exception = true;
        }
        assertTrue(exception);

        try {
            c1 = new Coordinates("A", "01");
            assertTrue(c1 instanceof Coordinates);
        } catch (NullPointerException e) {
            LoggerClass.getLogger().warn(e);
            e.printStackTrace();
        }
    }

    @Test
    public void testGetter() {
        assertEquals(10, c1.getRow(), 0);
        assertEquals(20, c1.getCol(), 0);
    }

    @Test
    public void testHashCode() {
        Coordinates c2 = new Coordinates(15, 25);

        assertEquals(c1.hashCode(), c1.hashCode());
        assertNotEquals(c1.hashCode(), c2.hashCode());
        assertNotEquals(c1.hashCode(), null);
    }

    @Test
    public void testEquals() {
        Coordinates c2 = new Coordinates(10, 20);
        Coordinates c3 = new Coordinates(15, 20);
        Coordinates c4 = new Coordinates(10, 25);

        assertFalse(c1.equals(null));
        assertFalse(c1.equals(10));
        assertFalse(c1.equals(c3));
        assertFalse(c1.equals(c4));
        assertTrue(c1.equals(c1));
        assertTrue(c1.equals(c2));
    }

    @Test
    public void testToString() {
        assertEquals("U11{10,20}", c1.toString());
    }

    @Test
    public void testPrintToString() {
        Coordinates c2 = new Coordinates(0, 0);

        assertEquals("A01", c2.printToString());
    }

}
