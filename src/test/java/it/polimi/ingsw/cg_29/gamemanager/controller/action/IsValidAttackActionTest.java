package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.AttackItem;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Alien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class IsValidAttackActionTest {

    Player player1, player2;
    Match currentMatch;

    @Before
    public void initMatch() throws NotWellFormedZoneException {
        List<String> clientIdList = new ArrayList<String>();
        clientIdList.add("pippo");
        clientIdList.add("pluto");
        ZoneCreator currentZoneCreator = new FermiZoneCreator();
        currentMatch = new Match(clientIdList, currentZoneCreator);

        player1 = currentMatch.getSpecificPlayerById("pippo");
        player2 = currentMatch.getSpecificPlayerById("pluto");

    }

    @Test
    public void TestValidAttackFromBeginningState() {

        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        Action attackAction = new AttackAction("pippo");

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, attackAction.isValid(currentMatch));

            // add AttackItem card
            player1.addItemCard(new AttackItem());

            // testing a player with AttackItem Card
            assertEquals(true, activateAttackAction.isValid(currentMatch));
            activateAttackAction.doAction(currentMatch);
            assertEquals(false, attackAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(false, attackAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidAttackFromMoveSecureState() {
        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        Action attackAction = new AttackAction("pippo");

        // change player1 position
        String start = ("L05");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);

        // change player1 RoundState
        player1.setRoundState(RoundState.MOVED_SECURE);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, attackAction.isValid(currentMatch));

            // add AttackItem card
            player1.addItemCard(new AttackItem());

            // testing a player with AttackItem Card
            assertEquals(true, activateAttackAction.isValid(currentMatch));
            activateAttackAction.doAction(currentMatch);
            assertEquals(false, attackAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(true, attackAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidAttackFromMoveDangerousState() {
        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        Action attackAction = new AttackAction("pippo");

        // change player1 position
        String start = ("I07");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);

        // change player1 RoundState
        player1.setRoundState(RoundState.MOVED_DANGEROUS);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, attackAction.isValid(currentMatch));

            // add AttackItem card
            player1.addItemCard(new AttackItem());

            // testing a player with AttackItem Card
            assertEquals(true, activateAttackAction.isValid(currentMatch));
            activateAttackAction.doAction(currentMatch);
            assertEquals(false, attackAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(true, attackAction.isValid(currentMatch));

        }
    }

    @Test
    public void TestValidAttackFromDrawnState() {
        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        Action attackAction = new AttackAction("pippo");

        // change player1 position
        String start = ("I07");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);

        // change player1 RoundState
        player1.setRoundState(RoundState.FAKE_NOISE);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, attackAction.isValid(currentMatch));

            // add AttackItem card
            player1.addItemCard(new AttackItem());

            // testing a player with AttackItem Card
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            activateAttackAction.doAction(currentMatch);
            assertEquals(false, attackAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(false, attackAction.isValid(currentMatch));

        }
    }

    @Test
    public void TestValidAttackFromEscapeSector() {
        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        Action attackAction = new AttackAction("pippo");

        // change player1 position
        String start = ("J05");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);

        // change player1 RoundState
        player1.setRoundState(RoundState.MOVED_DANGEROUS);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, attackAction.isValid(currentMatch));

            // add AttackItem card
            player1.addItemCard(new AttackItem());

            // testing a player with AttackItem Card
            assertEquals(true, activateAttackAction.isValid(currentMatch));
            activateAttackAction.doAction(currentMatch);
            assertEquals(false, attackAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(false, attackAction.isValid(currentMatch));

        }
    }

    @Test
    public void TestValidAttackFromHumanSector() {
        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        Action attackAction = new AttackAction("pippo");

        // change player1 position
        String start = ("L10");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);

        // change player1 RoundState
        player1.setRoundState(RoundState.MOVED_DANGEROUS);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, attackAction.isValid(currentMatch));

            // add AttackItem card
            player1.addItemCard(new AttackItem());

            // testing a player with AttackItem Card
            assertEquals(true, activateAttackAction.isValid(currentMatch));
            activateAttackAction.doAction(currentMatch);
            assertEquals(false, attackAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(false, attackAction.isValid(currentMatch));

        }
    }

    @Test
    public void TestValidAttackFromAlienSector() {
        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        Action attackAction = new AttackAction("pippo");

        // change player1 position
        String start = ("L10");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);

        // change player1 RoundState
        player1.setRoundState(RoundState.MOVED_DANGEROUS);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, attackAction.isValid(currentMatch));

            // add AttackItem card
            player1.addItemCard(new AttackItem());

            // testing a player with AttackItem Card
            assertEquals(true, activateAttackAction.isValid(currentMatch));
            activateAttackAction.doAction(currentMatch);
            assertEquals(false, attackAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            // testing a player without any itemCards
            assertEquals(false, attackAction.isValid(currentMatch));

        }
    }

}