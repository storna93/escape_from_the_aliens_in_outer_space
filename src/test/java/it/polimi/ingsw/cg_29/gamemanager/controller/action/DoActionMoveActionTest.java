package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.AdrenalineItem;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Card;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.EscapeGreen;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.EscapeRed;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Alien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.PlayerState;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.EscapeHatchSector;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Sector;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class DoActionMoveActionTest {
    Player player1, player2;
    Match currentMatch;

    @Before
    public void initMatch() throws NotWellFormedZoneException {
        List<String> clientIdList = new ArrayList<String>();
        clientIdList.add("pippo");
        clientIdList.add("pluto");
        ZoneCreator currentZoneCreator = new FermiZoneCreator();
        currentMatch = new Match(clientIdList, currentZoneCreator);

        player1 = currentMatch.getSpecificPlayerById("pippo");
        player2 = currentMatch.getSpecificPlayerById("pluto");

    }

    @Test
    public void TestDoHumanMoveFromInToMoveSecure() {
        String destination = "L11";
        Coordinates destinationCoord = new Coordinates(destination.substring(0, 1), destination.substring(1));
        Action moveAction = new MoveAction("pippo", destinationCoord);

        if (player1.getCharacter() instanceof Human) {

            moveAction.doAction(currentMatch);
            assertEquals(destinationCoord, player1.getCurrentPosition());
            assertEquals(1, player1.getCharacter().getRangeOfMove());
            assertEquals(RoundState.MOVED_SECURE, player1.getRoundState());

        }

        else if (player1.getCharacter() instanceof Alien) {

            int currentAlienRange = player1.getCharacter().getRangeOfMove();
            moveAction.doAction(currentMatch);
            assertEquals(destinationCoord, player1.getCurrentPosition());
            assertEquals(currentAlienRange, player1.getCharacter().getRangeOfMove());
            assertEquals(RoundState.MOVED_SECURE, player1.getRoundState());

        }

    }

    @Test
    public void TestDoHumanMoveFromInToMoveEscape() {

        String start = ("M02");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);

        String destination = "N01";
        Coordinates destinationCoord = new Coordinates(destination.substring(0, 1), destination.substring(1));
        Sector destinationSector = currentMatch.getCurrentZone().getSectorByCoordinates(destinationCoord);

        Action moveAction = new MoveAction("pippo", destinationCoord);

        if (((EscapeHatchSector) destinationSector).isUsable()) {

            if (player1.getCharacter() instanceof Human) {

                assertEquals(true, moveAction.isValid(currentMatch));

                moveAction.doAction(currentMatch);
                assertEquals(destinationCoord, player1.getCurrentPosition());
                assertEquals(1, player1.getCharacter().getRangeOfMove());
                assertEquals(false, ((EscapeHatchSector) destinationSector).isUsable());

                List<Card> discardPileEscapeDeck = currentMatch.getEscapeHatchDeck().getDiscardPile();
                int sizeOfDiscardPile = discardPileEscapeDeck.size();

                if (discardPileEscapeDeck.get(sizeOfDiscardPile - 1) instanceof EscapeGreen) {
                    assertEquals(PlayerState.WINNER, player1.getPlayerState());
                }
                if (discardPileEscapeDeck.get(sizeOfDiscardPile - 1) instanceof EscapeRed) {
                    assertEquals(RoundState.ENDING, player1.getRoundState());
                }

            }

            else if (player1.getCharacter() instanceof Alien) {

                assertEquals(false, moveAction.isValid(currentMatch));

            }
        } else {
            if (player1.getCharacter() instanceof Human) {

                assertEquals(true, moveAction.isValid(currentMatch));

                moveAction.doAction(currentMatch);
                assertEquals(destinationCoord, player1.getCurrentPosition());
                assertEquals(1, player1.getCharacter().getRangeOfMove());
                assertEquals(false, ((EscapeHatchSector) destinationSector).isUsable());

                assertEquals(RoundState.ENDING, player1.getRoundState());

            }

            else if (player1.getCharacter() instanceof Alien) {

                assertEquals(false, moveAction.isValid(currentMatch));

            }
        }

    }

    @Test
    public void TestDoHumanMoveFromInToMoveDangerous() {

        String start = ("O07");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);

        String destination = "N07";
        Coordinates destinationCoord = new Coordinates(destination.substring(0, 1), destination.substring(1));

        Action moveAction = new MoveAction("pippo", destinationCoord);

        player1.addItemCard(new AdrenalineItem());

        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());

        if (player1.getCharacter() instanceof Human) {

            activateAdrenalineAction.doAction(currentMatch);
            assertEquals(2, player1.getCharacter().getRangeOfMove());
            moveAction.doAction(currentMatch);
            assertEquals(destinationCoord, player1.getCurrentPosition());
            assertEquals(1, player1.getCharacter().getRangeOfMove());
            assertEquals(RoundState.MOVED_DANGEROUS, player1.getRoundState());

        }

        else if (player1.getCharacter() instanceof Alien) {

            int currentAlienRange = player1.getCharacter().getRangeOfMove();
            moveAction.doAction(currentMatch);
            assertEquals(destinationCoord, player1.getCurrentPosition());
            assertEquals(currentAlienRange, player1.getCharacter().getRangeOfMove());
            assertEquals(RoundState.MOVED_DANGEROUS, player1.getRoundState());

        }

    }
}
