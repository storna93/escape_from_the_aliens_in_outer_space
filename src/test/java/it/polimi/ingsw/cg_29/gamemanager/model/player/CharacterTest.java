package it.polimi.ingsw.cg_29.gamemanager.model.player;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CharacterTest {

    @Test
    public void testConstructorCharacter() {

        Human character1 = new Human(new DefendedHuman());
        assertEquals(true, character1.getCanDefense());

        character1 = new Human(new NormalHuman());
        assertEquals(false, character1.getBeSedate());
        assertEquals(false, character1.getCanAttack());
        assertEquals(false, character1.getCanDefense());
        assertEquals(1, character1.getRangeOfMove());

        character1 = new Human(new FastHuman());
        assertEquals(2, character1.getRangeOfMove());

        character1 = new Human(new AggressiveHuman());
        assertEquals(true, character1.getCanAttack());

        character1 = new Human(new SedateHuman());
        assertEquals(true, character1.getBeSedate());

        Alien character2 = new Alien(new NormalAlien());
        assertEquals(true, character2.getCanAttack());
        assertEquals(2, character2.getRangeOfMove());

        character2 = new Alien(new FastAlien());
        assertEquals(true, character2.getCanAttack());
        assertEquals(3, character2.getRangeOfMove());

    }

    @Test
    public void testChangeStrategyCharacter() {

        Human character1 = new Human(new NormalHuman());
        assertEquals(false, character1.getBeSedate());
        assertEquals(false, character1.getCanAttack());
        assertEquals(false, character1.getCanDefense());
        assertEquals(1, character1.getRangeOfMove());

        character1.activateStrategy(new SedateHuman());
        assertEquals(true, character1.getBeSedate());
        assertEquals(false, character1.getCanAttack());
        assertEquals(false, character1.getCanDefense());
        assertEquals(1, character1.getRangeOfMove());

        character1.activateStrategy(new FastHuman());
        assertEquals(true, character1.getBeSedate());
        assertEquals(false, character1.getCanAttack());
        assertEquals(false, character1.getCanDefense());
        assertEquals(2, character1.getRangeOfMove());

        character1.activateStrategy(new AggressiveHuman());
        assertEquals(true, character1.getBeSedate());
        assertEquals(true, character1.getCanAttack());
        assertEquals(false, character1.getCanDefense());
        assertEquals(2, character1.getRangeOfMove());

        character1.activateStrategy(new DefendedHuman());
        assertEquals(true, character1.getBeSedate());
        assertEquals(true, character1.getCanAttack());
        assertEquals(true, character1.getCanDefense());
        assertEquals(2, character1.getRangeOfMove());

        character1.deactivateStrategy(new DefendedHuman());
        character1.deactivateStrategy(new AggressiveHuman());
        character1.deactivateStrategy(new FastHuman());
        character1.deactivateStrategy(new SedateHuman());
        assertEquals(false, character1.getBeSedate());
        assertEquals(false, character1.getCanAttack());
        assertEquals(false, character1.getCanDefense());
        assertEquals(1, character1.getRangeOfMove());

        Alien character2 = new Alien(new NormalAlien());
        assertEquals(true, character2.getCanAttack());
        assertEquals(2, character2.getRangeOfMove());

        character2.activateStrategy(new FastAlien());
        assertEquals(true, character2.getCanAttack());
        assertEquals(3, character2.getRangeOfMove());

        character2.deactivateStrategy(new FastAlien());
        assertEquals(true, character2.getCanAttack());
        assertEquals(2, character2.getRangeOfMove());

        character1.activateStrategy(new DefendedHuman());
        character1.deactivateStrategy(new NormalHuman());
        assertEquals(false, character1.getBeSedate());
        assertEquals(false, character1.getCanAttack());
        assertEquals(false, character1.getCanDefense());
        assertEquals(1, character1.getRangeOfMove());

        character2.activateStrategy(new FastAlien());
        character1.deactivateStrategy(new NormalAlien());
        assertEquals(false, character1.getBeSedate());
        assertEquals(true, character1.getCanAttack());
        assertEquals(false, character1.getCanDefense());
        assertEquals(2, character1.getRangeOfMove());

    }

}
