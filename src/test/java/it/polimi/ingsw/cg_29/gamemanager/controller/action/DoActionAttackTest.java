package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.AttackItem;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.DefenseItem;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.TeleportItem;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.AggressiveHuman;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Alien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.PlayerState;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class DoActionAttackTest {

    Player player1, player2, player3, player4;
    Match currentMatch;
    String startPlayer2 = ("M04");
    Coordinates startPlayer2Coord = new Coordinates(startPlayer2.substring(0, 1), startPlayer2.substring(1));
    String startPlayer3 = ("M04");
    Coordinates startPlayer3Coord = new Coordinates(startPlayer3.substring(0, 1), startPlayer3.substring(1));
    String startPlayer4 = ("L04");
    Coordinates startPlayer4Coord = new Coordinates(startPlayer4.substring(0, 1), startPlayer4.substring(1));

    @Before
    public void initMatch() throws NotWellFormedZoneException {
        List<String> clientIdList = new ArrayList<String>();
        clientIdList.add("pippo");
        clientIdList.add("pluto");
        clientIdList.add("paperino");
        clientIdList.add("topolino");
        ZoneCreator currentZoneCreator = new FermiZoneCreator();
        currentMatch = new Match(clientIdList, currentZoneCreator);

        player1 = currentMatch.getSpecificPlayerById("pippo");
        player2 = currentMatch.getSpecificPlayerById("pluto");
        player3 = currentMatch.getSpecificPlayerById("paperino");
        player4 = currentMatch.getSpecificPlayerById("topolino");

        player2.getMovementRecord().replaceCurrentCoordinates(startPlayer2Coord);
        player2.addItemCard(new AttackItem());
        player2.addItemCard(new DefenseItem());

        player3.getMovementRecord().replaceCurrentCoordinates(startPlayer3Coord);
        player3.addItemCard(new AttackItem());
        player3.addItemCard(new TeleportItem());

        player4.getMovementRecord().replaceCurrentCoordinates(startPlayer4Coord);

    }

    @Test
    public void TestDoAlienAttackOnOneRivalPlayerWithoutDefense() {

        String start = ("L04");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);

        Action attackAction = new AttackAction("pippo");
        Action useAttackAction = new UseAttackAction("pippo");

        if (player1.getCharacter() instanceof Human) {

            player1.getCharacter().activateStrategy(new AggressiveHuman());
            assertEquals(true, player1.getCharacter().getCanAttack());
            assertEquals(1, player1.getCharacter().getRangeOfMove());

            useAttackAction.doAction(currentMatch);
            assertEquals(false, player1.getCharacter().getCanAttack());
            assertEquals(1, player1.getCharacter().getRangeOfMove());

            assertEquals(PlayerState.LOSER, player4.getPlayerState());
            assertEquals(PlayerState.PLAYING, player1.getPlayerState());
            assertEquals(RoundState.ENDING, player1.getRoundState());

        }

        else if (player1.getCharacter() instanceof Alien) {

            assertEquals(true, player1.getCharacter().getCanAttack());
            assertEquals(2, player1.getCharacter().getRangeOfMove());

            attackAction.doAction(currentMatch);
            assertEquals(true, player1.getCharacter().getCanAttack());
            if (player4.getCharacter() instanceof Alien)
                assertEquals(2, player1.getCharacter().getRangeOfMove());
            else
                assertEquals(3, player1.getCharacter().getRangeOfMove());

            assertEquals(PlayerState.LOSER, player4.getPlayerState());
            assertEquals(PlayerState.PLAYING, player1.getPlayerState());
            assertEquals(RoundState.ENDING, player1.getRoundState());

        }

    }

    @Test
    public void TestDoAlienAttackOnTwoRivalPlayerOneWithDefense() {

        String start = ("M04");
        Coordinates startCoord = new Coordinates(start.substring(0, 1), start.substring(1));
        player1.getMovementRecord().replaceCurrentCoordinates(startCoord);

        Action attackAction = new AttackAction("pippo");
        Action useAttackAction = new UseAttackAction("pippo");

        if (player1.getCharacter() instanceof Human) {

            player1.getCharacter().activateStrategy(new AggressiveHuman());
            assertEquals(true, player1.getCharacter().getCanAttack());
            assertEquals(1, player1.getCharacter().getRangeOfMove());

            useAttackAction.doAction(currentMatch);
            assertEquals(false, player1.getCharacter().getCanAttack());
            assertEquals(1, player1.getCharacter().getRangeOfMove());

            if (player2.getCharacter() instanceof Human) {

                assertEquals(PlayerState.PLAYING, player2.getPlayerState());
                assertEquals(1, player2.getItemCardList().size());
                assertEquals(true, player2.hasSpecificItemCard(new AttackItem()));
            } else {

                assertEquals(PlayerState.LOSER, player2.getPlayerState());
                assertEquals(0, player2.getItemCardList().size());

            }

            assertEquals(PlayerState.LOSER, player3.getPlayerState());
            assertEquals(0, player3.getItemCardList().size());

            assertEquals(PlayerState.PLAYING, player1.getPlayerState());
            assertEquals(RoundState.ENDING, player1.getRoundState());

        }

        else if (player1.getCharacter() instanceof Alien) {

            assertEquals(true, player1.getCharacter().getCanAttack());
            assertEquals(2, player1.getCharacter().getRangeOfMove());

            attackAction.doAction(currentMatch);
            assertEquals(true, player1.getCharacter().getCanAttack());
            if (player3.getCharacter() instanceof Alien)
                assertEquals(2, player1.getCharacter().getRangeOfMove());
            else
                assertEquals(3, player1.getCharacter().getRangeOfMove());

            if (player2.getCharacter() instanceof Human) {

                assertEquals(PlayerState.PLAYING, player2.getPlayerState());
                assertEquals(1, player2.getItemCardList().size());
                assertEquals(true, player2.hasSpecificItemCard(new AttackItem()));
            } else {

                assertEquals(PlayerState.LOSER, player2.getPlayerState());
                assertEquals(0, player2.getItemCardList().size());
            }

            assertEquals(PlayerState.LOSER, player3.getPlayerState());
            assertEquals(0, player3.getItemCardList().size());

            assertEquals(PlayerState.PLAYING, player1.getPlayerState());
            assertEquals(RoundState.ENDING, player1.getRoundState());

        }

    }
}
