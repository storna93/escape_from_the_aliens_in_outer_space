package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.AdrenalineItem;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.AttackItem;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.SedativesItem;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.TeleportItem;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Alien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.NormalAlien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.NormalHuman;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.PlayerState;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class IsValidActivateItemActionTest {

    Player player1, player2;
    Match currentMatch;

    @Before
    public void initMatch() throws NotWellFormedZoneException {
        List<String> clientIdList = new ArrayList<String>();
        clientIdList.add("pippo");
        clientIdList.add("pluto");
        ZoneCreator currentZoneCreator = new FermiZoneCreator();
        currentMatch = new Match(clientIdList, currentZoneCreator);

        player1 = currentMatch.getSpecificPlayerById("pippo");
        player2 = currentMatch.getSpecificPlayerById("pluto");

    }

    @Test
    public void TestValidActivationItemActionAtBEGINNINGState() {

        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());
        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        Action activateSedativesAction = new ActivateSedativesAction("pippo", new SedativesItem());
        Action activateTeleportAction = new ActivateTeleportAction("pippo", new TeleportItem());

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            // add three itemCards
            player1.addItemCard(new AdrenalineItem());
            player1.addItemCard(new AttackItem());
            player1.addItemCard(new TeleportItem());

            // testing a player with three itemCards
            assertEquals(true, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(true, activateAttackAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(true, activateTeleportAction.isValid(currentMatch));

            // add two itemCards
            player1.addItemCard(new SedativesItem());
            player1.addItemCard(new AttackItem());

            // testing a player with two itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(true, activateAttackAction.isValid(currentMatch));
            assertEquals(true, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            player1.addItemCard(new AdrenalineItem());
            player1.addItemCard(new AttackItem());

            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidActivationItemActionAtMOVESECUREState() {

        // set MOVED_SECURE RoundState and clear itemCardList
        player1.setRoundState(RoundState.MOVED_SECURE);
        player1.getItemCardList().clear();
        if (player1.getCharacter() instanceof Human)
            player1.getCharacter().activateStrategy(new NormalHuman());
        else if (player1.getCharacter() instanceof Alien)
            player1.getCharacter().activateStrategy(new NormalAlien());

        // define action

        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());
        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        // Action activateSpotlightAction =new ActivateSpotlightAction("pippo",,
        // new SpotlightItem());
        Action activateSedativesAction = new ActivateSedativesAction("pippo", new SedativesItem());
        Action activateTeleportAction = new ActivateTeleportAction("pippo", new TeleportItem());

        // test

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            // add three itemCards
            player1.addItemCard(new AdrenalineItem());
            player1.addItemCard(new AttackItem());
            player1.addItemCard(new TeleportItem());

            // testing a player with three itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(true, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(true, activateTeleportAction.isValid(currentMatch));

            // add two itemCards
            player1.addItemCard(new SedativesItem());
            player1.addItemCard(new AttackItem());

            // testing a player with two itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(true, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            player1.addItemCard(new AdrenalineItem());
            player1.addItemCard(new AttackItem());

            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidActivationItemActionAtMOVEDANGEROUSState() {

        // set MOVED_DANGEROUS RoundState and clear itemCardList
        player1.setRoundState(RoundState.MOVED_DANGEROUS);
        player1.getItemCardList().clear();
        if (player1.getCharacter() instanceof Human)
            player1.getCharacter().activateStrategy(new NormalHuman());
        else if (player1.getCharacter() instanceof Alien)
            player1.getCharacter().activateStrategy(new NormalAlien());

        // define action
        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());
        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        // Action activateSpotlightAction =new ActivateSpotlightAction("pippo",,
        // new SpotlightItem());
        Action activateSedativesAction = new ActivateSedativesAction("pippo", new SedativesItem());
        Action activateTeleportAction = new ActivateTeleportAction("pippo", new TeleportItem());

        // test

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            // add three itemCards
            player1.addItemCard(new AdrenalineItem());
            player1.addItemCard(new AttackItem());
            player1.addItemCard(new TeleportItem());

            // testing a player with three itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(true, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            // add two itemCards
            player1.addItemCard(new SedativesItem());
            player1.addItemCard(new AttackItem());

            // testing a player with two itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(true, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(true, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            player1.addItemCard(new AdrenalineItem());
            player1.addItemCard(new AttackItem());

            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidActivationItemActionAtENDINGtate() {

        // set ENDING RoundState and clear itemCardList
        player1.setRoundState(RoundState.ENDING);
        player1.getItemCardList().clear();
        if (player1.getCharacter() instanceof Human)
            player1.getCharacter().activateStrategy(new NormalHuman());
        else if (player1.getCharacter() instanceof Alien)
            player1.getCharacter().activateStrategy(new NormalAlien());

        // define action
        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());
        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        // Action activateSpotlightAction =new ActivateSpotlightAction("pippo",,
        // new SpotlightItem());
        Action activateSedativesAction = new ActivateSedativesAction("pippo", new SedativesItem());
        Action activateTeleportAction = new ActivateTeleportAction("pippo", new TeleportItem());

        // test

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            // add three itemCards
            player1.addItemCard(new AdrenalineItem());
            player1.addItemCard(new AttackItem());
            player1.addItemCard(new TeleportItem());

            // testing a player with three itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(true, activateTeleportAction.isValid(currentMatch));

            // add two itemCards
            player1.addItemCard(new SedativesItem());
            player1.addItemCard(new AttackItem());

            // testing a player with two itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            player1.addItemCard(new AdrenalineItem());
            player1.addItemCard(new AttackItem());

            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidActivationItemActionAtDRAWNtate() {

        // set DRAWN RoundState and clear itemCardList
        player1.setRoundState(RoundState.FAKE_NOISE);
        player1.getItemCardList().clear();
        if (player1.getCharacter() instanceof Human)
            player1.getCharacter().activateStrategy(new NormalHuman());
        else if (player1.getCharacter() instanceof Alien)
            player1.getCharacter().activateStrategy(new NormalAlien());

        // define action
        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());
        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        // Action activateSpotlightAction =new ActivateSpotlightAction("pippo",,
        // new SpotlightItem());
        Action activateSedativesAction = new ActivateSedativesAction("pippo", new SedativesItem());
        Action activateTeleportAction = new ActivateTeleportAction("pippo", new TeleportItem());

        // test

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            // add three itemCards
            player1.addItemCard(new AdrenalineItem());
            player1.addItemCard(new AttackItem());
            player1.addItemCard(new TeleportItem());

            // testing a player with three itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            // add two itemCards
            player1.addItemCard(new SedativesItem());
            player1.addItemCard(new AttackItem());

            // testing a player with two itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            player1.addItemCard(new AdrenalineItem());
            player1.addItemCard(new AttackItem());

            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidActivationItemActionAtSTANDBYtate() {

        // set STANDBY RoundState and clear itemCardList
        player1.setRoundState(RoundState.STANDBY);
        player1.getItemCardList().clear();
        if (player1.getCharacter() instanceof Human)
            player1.getCharacter().activateStrategy(new NormalHuman());
        else if (player1.getCharacter() instanceof Alien)
            player1.getCharacter().activateStrategy(new NormalAlien());

        // define action
        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());
        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        // Action activateSpotlightAction =new ActivateSpotlightAction("pippo",,
        // new SpotlightItem());
        Action activateSedativesAction = new ActivateSedativesAction("pippo", new SedativesItem());
        Action activateTeleportAction = new ActivateTeleportAction("pippo", new TeleportItem());

        // test

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            // add three itemCards
            player1.addItemCard(new AdrenalineItem());
            player1.addItemCard(new AttackItem());
            player1.addItemCard(new TeleportItem());

            // testing a player with three itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            // add two itemCards
            player1.addItemCard(new SedativesItem());
            player1.addItemCard(new AttackItem());

            // testing a player with two itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            player1.addItemCard(new AdrenalineItem());
            player1.addItemCard(new AttackItem());

            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidActivationItemActionAtWINNERtate() {

        // set WINNER RoundState and clear itemCardList
        player1.setPlayerState(PlayerState.WINNER);
        player1.getItemCardList().clear();
        if (player1.getCharacter() instanceof Human)
            player1.getCharacter().activateStrategy(new NormalHuman());
        else if (player1.getCharacter() instanceof Alien)
            player1.getCharacter().activateStrategy(new NormalAlien());

        // define action
        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());
        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        // Action activateSpotlightAction =new ActivateSpotlightAction("pippo",,
        // new SpotlightItem());
        Action activateSedativesAction = new ActivateSedativesAction("pippo", new SedativesItem());
        Action activateTeleportAction = new ActivateTeleportAction("pippo", new TeleportItem());

        // test

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            // add three itemCards
            player1.addItemCard(new AdrenalineItem());
            player1.addItemCard(new AttackItem());
            player1.addItemCard(new TeleportItem());

            // testing a player with three itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            // add two itemCards
            player1.addItemCard(new SedativesItem());
            player1.addItemCard(new AttackItem());

            // testing a player with two itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            player1.addItemCard(new AdrenalineItem());
            player1.addItemCard(new AttackItem());

            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidActivationItemActionAtLOSERtate() {

        // set LOSER RoundState and clear itemCardList
        player1.setPlayerState(PlayerState.LOSER);
        player1.getItemCardList().clear();
        if (player1.getCharacter() instanceof Human)
            player1.getCharacter().activateStrategy(new NormalHuman());
        else if (player1.getCharacter() instanceof Alien)
            player1.getCharacter().activateStrategy(new NormalAlien());

        // define action
        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());
        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        // Action activateSpotlightAction =new ActivateSpotlightAction("pippo",,
        // new SpotlightItem());
        Action activateSedativesAction = new ActivateSedativesAction("pippo", new SedativesItem());
        Action activateTeleportAction = new ActivateTeleportAction("pippo", new TeleportItem());

        // test

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            // add three itemCards
            player1.addItemCard(new AdrenalineItem());
            player1.addItemCard(new AttackItem());
            player1.addItemCard(new TeleportItem());

            // testing a player with three itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            // add two itemCards
            player1.addItemCard(new SedativesItem());
            player1.addItemCard(new AttackItem());

            // testing a player with two itemCards
            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

            player1.addItemCard(new AdrenalineItem());
            player1.addItemCard(new AttackItem());

            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(false, activateAttackAction.isValid(currentMatch));
            // assertEquals(false,activateSpotlightAction.isValid(currentMatch));
            assertEquals(false, activateSedativesAction.isValid(currentMatch));
            assertEquals(false, activateTeleportAction.isValid(currentMatch));

        }

    }

}
