package it.polimi.ingsw.cg_29.gamemanager.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.DangerousSectorDeck;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.EscapeHatchDeck;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.ItemDeck;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.SpotlightItem;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.TeleportItem;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Alien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.PlayerState;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class MatchTest {

    Match m1;

    @Before
    public void init() throws NotWellFormedZoneException {
        List<String> clientIdList = new ArrayList<String>();
        clientIdList.add("123");
        clientIdList.add("456");
        clientIdList.add("789");

        m1 = new Match(clientIdList, new FermiZoneCreator());
    }

    @Test
    public void testConstructorAndGetter() {
        assertTrue(m1.getCurrentZone() instanceof FermiZone);
        assertTrue(m1.getDangerousSectorDeck() instanceof DangerousSectorDeck);
        assertTrue(m1.getItemDeck() instanceof ItemDeck);
        assertTrue(m1.getEscapeHatchDeck() instanceof EscapeHatchDeck);
        assertEquals(MatchState.RUNNING, m1.getMatchState());
    }

    @Test
    public void testRoundCounter() {
        assertEquals(1, m1.getCurrentRound());
        m1.nextRound();
        assertEquals(2, m1.getCurrentRound());
    }

    @Test
    public void testPlayerList() {

        assertEquals(null, m1.getSpecificPlayerById("987"));
        assertNotEquals(null, m1.getSpecificPlayerById("123"));

    }

    @Test
    public void testToString() {
        String s1 = m1.toString();

        m1.nextRound();

        assertNotEquals(s1, m1.toString());
    }

    @Test
    public void testPrintState() {
        String state1 = m1.printState("123");
        String state2 = m1.printState("456");

        assertNotEquals(state1, state2);

        m1.getSpecificPlayerById("123").addItemCard(new TeleportItem());
        m1.getSpecificPlayerById("123").addItemCard(new SpotlightItem());

        String state3 = m1.printState("123");

        assertNotEquals(state1, state3);
    }

    @Test
    public void testGameOverConditionsRound() {
        m1.getSpecificPlayerById("789").setPlayerState(PlayerState.DISCONNECTED);
        m1.getSpecificPlayerById("789").setRoundState(RoundState.DISCONNECTED);
        for (int i = 1; i < 40 * 2; i++)
            for (Player p : m1.getPlayerList()) {
                if (p.getRoundState() != RoundState.STANDBY && p.getRoundState() != RoundState.DISCONNECTED) {
                    p.setRoundState(RoundState.STANDBY);
                    m1.nextPlayerRound(p);
                    break;
                }
            }

        assertTrue(m1.gameOverConditions());
    }

    @Test
    public void testGameOverConditionsAlien() {

        assertFalse(m1.gameOverConditions());

        for (Player p : m1.getPlayerList()) {
            if (p.getCharacter() instanceof Human) {
                p.setPlayerState(PlayerState.LOSER);
            }
        }

        assertTrue(m1.gameOverConditions());

        PlayerState alien1State = PlayerState.PLAYING;
        for (Player p : m1.getPlayerList()) {
            if (p.getCharacter() instanceof Alien) {
                alien1State = p.getPlayerState();
                break;
            }
        }

        assertEquals(alien1State, PlayerState.WINNER);
    }

    @Test
    public void testGameOverConditionsHuman() {

        assertFalse(m1.gameOverConditions());

        for (Player p : m1.getPlayerList()) {
            if (p.getCharacter() instanceof Human) {
                p.setPlayerState(PlayerState.WINNER);
            }
        }

        assertTrue(m1.gameOverConditions());

        PlayerState alien1State = PlayerState.PLAYING;
        for (Player p : m1.getPlayerList()) {
            if (p.getCharacter() instanceof Alien) {
                alien1State = p.getPlayerState();
                break;
            }
        }

        assertEquals(alien1State, PlayerState.LOSER);
    }

}
