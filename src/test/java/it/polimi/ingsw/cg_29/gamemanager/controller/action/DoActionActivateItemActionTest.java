package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.AdrenalineItem;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.AttackItem;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.SedativesItem;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.TeleportItem;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Alien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.HumanSector;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Sector;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class DoActionActivateItemActionTest {
    Player player1, player2;
    Match currentMatch;

    @Before
    public void initMatch() throws NotWellFormedZoneException {
        List<String> clientIdList = new ArrayList<String>();
        clientIdList.add("pippo");
        clientIdList.add("pluto");
        ZoneCreator currentZoneCreator = new FermiZoneCreator();
        currentMatch = new Match(clientIdList, currentZoneCreator);

        player1 = currentMatch.getSpecificPlayerById("pippo");
        player2 = currentMatch.getSpecificPlayerById("pluto");

    }

    @Test
    public void TestDoHumanActivateAdrenaline() {

        Action activateAdrenalineAction = new ActivateAdrenalineAction("pippo", new AdrenalineItem());

        if (player1.getCharacter() instanceof Human) {

            activateAdrenalineAction.doAction(currentMatch);
            assertEquals(2, player1.getCharacter().getRangeOfMove());
            assertEquals(RoundState.BEGINNING, player1.getRoundState());

        }

        else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateAdrenalineAction.isValid(currentMatch));
            assertEquals(RoundState.BEGINNING, player1.getRoundState());

        }

    }

    @Test
    public void TestDoHumanActivateSedative() {

        Action activateSedativeAction = new ActivateSedativesAction("pippo", new SedativesItem());

        if (player1.getCharacter() instanceof Human) {

            activateSedativeAction.doAction(currentMatch);
            assertEquals(true, player1.getCharacter().getBeSedate());
            assertEquals(RoundState.BEGINNING, player1.getRoundState());

        }

        else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateSedativeAction.isValid(currentMatch));
            assertEquals(RoundState.BEGINNING, player1.getRoundState());

        }

    }

    @Test
    public void TestDoHumanActivateTeleportFromBeginningState() {

        Action activateTeleportAction = new ActivateTeleportAction("pippo", new TeleportItem());

        if (player1.getCharacter() instanceof Human) {

            Coordinates currentPosition = player1.getCurrentPosition();
            Sector currentSector = currentMatch.getCurrentZone().getSectorByCoordinates(currentPosition);

            activateTeleportAction.doAction(currentMatch);
            assertTrue(currentSector instanceof HumanSector);
            assertEquals(RoundState.BEGINNING, player1.getRoundState());

        }

        else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateTeleportAction.isValid(currentMatch));
            assertEquals(RoundState.BEGINNING, player1.getRoundState());

        }

    }

    @Test
    public void TestDoHumanActivateTeleportFromMoveSecureState() {

        Action activateTeleportAction = new ActivateTeleportAction("pippo", new TeleportItem());
        // change player RoundState
        player1.setRoundState(RoundState.MOVED_SECURE);

        if (player1.getCharacter() instanceof Human) {

            Coordinates currentPosition = player1.getCurrentPosition();
            Sector currentSector = currentMatch.getCurrentZone().getSectorByCoordinates(currentPosition);

            activateTeleportAction.doAction(currentMatch);
            assertTrue(currentSector instanceof HumanSector);
            assertEquals(RoundState.ENDING, player1.getRoundState());

        }

        else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateTeleportAction.isValid(currentMatch));
            assertEquals(RoundState.MOVED_SECURE, player1.getRoundState());

        }

    }

    @Test
    public void TestDoHumanActivateTeleportFromEndingState() {

        Action activateTeleportAction = new ActivateTeleportAction("pippo", new TeleportItem());
        // change player RoundState
        player1.setRoundState(RoundState.ENDING);

        if (player1.getCharacter() instanceof Human) {

            Coordinates currentPosition = player1.getCurrentPosition();
            Sector currentSector = currentMatch.getCurrentZone().getSectorByCoordinates(currentPosition);

            activateTeleportAction.doAction(currentMatch);
            assertTrue(currentSector instanceof HumanSector);
            assertEquals(RoundState.ENDING, player1.getRoundState());

        }

        else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateTeleportAction.isValid(currentMatch));
            assertEquals(RoundState.ENDING, player1.getRoundState());

        }

    }

    @Test
    public void TestDoHumanActivateAttackFromBeginningState() {

        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());

        if (player1.getCharacter() instanceof Human) {

            activateAttackAction.doAction(currentMatch);
            assertEquals(true, player1.getCharacter().getCanAttack());
            assertEquals(RoundState.BEGINNING, player1.getRoundState());

        }

        else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateAttackAction.isValid(currentMatch));
            assertEquals(RoundState.BEGINNING, player1.getRoundState());

        }

    }

    @Test
    public void TestDoHumanActivateAttackFromMoveSecureState() {

        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        // change player RoundState
        player1.setRoundState(RoundState.MOVED_SECURE);

        if (player1.getCharacter() instanceof Human) {

            activateAttackAction.doAction(currentMatch);
            assertEquals(true, player1.getCharacter().getCanAttack());
            assertEquals(RoundState.MOVED_SECURE, player1.getRoundState());

        }

        else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateAttackAction.isValid(currentMatch));
            assertEquals(RoundState.MOVED_SECURE, player1.getRoundState());

        }

    }

    @Test
    public void TestDoHumanActivateAttackFromMoveDangerousState() {
        Action activateAttackAction = new ActivateAttackAction("pippo", new AttackItem());
        // change player RoundState
        player1.setRoundState(RoundState.MOVED_DANGEROUS);

        if (player1.getCharacter() instanceof Human) {

            activateAttackAction.doAction(currentMatch);
            assertEquals(true, player1.getCharacter().getCanAttack());
            assertEquals(RoundState.MOVED_DANGEROUS, player1.getRoundState());

        }

        else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, activateAttackAction.isValid(currentMatch));
            assertEquals(RoundState.MOVED_DANGEROUS, player1.getRoundState());

        }
    }

}
