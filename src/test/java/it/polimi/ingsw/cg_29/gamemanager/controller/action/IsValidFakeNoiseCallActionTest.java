package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Alien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class IsValidFakeNoiseCallActionTest {
    Player player1, player2;
    Match currentMatch;

    @Before
    public void initMatch() throws NotWellFormedZoneException {
        List<String> clientIdList = new ArrayList<String>();
        clientIdList.add("pippo");
        clientIdList.add("pluto");
        ZoneCreator currentZoneCreator = new FermiZoneCreator();
        currentMatch = new Match(clientIdList, currentZoneCreator);

        player1 = currentMatch.getSpecificPlayerById("pippo");
        player2 = currentMatch.getSpecificPlayerById("pluto");

    }

    @Test
    public void TestValidFakeNoiseCallActionFromBeginningState() {

        String fakeSector = "L05";
        Coordinates fakeCoord = new Coordinates(fakeSector.substring(0, 1), fakeSector.substring(1));

        Action fakeNoiseCallAction = new FakeNoiseCallAction("pippo", fakeCoord);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards

            assertEquals(false, fakeNoiseCallAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, fakeNoiseCallAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidFakeNoiseCallActionFromDrawnState() {

        String fakeSector = "L10";
        Coordinates fakeCoord = new Coordinates(fakeSector.substring(0, 1), fakeSector.substring(1));

        Action fakeNoiseCallAction = new FakeNoiseCallAction("pippo", fakeCoord);

        // change player1 RoundState
        player1.setRoundState(RoundState.FAKE_NOISE);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards

            assertEquals(true, fakeNoiseCallAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            assertEquals(true, fakeNoiseCallAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidFakeNoiseCallActionOnVoidSector() {

        String fakeSector = "H01";
        Coordinates fakeCoord = new Coordinates(fakeSector.substring(0, 1), fakeSector.substring(1));

        Action fakeNoiseCallAction = new FakeNoiseCallAction("pippo", fakeCoord);

        // change player1 RoundState
        player1.setRoundState(RoundState.FAKE_NOISE);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards

            assertEquals(false, fakeNoiseCallAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, fakeNoiseCallAction.isValid(currentMatch));

        }

    }

    @Test
    public void TestValidFakeNoiseCallActionOnNonExistingdSector() {

        String fakeSector = "H100";
        Coordinates fakeCoord = new Coordinates(fakeSector.substring(0, 1), fakeSector.substring(1));

        Action fakeNoiseCallAction = new FakeNoiseCallAction("pippo", fakeCoord);

        // change player1 RoundState
        player1.setRoundState(RoundState.FAKE_NOISE);

        if (player1.getCharacter() instanceof Human) {

            // testing a player without any itemCards

            assertEquals(false, fakeNoiseCallAction.isValid(currentMatch));

        } else if (player1.getCharacter() instanceof Alien) {

            assertEquals(false, fakeNoiseCallAction.isValid(currentMatch));

        }

    }

}
