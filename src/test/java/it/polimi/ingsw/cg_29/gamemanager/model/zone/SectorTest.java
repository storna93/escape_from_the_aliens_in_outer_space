package it.polimi.ingsw.cg_29.gamemanager.model.zone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_29.LoggerClass;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class SectorTest {

    Zone z1 = new FermiZoneCreator().createZone();
    Sector s1;

    @Before
    public void init() {
        s1 = z1.getSectorByString("L", "05");
    }

    @Test
    public void testConstructor() {
        s1 = new AlienSector(0, 0, z1);
        assertTrue(s1 instanceof AlienSector);
        s1 = new HumanSector(0, 0, z1);
        assertTrue(s1 instanceof HumanSector);
        s1 = new DangerousSector(0, 0, z1);
        assertTrue(s1 instanceof DangerousSector);
        s1 = new SecureSector(0, 0, z1);
        assertTrue(s1 instanceof SecureSector);
        s1 = new VoidSector(0, 0, z1);
        assertTrue(s1 instanceof VoidSector);
        s1 = new EscapeHatchSector(0, 0, z1);
        assertTrue(s1 instanceof EscapeHatchSector);

        s1 = new AlienSector("A", "01", z1);
        assertTrue(s1 instanceof AlienSector);
        s1 = new HumanSector("A", "01", z1);
        assertTrue(s1 instanceof HumanSector);
        s1 = new DangerousSector("A", "01", z1);
        assertTrue(s1 instanceof DangerousSector);
        s1 = new SecureSector("A", "01", z1);
        assertTrue(s1 instanceof SecureSector);
        s1 = new VoidSector("A", "01", z1);
        assertTrue(s1 instanceof VoidSector);
        s1 = new EscapeHatchSector("A", "01", z1);
        assertTrue(s1 instanceof EscapeHatchSector);

        boolean exception = false;
        try {
            s1 = new AlienSector("A", "01", null);
        } catch (NullPointerException e) {
            LoggerClass.getLogger().warn(e);
            exception = true;
        }
        assertTrue(exception);

        exception = false;
        try {
            s1 = new AlienSector("A", null, z1);
        } catch (NumberFormatException e) {
            LoggerClass.getLogger().warn(e);
            exception = true;
        }
        assertTrue(exception);

        try {
            s1 = new AlienSector("A", "01", z1);
            assertTrue(s1 instanceof AlienSector);
        } catch (NullPointerException e) {
            LoggerClass.getLogger().warn(e);
            e.printStackTrace();
        }
    }

    @Test
    public void getNearSectorsProxy() {
        NearSectorsProxy current = s1.getNearSectorsProxy();
        NearSectorsProxy expected = new NearSectorsProxy(new Coordinates(s1.getRow(), s1.getCol()), z1);
        assertEquals(expected, current);
    }

    @Test
    public void testHashCode() {
        Sector s2 = new DangerousSector(0, 0, z1);
        EscapeHatchSector s5 = new EscapeHatchSector("L", "05", z1);
        EscapeHatchSector s6 = new EscapeHatchSector("L", "05", z1);

        s5.block();

        assertEquals(s1.hashCode(), s1.hashCode());
        assertNotEquals(s1.hashCode(), s2.hashCode());
        assertNotEquals(s1.hashCode(), null);

        assertEquals(s5.hashCode(), s5.hashCode());
        assertNotEquals(s5.hashCode(), s6.hashCode());
    }

    @Test
    public void testEquals() {
        Sector s2 = new SecureSector("L", "05", z1);
        Sector s3 = new HumanSector("L", "05", z1);
        Sector s31 = new HumanSector("L", "05", z1);
        HumanSector s32 = new HumanSector("L", "05", z1);
        Sector s4 = new SecureSector(0, 0, z1);
        EscapeHatchSector s5 = new EscapeHatchSector("L", "05", z1);
        EscapeHatchSector s6 = new EscapeHatchSector("L", "05", z1);

        s5.block();

        s31.getNearSectors(1);

        assertFalse(s1.equals(null));
        assertFalse(s2.equals(s5));
        assertFalse(s2.equals(z1));
        assertFalse(s2.equals(9));
        assertFalse(s1.equals(s3));
        assertFalse(s3.equals(s31));
        assertFalse(s3.equals(s4));
        assertFalse(s5.equals(s6));
        assertFalse(s5.equals(s32));
        assertTrue(s1.equals(s1));
        assertTrue(s1.equals(s2));
        assertTrue(s5.equals(s5));
    }

    @Test
    public void testToString() {
        assertEquals("Sector[[L05][nearSectorsProxy=NearSectorsProxy [zone=class it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZone, nearSectorsCoordinatesList=[[L05{4,11}]]]]]", s1.toString());
    }

    @Test
    public void testPrintToString() {
        s1 = new AlienSector(0, 0, z1);
        assertEquals("<A01(A)>", s1.printToString());
        s1 = new HumanSector(0, 0, z1);
        assertEquals("<A01(H)>", s1.printToString());
        s1 = new DangerousSector(0, 0, z1);
        assertEquals("<A01(D)>", s1.printToString());
        s1 = new SecureSector(0, 0, z1);
        assertEquals("<A01(S)>", s1.printToString());
        s1 = new VoidSector(0, 0, z1);
        assertEquals("        ", s1.printToString());
        s1 = new EscapeHatchSector(0, 0, z1);
        assertEquals("<A01(E)>", s1.printToString());
    }

    @Test
    public void testGetNearSector() {
        HashSet<Coordinates> expected = new HashSet<Coordinates>();
        expected.add(new Coordinates("L", "03"));
        expected.add(new Coordinates("L", "04"));
        expected.add(new Coordinates("L", "06"));
        expected.add(new Coordinates("L", "07"));
        expected.add(new Coordinates("K", "04"));
        expected.add(new Coordinates("M", "04"));

        assertEquals(expected, s1.getNearSectors(2));
    }

    @Test
    public void testNearSectorProxy() {
        Sector s2 = new SecureSector("L", "05", z1);

        Set<Coordinates> firstCall = s1.getNearSectors(100);
        Set<Coordinates> secondCall = s1.getNearSectors(100);
        assertEquals(firstCall, secondCall);

        assertFalse(s1.getNearSectorsProxy().equals(null));
        assertFalse(s1.getNearSectorsProxy().equals(9));
        assertFalse(s1.getNearSectorsProxy().equals(s2.getNearSectorsProxy()));
        assertTrue(s1.getNearSectorsProxy().equals(s1.getNearSectorsProxy()));
    }

    @Test
    public void testBlockEscapeHatch() {
        EscapeHatchSector s2 = new EscapeHatchSector("L", "05", z1);
        assertTrue(s2.isUsable());
        s2.block();
        assertFalse(s2.isUsable());
        assertEquals("<L05(#)>", s2.printToString());

    }

}
