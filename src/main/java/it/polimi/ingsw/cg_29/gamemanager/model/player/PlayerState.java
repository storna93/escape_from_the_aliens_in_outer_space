package it.polimi.ingsw.cg_29.gamemanager.model.player;

/**
 * This Enumeration defines all and only the possible states that a Player could
 * have during the game.
 * 
 * @author Fulvio
 * @version 1.0
 */

public enum PlayerState {

    DISCONNECTED, PLAYING, WINNER, LOSER;

}
