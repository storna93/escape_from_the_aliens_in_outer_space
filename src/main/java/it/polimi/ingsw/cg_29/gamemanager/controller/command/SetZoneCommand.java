package it.polimi.ingsw.cg_29.gamemanager.controller.command;

import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.GalileiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.GalvaniZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.view.View;

/**
 * A command to change map befor starting match.
 *
 * @author Luca
 * @version 1.0
 *
 */
public class SetZoneCommand extends Command {

    /**
     * Array with zone creators.
     */
    private static final ZoneCreator[] arrayZone = { new GalileiZoneCreator(), new FermiZoneCreator(), new GalvaniZoneCreator() };

    /**
     * Number of the zone choosed by client.
     */
    private int numberOfTheZone;

    /**
     * Create a new command
     * 
     * @param controller
     *            the controller that manages current match
     * @param currentClient
     *            the id of client who sent command
     */
    public SetZoneCommand(Controller controller, View currentClient, int numberOfTheZone) {
        super(controller, currentClient);
        this.numberOfTheZone = numberOfTheZone;
    }

    /**
     * Launch this command.
     */
    @Override
    public void execute() {
        if (this.matchIsRunning())
            this.getCurrentClient().send("The match is already running.");
        else if (!this.getCurrentClientId().equals(this.getController().getMatchAdminId()))
            this.getCurrentClient().send("You're not the Match Administrator.");
        else if (0 < this.numberOfTheZone && this.numberOfTheZone <= arrayZone.length) {
            this.getController().setZoneCrator(arrayZone[numberOfTheZone - 1]);
            this.getCurrentClient().send("Zone is set.");
        }

    }

}
