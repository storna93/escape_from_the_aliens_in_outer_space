package it.polimi.ingsw.cg_29.gamemanager.model.zone;

/**
 * Implementation of Sector representing HumanSector.
 * 
 * @author Luca
 * @version 1.0
 *
 */
public class HumanSector extends Sector {

    /**
     * Return an HumanSector with specific coordinates and reference to zone and
     * initializes the state.
     * 
     * @param row
     *            reference to row of the zone by integer number.
     * @param col
     *            reference to column of the zone by integer number.
     * @param zone
     *            reference to the zone.
     * @throws NullPointerException
     */
    protected HumanSector(int row, int col, Zone zone) {
        super(row, col, zone);
    }

    /**
     * Return an HumanSector with specific coordinates and reference to zone and
     * initializes the state.
     * 
     * @param col
     *            reference to column of the zone by string.
     * @param row
     *            reference to row of the zone by string.
     * @param zone
     *            reference to the zone.
     * @throws NullPointerException
     *             , NumberFormatException
     */
    public HumanSector(String col, String row, Zone zone) {
        super(col, row, zone);
    }

    /**
     * @return a "pretty print" string representing this Sector.
     */
    @Override
    public String printToString() {
        return "<" + super.printToString() + "(H)>";
    }

}
