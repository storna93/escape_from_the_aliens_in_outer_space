package it.polimi.ingsw.cg_29.gamemanager.controller.command;

import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;
import it.polimi.ingsw.cg_29.gamemanager.view.View;

/**
 * A command to show map.
 *
 * @author Luca
 * @version 1.0
 *
 */
public class ShowZoneCommand extends Command {

    /**
     * Create a new command
     * 
     * @param controller
     *            the controller that manages current match
     * @param currentClient
     *            the id of client who sent command
     */
    public ShowZoneCommand(Controller controller, View currentClient) {
        super(controller, currentClient);
    }

    /**
     * Launch this command.
     */
    @Override
    public void execute() {
        if (this.matchIsRunning())
            this.getCurrentClient().send(this.getCurrentMatch().getCurrentZone().printToString());
        else
            this.getCurrentClient().send("The match is not running.");

    }

}
