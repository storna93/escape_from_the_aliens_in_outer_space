package it.polimi.ingsw.cg_29.gamemanager.publisher;

import it.polimi.ingsw.cg_29.LoggerClass;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Contains the implementation of the subscriber thread that represent a client
 * connected to the server by socket.
 * 
 * @author Luca
 * @version 1.0
 *
 */
public class SubscriberServerSocketThread extends SubscriberServerThread {

    /**
     * Output stream of the socket.
     */
    private PrintWriter socketOut;

    /**
     * Creates a new SubscriberServerSocketThread with specific socket
     *
     * @param socket
     *            is the socket of client.
     *
     */
    public SubscriberServerSocketThread(Socket socket) {
        try {
            socketOut = new PrintWriter(socket.getOutputStream());
        } catch (IOException e) {
            LoggerClass.getLogger().warn(e);
        }
    }

    /**
     * Send the message to the client by socket.
     * 
     * @param message
     *            message to send.
     */
    @Override
    public void send(String message) {
        socketOut.println(message.replaceAll("\n", "@new_line"));
        socketOut.flush();
    }
}
