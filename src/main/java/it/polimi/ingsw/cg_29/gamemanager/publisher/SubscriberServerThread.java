package it.polimi.ingsw.cg_29.gamemanager.publisher;

import it.polimi.ingsw.cg_29.LoggerClass;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Contains the implementation of the subscriber thread that represent a client
 * connected to the server.
 * 
 * @author Luca
 * @version 1.0
 *
 */
public abstract class SubscriberServerThread extends Thread {

    /**
     * buffer is useful for manage most messages those arrive at the same time.
     */
    private Queue<String> buffer;

    /**
     * 
     */
    public SubscriberServerThread() {
        buffer = new ConcurrentLinkedQueue<String>();
    }

    /**
     * Starts the thread and wait for new message in queue to send.
     */
    @Override
    public void run() {
        while (true) {
            String message = buffer.poll();
            if (message != null) {
                send(message);
            }

            else {
                try {
                    synchronized (buffer) {
                        buffer.wait();
                    }
                } catch (InterruptedException e) {
                    LoggerClass.getLogger().warn(e);
                }
            }
        }
    }

    /**
     * Add a message to queue to send the message to the client of this thread.
     * 
     * @param message
     *            message to send.
     */
    public void dispatchMessage(String message) {
        buffer.add(message);
        synchronized (buffer) {
            buffer.notify();
        }
    }

    /**
     * Send the message to the client of this thread.
     * 
     * @param message
     *            message to send.
     */
    public abstract void send(String message);
}
