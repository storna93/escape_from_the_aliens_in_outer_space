package it.polimi.ingsw.cg_29.gamemanager.model.zone;

/**
 * Implementation of AbstractFactory. This is a ConcreteFactory of Fermi Zone.
 * 
 * @author Luca
 * @version 1.0
 */
public class FermiZoneCreator extends ZoneCreator {

    /**
     * 
     */
    public FermiZoneCreator() {
        super();
    }

    /**
     * @return FermiZone representation, created by parsing a String matrix
     */
    @Override
    public FermiZone createZone() {
        FermiZone zone = new FermiZone();

        String[][] mapString = { { "V", "V", "V", "V", "V", "V", "V", "V", "V", "E", "V", "V", "V", "E", "V", "V", "V", "V", "V", "V", "V", "V", "V" }, { "V", "V", "V", "V", "V", "V", "V", "V", "V", "V", "S", "V", "D", "V", "V", "V", "V", "V", "V", "V", "V", "V", "V" }, { "V", "V", "V", "V", "V", "V", "V", "V", "V", "D", "S", "D", "S", "D", "V", "V", "V", "V", "V", "V", "V", "V", "V" }, { "V", "V", "V", "V", "V", "V", "V", "V", "V", "S", "D", "D", "D", "S", "V", "V", "V", "V", "V", "V", "V", "V", "V" }, { "V", "V", "V", "V", "V", "V", "V", "V", "V", "E", "V", "S", "V", "E", "V", "V", "V", "V", "V", "V", "V", "V", "V" }, { "V", "V", "V", "V", "V", "V", "V", "V", "S", "V", "V", "S", "V", "V", "S", "V", "V", "V", "V", "V", "V", "V", "V" },
                { "V", "V", "V", "V", "V", "V", "V", "V", "D", "S", "V", "S", "V", "D", "S", "V", "V", "V", "V", "V", "V", "V", "V" }, { "V", "V", "V", "V", "V", "V", "V", "V", "V", "S", "S", "S", "S", "S", "V", "V", "V", "V", "V", "V", "V", "V", "V" }, { "V", "V", "V", "V", "V", "V", "V", "V", "D", "V", "V", "A", "V", "V", "S", "V", "V", "V", "V", "V", "V", "V", "V" }, { "V", "V", "V", "V", "V", "V", "V", "S", "S", "S", "V", "H", "V", "S", "D", "S", "V", "V", "V", "V", "V", "V", "V" }, { "V", "V", "V", "V", "V", "V", "V", "S", "V", "S", "S", "S", "S", "S", "V", "D", "V", "V", "V", "V", "V", "V", "V" }, { "V", "V", "V", "V", "V", "V", "V", "V", "D", "S", "V", "S", "V", "D", "S", "V", "V", "V", "V", "V", "V", "V", "V" },
                { "V", "V", "V", "V", "V", "V", "V", "V", "V", "V", "D", "V", "S", "V", "V", "V", "V", "V", "V", "V", "V", "V", "V" }, { "V", "V", "V", "V", "V", "V", "V", "V", "V", "V", "V", "V", "V", "V", "V", "V", "V", "V", "V", "V", "V", "V", "V" } };

        for (int r = 0; r < mapString.length; r++) {
            for (int c = 0; c < mapString[0].length; c++) {
                zone.addSectorByString(r, c, mapString[r][c]);
            }
        }

        return zone;
    }

}
