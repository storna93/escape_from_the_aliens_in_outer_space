package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.PlayerState;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;

/**
 * This Action allows every Player to reconnect to the currentMatch.
 * 
 * @author Fulvio
 * @version 1.0
 *
 */
public class ReconnectAction extends Action {
    /**
     * creates a ReconnectAction assigning that to a clientId who wants to
     * reconnect to the currentMatch, specifying in which RoundState (only
     * DISCONNECTED) that Action could be valid.
     * 
     * @param clientId
     */
    public ReconnectAction(String clientId) {
        super(clientId);
        this.addValidState(RoundState.DISCONNECTED);

    }

    /**
     * This method defines if a certain Player could reconnect to a certain
     * Match or not.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     * @return true (if the Action is a valid Action for a certain Player in a
     *         certain Match) false (otherwise).
     */
    @Override
    public boolean isValid(Match currentMatch) {
        return validConnectionCommand(currentMatch);
    }

    /**
     * This method does a validated ReconnectAction done by a certain Player in
     * a certain Match, reconnecting this Player to the currentMatch.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     */
    @Override
    public void doAction(Match currentMatch) {

        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());

        this.publishInfo(this.getClientId() + " has reconnected to the match.");

        if (currentPlayer.getPlayerState() == PlayerState.DISCONNECTED)
            currentPlayer.setPlayerState(PlayerState.PLAYING);

        currentPlayer.setRoundState(RoundState.STANDBY);
    }
}
