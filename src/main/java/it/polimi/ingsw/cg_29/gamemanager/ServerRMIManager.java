package it.polimi.ingsw.cg_29.gamemanager;

import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.gamemanager.commons.ServerRMIManagerObjectInterface;

import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Contains the implementation of the Thread that accept clients RMI
 * connections.
 * 
 * @author Fulvio
 * @version 1.0
 */
public class ServerRMIManager extends Thread {

    /**
     * server is the reference to the real unique Server.
     */
    private Server server;
    /**
     * NAME is the name of the remote object Server that ServerRMIManager offers
     * on the registry at 7777 port.
     */
    private static final String NAME = "Server";

    /**
     * creates the Manager of all RMI connections to the real unique server.
     * 
     * @param server
     */
    public ServerRMIManager(Server server) {
        this.server = server;

    }

    /**
     * This method creates the remote object serverRMIManager in the registry on
     * the 7777 port.
     * 
     * @throws IOException
     * @throws AlreadyBoundException
     * @throws RemoteException
     */
    private void startRMI() throws AlreadyBoundException, RemoteException {

        Registry registry = LocateRegistry.getRegistry(7777);

        ServerRMIManagerObject serverRMIManager = new ServerRMIManagerObject(this.server);
        ServerRMIManagerObjectInterface stub = (ServerRMIManagerObjectInterface) UnicastRemoteObject.exportObject(serverRMIManager, 0);

        registry.bind(NAME, stub);

    }

    @Override
    /**
     * This method starts the RMI server and manages the respective exceptions.
     */
    public void run() {

        try {
            this.startRMI();
        } catch (AlreadyBoundException e) {
            LoggerClass.getLogger().warn(e);
            server.showOutputServerMessage("Error: Has already been define a remote object serverRMIManager.");
        } catch (RemoteException e) {
            LoggerClass.getLogger().warn(e);
            server.showOutputServerMessage("Not found the remote object Broker that this is going to subcribe on");
        }

    }

}
