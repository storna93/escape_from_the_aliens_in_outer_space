package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Card;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.TeleportItem;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;

/**
 * This Action allows Human Player to move onto HumanSector.
 * 
 * @author Fulvio
 * @version 1.0
 *
 */
public class ActivateTeleportAction extends ActivateItemAction {
    /**
     * creates an activateTeleportAction assigning that to a clientId who has an
     * teleportItem, specifying in which RoundState (only
     * BEGINNING,MOVED_SECURE,ENDING) that Action could be valid.
     * 
     * @param clientId
     * @param teleportItem
     */
    public ActivateTeleportAction(String clientId, Card teleportItem) {
        super(clientId, teleportItem);
        this.addValidState(RoundState.BEGINNING);
        this.addValidState(RoundState.MOVED_SECURE);
        this.addValidState(RoundState.ENDING);

    }

    /**
     * This method defines if a certain Player could activate a TeleportItem or
     * not.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     * @return true (if the Action is a valid Action for a certain Player in a
     *         certain Match) false (otherwise).
     */
    @Override
    public boolean isValid(Match currentMatch) {
        return validPlayerAndValidActivationItemCard(currentMatch);
    }

    /**
     * This method does a validated ActivateTeleportAction done by a certain
     * Player in a certain Match, moving this Player onto HumanSector.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     */
    @Override
    public void doAction(Match currentMatch) {
        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());

        try {
            currentPlayer.setCurrentPosition(currentMatch.getCurrentZone().getHumanSectorCoordinates());
        } catch (NotWellFormedZoneException e) {
            LoggerClass.getLogger().warn(e);
            this.publishInfo("Spown sector isn't avaiable.");
        }

        if (currentPlayer.getRoundState() == RoundState.MOVED_SECURE) {
            currentPlayer.setRoundState(RoundState.ENDING);
        }
        if (currentPlayer.getRoundState() == RoundState.BEGINNING) {
            currentPlayer.setRoundState(RoundState.BEGINNING);
        }
        if (currentPlayer.getRoundState() == RoundState.ENDING) {
            currentPlayer.setRoundState(RoundState.ENDING);
        }

        this.publishInfo(this.getClientId() + " used a Teleport Item Card and go to Human Sector " + currentPlayer.getCurrentPosition().printToString());

        this.discardUsedItemCard(currentMatch, new TeleportItem());
    }

}
