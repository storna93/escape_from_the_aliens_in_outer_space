package it.polimi.ingsw.cg_29.gamemanager.model.player;

/**
 * This Alien Strategy allows to manage the rangeOfMove of an Alien Player.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class FastAlien implements AlienStrategy {
    /**
     * Creates a Strategy FastAlien.
     */
    public FastAlien() {
        super();
    }

    /**
     * addBehaviour allows to add the FastAlien Strategy to a certain Alien
     * setting three the rangeOfMove attribute (now this Alien could cross three
     * Sectors during a MoveAction).
     * 
     * @param character
     *            that is the Alien on which will be activated the FastAlien
     *            Strategy.
     */
    @Override
    public void addBehaviour(Character character) {
        character.setRangeOfMove(3);
        character.setCanAttack(true);
    }

    /**
     * removeBehaviour allows to remove the FastAlien Strategy to a certain
     * Alien setting two the rangeOfMove attribute (now this Alien could not
     * cross three Sectors during a MoveAction but only two).
     * 
     * @param character
     *            that is the Alien on which will be deactivated the FastAlien
     *            Strategy.
     */
    @Override
    public void removeBehaviour(Character character) {
        character.setRangeOfMove(2);
        character.setCanAttack(true);
    }
}
