package it.polimi.ingsw.cg_29.gamemanager.model.zone;

/**
 * This class represent Galvani Zone.
 * 
 * @author Luca
 * @version 1.0
 */
public class GalvaniZone extends Zone {

    /**
     *
     */
    protected GalvaniZone() {
        super();
    }

}