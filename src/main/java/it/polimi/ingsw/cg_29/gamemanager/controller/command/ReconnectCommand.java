package it.polimi.ingsw.cg_29.gamemanager.controller.command;

import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.Action;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.ReconnectAction;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.view.View;

/**
 * A command to reconnect to the match.
 *
 * @author Luca
 * @version 1.0
 *
 */
public class ReconnectCommand extends Command {

    /**
     * Create a new command
     * 
     * @param controller
     *            the controller that manages current match
     * @param currentClient
     *            the id of client who sent command
     */
    public ReconnectCommand(Controller controller, View currentClient) {
        super(controller, currentClient);
    }

    /**
     * Launch this command.
     */
    @Override
    public void execute() {

        if (this.matchIsRunning()) {

            Match match = this.getCurrentMatch();
            Action reconnect = new ReconnectAction(this.getCurrentClientId());

            if (reconnect.isValid(match)) {
                this.getCurrentClient().send("You are reconnected.");
                this.addActionToObserver(reconnect);
                reconnect.doAction(match);
            } else
                this.getCurrentClient().send("You cannot reconnect.");

        } else {
            this.getCurrentClient().send("The match is not running.");
        }

    }

}
