package it.polimi.ingsw.cg_29.gamemanager;

import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;
import it.polimi.ingsw.cg_29.gamemanager.publisher.Broker;
import it.polimi.ingsw.cg_29.gamemanager.view.View;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

/**
 * Contains the implementation of the Server class. This server is able to
 * manage multiple clients.
 *
 * @author Luca
 * @version 1.0
 */
public class Server {

    /**
     * Number of minimum players to start current match.
     */
    public static final int MIN_PLAYERS = 2;

    /**
     * Number of maximum players to start current match.
     */
    public static final int MAX_PLAYERS = 8;

    /**
     * A map that contains the different controllers that manage the different
     * matches.
     */
    private Map<String, Controller> controllerList;

    /**
     * Counter to create unique ids for controllers
     */
    private static int controller_counter = 0;

    /**
     * Id of the controller where the match is not started yet.
     */
    private String currentControllerId;

    /**
     * Reference of TimerToStartMatch that instantiates a new timer to start a
     * match after reached minimum number of players.
     */
    private TimerToStartMatch currentTimer = null;

    /**
     * Reference of broker that dispatches message to client of different
     * controllers.
     */
    private Broker broker;

    /**
     * Creates new server instantiates new controller.
     * 
     * @throws RemoteException
     */
    public Server() throws RemoteException {
        this.controllerList = new HashMap<String, Controller>();
        this.broker = new Broker(this);
        this.broker.start();
        this.setNewController();
    }

    /**
     * @return the broker
     */
    public Broker getBroker() {
        return broker;
    }

    /**
     * @return the controller with currentControllerId
     */
    public Controller getCurrentController() {
        return this.controllerList.get(this.currentControllerId);
    }

    /**
     * 
     * @param controllerId
     *            id of a specific controller
     * @return the controller with specific id
     */
    public Controller getControllerById(String controllerId) {
        return this.controllerList.get(controllerId);
    }

    /**
     * Initialize a new controller and add it to controllerList.
     */
    public void setNewController() {
        controller_counter++;
        currentControllerId = "controller_" + controller_counter;
        this.controllerList.put(currentControllerId, new Controller(currentControllerId, this.broker));
    }

    /**
     * Add new client to current controller and set his view observed by current
     * controller. If current controller reached max number of players set new
     * controller.
     * 
     * @param newClientView
     *            the view of a connected client
     * @return the controller id
     */
    public synchronized String addClientToController(View newClientView) {
        if (this.getCurrentController().getNumberOfPlayer() == MAX_PLAYERS)
            this.setNewController();

        Controller controller = this.getCurrentController();
        newClientView.addObserver(controller);
        controller.addClient(newClientView);

        return controller.getControllerId();
    }

    /**
     * Verify the number of players in the specific controller to start match.
     * 
     * @param controllerId
     *            id of specific controller
     */
    public synchronized void verifiesConditionsToStartMatch(String controllerId) {
        Controller controller = this.getControllerById(controllerId);
        
        if(controller.getMatch() != null)
            return;
        
        int numberOfPlayers = controller.getNumberOfPlayer();
        if (numberOfPlayers == 1) {
            controller.sendToAll("Now you're Match Administrator: you can set the Zone before starting match.");
        }
        if (numberOfPlayers < MIN_PLAYERS) {
            controller.sendToAll("Waiting for players to start Match ...");
        }
        if (numberOfPlayers == MIN_PLAYERS) {
            this.currentTimer = new TimerToStartMatch(this);
            this.currentTimer.start();
        }
        if (MIN_PLAYERS < numberOfPlayers && numberOfPlayers < MAX_PLAYERS) {
            controller.sendToAll(numberOfPlayers + " players connected: match starts soon.");
        }
        if (numberOfPlayers == MAX_PLAYERS) {
            if (currentTimer != null)
                this.currentTimer.interrupt();
            controller.sendToAll("Reached max number of player.");
            controller.startMatch();
        }
    }

    /**
     * Show output message of the server.
     * 
     * @param message
     */
    public void showOutputServerMessage(String message) {
        System.out.println(message);
        LoggerClass.getLogger().info(message);
    }

    /**
     * Runs the Server
     *
     * @param args
     * @throws AlreadyBoundException
     * @throws RemoteException
     */
    public static void main(String[] args) throws AlreadyBoundException, RemoteException {

        Server server = new Server();
        ServerSocketManager serverSocketManager = new ServerSocketManager(server);
        serverSocketManager.start();
        ServerRMIManager serverRmiManager = new ServerRMIManager(server);
        serverRmiManager.start();

    }
}
