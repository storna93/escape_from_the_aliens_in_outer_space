package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * It is the super type to manage several Card type: ItemCard, CharacterCard,
 * EscapeHatchCard and DangerousSectorCard.
 * 
 * @author Fulvio
 * @version 1.0
 */

public abstract class Card {
    /**
     * Creates a generic Card.
     */
    protected Card() {
    }

}
