package it.polimi.ingsw.cg_29.gamemanager.controller.command;

import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.Action;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.FakeNoiseCallAction;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.view.View;

/**
 * A command to say to other player a fake noise.
 *
 * @author Luca
 * @version 1.0
 *
 */
public class FakeNoiseCommand extends Command {

    /**
     * Sector in which to do fake noise.
     */
    private String sector;

    /**
     * Create a new command
     * 
     * @param controller
     *            the controller that manages current match
     * @param currentClient
     *            the id of client who sent command
     */
    public FakeNoiseCommand(Controller controller, View currentClient, String sector) {
        super(controller, currentClient);
        this.sector = sector;
    }

    /**
     * Launch this command.
     */
    @Override
    public void execute() {

        if (this.matchIsRunning()) {

            Match match = this.getCurrentMatch();
            Action fakeNoise = new FakeNoiseCallAction(this.getCurrentClientId(), new Coordinates(this.sector.substring(0, 1), this.sector.substring(1)));

            if (fakeNoise.isValid(match)) {
                this.getCurrentClient().send("Fake noise done.");
                this.addActionToObserver(fakeNoise);
                fakeNoise.doAction(match);
            } else
                this.getCurrentClient().send("You cannot fake noise or sector doesn't exist.");
        } else
            this.getCurrentClient().send("The match is not running.");

    }

}
