package it.polimi.ingsw.cg_29.gamemanager.model.player;

/**
 * This Strategy interface allows to manage two types of Strategy for a certain
 * Character: HumanStrategy or AlienStrategy. Strategy even allows to implement
 * a Strategy Pattern on class Character with the goal to add or remove
 * Character behavior.
 * 
 * @author Fulvio
 * @version 1.0
 */

public interface Strategy {

    /**
     * addBehaviour allows to add a Strategy to a certain Character setting the
     * respective Character attributes.
     * 
     * @param character
     *            that is the Character on which will be charged the Strategy.
     */
    public abstract void addBehaviour(Character character);

    /**
     * removeBehaviour allows to remove a Strategy to a certain Character.
     * resetting the respective Character attributes.
     * 
     * @param character
     *            that is the Character on which will be removed the Strategy.
     */
    public abstract void removeBehaviour(Character character);

}
