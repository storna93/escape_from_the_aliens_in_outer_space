package it.polimi.ingsw.cg_29.gamemanager.model.player;

/**
 * This class defines some properties (some abilities) for the Player in order
 * to the Character Card that the Player has. Character is the super type of
 * Human and Alien.
 * 
 * @author Fulvio
 * @version 1.0
 */

public abstract class Character {

    /**
     * rangeOfMove defines the maximum number of Sector that the Player could
     * cross in a single Round doing a Move Action.
     */
    private int rangeOfMove;

    /**
     * canAttack defines if the Player could or not Attack after have been
     * moved.
     */
    private boolean canAttack;

    /**
     * beSedate defines if the Player could or not avoid to Draw a
     * DangerousSectorCard if him has been moved onto a DangerousSector.
     */
    private boolean beSedate;

    /**
     * canDefense defines if the Player could or not resist the other Player
     * Attack.
     */
    private boolean canDefense;

    /**
     * behavior defines the Strategy to manage.
     */
    private Strategy behaviour;

    /**
     * creates a Character that has all the minimum abilities.
     */
    public Character(Strategy behaviour) {

        this.rangeOfMove = 1;
        this.canAttack = false;
        this.beSedate = false;
        this.canDefense = false;

        this.setBehaviour(behaviour);
        behaviour.addBehaviour(this);
    }

    /**
     * 
     * @return the last behavior activated.
     */
    public Strategy getBehaviour() {
        return behaviour;
    }

    /**
     * setBehaviour allows to set behavior at the value of the last Strategy
     * that had been added on the Character of a certain Player.
     * 
     * @param behaviour
     *            that is a Strategy
     */
    public void setBehaviour(Strategy behaviour) {
        this.behaviour = behaviour;
    }

    /**
     * @return the rangeOfMove
     */
    public int getRangeOfMove() {
        return rangeOfMove;
    }

    /**
     * @param rangeOfMove
     *            the rangeOfMove to set.
     */
    public void setRangeOfMove(int rangeOfMove) {
        this.rangeOfMove = rangeOfMove;
    }

    /**
     * @return the canAttack
     */
    public Boolean getCanAttack() {
        return canAttack;
    }

    /**
     * @param canAttack
     *            the canAttack to set.
     */
    public void setCanAttack(Boolean canAttack) {
        this.canAttack = canAttack;
    }

    /**
     * @return the beSedate
     */
    public Boolean getBeSedate() {
        return beSedate;
    }

    /**
     * @param beSedate
     *            the beSedate to set.
     */
    public void setBeSedate(Boolean beSedate) {
        this.beSedate = beSedate;
    }

    /**
     * @return the canDefense
     */
    public Boolean getCanDefense() {
        return canDefense;
    }

    /**
     * @param canDefense
     *            the canDefense to set.
     */
    public void setCanDefense(Boolean canDefense) {
        this.canDefense = canDefense;
    }

    /**
     * This method allows to activate a Strategy on a certain Character setting
     * the Character attributes in order to the Character Strategy (behavior)
     * activated.
     * 
     * @param behavior
     *            that is the Strategy that will be activated on a certain
     *            Character.
     */
    public void activateStrategy(Strategy behaviour) {
        this.setBehaviour(behaviour);
        behaviour.addBehaviour(this);
    }

    /**
     * This method allows to deactivate a Strategy on a certain Character
     * resetting the Character attributes in order to the Character Strategy
     * behavior deactivated.
     * 
     * @param behavior
     *            that is the Strategy that will be deactivated on a certain
     *            Character.
     */
    public void deactivateStrategy(Strategy behaviour) {
        this.setBehaviour(behaviour);
        behaviour.removeBehaviour(this);
    }

}
