package it.polimi.ingsw.cg_29.gamemanager.model;

import it.polimi.ingsw.cg_29.gamemanager.model.deck.Card;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.CharacterDeck;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.CharacterDeckCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.DangerousSectorDeck;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.DangerousSectorDeckCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Deck;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.EscapeHatchDeck;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.EscapeHatchDeckCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.ItemDeck;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.ItemDeckCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Alien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.PlayerState;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Zone;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of Match. It represents the total current state of the game.
 * 
 * @author Luca
 * @version 1.0
 */
public class Match {

    /**
     * Identifies number of the round in every state of the match.
     */
    private int currentRound;

    /**
     * Reference to Zone of this Match.
     */
    private final Zone currentZone;

    /**
     * Reference to current state of this match.
     */
    private MatchState matchState;

    /**
     * Reference to character deck in every state of the match.
     */
    private CharacterDeck characterDeck;

    /**
     * Reference to dangerous deck in every state of the match.
     */
    private DangerousSectorDeck dangerousSectorDeck;

    /**
     * Reference to item deck in every state of the match.
     */
    private ItemDeck itemDeck;

    /**
     * Reference to escape hatch deck in every state of the match.
     */
    private EscapeHatchDeck escapeHatchDeck;

    /**
     * Reference to players of this match.
     */
    private final List<Player> playersList;

    /**
     * The maximum number of rounds playable.
     */
    private static final int END_ROUND = 40;

    /**
     * Reference to last human playing in the zone of this match.
     */
    private String lastHumanId = null;

    /**
     * Implementation of Match. Create and initialize an instance of Match.
     * 
     * @param numberOfPlayer
     * @throws NotWellFormedZoneException
     */
    public Match(List<String> clientIdList, ZoneCreator currentZoneCreator) throws NotWellFormedZoneException {

        this.matchState = MatchState.RUNNING;

        this.currentRound = 1;

        this.currentZone = currentZoneCreator.createZone();

        this.characterDeck = new CharacterDeckCreator().createDeck(clientIdList.size());
        this.dangerousSectorDeck = new DangerousSectorDeckCreator().createDeck();
        this.itemDeck = new ItemDeckCreator().createDeck();
        this.escapeHatchDeck = new EscapeHatchDeckCreator().createDeck();

        this.playersList = new ArrayList<Player>();
        for (String currentId : clientIdList) {
            this.playersList.add(new Player(currentId, this.characterDeck.drawCard(), this.currentZone));
        }
        this.playersList.get(0).setRoundState(RoundState.BEGINNING);
    }

    /**
     * @return currentRound
     */
    public int getCurrentRound() {
        return currentRound;
    }

    /**
     * Change round to next.
     */
    public void nextRound() {
        this.currentRound++;
    }

    /**
     * @return zone of this Match
     */
    public Zone getCurrentZone() {
        return currentZone;
    }

    /**
     * @return state of this Match
     */
    public MatchState getMatchState() {
        return matchState;
    }

    /**
     * @param matchState
     *            state of this Match to set
     */
    public void setMatchState(MatchState matchState) {
        this.matchState = matchState;
    }

    /**
     * @return dangerousSectorDeck of this Match
     */
    public DangerousSectorDeck getDangerousSectorDeck() {
        return dangerousSectorDeck;
    }

    /**
     * @return itemDeck of this Match
     */
    public ItemDeck getItemDeck() {
        return itemDeck;
    }

    /**
     * @return escapeHatchDeck of this Match
     */
    public EscapeHatchDeck getEscapeHatchDeck() {
        return escapeHatchDeck;
    }

    /**
     * 
     * @param clientId
     *            the id of specific client
     * @return the player instance with the specific clientId
     */
    public Player getSpecificPlayerById(String clientId) {
        for (Player currentPlayer : this.playersList) {
            if (clientId.equals(currentPlayer.getClientId()))
                return currentPlayer;
        }

        return null;
    }

    /**
     * Implementation of toString. Intended only for debugging.
     */
    @Override
    public String toString() {
        return "Match [\n currentRound=" + currentRound + "\n currentZone=\n\n" + currentZone + "\n\n matchState=" + matchState + "\n characterDeck=" + characterDeck + "\n dangerousSectorDeck=" + dangerousSectorDeck + "\n itemDeck=" + itemDeck + "\n escapeHatchDeck=" + escapeHatchDeck + "\n playersList=" + playersList + "]";
    }

    public List<Player> getPlayerList() {
        return playersList;
    }

    /**
     * 
     * @param clientId
     *            the id of specific client
     * @return a "pretty print" string represents current match state for a
     *         specific player.
     * @throws IllegalArgumentException
     */
    public String printState(String clientId) {
        List<Deck> deckOfMatch = new ArrayList<Deck>();
        deckOfMatch.add(this.dangerousSectorDeck);
        deckOfMatch.add(this.itemDeck);
        deckOfMatch.add(this.escapeHatchDeck);

        Player currentPlayer = getSpecificPlayerById(clientId);
        String currentState = "========================================================";

        currentState += "\n\nCLIENT_ID: " + currentPlayer.getClientId();
        currentState += "\n\nROUND: " + this.currentRound;
        currentState += "\n\nDECKS:";
        for (Deck deck : deckOfMatch) {
            currentState += "\n" + deck.getClass().getSimpleName() + ": (cards = " + deck.getCardList().size() + "; discard cards = " + deck.getDiscardPile().size() + ")";
        }

        currentState += "\n\nPLAYER: ";
        currentState += "[state: " + currentPlayer.getPlayerState().toString() + " (roundState: " + currentPlayer.getRoundState().toString() + ")]";
        currentState += "\ncharacter: " + currentPlayer.getCharacter().getClass().getSimpleName();
        currentState += "\nrange of move: " + currentPlayer.getCharacter().getRangeOfMove();
        currentState += "\ncurrentPosition: " + currentPlayer.getCurrentPosition().printToString();
        currentState += "\nhand: [";
        int counter = 0;
        for (Card currentCard : currentPlayer.getItemCardList()) {
            if (counter > 0)
                currentState += ", ";
            currentState += "(" + (currentPlayer.getItemCardList().indexOf(currentCard) + 1) + ") " + currentCard.getClass().getSimpleName();
            counter++;
        }
        currentState += "]\n\n";
        currentState += "========================================================\n";

        return currentState;
    }

    /**
     * Change Player round state and adjourns round counter.
     * 
     * @param currentPlayer
     *            the reference to current playing player.
     */
    public void nextPlayerRound(Player currentPlayer) {
        boolean playersPlaying = false;

        for (Player p : this.playersList)
            if (p.getPlayerState() == PlayerState.PLAYING)
                playersPlaying = true;

        if (!playersPlaying)
            return;

        int index = this.playersList.indexOf(currentPlayer) + 1;
        Player nextPlayer;
        if (index == this.playersList.size()) {
            nextPlayer = this.playersList.get(0);
            this.nextRound();
        } else {
            nextPlayer = this.playersList.get(index);
        }

        if (nextPlayer.getPlayerState() == PlayerState.PLAYING)
            nextPlayer.setRoundState(RoundState.BEGINNING);
        else
            this.nextPlayerRound(nextPlayer);
    }

    /**
     * If only one human is playing add his id to lastHumanId
     * 
     * @return true if there is at least one human playing
     */
    private boolean verifyLastHumanPlaying() {

        int humanPlayingCounter = 0;
        String playingHumanId = "";
        for (Player p : this.playersList) {
            if (p.getCharacter() instanceof Human && p.getPlayerState() == PlayerState.PLAYING) {
                humanPlayingCounter++;
                playingHumanId = p.getClientId();
            }
        }

        if (humanPlayingCounter == 1) {
            this.lastHumanId = playingHumanId;
        }

        if (humanPlayingCounter == 0) {
            return false;
        }
        return true;
    }

    /**
     * Set the state of match and players in a condition of human win.
     */
    private void humansWin() {
        for (Player p : this.playersList) {
            if (p.getRoundState() != RoundState.DISCONNECTED)
                p.setRoundState(RoundState.STANDBY);
            if ((p.getCharacter() instanceof Alien && p.getPlayerState() == PlayerState.PLAYING) || p.getPlayerState() == PlayerState.DISCONNECTED) {
                p.setPlayerState(PlayerState.LOSER);
            }
        }
        this.matchState = MatchState.FINISHED;
    }

    /**
     * Set the state of match and players in a condition of alien win.
     */
    private void aliensWin() {
        for (Player p : this.playersList) {
            if (p.getRoundState() != RoundState.DISCONNECTED)
                p.setRoundState(RoundState.STANDBY);
            if ((p.getCharacter() instanceof Human && p.getPlayerState() == PlayerState.PLAYING) || p.getPlayerState() == PlayerState.DISCONNECTED) {
                p.setPlayerState(PlayerState.LOSER);
            }
            if (p.getCharacter() instanceof Alien && p.getPlayerState() == PlayerState.PLAYING) {
                p.setPlayerState(PlayerState.WINNER);
            }
        }
        this.matchState = MatchState.FINISHED;
    }

    /**
     * Verifies the conditions of ending match. Set the state of match and
     * players in a final state.
     * 
     * @return true if the match is over.
     */
    public boolean gameOverConditions() {

        if (this.currentRound >= END_ROUND) {
            this.aliensWin();
            return true;
        }

        return lastHumanGameOver();
    }

    /**
     * @return true if the match is over because of the last human.
     */
    private boolean lastHumanGameOver() {
        boolean humanPlaying = this.verifyLastHumanPlaying();

        if (this.lastHumanId == null) {

            if (!humanPlaying) {
                this.aliensWin();
                return true;
            }

        } else {

            Player lastHumanPlayer = this.getSpecificPlayerById(this.lastHumanId);

            if (lastHumanPlayer.getPlayerState() == PlayerState.WINNER) {
                this.humansWin();
                return true;
            }

            if (lastHumanPlayer.getPlayerState() == PlayerState.LOSER || lastHumanPlayer.getPlayerState() == PlayerState.DISCONNECTED) {
                this.aliensWin();
                return true;
            }
        }

        return false;
    }

}
