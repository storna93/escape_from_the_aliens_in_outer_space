package it.polimi.ingsw.cg_29.gamemanager.model.player;

/**
 * Human class allows to activate or deactivate an Human Strategy on a certain
 * Human
 * 
 * @author Fulvio
 * @version 1.0
 */

public class Human extends Character {

    /**
     * creates an Human Character.
     *
     * @param behaviour
     *            that is the first Human Strategy activated on a certain Human
     */
    public Human(HumanStrategy behaviour) {
        super(behaviour);
    }

}
