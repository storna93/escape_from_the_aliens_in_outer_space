package it.polimi.ingsw.cg_29.gamemanager.view;

import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.gamemanager.Server;
import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * Contains the implementation of the view that represents a client connected by
 * socket to the server.
 * 
 * @author Luca
 * @version 1.0
 *
 */
public class ServerSocketView extends View implements Runnable {

    /**
     * The socket of the client.
     */
    private Socket socket;

    /**
     * Input stream of the socket.
     */
    private Scanner socketIn;

    /**
     * Output stream of the socket.
     */
    private PrintWriter socketOut;

    /**
     * Reference to the server.
     */
    private Server server;

    /**
     * Creates a new ServerSocketView with specific socket
     *
     * @param socket
     *            is the socket of client.
     *
     * @param server
     *            reference to the server.
     * @throws IOException
     */
    public ServerSocketView(Socket socket, Server server) throws IOException {

        this.socket = socket;
        socketIn = new Scanner(socket.getInputStream());
        socketOut = new PrintWriter(socket.getOutputStream());
        this.server = server;
    }

    /**
     * @return the socket
     */
    public Socket getSocket() {
        return socket;
    }

    /**
     * Executes the view
     */
    @Override
    public void run() {

        String command = socketIn.nextLine();

        if (command.matches("first_connection")) {
            String controllerId = this.server.addClientToController(this);
            Controller controller = this.server.getControllerById(controllerId);

            socketOut.println("set_controller_id(" + controllerId + ")");
            socketOut.println("set_client_id(" + this.getClientId() + ")");
            socketOut.flush();
            this.addObserver(controller);

        } else if (command.matches(".*#.*#.*")) {
            String controllerId = command.substring(0, command.indexOf("#"));
            command = command.substring(command.indexOf("#") + 1);
            String clientId = command.substring(0, command.indexOf("#"));
            command = command.substring(command.indexOf("#") + 1);
            this.setClientId(clientId);
            this.addObserver(this.server.getControllerById(controllerId));
            this.setChanged();
            this.notifyObservers(command);
            socketIn.close();
        }

        return;
    }

    /**
     * Sent a command to client to close socket and close the streams.
     */
    public void closeConnection() {
        socketOut.println("close_socket");
        socketOut.flush();
        socketOut.close();
        try {
            socket.close();
        } catch (IOException e) {
            LoggerClass.getLogger().warn(e);
            server.showOutputServerMessage("The Socket doesn't close.");
        }
        return;
    }

    /**
     * Send message to client by socket.
     */
    @Override
    public void send(String message) {
        String messageToSend = message.replaceAll("\n", "@new_line");
        socketOut.println(messageToSend);
        socketOut.flush();
        return;
    }
}
