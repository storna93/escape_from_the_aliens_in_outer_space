package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * When drawn, this Card: - ensures that all Players know the current position
 * of the Player that drawn this Card. - bind to draw an ItemCard for the Player
 * that drawn this Card.
 * 
 * 
 * @author Fulvio
 * @version 1.0
 */

public class NoiseInYourSectorIcon extends NoiseInYourSector {
    /**
     * Creates a DanegerousSectorCard NoiseInYourSectorIcon.
     */
    protected NoiseInYourSectorIcon() {
        super();
    }

}
