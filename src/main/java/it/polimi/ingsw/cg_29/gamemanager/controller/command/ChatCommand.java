package it.polimi.ingsw.cg_29.gamemanager.controller.command;

import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;
import it.polimi.ingsw.cg_29.gamemanager.view.View;

/**
 * A command to send message to other player.
 *
 * @author Luca
 * @version 1.0
 *
 */
public class ChatCommand extends Command {

    /**
     * Message to send to all clients
     */
    private String chatMessage;

    /**
     * Create a new command
     * 
     * @param controller
     *            the controller that manages current match
     * @param currentClient
     *            the id of client who sent command
     */
    public ChatCommand(Controller controller, View currentClient, String chatMessage) {
        super(controller, currentClient);
        this.chatMessage = chatMessage;
    }

    /**
     * Launch this command.
     */
    @Override
    public void execute() {
        this.getController().sendToAll("[CHAT] " + this.getCurrentClientId() + ": \"" + this.chatMessage + "\"");
    }

}
