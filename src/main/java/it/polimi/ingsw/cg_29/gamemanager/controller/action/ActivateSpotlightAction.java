package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Card;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.SpotlightItem;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.PlayerState;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Sector;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.VoidSector;

import java.util.List;
import java.util.Set;

/**
 * This Action allows Human Player to see what Player are on some Sectors.
 * 
 * @author Fulvio
 * @version 1.0
 *
 */
public class ActivateSpotlightAction extends ActivateItemAction {

    /**
     * litCoordinates is the Coordinates of the middle Sector chosen by the
     * Player on this Player wants to see which Players are present.
     * 
     */
    private Coordinates litCoordinates;
    /**
     * spotlightResultForTesting specifies the result of a
     * ActivateSpotlightAction done and it is used only for testing.
     */
    private String spotlightResultForTesting = "";

    /**
     * creates an activateSpotlightAction assigning that to a clientId who has
     * an spotlightItem and has choice the litSector on he wants to see,
     * specifying in which RoundState (only
     * BEGINNING,MOVED_DANGEROUS,MOVED_SECURE,ENDING) that Action could be
     * valid.
     * 
     * @param clientId
     * @param spotlightItem
     */
    public ActivateSpotlightAction(String clientId, Coordinates litCoordinates, Card spotlightItem) {
        super(clientId, spotlightItem);
        this.litCoordinates = litCoordinates;

        this.addValidState(RoundState.BEGINNING);
        this.addValidState(RoundState.MOVED_DANGEROUS);
        this.addValidState(RoundState.MOVED_SECURE);
        this.addValidState(RoundState.ENDING);
    }

    /**
     * @return the spotlightResultForTesting
     */
    public String getSpotlightResultForTesting() {
        return spotlightResultForTesting;
    }

    /**
     * This method defines if a certain Player could activate a SpotlightItem or
     * not.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     * @return true (if the Action is a valid Action for a certain Player in a
     *         certain Match) false (otherwise).
     */
    @Override
    public boolean isValid(Match currentMatch) {
        if (!validPlayerAndValidActivationItemCard(currentMatch))
            return false;

        Sector litSector = currentMatch.getCurrentZone().getSectorByCoordinates(litCoordinates);
        if (litSector == null)
            return false;
        if (litSector instanceof VoidSector)
            return false;
        if (!currentMatch.getCurrentZone().sectorExist(litCoordinates))
            return false;
        return true;

    }

    /**
     * This method does a validated ActivateSpotlightAction done by a certain
     * Player in a certain Match, displaying if some Players are on the
     * litCoordinates Sector or on the adjacent Sectors and specifying that
     * thing.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     */
    @Override
    public void doAction(Match currentMatch) {

        List<Player> playerList = currentMatch.getPlayerList();
        Sector litSector = currentMatch.getCurrentZone().getSectorByCoordinates(this.litCoordinates);
        Set<Coordinates> nearSectorSet = litSector.getNearSectors(1);
        nearSectorSet.add(this.litCoordinates);
        String spotlightResult = "";

        for (Coordinates coord : nearSectorSet) {
            for (Player player : playerList) {
                Coordinates currentPositionPlayer = player.getCurrentPosition();

                if (currentPositionPlayer.equals(coord) && !player.getPlayerState().equals(PlayerState.LOSER)) {
                    spotlightResult += player.getClientId() + " has been revealed in " + coord.printToString() + "; ";
                }
            }
        }
        this.spotlightResultForTesting += spotlightResult;
        this.publishInfo(this.getClientId() + " used a Spotlight Item Card in " + this.litCoordinates.printToString() + ".");

        if ("".equals(spotlightResult))
            this.publishInfo("No player has been revealed.");
        else
            this.publishInfo(spotlightResult);

        this.discardUsedItemCard(currentMatch, new SpotlightItem());
    }
}
