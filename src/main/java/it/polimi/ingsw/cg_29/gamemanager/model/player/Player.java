package it.polimi.ingsw.cg_29.gamemanager.model.player;

import it.polimi.ingsw.cg_29.gamemanager.model.deck.AlienCard;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Card;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Zone;

import java.util.ArrayList;
import java.util.List;

/**
 * This class defines the attributes of the Player that is playing a match.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class Player {

    /**
     * character allows to join a Character to a certain Player.
     */
    private Character character;

    /**
     * characterCard allows to defines the Character for a certain Player.
     */
    private Card characterCard;

    /**
     * itemCardList is the list of the ItemCards owned by a Player in a certain
     * moment during the match.
     */
    private List<Card> itemCardList;

    /**
     * movementRecord is the list of sectors crossed by a Player during the
     * match.
     */
    private MovementRecord movementRecord;

    /**
     * roundState is the current Round state of a Player during the match (that
     * says if a Player could or not do any Action in a certain moment in the
     * match).
     */
    private RoundState roundState;

    /**
     * playerState is the current state of a Player during the match (that says
     * if a Player is playing or is winner or is loser).
     */
    private PlayerState playerState;

    /**
     * clientId is the String identifier of a Player during the match.
     */
    private final String clientId;

    /**
     * name is the String name join to Player during the match (that is a sort
     * of nickname).
     */
    private String name = "";

    /**
     * Constructs a Player that: - has an empty list of itemCard; - is PLAYING
     * and waiting for starts his Round (STANDBY); - will play onto the
     * specified Zone with the specified clientId.
     * 
     * In order to charcaterCard defines: - the Player Character; - the first
     * position (the starting Sector) in the MovementRecord.
     * 
     * @param clientId
     * @param characterCard
     * @param zone
     * @throws NotWellFormedZoneException
     */
    public Player(String clientId, Card characterCard, Zone zone) throws NotWellFormedZoneException {

        this.clientId = clientId;

        this.characterCard = characterCard;

        if (characterCard instanceof AlienCard) {
            this.character = new Alien(new NormalAlien());
            this.movementRecord = new MovementRecord(zone.getAlienSectorCoordinates());
        } else {
            this.character = new Human(new NormalHuman());
            this.movementRecord = new MovementRecord(zone.getHumanSectorCoordinates());
        }

        this.itemCardList = new ArrayList<Card>();

        this.roundState = RoundState.STANDBY;
        this.playerState = PlayerState.PLAYING;

    }

    // getter

    /**
     * 
     * @return the clientId(), which refers to a certain player
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * @return the movementRecord, which refers to a certain player
     */
    public MovementRecord getMovementRecord() {
        return movementRecord;
    }

    /**
     * @return the character, which refers to a certain player
     */
    public Character getCharacter() {
        return character;
    }

    /**
     * @return the characterCard, which refers to a certain player
     */
    public Card getCharacterCard() {
        return characterCard;
    }

    /**
     * @return the itemCardList, which refers to a certain player
     */
    public List<Card> getItemCardList() {
        return itemCardList;
    }

    /**
     * @return the roundState, which refers to a certain player
     */
    public RoundState getRoundState() {
        return roundState;
    }

    /**
     * @return the playerState, which refers to a certain player
     */
    public PlayerState getPlayerState() {
        return playerState;
    }

    // setter

    /**
     * @param roundState
     *            the roundState to set
     */
    public void setRoundState(RoundState roundState) {
        this.roundState = roundState;
    }

    /**
     * @param playerState
     *            the playerState to set
     */
    public void setPlayerState(PlayerState playerState) {
        this.playerState = playerState;
    }

    /**
     * @return the name, which refers to a certain player
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    // ItemCard Management

    /**
     * This method allows to add a new ItemCard to the List of ItemCards of a
     * certain Player.
     * 
     * @param card
     *            that is the ItemCard that has just drawn.
     */
    public void addItemCard(Card card) {
        this.itemCardList.add(card);
    }

    /**
     * This method allows to know if a specified ItemCard (if it is there) is
     * present or not in itemCardList, and then (if it is there) to remove it.
     * 
     * @param cardType
     *            that is the type of ItemCard required.
     * @return true (if the Player has a Card of the type cardType) false
     *         (otherwise).
     */
    public boolean hasSpecificItemCard(Card cardType) {

        if (this.itemCardList.isEmpty())
            return false;

        for (int i = 0; i < this.itemCardList.size(); i++) {
            if (this.itemCardList.get(i).getClass() == cardType.getClass()) {
                this.itemCardList.remove(i);
                return true;
            }
        }
        return false;
    }

    // position Management

    /**
     * This method allows to set the new current position of a certain Player in
     * the respective MovementRecord.
     * 
     * @param coord
     *            that is the new Coordinates to set as currentPosition
     */
    public void setCurrentPosition(Coordinates coord) {
        this.movementRecord.setNewCurrentCoordinates(coord);
    }

    /**
     * 
     * @return the currentPosition of a certain Player.
     */
    public Coordinates getCurrentPosition() {
        return this.movementRecord.getCurrentCoordinates();
    }

    /**
     * This method doesn't show anything.
     * 
     * @return null
     */
    public String printToString() {

        return null;
    }

}
