package it.polimi.ingsw.cg_29.gamemanager.commons;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Defines the interface of the Broker remote object. Every clients that wants
 * to subscribe to this Broker for receiving messages from it, could call the
 * method subscribe() offered by this.
 * 
 * @author Fulvio
 * @version 1.0
 */
public interface BrokerObjectInterface extends Remote {

    /**
     * This method could be remotely call for subscribing do the Broker.
     * 
     * @param client
     *            , that is the interface of a certain Client that uses RMI
     *            communication and that wants to subscribe to this.
     * @param controllerId
     *            name of topic to subscribe
     * @throws RemoteException
     */
    public void subscribe(ClientRMIObjectInterface client, String controllerId) throws RemoteException;
}