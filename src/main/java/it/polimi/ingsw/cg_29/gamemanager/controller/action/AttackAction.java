package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;

/**
 * This Action allows Alien Player to attack other Player that are in the same
 * currentSector.
 * 
 * @author Fulvio
 * @version 1.0
 *
 */
public class AttackAction extends Action {
    /**
     * creates an attackAction assigning that to a clientId who wants to attack,
     * specifying in which RoundState (only MOVED_SECURE, MOVED_DANGEROUS) that
     * Action could be valid.
     * 
     * @param clientId
     */
    public AttackAction(String clientId) {
        super(clientId);
        this.addValidState(RoundState.MOVED_DANGEROUS);
        this.addValidState(RoundState.MOVED_SECURE);

    }

    /**
     * This method defines if a certain Player could do an Attack Action or not.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     * @return true (if the Action is a valid Action for a certain Player in a
     *         certain Match) false (otherwise).
     */
    @Override
    public boolean isValid(Match currentMatch) {
        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());
        if (!validPlayer(currentPlayer))
            return false;

        if (currentPlayer.getCharacter() instanceof Human)
            return false;

        return validSectorWhereAttack(currentMatch);

    }

    /**
     * This method does a validated AttackAction done by a certain Player in a
     * certain Match, like an Alien Attack.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     */
    @Override
    public void doAction(Match currentMatch) {
        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());

        this.publishInfo(currentPlayer.getClientId() + "(" + currentPlayer.getCharacter().getClass().getSimpleName() + ") attacked in " + currentPlayer.getCurrentPosition().printToString() + ".");

        this.doAttack(currentMatch, currentPlayer);

    }
}
