package it.polimi.ingsw.cg_29.gamemanager.model.player;

/**
 * This Human Strategy allows to manage the Sedatives ability of a Human Player.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class SedateHuman implements HumanStrategy {
    /**
     * Creates a Strategy SedateHuman.
     */
    public SedateHuman() {
        super();
    }

    /**
     * addBehaviour allows to add the SedateHuman Strategy to a certain Human
     * setting true the BeSedate attribute (now this Human could not draw any
     * DangerousSectorCard if him move onto a DangerousSector).
     * 
     * @param character
     *            that is the Human on which will be activated the SedateHuman
     *            Strategy.
     */
    @Override
    public void addBehaviour(Character character) {
        character.setBeSedate(true);
    }

    /**
     * removeBehaviour allows to remove the SedateHuman Strategy to a certain
     * Human setting false the BeSedate attribute (now this Human could/must
     * draw a DangerousSectorCard if him move onto a DangerousSector in order to
     * him game choices: attack or not).
     * 
     * @param character
     *            that is the Human on which will be deactivated the SedateHuman
     *            Strategy.
     */
    @Override
    public void removeBehaviour(Character character) {
        character.setBeSedate(false);
    }
}
