package it.polimi.ingsw.cg_29.gamemanager.model.player;

/**
 * This Human Strategy allows to manage the rangeOfMove of a Human Player.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class FastHuman implements HumanStrategy {
    /**
     * Creates a Strategy FastHuman.
     */
    public FastHuman() {
        super();
    }

    /**
     * addBehaviour allows to add the FastHuman Strategy to a certain Human
     * setting two the rangeOfMove attribute (now this Human could cross two
     * Sectors during a MoveAction).
     * 
     * @param character
     *            that is the Human on which will be activated the FastHuman
     *            Strategy.
     */
    @Override
    public void addBehaviour(Character character) {
        character.setRangeOfMove(2);
    }

    /**
     * removeBehaviour allows to remove the FastHuman Strategy to a certain
     * Human setting one the rangeOfMove attribute (now this Human could cross
     * only one Sector during a MoveAction).
     * 
     * @param character
     *            that is the Human on which will be deactivated the FastHuman
     *            Strategy.
     */
    @Override
    public void removeBehaviour(Character character) {
        character.setRangeOfMove(1);
    }

}
