package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * When used, this Card allows the Human Player to be protected if someone
 * attack him.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class DefenseItem extends ItemCard {
    /**
     * Creates a Card DefenseItem.
     */
    public DefenseItem() {
        super();
    }

}
