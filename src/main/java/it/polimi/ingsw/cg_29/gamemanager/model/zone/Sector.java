package it.polimi.ingsw.cg_29.gamemanager.model.zone;

import java.util.Set;

/**
 * Implementation of Sector. This is an element of game zone.
 * 
 * @author Luca
 * @version 1.0
 *
 */
public abstract class Sector extends Coordinates {

    /**
     * reference to nearSectorProxy from which are calculated the near sectors
     * to this one.
     */
    private NearSectorsProxy nearSectorsProxy;

    /**
     * Return a Sector with specific coordinates and reference to zone and
     * initialize the nearSectorProxy.
     * 
     * @param row
     *            reference to row of the zone by integer number.
     * @param col
     *            reference to column of the zone by integer number.
     * @param zone
     *            reference to the zone.
     * @throws NullPointerException
     */
    protected Sector(int row, int col, Zone zone) {
        super(row, col);

        this.nearSectorsProxy = new NearSectorsProxy(new Coordinates(row, col), zone);
    }

    /**
     * Return a Sector with specific coordinates and reference to zone.
     * 
     * @param col
     *            reference to column of the zone by string.
     * @param row
     *            reference to row of the zone by string.
     * @param zone
     *            reference to the zone.
     * @throws NullPointerException
     *             , NumberFormatException
     */
    protected Sector(String col, String row, Zone zone) {
        this(Integer.parseInt(row) - 1, ((int) col.charAt(0)) - 65, zone);
    }

    /**
     * @return the nearSectorsProxy
     */
    public NearSectorsProxy getNearSectorsProxy() {
        return nearSectorsProxy;
    }

    /**
     * Implementation of hashCode
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((nearSectorsProxy == null) ? 0 : nearSectorsProxy.hashCode());
        return result;
    }

    /**
     * Implementation of equals
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Sector other = (Sector) obj;
        if (!nearSectorsProxy.equals(other.nearSectorsProxy))
            return false;
        return true;
    }

    /**
     * Implementation of toString Intended only for debugging.
     */
    @Override
    public String toString() {
        return "Sector[[" + super.printToString() + "][nearSectorsProxy=" + nearSectorsProxy + "]]";
    }

    /**
     * Call a proxy method to calculate sectors at specific distance from this
     * one.
     * 
     * @param range
     *            the distance to calculate near sectors
     * @return a set of coordinates represent the sectors reachable at specific
     *         range
     */
    public Set<Coordinates> getNearSectors(int range) {
        return this.nearSectorsProxy.getNearSectors(range, this.getRow(), this.getCol());
    }

}
