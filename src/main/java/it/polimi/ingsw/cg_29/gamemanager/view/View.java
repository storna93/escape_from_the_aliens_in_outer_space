package it.polimi.ingsw.cg_29.gamemanager.view;

import java.util.Observable;

/**
 * Contains the implementation of the view that represent a client connected to
 * the server.
 * 
 * @author Luca
 * @version 1.0
 *
 */
public abstract class View extends Observable {

    /**
     * Reference to the client of this view.
     */
    private String clientId;

    /**
     * @return the id of the client of this view.
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * Set the id of the client of this view.
     * 
     * @param clientId
     *            string representing client id.
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    /**
     * Send the message to the client of this view.
     * 
     * @param message
     */
    public abstract void send(String message);

}