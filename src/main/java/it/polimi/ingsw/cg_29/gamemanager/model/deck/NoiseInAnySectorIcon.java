package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * When drawn, this Card: - When drawn, this Card binds to say any Sector to all
 * Player, for the Player that drawn this Card. - bind to draw an ItemCard for
 * the Player that drawn this Card.
 * 
 * 
 * @author Fulvio
 * @version 1.0
 */

public class NoiseInAnySectorIcon extends NoiseInAnySector {
    /**
     * Creates a DanegerousSectorCard NoiseInAnySectorIcon.
     */
    protected NoiseInAnySectorIcon() {
        super();
    }

}
