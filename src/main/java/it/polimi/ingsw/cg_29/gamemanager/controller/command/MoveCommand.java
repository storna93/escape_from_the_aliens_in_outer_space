package it.polimi.ingsw.cg_29.gamemanager.controller.command;

import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.Action;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.MoveAction;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.view.View;

/**
 * A command to move in specific sector.
 *
 * @author Luca
 * @version 1.0
 *
 */
public class MoveCommand extends Command {

    /**
     * Sector in which to move.
     */
    private String destination;

    /**
     * Create a new command
     * 
     * @param controller
     *            the controller that manages current match
     * @param currentClient
     *            the id of client who sent command
     */
    public MoveCommand(Controller controller, View currentClient, String destination) {
        super(controller, currentClient);
        this.destination = destination;
    }

    /**
     * Launch this command.
     */
    @Override
    public void execute() {

        if (this.matchIsRunning()) {

            Match match = this.getCurrentMatch();
            Action move = new MoveAction(this.getCurrentClientId(), new Coordinates(this.destination.substring(0, 1), this.destination.substring(1)));

            if (move.isValid(match)) {
                this.getCurrentClient().send("Movement done.");
                this.addActionToObserver(move);
                move.doAction(match);
            } else
                this.getCurrentClient().send("Invalid movement.");
        } else
            this.getCurrentClient().send("The match is not running.");

    }

}
