package it.polimi.ingsw.cg_29.gamemanager.controller;

import it.polimi.ingsw.cg_29.gamemanager.controller.command.ActivateItemCommand;
import it.polimi.ingsw.cg_29.gamemanager.controller.command.ActivateSpotlightCommand;
import it.polimi.ingsw.cg_29.gamemanager.controller.command.AttackCommand;
import it.polimi.ingsw.cg_29.gamemanager.controller.command.ChatCommand;
import it.polimi.ingsw.cg_29.gamemanager.controller.command.DisconnectCommand;
import it.polimi.ingsw.cg_29.gamemanager.controller.command.DrawCommand;
import it.polimi.ingsw.cg_29.gamemanager.controller.command.FakeNoiseCommand;
import it.polimi.ingsw.cg_29.gamemanager.controller.command.GetLogCommand;
import it.polimi.ingsw.cg_29.gamemanager.controller.command.HelpCommand;
import it.polimi.ingsw.cg_29.gamemanager.controller.command.MoveCommand;
import it.polimi.ingsw.cg_29.gamemanager.controller.command.PassCommand;
import it.polimi.ingsw.cg_29.gamemanager.controller.command.ReconnectCommand;
import it.polimi.ingsw.cg_29.gamemanager.controller.command.SetZoneCommand;
import it.polimi.ingsw.cg_29.gamemanager.controller.command.ShowStateCommand;
import it.polimi.ingsw.cg_29.gamemanager.controller.command.ShowZoneCommand;
import it.polimi.ingsw.cg_29.gamemanager.controller.command.UseAttackCommand;
import it.polimi.ingsw.cg_29.gamemanager.controller.command.UseSedativesCommand;
import it.polimi.ingsw.cg_29.gamemanager.view.View;

/**
 * Contains a parser for commands sent by client and creates a specific Command
 * to execute.
 *
 * @author Luca
 * @version 1.0
 *
 */
public class CommandParser {

    /**
     * Creates a new CommandParser.
     */
    protected CommandParser() {
    }

    /**
     * Parse a string and creates a command that modifies the controller or the
     * match
     * 
     * @param controller
     *            reference to controller that received command
     * @param currentClient
     *            reference to clientView that sent command
     * @param command
     *            string received
     */
    protected void parseCommand(Controller controller, View currentClient, String command) {

        if (command.matches("help")) {
            new HelpCommand(controller, currentClient).execute();
            return;
        }

        if (command.matches("set_zone\\([123]\\)")) {
            new SetZoneCommand(controller, currentClient, Integer.parseInt(command.substring("set_zone(".length(), (command).length() - 1))).execute();
            return;
        }

        if (command.matches("show_zone")) {
            new ShowZoneCommand(controller, currentClient).execute();
            return;
        }

        if (command.matches("show_state") || command.matches("")) {
            new ShowStateCommand(controller, currentClient).execute();
            return;
        }

        if (command.matches("get_log")) {
            new GetLogCommand(controller, currentClient).execute();
            return;
        }

        if (command.matches("chat\\(.*\\)")) {
            new ChatCommand(controller, currentClient, command.substring("chat(".length(), command.length() - 1)).execute();
            return;
        }

        if (command.matches("move\\([A-Z][0-9][0-9]\\)")) {
            new MoveCommand(controller, currentClient, command.substring("move(".length(), (command).length() - 1)).execute();
            return;
        }

        if (command.matches("attack")) {
            new AttackCommand(controller, currentClient).execute();
            return;
        }

        if (command.matches("use_attack")) {
            new UseAttackCommand(controller, currentClient).execute();
            return;
        }

        if (command.matches("use_sedatives")) {
            new UseSedativesCommand(controller, currentClient).execute();
            return;
        }

        if (command.matches("activate_item\\([123]\\)")) {
            new ActivateItemCommand(controller, currentClient, Integer.parseInt(command.substring("activate_item(".length(), (command).length() - 1))).execute();
            return;
        }

        if (command.matches("activate_spotlight\\([A-Z][0-9][0-9]\\)")) {
            new ActivateSpotlightCommand(controller, currentClient, command.substring("activate_spotlight(".length(), (command).length() - 1)).execute();
            return;
        }

        if (command.matches("fake_noise\\([A-Z][0-9][0-9]\\)")) {
            new FakeNoiseCommand(controller, currentClient, command.substring("fake_noise(".length(), (command).length() - 1)).execute();
            return;
        }

        if (command.matches("draw")) {
            new DrawCommand(controller, currentClient).execute();
            return;
        }

        if (command.matches("reconnect")) {
            new ReconnectCommand(controller, currentClient).execute();
            return;
        }

        if (command.matches("disconnect")) {
            new DisconnectCommand(controller, currentClient).execute();
            return;
        }

        if (command.matches("pass")) {
            new PassCommand(controller, currentClient).execute();
            return;
        }

        currentClient.send("Invalid command, send \"help\" to see valid commands.");
        return;
    }
}
