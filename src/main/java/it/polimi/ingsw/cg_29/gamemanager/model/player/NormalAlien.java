package it.polimi.ingsw.cg_29.gamemanager.model.player;

/**
 * This Alien Strategy allows to reset all the abilities that a Alien Player has
 * into Normal Alien abilities.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class NormalAlien implements AlienStrategy {
    /**
     * Creates a Strategy NormalAlien.
     */
    public NormalAlien() {
        super();
    }

    /**
     * addBehaviour allows to add the NormalAlien Strategy to a certain Alien
     * resetting to false the canAttack attribute and to 2 the rangeOfMove
     * attribute (now this Alien could not cross three Sectors during a
     * MoveAction but only two).
     * 
     * @param character
     *            that is the Alien on which will be activated the NormalAlien
     *            Strategy.
     */
    @Override
    public void addBehaviour(Character character) {
        character.setBeSedate(false);
        character.setCanDefense(false);
        character.setRangeOfMove(2);
        character.setCanAttack(true);
    }

    /**
     * removeBehaviour allows to add the NormalAlien Strategy to a certain Alien
     * resetting to false the canAttack attribute and to 2 the rangeOfMove
     * attribute (now this Alien could not cross three Sectors during a
     * MoveAction but only two).
     * 
     * @param character
     *            that is the Alien on which will be deactivated the NormalAlien
     *            Strategy.
     */
    @Override
    public void removeBehaviour(Character character) {
        character.setBeSedate(false);
        character.setCanDefense(false);
        character.setRangeOfMove(2);
        character.setCanAttack(true);
    }

}
