package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * It is the super type to manage all EscapeHatchCards: EscapeGreen and
 * EscapeRed.
 * 
 * @author Fulvio
 * @version 1.0
 */

public abstract class EscapeHatchCard extends Card {
    /**
     * Creates a generic EscapeHatchCard.
     */
    protected EscapeHatchCard() {
        super();
    }
}
