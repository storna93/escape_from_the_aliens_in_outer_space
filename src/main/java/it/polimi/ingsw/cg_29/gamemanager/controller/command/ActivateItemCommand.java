package it.polimi.ingsw.cg_29.gamemanager.controller.command;

import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.Action;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.ActivateAdrenalineAction;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.ActivateAttackAction;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.ActivateSedativesAction;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.ActivateTeleportAction;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.AdrenalineItem;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.AttackItem;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Card;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.DefenseItem;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.SedativesItem;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.SpotlightItem;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.TeleportItem;
import it.polimi.ingsw.cg_29.gamemanager.view.View;

import java.util.List;

/**
 * A command to activate card in player's hand.
 *
 * @author Luca
 * @version 1.0
 *
 */
public class ActivateItemCommand extends Command {

    /**
     * Number of card in the hand of player to activate.
     */
    private int numberOfCard;

    /**
     * Counter to show special message.
     */
    private static int counter;

    /**
     * Create a new command
     * 
     * @param controller
     *            the controller that manages current match
     * @param currentClient
     *            the id of client who sent command
     * @param numberOfCard
     */
    public ActivateItemCommand(Controller controller, View currentClient, int numberOfCard) {
        super(controller, currentClient);
        this.numberOfCard = numberOfCard;
    }

    /**
     * Launch this command.
     */
    @Override
    public void execute() {

        if (this.matchIsRunning()) {
            List<Card> currentHand = this.getCurrentMatch().getSpecificPlayerById(this.getCurrentClientId()).getItemCardList();
            if (numberOfCard > currentHand.size()) {
                this.getCurrentClient().send("You have not this card in your hand.");
                return;
            }
            Match match = this.getCurrentMatch();
            Card cardUsed = currentHand.get(numberOfCard - 1);

            if (cardUsed instanceof SpotlightItem) {
                this.getCurrentClient().send("To use Spotlight you have to send: \"activate_spotlight(SECTOR_COORDINATES)\"");
                return;
            }

            if (cardUsed instanceof DefenseItem) {
                this.getCurrentClient().send("The Defense has a passive effect.");
                return;
            }

            Action activateItem = null;

            if (cardUsed instanceof AttackItem)
                activateItem = new ActivateAttackAction(this.getCurrentClientId(), cardUsed);

            if (cardUsed instanceof AdrenalineItem)
                activateItem = new ActivateAdrenalineAction(this.getCurrentClientId(), cardUsed);

            if (cardUsed instanceof SedativesItem)
                activateItem = new ActivateSedativesAction(this.getCurrentClientId(), cardUsed);

            if (cardUsed instanceof TeleportItem)
                activateItem = new ActivateTeleportAction(this.getCurrentClientId(), cardUsed);

            if (activateItem == null) {
                this.getCurrentClient().send("ITEM UNKNOWN " + cardUsed.getClass().getSimpleName() + "???");
                return;
            }

            if (activateItem.isValid(match)) {
                counter = 0;
                this.getCurrentClient().send(cardUsed.getClass().getSimpleName() + " used.");
                this.addActionToObserver(activateItem);
                activateItem.doAction(match);
            } else {
                counter++;
                if (counter > 2) {
                    counter = 0;
                    this.getCurrentClient().send(
                            "\n        OAK:  <<NOW IS NOT THE TIME TO USE THAT!>>\n\n" +

                            "                        /:/+::-`\n" + "                        `:::o------:///:--..\n" + "                         +:----------------:+:\n" + "                         `s+/+::::/://::/+o+++\n" + "                         .y+++....-so/..-:/so+\n" + "                          -ooodh+-.:-.-/yy/yh-\n" + "                          `oy:/shd+.-sddso/ss/`\n" + "                          `os/+-s/-..-:y`+//o/:\n" + "                           /y----......-.://o+.\n" + "                            /:..----...--::s:.\n" + "    `-:/-.                   .::-+o++ooo//:`\n" + "   -:+.+.+`                   `/s++++++sy.\n" + "  .o././.:-         ``.-.--:::++/so+oosoy+//-.`\n" + "  +/./-/:-/.--    `/---...-/..s/+syyssoos+-o:://::-.`\n"
                                    + "  +-:/---.o:./`   -:.....-/..:h+soysossoh/-:+---....+:\n" + "  -:....-.-.+`   `+/...../:::s+sooyoooysh//:o:..```./+`\n" + "  `+`../...:-   `/.+....-::::o/+//o//++ss-://.`````:-++.\n" + "   +..--..-+   `+-.:/.../-..++////+/////+..`/.````:----/\n" + "   :+--:-:o/. `/-..-/.../-..o//////////+:..-:```../----+-\n" + "   ./:/+++o.://:---/-...::.-+//////////+...:..`.`:/-----+-\n" + "    +--:+s/--:++/::+.....+./+//////////+`.:..````-/------/:\n" + "    :+--/o-----:/::+.....+.o///////////+./:-----:/o////--::/.\n" + "     o/:/-------/-//.....:/o//////////+/oyysyyssssssyhd/-+.-/-\n" + "     `::--------+://-....-ss//////////o+:yoso++++sysssds//:..+\n" + "       `-::::::-/:s:--....sy+///o/////s++yyyyyyhhhhhhhdh+----/\n"
                                    + "         `.:::::/-s------.oyo///o/////s:+dsssssssyyyyyds:---+`\n" + "           `.````o--:----shysoso+++++s:sdyhsyyyyyyhhddo---/:\n" + "                 .o-------yNNm+/+/+dmNd:dyyo:+//soossydo--:/\n" + "                 .+-------ymddyhyyydddh:os++:/+:+ho/yyo/-:/\n" + "                 -os+-----hdhhhdhdhhhhh/:os/o/++:+/+os+.::`\n" + "                 /yys:----hdhhhdhdhhhhh+-:/+++++o++o/:o:-\n" + "                 o+/-/----yhhhhdhhhhhhho-----+/++o+/++:`\n" + "                .+:--:----yhhhhddhdhhhhs-----/:-----:/\n" + "                -/``-:----sdhhhdmdmhhhhh-----/:-----:/\n" + "                -:`.::----sdhhhhdmmdhhhh-----//-----:/\n" + "                ./.-/:----sdhhhhhdmdhhhh:----::....-:+\n"
                                    + "                `+--::.---sdhhhhhdhdhhhho-----:....-:/\n" + "                 +:-:-..--sddhhhhdyhhhyyy---../...../-\n" + "                 -+:-.....ohdhhhhhsyhyyhh:-.../---:-o`\n" + "                 .+......./hhhyyydssdyyhy+......--..+\n" + "                  +.......:hyhhhydssyhyhss..........+\n" + "                  :/-.....:hyhhyyy::/hyhsy-........./\n" + "                   :yoossoshyyyyh/   oshsss+++++++os.\n" + "                   `yssyysyyyyyyh/   -yysssssyyyyyyh\n" + "                    +sssyyyyyyyyh:    oyysssssyyyyhh\n" + "                    /ssyyyyyyyyyh.    -hysssssyyyyhd`\n" + "                    sshyyhhyyyyyh.    `yyssssyyyhyhm`\n" + "                    syhsyyhhyyyyh`     /hsssyhyyhhym\n"
                                    + "                    .dssssshhyyyh`     .hysshyyyyyyd\n" + "                    +yyyssssyyyyy      `hyyhysssyyyh-\n" + "                  `:ymmmmddhhhhhy      `yyyysssssyyyo\n" + "               `shdhhhdddmmmmmmmh      `yyyyhdhhhddho\n" + "               /dhhhddddddmmmys+.       .smdmdddhhhdh+`\n" + "               :dmdddmmdho/.`            hddhdddhhhhmmh`\n" + "                 `...-.                  /hmmdddhddmmmd`\n" + "                                            -/oyyhhhyo.\n");
                } else
                    this.getCurrentClient().send("You cannot activate " + cardUsed.getClass().getSimpleName() + ".");
            }
        } else
            this.getCurrentClient().send("The match is not running.");

    }

}
