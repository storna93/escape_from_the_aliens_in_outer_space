package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * It is the super type to manage all ItemCards: AdrenalineItem, AttackItem,
 * DefenseItem, SedativesItem, SpotlightItem and TeleportItem.
 * 
 * @author Fulvio
 * @version 1.0
 */

public abstract class ItemCard extends Card {
    /**
     * Creates a generic ItemCard.
     */
    protected ItemCard() {
        super();
    }

}
