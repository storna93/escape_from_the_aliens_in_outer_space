package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * When drawn, this Card ensures that all Players know nothing about the current
 * position of the Player that drawn this Card.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class Silence extends DangerousSectorCard {
    /**
     * Creates a DanegerousSectorCard Silence.
     */
    protected Silence() {
        super();
    }

}
