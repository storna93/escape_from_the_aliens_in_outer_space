package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Sector;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.VoidSector;

/**
 * This Action allows every Player to activate do a FakeNoiseCall of the chosen
 * Sector by the Player.
 * 
 * @author Fulvio
 * @version 1.0
 *
 */
public class FakeNoiseCallAction extends Action {

    /**
     * fakeCoordinates is the fake currentPosition specifies by the Player who
     * has clientId
     */
    private Coordinates fakeCoordinates;
    /**
     * fakeNoisetResultForTesting is the result of FakeNoiseCallAction and it is
     * used only for do tests.
     */
    private String fakeNoisetResultForTesting = "";

    /**
     * creates a FakeNoiseCallAction assigning that to a clientId that has to
     * says a fakeCoordinates, specifying in which RoundState (only FAKE_NOISE)
     * that Action could be valid.
     * 
     * @param clientId
     * @param fakeCoordinates
     */
    public FakeNoiseCallAction(String clientId, Coordinates fakeCoordinates) {
        super(clientId);

        this.fakeCoordinates = fakeCoordinates;
        this.addValidState(RoundState.FAKE_NOISE);

    }

    /**
     * This method defines if a certain Player could do an FakeNoiseCall for a
     * certain Sector or not.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     * @return true (if the Action is a valid Action for a certain Player in a
     *         certain Match) false (otherwise).
     */
    @Override
    public boolean isValid(Match currentMatch) {

        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());
        if (!validPlayer(currentPlayer))
            return false;

        Sector fakeSector = currentMatch.getCurrentZone().getSectorByCoordinates(fakeCoordinates);
        if (fakeSector == null)
            return false;
        if (fakeSector instanceof VoidSector)
            return false;
        if (!currentMatch.getCurrentZone().sectorExist(fakeCoordinates))
            return false;
        return true;

    }

    /**
     * This method does a validated FakeNoiseCallAction done by a certain Player
     * in a certain Match, publishing to all Player a Noise coming from a Sector
     * choices by the Player.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     */
    @Override
    public void doAction(Match currentMatch) {
        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());

        this.publishInfo("[NOISE] " + currentPlayer.getClientId() + ": NOISE IN SECTOR " + fakeCoordinates.printToString());
        this.fakeNoisetResultForTesting += "[NOISE] " + currentPlayer.getClientId() + ": NOISE IN SECTOR " + fakeCoordinates.printToString();

        currentPlayer.setRoundState(RoundState.ENDING);

    }

    /**
     * @return the fakeNoisetResultForTesting
     */
    public String getFakeNoisetResultForTesting() {
        return fakeNoisetResultForTesting;
    }

}
