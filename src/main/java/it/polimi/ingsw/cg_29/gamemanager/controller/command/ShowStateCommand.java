package it.polimi.ingsw.cg_29.gamemanager.controller.command;

import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;
import it.polimi.ingsw.cg_29.gamemanager.view.View;

/**
 * A command to show match and player state.
 *
 * @author Luca
 * @version 1.0
 *
 */
public class ShowStateCommand extends Command {

    /**
     * Create a new command
     * 
     * @param controller
     *            the controller that manages current match
     * @param currentClient
     *            the id of client who sent command
     */
    public ShowStateCommand(Controller controller, View currentClient) {
        super(controller, currentClient);
    }

    /**
     * Launch this command.
     */
    @Override
    public void execute() {
        if (this.matchIsRunning())
            this.getCurrentClient().send("[STATE] \n" + this.getCurrentMatch().printState(this.getCurrentClientId()));
        else
            this.getCurrentClient().send("Connected to the server. The match is not running. Send \"help\" to see valid commands.");

    }

}
