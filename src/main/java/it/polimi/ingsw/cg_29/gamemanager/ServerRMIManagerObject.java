package it.polimi.ingsw.cg_29.gamemanager;

import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.gamemanager.commons.ClientRMIObjectInterface;
import it.polimi.ingsw.cg_29.gamemanager.commons.ServerRMIManagerObjectInterface;
import it.polimi.ingsw.cg_29.gamemanager.view.ServerRMIView;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Defines the remote object ServerRMIManager, that offers the method for the
 * first client connection to the ServerRMI.
 * 
 * @author Fulvio
 * @version 1.0
 */
public class ServerRMIManagerObject implements ServerRMIManagerObjectInterface {
    /**
     * server is the reference that associates ServerRMI object to the real
     * unique Server.
     */
    private Server server;

    /**
     * creates a remote object ServerRMIManagerObject that is necessary to the
     * RMI communication between Server and Client.
     * 
     * @param server
     */
    public ServerRMIManagerObject(Server server) {
        super();
        this.server = server;
    }

    @Override
    /**
     * This method could be remotely call by client for being connected to
     * the ServerRMI,
     * then creates the executor to run a new ServerRMIView.
     * 
     * @param client that is the interface of the client remote object that wants
     * to be connected to this.
     * 
     * @throws RemoteException
     */
    public void firstConnection(ClientRMIObjectInterface client, String clientId, String controllerId) throws RemoteException {

        ExecutorService executor = Executors.newCachedThreadPool();
        ServerRMIView newClientView;

        try {
            newClientView = new ServerRMIView(this.server, client, clientId, controllerId);
            executor.submit(newClientView);
        } catch (IOException e) {
            LoggerClass.getLogger().warn(e);

            executor.shutdown();
        }

    }

}
