package it.polimi.ingsw.cg_29.gamemanager.model.zone;

/**
 * Implementation of Coordinates characterized by parameters of type (row,
 * column)
 * 
 * @author Luca
 * @version 1.0
 */
public class Coordinates {

    /**
     * representation of row by an integer.
     */
    private int row;

    /**
     * representation of column by an integer.
     */
    private int col;

    /**
     * Return a coordinates element.
     * 
     * @param row
     *            integer representing row coordinate
     * @param col
     *            integer representing column coordinate
     */
    public Coordinates(int row, int col) {
        this.row = row;
        this.col = col;
    }

    /**
     * Implementation of constructor parses string to integer
     * 
     * @param col
     *            string representing column coordinate
     * @param row
     *            string representing row coordinate
     * @throws NullPointerException
     *             , NumberFormatException
     */
    public Coordinates(String col, String row) {
        this(Integer.parseInt(row) - 1, ((int) col.charAt(0)) - 65);
    }

    /**
     * Implementation of getter
     * 
     * @return row coordinate
     */
    public int getRow() {
        return row;
    }

    /**
     * Implementation of getter
     * 
     * @return column coordinate
     */
    public int getCol() {
        return col;
    }

    /**
     * Implementation of hashCode
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + col;
        result = prime * result + row;
        return result;
    }

    /**
     * Implementation of equals
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Coordinates other = (Coordinates) obj;
        if (col != other.col)
            return false;
        if (row != other.row)
            return false;
        return true;
    }

    /**
     * Implementation of toString Intended only for debugging.
     */
    @Override
    public String toString() {
        return this.printToString() + "{" + row + "," + col + "}";
    }

    /**
     * @return a "pretty print" string representing this Coordinates.
     */
    public String printToString() {
        String zeroChar = "";

        if (this.row > -9 && this.row < 9) {
            zeroChar += "0";
        }

        return "" + (char) (col + 65) + zeroChar + (row + 1) + "";
    }

}
