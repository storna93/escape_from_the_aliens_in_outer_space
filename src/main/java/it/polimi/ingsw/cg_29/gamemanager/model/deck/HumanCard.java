package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * When used, this Card defines the Character Human for a certain Player.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class HumanCard extends CharacterCard {
    /**
     * Creates a character Card Human.
     */
    protected HumanCard() {
        super();
    }

}
