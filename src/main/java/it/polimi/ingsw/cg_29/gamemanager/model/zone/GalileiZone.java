package it.polimi.ingsw.cg_29.gamemanager.model.zone;

/**
 * This class represent Galilei Zone.
 * 
 * @author Luca
 * @version 1.0
 */
public class GalileiZone extends Zone {

    /**
     *
     */
    protected GalileiZone() {
        super();
    }

}
