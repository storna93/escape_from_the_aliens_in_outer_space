package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * When drawn, this Card defines that the EscapeHatch could not be used .
 * 
 * @author Fulvio
 * @version 1.0
 */

public class EscapeGreen extends EscapeHatchCard {
    /**
     * Creates a Card EscapeGreen.
     */
    protected EscapeGreen() {
        super();
    }

}
