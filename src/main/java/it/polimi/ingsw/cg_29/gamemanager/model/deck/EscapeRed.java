package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * When drawn, this Card defines that the EscapeHatch could be used .
 * 
 * @author Fulvio
 * @version 1.0
 */

public class EscapeRed extends EscapeHatchCard {
    /**
     * Creates a Card EscapeRed.
     */
    protected EscapeRed() {
        super();
    }
}
