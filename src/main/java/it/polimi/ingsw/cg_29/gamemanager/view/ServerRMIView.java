package it.polimi.ingsw.cg_29.gamemanager.view;

import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.gamemanager.Server;
import it.polimi.ingsw.cg_29.gamemanager.commons.ClientRMIObjectInterface;
import it.polimi.ingsw.cg_29.gamemanager.commons.ServerRMIViewObjectInterface;
import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;

import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Contains the implementation of the view that represents a client connected by
 * RMI to the server.
 * 
 * @author Fulvio
 * @version 1.0
 *
 */
public class ServerRMIView extends View implements Runnable {
    /**
     * server is the reference to the real and unique Server.
     */
    private Server server;
    /**
     * controllerId is the identification of the controller that manages the
     * clients of a certain match.
     */
    private String controllerId = "";
    /**
     * clientId is the identification that allows serverRMIView to refer to the
     * correct client in the request-response procedure.
     */
    private String clientId = "";
    /**
     * client is the identification that allows serverRMIView to refer to the
     * correct remote object client in the request-response procedure.
     */
    private ClientRMIObjectInterface client;

    /**
     * creates the ServerRMIView thread that maintains the request-response with
     * the specific client
     * 
     * @param server
     * @param client
     * @param controllerId
     * @param clientId
     * @throws IOException
     */
    public ServerRMIView(Server server, ClientRMIObjectInterface client, String clientId, String controllerId) throws IOException {
        this.server = server;
        this.client = client;

        this.clientId = clientId;
        this.controllerId = controllerId;
    }

    @Override
    /**
     * This method sets the valid identification useful in 
     * Server-Client communication.
     * Then this method creates the serverRMIView remote object that is necessary to the RMI communication
     * between Server and Client.
     */
    public void run() {

        // If is first connection add this client to current controller.
        if ("".equals(this.clientId) || "".equals(this.controllerId))
            this.controllerId = this.server.addClientToController(this);
        else {
            this.setClientId(this.clientId);
        }

        Controller currentController = this.server.getControllerById(controllerId);
        this.clientId = this.getClientId();
        this.addObserver(currentController);

        try {
            Registry registry = LocateRegistry.getRegistry(7777);

            ServerRMIViewObject serverRMIView = new ServerRMIViewObject(this);
            ServerRMIViewObjectInterface stub = (ServerRMIViewObjectInterface) UnicastRemoteObject.exportObject(serverRMIView, 0);

            registry.bind("serverRMIView" + client, stub);
            client.setId(controllerId, clientId, stub);

        } catch (RemoteException e) {
            LoggerClass.getLogger().warn(e);
            this.server.showOutputServerMessage("Not found the remote object CientRMI that this is going to set the" + "identifiers on.");

        } catch (AlreadyBoundException e) {
            LoggerClass.getLogger().warn(e);
            this.server.showOutputServerMessage("Error: Has already been define a remote object serverRMIView.");
        }
    }

    @Override
    /**
     * This method turn to dispatchMessage method to dispatch the message response of the server.
     */
    public void send(String message) {
        try {
            client.dispatchMessage(message);
        } catch (RemoteException e) {
            LoggerClass.getLogger().warn(e);
            this.server.showOutputServerMessage("Not found the remote object CientRMI that this is going to dispatch the " + "response message");

        }

    }

    /**
     * This method communicates and notify all observers that has been received
     * a new command by the specific client
     * 
     * @param commandToNotify
     */
    public void receiveCommand(String command) {
        String commandToNotify = command;
        String controllerIdentifier = commandToNotify.substring(0, commandToNotify.indexOf("#"));
        commandToNotify = commandToNotify.substring(commandToNotify.indexOf("#") + 1);
        commandToNotify = commandToNotify.substring(commandToNotify.indexOf("#") + 1);
        this.addObserver(this.server.getControllerById(controllerIdentifier));
        this.setChanged();
        this.notifyObservers(commandToNotify);

    }
}
