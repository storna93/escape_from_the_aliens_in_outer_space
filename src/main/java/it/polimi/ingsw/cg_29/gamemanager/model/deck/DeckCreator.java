package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * This DeckCreator realizes a factoryMethod on the object Deck. It is the super
 * type of all DeckCreators to manage several Deck type creation:
 * CharacterDeckCreator, DangerousSectorDeckCreator, EscapeHatchDeckCreator and
 * ItemDeckCreator, using the method createDeck().
 * 
 * @author Fulvio
 * @version 1.0
 */

public abstract class DeckCreator {
    /**
     * Creates a creator for a generic Deck.
     */
    protected DeckCreator() {
        super();
    }

    /**
     * This method allows to create a new object Deck and then to populate it.
     */
    public abstract Deck createDeck();
}
