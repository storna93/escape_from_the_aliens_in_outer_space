package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.NormalHuman;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;

/**
 * This Action allows every Player to pass to the next Player.
 * 
 * @author Fulvio
 * @version 1.0
 *
 */
public class PassAction extends Action {
    /**
     * creates a PassAction assigning that to a clientId who wants to terminate
     * his Round and pass, specifying in which RoundState (only ENDING,
     * MOVED_SECURE) that Action could be valid.
     * 
     * @param clientId
     */
    public PassAction(String clientId) {
        super(clientId);
        this.addValidState(RoundState.ENDING);
        this.addValidState(RoundState.MOVED_SECURE);

    }

    /**
     * This method defines if a certain Player could pass to another Player or
     * not.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     * @return true (if the Action is a valid Action for a certain Player in a
     *         certain Match) false (otherwise).
     */
    @Override
    public boolean isValid(Match currentMatch) {
        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());
        if (!validPlayer(currentPlayer))
            return false;

        return true;
    }

    /**
     * This method does a validated PassAction done by a certain Player in a
     * certain Match, putting this Player into STANDBY state and the next Player
     * into PLAYING state.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     */
    @Override
    public void doAction(Match currentMatch) {

        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());

        if (currentPlayer.getCharacter() instanceof Human) {
            currentPlayer.getCharacter().activateStrategy(new NormalHuman());
        }

        this.publishInfo(this.getClientId() + " passed.");
        this.endOfTurn(currentMatch, currentPlayer);

    }
}
