package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * When used, this Card allows the Human Player to move onto the
 * HumanStartSector immediately.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class TeleportItem extends ItemCard {
    /**
     * Creates a Card TeleportItem.
     */
    public TeleportItem() {
        super();
    }

}
