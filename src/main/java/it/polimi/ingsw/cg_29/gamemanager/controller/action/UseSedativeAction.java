package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.SedativesItem;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.player.SedateHuman;

/**
 * This Action allows Human Player to not draw any DangerousSectorCard during
 * the round.
 * 
 * @author Fulvio
 * @version 1.0
 *
 */
public class UseSedativeAction extends Action {
    /**
     * creates an UseSedativeAction assigning that to a clientId who wants to
     * not draw a dangerousSectorCard, specifying in which RoundState (only
     * MOVED_DANGEROUS) that Action could be valid.
     * 
     * @param clientId
     */
    public UseSedativeAction(String clientId) {
        super(clientId);

        this.addValidState(RoundState.MOVED_DANGEROUS);
    }

    /**
     * This method defines if a certain Player could useSedative or not.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     * @return true (if the Action is a valid Action for a certain Player in a
     *         certain Match) false (otherwise).
     */
    @Override
    public boolean isValid(Match currentMatch) {
        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());
        if (!validPlayer(currentPlayer))
            return false;
        if (!(currentPlayer.getCharacter() instanceof Human))
            return false;
        if (!currentPlayer.getCharacter().getBeSedate())
            return false;
        return true;
    }

    /**
     * This method does a validated UseSedativeAction done by a certain Player
     * in a certain Match, allowing this Player to not draw a
     * DangerousSectorCard if this Player move onto a DangerousSector.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     */
    @Override
    public void doAction(Match currentMatch) {
        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());

        this.publishInfo(currentPlayer.getClientId() + " avoids drawing a card because he has activated Sedatives Item Card before.");

        currentPlayer.getCharacter().deactivateStrategy(new SedateHuman());
        this.discardUsedItemCard(currentMatch, new SedativesItem());
        currentPlayer.setRoundState(RoundState.ENDING);

    }

}
