package it.polimi.ingsw.cg_29.gamemanager.controller.command;

import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.Action;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.AttackAction;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.view.View;

/**
 * A command to attack.
 *
 * @author Luca
 * @version 1.0
 *
 */
public class AttackCommand extends Command {

    /**
     * Create a new command
     * 
     * @param controller
     *            the controller that manages current match
     * @param currentClient
     *            the id of client who sent command
     */
    public AttackCommand(Controller controller, View currentClient) {
        super(controller, currentClient);
    }

    /**
     * Launch this command.
     */
    @Override
    public void execute() {

        if (this.matchIsRunning()) {

            Match match = this.getCurrentMatch();
            Action attack = new AttackAction(this.getCurrentClientId());

            if (attack.isValid(match)) {
                this.getCurrentClient().send("Attack done.");
                this.addActionToObserver(attack);
                attack.doAction(match);
            } else {
                this.getCurrentClient().send("You cannot attack.");
            }
        } else
            this.getCurrentClient().send("The match is not running.");

    }

}
