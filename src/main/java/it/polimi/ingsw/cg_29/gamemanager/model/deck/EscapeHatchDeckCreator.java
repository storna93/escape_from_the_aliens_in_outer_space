package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * This EscapeHatchDeckCreator realizes a factoryMethod on the object
 * EscapeHatchDeck using the method createDeck().
 * 
 * @author Fulvio
 * @version 1.0
 */

public class EscapeHatchDeckCreator extends DeckCreator {
    /**
     * Creates a creator for a EscapeHatchDeckCreator.
     */
    public EscapeHatchDeckCreator() {
        super();

    }

    /**
     * This method allows to create a new object EscapeHatchDeck and then to
     * populate it with: 3 EscapeGreen; 3 EscapeRed.
     * 
     * @return d that is the new shuffled EscapeHatchDeck that has been realized
     *         using the factoryMethod.
     */
    @Override
    public EscapeHatchDeck createDeck() {

        EscapeHatchDeck d = new EscapeHatchDeck();

        for (int i = 0; i < 3; i++) {
            EscapeHatchCard c = new EscapeRed();
            d.addCard(c);
        }
        for (int i = 0; i < 3; i++) {
            EscapeHatchCard c = new EscapeGreen();
            d.addCard(c);
        }

        d.deckShuffle();

        return d;
    }

}
