package it.polimi.ingsw.cg_29.gamemanager.publisher;

import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.gamemanager.Server;
import it.polimi.ingsw.cg_29.gamemanager.commons.BrokerObjectInterface;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * Contains the implementation of the Broker class. This broker accept socket
 * and RMI connections and dispatches messages to clients of a topic.
 *
 * @author Luca
 * @version 1.0
 */
public class Broker extends Thread {

    /**
     * Number of port to accept socket connections.
     */
    private static final int PORT = 29998;

    /**
     * Reference of server.
     */
    private Server server;

    /**
     * Map represents list of topics. Each topic represents a different match.
     */
    private Map<String, List<SubscriberServerThread>> topics;

    /**
     * Creates a new broker and initializes Topics list
     * 
     * @param server
     *            reference to server.
     */
    public Broker(Server server) {
        this.server = server;
        this.topics = new HashMap<String, List<SubscriberServerThread>>();

        BrokerObject broker = new BrokerObject(this);

        try {
            Registry registry = LocateRegistry.createRegistry(7777);
            BrokerObjectInterface stub = (BrokerObjectInterface) UnicastRemoteObject.exportObject(broker, 0);
            registry.bind("Broker", stub);
        } catch (AlreadyBoundException | RemoteException e) {
            LoggerClass.getLogger().warn(e);
            server.showOutputServerMessage("Error: Has already been define a remote object Broker.");
        }

    }

    /**
     * Add a new subscriber to a specific topic.
     * 
     * @param controllerId
     *            the reference to specific topic
     * @param subsciber
     *            instance of new subscriber
     */
    public void addSubscriber(String controllerId, SubscriberServerThread subsciber) {
        subsciber.start();
        if (!this.topics.containsKey(controllerId)) {
            this.topics.put(controllerId, new ArrayList<SubscriberServerThread>());
        }
        this.topics.get(controllerId).add(subsciber);

        this.server.verifiesConditionsToStartMatch(controllerId);
    }

    /**
     * Dispatch a message to subscribers of a specific topic.
     * 
     * @param controllerId
     *            the reference to specific topic
     * @param message
     *            the message to send
     */
    public void publish(String controllerId, String message) {
        if (this.topics.containsKey(controllerId)) {
            for (SubscriberServerThread subsciber : this.topics.get(controllerId)) {
                subsciber.dispatchMessage(message);
            }
        }
    }

    /**
     * Starts the broker to accept socket connections
     *
     * @throws IOException
     */
    private void startBroker() throws IOException {

        ServerSocket brokerSocket = new ServerSocket(PORT);
        server.showOutputServerMessage("Broker ready. [port: " + PORT + "]");
        Scanner socketIn = null;

        while (true) {
            try {

                Socket socketReceive = brokerSocket.accept();
                socketIn = new Scanner(socketReceive.getInputStream());
                SubscriberServerSocketThread newClientSubscriber = new SubscriberServerSocketThread(socketReceive);

                String messageReceive = "";
                while (!messageReceive.contains("subscribe_topic#"))
                    messageReceive = socketIn.nextLine();

                String controllerId = messageReceive.substring("subscribe_topic#".length());

                this.addSubscriber(controllerId, newClientSubscriber);

            } catch (IOException e) {
                LoggerClass.getLogger().warn(e);
                break;
            }
        }
        brokerSocket.close();
        socketIn.close();
    }

    /**
     * Start this thread and wait for subscribers.
     */
    @Override
    public void run() {
        try {
            this.startBroker();
        } catch (IOException e) {
            LoggerClass.getLogger().warn(e);
            server.showOutputServerMessage("Error: another BrokerSocket is already running on this port.");
        }
    }

}
