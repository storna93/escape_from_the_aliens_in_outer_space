package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import it.polimi.ingsw.cg_29.gamemanager.controller.TimerToEndRound;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.MatchState;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Card;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Deck;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.DefenseItem;
import it.polimi.ingsw.cg_29.gamemanager.model.player.AggressiveHuman;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Alien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.DefendedHuman;
import it.polimi.ingsw.cg_29.gamemanager.model.player.FastAlien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.PlayerState;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.AlienSector;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.EscapeHatchSector;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.HumanSector;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Sector;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * Defines the super type of all possible Actions during a Match. Defines some
 * methods called only by Actions as supports for them.
 * 
 * @author Fulvio
 * @version 1.0
 */

public abstract class Action extends Observable {

    /**
     * validState is the List of all valid RoundStates in which a certain Player
     * can do a certain Action.
     * 
     */
    private List<RoundState> validState;

    /**
     * clientId is the Player identifier.
     */
    private String clientId;

    /**
     * Construct an Action: - setting as clientId the same of the Player that
     * had called the Action; - Instantiating a empty list of valid RoundStates,
     * for the Player that had called the Action.
     * 
     * @param clientId
     *            that is the Player identifier that had called a certain
     *            Action.
     */
    public Action(String clientId) {
        this.clientId = clientId;
        this.validState = new ArrayList<RoundState>();
    }

    /**
     * This method allows to add in the list validState another State in which
     * is possible called a certain action for a certain Player.
     * 
     * @param validState
     */
    protected void addValidState(RoundState validState) {
        this.validState.add(validState);
    }

    /**
     * 
     * @return the clientId of a certain Action.
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * This method allows to know if a certain Player is not null or not.
     * 
     * @param currentPlayer
     *            that is the Player who it doesn't know if exists or not.
     * @return true (if currentPlayer exists) false (otherwise).
     */
    protected boolean playerExists(Player currentPlayer) {
        if (currentPlayer == null)
            return false;
        return true;
    }

    /**
     * This method allows to know if a certain Player is PLAYING or not.
     * 
     * @param currentPlayer
     *            that is the Player who it doesn't know if is playing or not.
     * @return true (if currentPlayer is playing) false (otherwise).
     */
    protected boolean isPlaying(Player currentPlayer) {
        if (currentPlayer.getPlayerState().equals(PlayerState.PLAYING))
            return true;
        return false;
    }

    /**
     * This method allows to know if the RoundState of a certain Player is also
     * a validState to do a certain Action or not.
     * 
     * @param currentPlayer
     *            that is the Player who it doesn't know if his RoundState is
     *            also a validState or not.
     * @return true (if currentPlayer RoundState is a validState) false
     *         (otherwise).
     */
    protected boolean isValidState(Player currentPlayer) {
        for (RoundState valid : this.validState)
            if (currentPlayer.getRoundState().equals(valid))
                return true;
        return false;
    }

    /**
     * This method allows to know: - if the RoundState of a certain Player is
     * also a validState or not; - if a certain Player exists or not.
     * 
     * @param currentPlayer
     *            that is the Player who it doesn't know if is validPlayer or
     *            not.
     * @return true (if currentPlayer is a validPlayer) false (otherwise).
     */
    protected boolean validPlayer(Player currentPlayer) {
        if (!this.playerExists(currentPlayer))
            return false;
        if (!this.isPlaying(currentPlayer))
            return false;
        if (!this.isValidState(currentPlayer))
            return false;
        return true;
    }

    /**
     * This method puts into effect a validated Attack: - if the attacked Sector
     * is empty then the Player who puts in effect the attack goes into
     * RoundState ENDING; - if the attacked Sector is not empty then all the
     * Players in this Sector who not have any defense goes into PlayerState
     * LOSER and discard every owned ItemCard; all the Players in this Sector
     * who have some defense become undefended. And then the Player who puts in
     * effect the attack goes into RoundState ENDING; - if the Player that
     * attacks is Alien and the Player that loses is Human then the Alien
     * becomes FastAlien.
     * 
     * @param currentMatch
     *            that is the Match in which it has a validated Attack.
     * @param currentPlayer
     *            that is the Player who does a validated Attack.
     */
    protected void doAttack(Match currentMatch, Player currentPlayer) {
        Coordinates currentPosition = currentPlayer.getCurrentPosition();
        List<Player> playerList = currentMatch.getPlayerList();
        String attackResult = "";

        for (Player player : playerList) {

            if (player.getCurrentPosition().equals(currentPosition))
                if (!player.getClientId().equals(currentPlayer.getClientId()) && !player.getPlayerState().equals(PlayerState.LOSER)) {
                    if (!player.hasSpecificItemCard(new DefenseItem()) || !(player.getCharacter() instanceof Human)) {
                        player.setPlayerState(PlayerState.LOSER);
                        attackResult += player.getClientId() + "(" + player.getCharacter().getClass().getSimpleName() + ") died; ";
                        if (currentPlayer.getCharacter() instanceof Alien && player.getCharacter() instanceof Human) {
                            currentPlayer.getCharacter().activateStrategy(new FastAlien());
                        }

                        List<Card> itemCardList = player.getItemCardList();
                        Deck itemDeck = currentMatch.getItemDeck();
                        for (Card card : itemCardList)
                            itemDeck.discardCard(card);
                        itemCardList.clear();
                    } else {
                        player.getCharacter().deactivateStrategy(new DefendedHuman());
                        this.publishInfo(player.getClientId() + " use DefenseItem.");

                    }
                }

        }

        if ("".equals(attackResult))
            this.publishInfo("No player has been killed.");
        else
            this.publishInfo(attackResult);

        if (currentPlayer.getCharacter() instanceof Human)
            currentPlayer.getCharacter().deactivateStrategy(new AggressiveHuman());
        currentPlayer.setRoundState(RoundState.ENDING);
    }

    /**
     * This method allows to notify a message to all player the effect of a
     * certain action.
     * 
     * @param message
     */
    public void publishInfo(String message) {
        this.setChanged();
        this.notifyObservers(message);
    }

    /**
     * This method defines if a certain Action done by a certain Player in a
     * certain Match is valid or not.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     * @return true (if the Action is a valid Action for a certain Player in a
     *         certain Match) false (otherwise).
     */
    public abstract boolean isValid(Match currentMatch);

    /**
     * This method does a validated Action done by a certain Player in a certain
     * Match.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     */
    public abstract void doAction(Match currentMatch);

    /**
     * This method discards a ItemCard that has just been activated then used by
     * the current Player in a certain Match (currentMatch).
     * 
     * @param currentMatch
     * @param usedItemCard
     */
    public void discardUsedItemCard(Match currentMatch, Card usedItemCard) {
        currentMatch.getItemDeck().discardCard(usedItemCard);
    }

    /**
     * This method is called at the end of the round of a certain Player,
     * setting STANDBY his RoundState. Then this method enables to play the
     * correct next Player, if there are not other Players, this Method ends off
     * the Match.
     * 
     * @param currentMatch
     * @param currentPlayer
     */
    public void endOfTurn(Match currentMatch, Player currentPlayer) {

        currentPlayer.setRoundState(RoundState.STANDBY);

        currentMatch.nextPlayerRound(currentPlayer);

        this.setChanged();
        this.notifyObservers("[TIMER STOP]");

        List<Player> playerList = currentMatch.getPlayerList();

        if (currentMatch.gameOverConditions()) {
            this.publishInfo("THE MATCH IS OVER!");
            String matchResult = "";
            for (Player player : playerList)
                matchResult += player.getClientId() + " (" + player.getCharacter().getClass().getSimpleName() + "): " + player.getPlayerState() + "; ";
            this.publishInfo(matchResult);
        } else {
            for (Player player : playerList)
                if (player.getRoundState() == RoundState.BEGINNING) {
                    this.publishInfo("Now it is the turn of " + player.getClientId() + " (" + TimerToEndRound.SECONDS_TO_END_TURN + " seconds to end round).");
                    this.setChanged();
                    this.notifyObservers("[TIMER START]" + player.getClientId());
                }
        }
    }

    /**
     * This method return true if the sector Position in which current Player
     * is, could be the destination of an attack action, false otherwise.
     * 
     * @param currentMatch
     * @return
     */
    public boolean validSectorWhereAttack(Match currentMatch) {
        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());
        Coordinates currentPosition = currentPlayer.getCurrentPosition();
        Sector currentSector = currentMatch.getCurrentZone().getSectorByCoordinates(currentPosition);

        if (currentSector == null)
            return false;
        if (currentSector instanceof EscapeHatchSector)
            return false;
        if (currentSector instanceof AlienSector)
            return false;
        if (currentSector instanceof HumanSector)
            return false;
        return true;
    }

    /**
     * This method returns true if current turn is current player's turn and the
     * game is not over.
     * 
     * @param currentMatch
     *            reference to the match.
     * @return true if current turn is current player's turn and the game is not
     *         over.
     */
    public boolean validConnectionCommand(Match currentMatch) {
        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());
        if (!this.playerExists(currentPlayer) || !this.isValidState(currentPlayer))
            return false;

        if (currentMatch.getMatchState() == MatchState.FINISHED)
            return false;

        return true;
    }
}
