package it.polimi.ingsw.cg_29.gamemanager.controller;

import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.Action;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.DisconnectAction;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;

/**
 * Thread that disconnect the active client after selected time.
 * 
 * @author Luca
 * @version 1.0
 *
 */
public class TimerToEndRound extends Thread {

    /**
     * Time in second to end round.
     */
    public static final int SECONDS_TO_END_TURN = 120;

    /**
     * Reference to controller of the match.
     */
    private Controller controller;

    /**
     * reference to client id
     */
    private String clientId;

    /**
     * return a new TimerToEndRound whit the reference to the client and the
     * controller
     * 
     * @param controller
     * @param clientId
     */
    public TimerToEndRound(Controller controller, String clientId) {
        this.controller = controller;
        this.clientId = clientId;
    }

    /**
     * Disconnect client after selected time.
     */
    @Override
    public void run() {
        try {

            Thread.sleep((long) SECONDS_TO_END_TURN * 1000);

            Match match = this.controller.getMatch();
            Action disconnect = new DisconnectAction(this.clientId);

            if (disconnect.isValid(match)) {
                this.controller.sendToAll("Round timer of " + this.clientId + " finished.");
                disconnect.addObserver(this.controller.getActionObserver());
                disconnect.doAction(match);
            }
        } catch (InterruptedException e) {
            LoggerClass.getLogger().warn(e);
            this.controller.showOutputControllerMessage(this.clientId + " completes his round before timer.");
        }
    }

}
