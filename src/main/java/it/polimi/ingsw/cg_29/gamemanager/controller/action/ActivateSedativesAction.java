package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Card;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.player.SedateHuman;

/**
 * This Action allows Human Player to activate SedativeHuman strategy.
 * 
 * @author Fulvio
 * @version 1.0
 *
 */
public class ActivateSedativesAction extends ActivateItemAction {
    /**
     * creates an activateSedativesAction assigning that to a clientId who has
     * an sedativesItem, specifying in which RoundState (only
     * BEGINNING,MOVED_DANGEROUS) that Action could be valid.
     * 
     * @param clientId
     * @param sedativesItem
     */
    public ActivateSedativesAction(String clientId, Card sedativesItem) {
        super(clientId, sedativesItem);
        this.addValidState(RoundState.BEGINNING);
        this.addValidState(RoundState.MOVED_DANGEROUS);

    }

    /**
     * This method defines if a certain Player could activate a SedativesItem or
     * not.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     * @return true (if the Action is a valid Action for a certain Player in a
     *         certain Match) false (otherwise).
     */
    @Override
    public boolean isValid(Match currentMatch) {
        return validPlayerAndValidActivationItemCard(currentMatch);
    }

    /**
     * This method does a validated ActivateSedativesAction done by a certain
     * Player in a certain Match, activating the SedateHuman strategy on this
     * Player.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     */
    @Override
    public void doAction(Match currentMatch) {
        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());
        currentPlayer.getCharacter().activateStrategy(new SedateHuman());

        this.publishInfo(this.getClientId() + " used a Sedatives Item Card.");
    }

}
