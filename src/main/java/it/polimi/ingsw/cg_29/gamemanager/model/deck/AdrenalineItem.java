package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * When used, this Card allows the Human Player to move twice as far.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class AdrenalineItem extends ItemCard {
    /**
     * Creates a Card AdrenalineItem.
     */
    public AdrenalineItem() {
        super();
    }

}
