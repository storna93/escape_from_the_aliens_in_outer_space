package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * When drawn, this Card ensures that all Players know the current position of
 * the Player that drawn this Card.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class NoiseInYourSector extends DangerousSectorCard {
    /**
     * Creates a DanegerousSectorCard NoiseInYourSector.
     */
    protected NoiseInYourSector() {
        super();
    }

}
