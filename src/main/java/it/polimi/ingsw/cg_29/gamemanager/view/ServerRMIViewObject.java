package it.polimi.ingsw.cg_29.gamemanager.view;

import it.polimi.ingsw.cg_29.gamemanager.commons.ServerRMIViewObjectInterface;

/**
 * Defines the remote object ServerRMIView, that offers the method for the
 * sending of commands by the specific client already connected to the Server.
 * to the ServerRMI.
 * 
 * @author Fulvio
 * @version 1.0
 */
public class ServerRMIViewObject implements ServerRMIViewObjectInterface {

    /**
     * serverRMIView is the reference to the serverRMIView that maintains the
     * request-response with the specific client
     */
    private ServerRMIView serverRMIView;

    /**
     * creates the remote object ServerRMIViewObject that is necessary to the
     * RMI communication between Server and Client.
     * 
     * @param server
     * @param serverRMIView
     */
    public ServerRMIViewObject(ServerRMIView serverRMIView) {
        super();
        this.serverRMIView = serverRMIView;

    }

    /**
     * This method could be remotely call by the respective client for send a
     * command message to the Server
     * 
     * @param command
     */
    @Override
    public void sendCommand(String command) {
        serverRMIView.receiveCommand(command);

    }
}
