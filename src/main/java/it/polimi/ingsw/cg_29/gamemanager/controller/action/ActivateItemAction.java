package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Card;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;

/**
 * Defines the super type of all possible ActivateItemActions during a Match.
 * Defines one methods called only by ActivateItemActions as support for them.
 * 
 * @author Fulvio
 * @version 1.0
 */
public abstract class ActivateItemAction extends Action {

    /**
     * itemCard is used to store the itemCard that references to this.
     */
    private Card itemCard;

    /**
     * creates a generic ActivateItemAction which references to a clientId who
     * has an itemCard.
     * 
     * @param clientId
     * @param itemCard
     */
    public ActivateItemAction(String clientId, Card itemCard) {
        super(clientId);
        this.itemCard = itemCard;
    }

    /**
     * validActivationItemCard allows to says if a certain currentPlaye could
     * activate a specific itemCard or not.
     * 
     * @param currentPlayer
     * @return true (if the currentPlayer is Human and has the itemCard) false
     *         (otherwise)
     */

    public boolean validPlayerAndValidActivationItemCard(Match currentMatch) {
        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());
        if (!validPlayer(currentPlayer))
            return false;
        if (!(currentPlayer.getCharacter() instanceof Human))
            return false;
        if (!currentPlayer.hasSpecificItemCard(itemCard))
            return false;
        return true;
    }

}
