package it.polimi.ingsw.cg_29.gamemanager.model.player;

import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;

import java.util.ArrayList;
import java.util.List;

/**
 * This class maintains all the Coordinates of all Sectors crossed by a certain
 * Player during the game. This class allows to add the new current player
 * position Round by Round and to get it.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class MovementRecord {

    /**
     * cordinatesList maintains all the Coordinates of the Sectors which a
     * Player have moved onto Round by Round.
     */
    private List<Coordinates> coordinatesList;

    // constructor
    /**
     * The MovementRecord of a certain Player has been initialized adding the
     * starting position of the Player: - HumanSector, if the Character Player
     * is a Human; - AlienSector, if the Character Player is an Alien.
     * 
     * @param coordinatesStart
     *            that is HumanSector Coordinates or AlienSector Coordinates.
     */
    public MovementRecord(Coordinates coordinatesStart) {
        this.coordinatesList = new ArrayList<Coordinates>();
        this.coordinatesList.add(coordinatesStart);
    }

    /**
     * 
     * @return the CurrentPosition Coordinates of a certain Player.
     */
    public Coordinates getCurrentCoordinates() {
        return this.coordinatesList.get(coordinatesList.size() - 1);
    }

    /**
     * This method adds the new current Player position on the coordinatesList;
     * 
     * @param newPosition
     */
    public void setNewCurrentCoordinates(Coordinates newPosition) {
        this.coordinatesList.add(newPosition);

    }

    /**
     * This method replaces the current Player position on the coordinatesList
     * with another newPosition;
     * 
     * @param newPosition
     */
    public void replaceCurrentCoordinates(Coordinates newPosition) {
        this.coordinatesList.set(coordinatesList.size() - 1, newPosition);

    }

    /**
     * 
     * @return the List<Coordinates> of all the coordinates that the Player
     *         moved onto during the previous Rounds.
     */
    public List<Coordinates> getCoordinatesList() {
        return this.coordinatesList;
    }

}
