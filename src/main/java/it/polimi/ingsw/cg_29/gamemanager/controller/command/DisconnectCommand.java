package it.polimi.ingsw.cg_29.gamemanager.controller.command;

import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.Action;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.DisconnectAction;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.view.View;

/**
 * A command to disconnect from the match.
 *
 * @author Luca
 * @version 1.0
 *
 */
public class DisconnectCommand extends Command {

    /**
     * Create a new command
     * 
     * @param controller
     *            the controller that manages current match
     * @param currentClient
     *            the id of client who sent command
     */
    public DisconnectCommand(Controller controller, View currentClient) {
        super(controller, currentClient);
    }

    /**
     * Launch this command.
     */
    @Override
    public void execute() {

        if (this.matchIsRunning()) {

            Match match = this.getCurrentMatch();
            Action disconnect = new DisconnectAction(this.getCurrentClientId());

            if (disconnect.isValid(match)) {
                this.getCurrentClient().send("You are disconnected.");
                this.addActionToObserver(disconnect);
                disconnect.doAction(match);
            } else
                this.getCurrentClient().send("You cannot disconnect.");

        } else {
            this.getCurrentClient().send("You are disconnected.");
            this.getController().removeClientFromThisMatch(this.getCurrentClientId());
        }

    }

}
