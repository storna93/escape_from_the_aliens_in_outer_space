package it.polimi.ingsw.cg_29.gamemanager.controller.command;

import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.Action;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.DrawAction;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.view.View;

/**
 * A command to draw card from dangerous deck.
 *
 * @author Luca
 * @version 1.0
 *
 */
public class DrawCommand extends Command {

    /**
     * Create a new command
     * 
     * @param controller
     *            the controller that manages current match
     * @param currentClient
     *            the id of client who sent command
     */
    public DrawCommand(Controller controller, View currentClient) {
        super(controller, currentClient);
    }

    /**
     * Launch this command.
     */
    @Override
    public void execute() {

        if (this.matchIsRunning()) {

            Match match = this.getCurrentMatch();
            Action draw = new DrawAction(this.getCurrentClientId());

            if (draw.isValid(match)) {
                this.getCurrentClient().send("Draft done.");
                this.addActionToObserver(draw);
                draw.doAction(match);
                if (match.getSpecificPlayerById(this.getCurrentClientId()).getRoundState() == RoundState.FAKE_NOISE)
                    this.getCurrentClient().send("You drawn a Noise In Any Sector Card. Send \"fake_noise(SECTOR_COORDINATES)\".");
            } else {
                this.getCurrentClient().send("You cannot draw.");
            }
        } else
            this.getCurrentClient().send("The match is not running.");

    }

}
