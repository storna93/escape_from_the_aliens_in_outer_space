package it.polimi.ingsw.cg_29.gamemanager.publisher;

import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.gamemanager.commons.ClientRMIObjectInterface;

import java.rmi.RemoteException;

/**
 * Contains the implementation of the subscriber thread that represent a client
 * connected to the server by RMI.
 * 
 * @author Fulvio
 * @version 1.0
 *
 */
public class SubscriberServerRMIThread extends SubscriberServerThread {

    /**
     * clientToSubscribe is the reference to the stub of a certain client that
     * has been subscribed to the Broker.
     */
    private ClientRMIObjectInterface clientToSubscribe;

    /**
     * creates a SubscriberServerRMIThread that takes care of the Broker
     * publishing to a single specific clientToSubscribe.
     * 
     * @param clientToSubscribe
     */
    public SubscriberServerRMIThread(ClientRMIObjectInterface clientToSubscribe) {
        super();
        this.clientToSubscribe = clientToSubscribe;
    }

    /**
     * This method is used by Broker for send a message to all
     * client-subscribers.
     * 
     * @param message
     */
    @Override
    public void send(String message) {
        try {
            clientToSubscribe.dispatchMessage(message);
        } catch (RemoteException e) {
            LoggerClass.getLogger().warn(e);
        }
    }

}
