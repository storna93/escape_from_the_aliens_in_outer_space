package it.polimi.ingsw.cg_29.gamemanager.controller;

import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.FermiZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.ZoneCreator;
import it.polimi.ingsw.cg_29.gamemanager.publisher.Broker;
import it.polimi.ingsw.cg_29.gamemanager.view.ServerSocketView;
import it.polimi.ingsw.cg_29.gamemanager.view.View;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Contains the controller of the game.
 *
 * @author Luca
 * @version 1.0
 */
public class Controller implements Observer {

    /**
     * Counter to create unique ids for clients
     */
    private static int client_counter = 0;

    /**
     * Id of this controller
     */
    private String controllerId;

    /**
     * Reference to zoneCreator to get the zone of the match.
     */
    private ZoneCreator zoneCrator = new FermiZoneCreator();

    /**
     * Reference of current match.
     */
    private Match match = null;

    /**
     * id of admin of the match who can modify zone before starting match
     */
    private String matchAdminId = "";

    /**
     * This contains the list of ids of clients.
     */
    private List<String> clientList;

    /**
     * Reference of commandParser that translate clients commands.
     */
    private CommandParser commandParser;

    /**
     * Reference of broker that dispatches message to client of this controller.
     */
    private Broker broker;

    /**
     * Reference of actionObserver that observers actions of this controller.
     */
    private ActionObserver actionObserver;

    /**
     * Reference of customLog that saves informations of the match of this
     * controller.
     */
    private CustomLog customLog;

    /**
     * Reference of timerToEndRound that instantiates a new timer to end each
     * round.
     */
    private TimerToEndRound timerToEndRound = null;

    /**
     * Create a new controller and initializes some attributes.
     * 
     * @param controllerId
     *            unique id of this controller
     * @param broker
     *            reference of broker that dispatches message to client of this
     *            controller.
     */
    public Controller(String controllerId, Broker broker) {
        this.controllerId = controllerId;
        this.clientList = new ArrayList<String>();
        this.broker = broker;
        this.commandParser = new CommandParser();
        this.actionObserver = new ActionObserver(this);
        this.customLog = new CustomLog(this);
    }

    /**
     * @return the controllerId
     */
    public String getControllerId() {
        return controllerId;
    }

    /**
     * @return the match
     */
    public Match getMatch() {
        return match;
    }

    /**
     * @return the actionObserver
     */
    public ActionObserver getActionObserver() {
        return actionObserver;
    }

    /**
     * @return the matchAdminId
     */
    public String getMatchAdminId() {
        return matchAdminId;
    }

    /**
     * @param zoneCrator
     *            the zoneCrator to set
     */
    public void setZoneCrator(ZoneCreator zoneCrator) {
        this.zoneCrator = zoneCrator;
    }

    /**
     * @return the log
     */
    public CustomLog getLog() {
        return customLog;
    }

    /**
     * Creates and starts match
     */
    public void startMatch() {
        Collections.shuffle(this.clientList);
        try {
            this.match = new Match(this.clientList, this.zoneCrator);
        } catch (NotWellFormedZoneException e) {
            LoggerClass.getLogger().warn(e);
            LoggerClass.getLogger().warn(e);
            this.zoneCrator = new FermiZoneCreator();
            startMatch();
        }
        this.showOutputControllerMessage("A new Match has been initialized.");
        this.sendToAll("[START] [ZONE " + this.zoneCrator.getClass().getSimpleName() + "] \n" + this.match.getCurrentZone().printToString());
        this.sendToAll("[START] " + "Press ENTER to show your state; send \"help\" tho show commands.");
        this.sendToAll("Match start with " + this.getNumberOfPlayer() + " players!");
        this.sendToAll("It is turn of " + this.clientList.get(0) + " (" + TimerToEndRound.SECONDS_TO_END_TURN + " seconds to end round).");
        this.setNewTimer(this.clientList.get(0));
    }

    /**
     * @return number of client connect to this controller.
     */
    public int getNumberOfPlayer() {
        return clientList.size();
    }

    /**
     * Add a client to this controller and set his client id
     * 
     * @param newClientView
     *            client's view
     */
    public void addClient(View newClientView) {
        client_counter++;
        String currentClientId = "client_" + client_counter;

        newClientView.setClientId(currentClientId);
        clientList.add(currentClientId);

        if (this.getNumberOfPlayer() == 1) {
            this.setMatchAdmin();
            this.showOutputControllerMessage(this.getNumberOfPlayer() + " client connected.");
        } else {
            this.showOutputControllerMessage(this.getNumberOfPlayer() + " clients connected.");
        }

    }

    /**
     * Removes a client disconnects before starting match
     * 
     * @param clientId
     *            client to remove
     */
    public void removeClientFromThisMatch(String clientId) {
        this.clientList.remove(clientId);
        if (this.matchAdminId.equals(clientId) && !this.clientList.isEmpty()) {
            this.setMatchAdmin();
            this.sendToAll("Now " + this.matchAdminId + " is the match administrator: he can set the Zone before starting match.");
        }
    }

    /**
     * Sets the administrator of the Match that can decide Zone before starting
     * match
     * 
     */
    private void setMatchAdmin() {
        if (!this.clientList.isEmpty()) {
            this.matchAdminId = this.clientList.get(0);
        }
    }

    /**
     * Send a message to all clients of this controller
     * 
     * @param message
     *            to send
     */
    public void sendToAll(String message) {
        if ("[CHAT".equals(message.substring(0, 5)) || "[START]".equals(message.substring(0, 7))) {
            this.broker.publish(this.controllerId, message);
        } else {
            String infoTag = "[INFO] ";
            if (this.match != null)
                infoTag += "[ROUND: " + this.match.getCurrentRound() + "] ";
            this.broker.publish(this.controllerId, infoTag + message);

            this.customLog.addEventNotification(message);
        }
    }

    /**
     * Show output message of this controller.
     * 
     * @param message
     */
    public void showOutputControllerMessage(String message) {
        System.out.println("[" + this.getControllerId() + "] " + message);
        LoggerClass.getLogger().info("[" + this.getControllerId() + "] " + message);
    }

    /**
     * clientView receive a command from client and update controller by this
     * method
     */
    @Override
    public synchronized void update(Observable o, Object arg) {
        View currentClient = (View) o;
        String command = (String) arg;

        this.showOutputControllerMessage("[" + currentClient.getClientId() + "] " + "sent: \"" + command + "\"");

        commandParser.parseCommand(this, currentClient, command);

        if (currentClient instanceof ServerSocketView)
            ((ServerSocketView) currentClient).closeConnection();
    }

    /**
     * Creates a new TimerToEndRound that disconnect the active client after
     * selected time.
     * 
     * @param clientId
     *            id of active client
     */
    public void setNewTimer(String clientId) {
        this.timerToEndRound = new TimerToEndRound(this, clientId);
        this.timerToEndRound.start();
    }

    /**
     * Interrupt TimerToEndRound when active cliend ends his round.
     */
    public void interruptTimer() {
        if (this.timerToEndRound != null)
            this.timerToEndRound.interrupt();
    }
}