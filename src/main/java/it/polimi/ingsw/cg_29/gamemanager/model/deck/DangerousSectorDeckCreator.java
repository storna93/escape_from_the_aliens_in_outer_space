package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * This DangerousSectorDeckCreator realizes a factoryMethod on the object
 * DangerousSectorDeck using the method createDeck().
 * 
 * @author Fulvio
 * @version 1.0
 */

public class DangerousSectorDeckCreator extends DeckCreator {
    /**
     * Creates a creator for a DangerousSectorDeckCreator.
     */
    public DangerousSectorDeckCreator() {

    }

    /**
     * This method allows to create a new object DangerousSectorDeck and then to
     * populate it with: 5 Silence; 6 NoiseInAnySector; 4 NoiseInAnySectorIcon;
     * 6 NoiseInYourSector; 4 NoiseInYourSectorIcon.
     * 
     * @return d that is the new shuffled DangerousSectorDeck that has been
     *         realized using the factoryMethod.
     */
    @Override
    public DangerousSectorDeck createDeck() {

        DangerousSectorDeck d = new DangerousSectorDeck();

        for (int i = 0; i < 5; i++) {
            DangerousSectorCard c = new Silence();
            d.addCard(c);
        }
        for (int i = 0; i < 6; i++) {
            DangerousSectorCard c = new NoiseInAnySector();
            d.addCard(c);
        }

        for (int i = 0; i < 4; i++) {
            DangerousSectorCard c = new NoiseInAnySectorIcon();
            d.addCard(c);
        }

        for (int i = 0; i < 6; i++) {
            DangerousSectorCard c = new NoiseInYourSector();
            d.addCard(c);
        }

        for (int i = 0; i < 4; i++) {
            DangerousSectorCard c = new NoiseInYourSectorIcon();
            d.addCard(c);
        }
        d.deckShuffle();

        return d;
    }

}
