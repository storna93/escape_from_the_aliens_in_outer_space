package it.polimi.ingsw.cg_29.gamemanager.model.zone;

/**
 * Implementation of AbstractFactory. This is a ConcreteFactory of Galilei Zone.
 * 
 * @author Luca
 * @version 1.0
 */
public class GalileiZoneCreator extends ZoneCreator {

    /**
     *
     */
    public GalileiZoneCreator() {
        super();
    }

    /**
     * @return ZoneCreator representation, created by parsing a String matrix
     */
    @Override
    public GalileiZone createZone() {
        GalileiZone zone = new GalileiZone();

        String[][] mapString = { { "V", "D", "S", "V", "V", "S", "D", "S", "S", "S", "D", "D", "D", "D", "V", "S", "S", "S", "V", "V", "S", "S", "V" }, { "D", "E", "D", "D", "S", "D", "D", "S", "D", "D", "S", "S", "S", "D", "D", "D", "D", "D", "D", "D", "D", "E", "D" }, { "D", "D", "D", "D", "D", "D", "D", "S", "D", "D", "D", "D", "D", "S", "V", "S", "D", "D", "V", "V", "D", "D", "S" }, { "S", "D", "D", "V", "D", "D", "D", "D", "D", "D", "D", "S", "D", "D", "V", "S", "S", "S", "D", "V", "D", "D", "S" }, { "S", "S", "D", "D", "D", "D", "D", "V", "D", "D", "S", "D", "S", "D", "S", "D", "D", "D", "D", "D", "S", "D", "S" }, { "S", "D", "D", "V", "D", "D", "D", "D", "V", "D", "D", "A", "D", "D", "D", "D", "S", "S", "D", "D", "D", "D", "S" },
                { "V", "V", "D", "V", "V", "D", "S", "S", "D", "V", "V", "V", "V", "V", "D", "D", "D", "S", "D", "S", "D", "V", "V" }, { "V", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "H", "D", "D", "D", "D", "D", "S", "D", "S", "D", "S", "V" }, { "S", "D", "D", "D", "D", "D", "D", "D", "S", "D", "S", "S", "S", "D", "S", "D", "D", "D", "D", "V", "D", "D", "D" }, { "S", "S", "D", "S", "D", "S", "D", "V", "D", "D", "D", "D", "D", "D", "D", "D", "D", "V", "V", "V", "D", "D", "S" }, { "S", "D", "D", "D", "D", "D", "D", "V", "D", "D", "S", "S", "S", "D", "D", "D", "S", "V", "V", "D", "D", "D", "S" }, { "S", "D", "D", "D", "S", "V", "S", "D", "V", "V", "D", "D", "D", "D", "D", "S", "D", "S", "D", "D", "S", "D", "S" },
                { "S", "E", "D", "D", "D", "V", "D", "D", "D", "D", "V", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "E", "D" }, { "D", "D", "S", "S", "V", "V", "S", "S", "S", "S", "D", "S", "D", "S", "S", "D", "S", "V", "V", "S", "D", "D", "D" } };

        for (int r = 0; r < mapString.length; r++) {
            for (int c = 0; c < mapString[0].length; c++) {
                zone.addSectorByString(r, c, mapString[r][c]);
            }
        }

        return zone;
    }

}
