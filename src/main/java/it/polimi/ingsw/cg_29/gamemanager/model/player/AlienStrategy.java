package it.polimi.ingsw.cg_29.gamemanager.model.player;

/**
 * This Strategy interface allows to manage only AlienStrategy. AlienStrategy
 * also allows to implement a Strategy Pattern on class Alien with the goal to
 * adding or removing Alien behavior, activating or deactivating one of the
 * following Alien Strategy: AlienHuman,FastAlien.
 * 
 * @author Fulvio
 * @version 1.0
 */

public interface AlienStrategy extends Strategy {

}
