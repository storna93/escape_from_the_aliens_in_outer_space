package it.polimi.ingsw.cg_29.gamemanager.model.player;

/**
 * This Strategy interface allows to manage only HumanStrategy. HumanStrategy
 * also allows to implement a Strategy Pattern on class Human with the goal to
 * adding or removing Human behavior, activating or deactivating one or more of
 * the following Human Strategy: NormalHuman, DefendedHuman, FastHuman,
 * SedateHuman.
 * 
 * @author Fulvio
 * @version 1.0
 */

public interface HumanStrategy extends Strategy {

}