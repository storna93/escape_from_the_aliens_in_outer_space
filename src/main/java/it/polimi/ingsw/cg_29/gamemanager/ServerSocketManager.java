package it.polimi.ingsw.cg_29.gamemanager;

import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.gamemanager.view.ServerSocketView;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Contains the implementation of the Thread that accept clients socket
 * connections.
 * 
 * @author Luca
 * @version 1.0
 */
public class ServerSocketManager extends Thread {

    /**
     * Number of port of socket.
     */
    private static final int PORT = 29999;

    /**
     * Reference to server.
     */
    private Server server;

    /**
     * Create a new Thread that accept clients socket connection.
     * 
     * @param server
     *            reference to server
     */
    protected ServerSocketManager(Server server) {
        this.server = server;
    }

    /**
     * Starts the server socket, wait player and instantiate new view for any
     * client connected
     *
     * @throws IOException
     */
    private void startSocket() throws IOException {

        ExecutorService executor = Executors.newCachedThreadPool();

        ServerSocket serverSocket = new ServerSocket(PORT);
        this.server.showOutputServerMessage("Server ready. [port: " + PORT + "]");

        while (true) {
            try {

                Socket socketReceive = serverSocket.accept();
                ServerSocketView newClientView = new ServerSocketView(socketReceive, this.server);
                executor.submit(newClientView);

            } catch (IOException e) {
                LoggerClass.getLogger().warn(e);
                break;
            }
        }
        executor.shutdown();
        serverSocket.close();
    }

    /**
     * Start this Thread and launch server socket
     */
    @Override
    public void run() {
        try {
            this.startSocket();
        } catch (IOException e) {
            LoggerClass.getLogger().warn(e);
            this.server.showOutputServerMessage("Error: another ServerSocketManager is already running on this port.");
        }
    }

}
