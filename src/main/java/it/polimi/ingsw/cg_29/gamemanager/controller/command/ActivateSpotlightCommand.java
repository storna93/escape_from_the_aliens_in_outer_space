package it.polimi.ingsw.cg_29.gamemanager.controller.command;

import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.Action;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.ActivateSpotlightAction;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.SpotlightItem;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.view.View;

/**
 * A command to activate spotlight in specific sector.
 *
 * @author Luca
 * @version 1.0
 *
 */
public class ActivateSpotlightCommand extends Command {

    /**
     * Coordinates of sector to be illuminated.
     */
    private String sector;

    /**
     * Create a new command
     * 
     * @param controller
     *            the controller that manages current match
     * @param currentClient
     *            the id of client who sent command
     */
    public ActivateSpotlightCommand(Controller controller, View currentClient, String sector) {
        super(controller, currentClient);
        this.sector = sector;
    }

    /**
     * Launch this command.
     */
    @Override
    public void execute() {

        if (this.matchIsRunning()) {

            Match match = this.getCurrentMatch();
            Action spotlight = new ActivateSpotlightAction(this.getCurrentClientId(), new Coordinates(this.sector.substring(0, 1), this.sector.substring(1)), new SpotlightItem());

            if (spotlight.isValid(match)) {
                this.getCurrentClient().send("SpotlightItem used.");
                this.addActionToObserver(spotlight);
                spotlight.doAction(match);
            } else
                this.getCurrentClient().send("You cannot activate SpotlightItem.");
        } else
            this.getCurrentClient().send("The match is not running.");

    }

}
