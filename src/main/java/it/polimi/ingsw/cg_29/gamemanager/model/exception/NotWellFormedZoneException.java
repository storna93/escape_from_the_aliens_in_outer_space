package it.polimi.ingsw.cg_29.gamemanager.model.exception;

/**
 * Contains the implementation of exception that is launched when a zone doesn't
 * contain essential sectors.
 * 
 * @author Luca
 * @version 1.0
 */
public class NotWellFormedZoneException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Create a new NotWellFormedZoneException with specific message.
     * 
     * @param message
     *            informations about this exception.
     */
    public NotWellFormedZoneException(String message) {
        super("NotWellFormedZoneException: " + message);
    }

}
