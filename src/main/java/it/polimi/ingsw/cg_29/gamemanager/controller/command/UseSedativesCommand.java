package it.polimi.ingsw.cg_29.gamemanager.controller.command;

import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.Action;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.UseSedativeAction;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.view.View;

/**
 * A command to avoid draw card after human activate SedativesItem.
 *
 * @author Luca
 * @version 1.0
 *
 */
public class UseSedativesCommand extends Command {

    /**
     * Create a new command
     * 
     * @param controller
     *            the controller that manages current match
     * @param currentClient
     *            the id of client who sent command
     */
    public UseSedativesCommand(Controller controller, View currentClient) {
        super(controller, currentClient);
    }

    /**
     * Launch this command.
     */
    @Override
    public void execute() {

        if (this.matchIsRunning()) {

            Match match = this.getCurrentMatch();
            Action useSedative = new UseSedativeAction(this.getCurrentClientId());

            if (useSedative.isValid(match)) {
                this.getCurrentClient().send("Sedatives used.");
                this.addActionToObserver(useSedative);
                useSedative.doAction(match);
            } else {
                this.getCurrentClient().send("You cannot use sedatives.");
            }
        } else
            this.getCurrentClient().send("The match is not running.");

    }

}
