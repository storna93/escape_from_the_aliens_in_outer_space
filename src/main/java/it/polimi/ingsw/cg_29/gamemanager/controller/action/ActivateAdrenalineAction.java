package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Card;
import it.polimi.ingsw.cg_29.gamemanager.model.player.FastHuman;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;

/**
 * This Action allows Human Player to activate FastHuman strategy.
 * 
 * @author Fulvio
 * @version 1.0
 *
 */
public class ActivateAdrenalineAction extends ActivateItemAction {

    /**
     * creates an activateAdrenalineAction assigning that to a clientId who has
     * an adrenalineItem, specifying in which RoundState (only BEGINNING) that
     * Action could be valid.
     * 
     * @param clientId
     * @param adrenalineItem
     */
    public ActivateAdrenalineAction(String clientId, Card adrenalineItem) {
        super(clientId, adrenalineItem);
        this.addValidState(RoundState.BEGINNING);
    }

    /**
     * This method defines if a certain Player could activate a AdrenalineItem
     * or not.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     * @return true (if the Action is a valid Action for a certain Player in a
     *         certain Match) false (otherwise).
     */
    @Override
    public boolean isValid(Match currentMatch) {
        return validPlayerAndValidActivationItemCard(currentMatch);
    }

    /**
     * This method does a validated ActivateAdrenalineAction done by a certain
     * Player in a certain Match, activating the fastHuman strategy on this
     * Player.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     */
    @Override
    public void doAction(Match currentMatch) {
        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());
        currentPlayer.getCharacter().activateStrategy(new FastHuman());

        this.publishInfo(this.getClientId() + " used an Adrenaline Item Card.");
    }

}
