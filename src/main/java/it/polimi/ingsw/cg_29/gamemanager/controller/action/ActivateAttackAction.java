package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Card;
import it.polimi.ingsw.cg_29.gamemanager.model.player.AggressiveHuman;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;

/**
 * This Action allows Human Player to activate AggressiveHuman strategy.
 * 
 * @author Fulvio
 * @version 1.0
 *
 */
public class ActivateAttackAction extends ActivateItemAction {

    /**
     * creates an activateAttackAction assigning that to a clientId who has an
     * attackItem, specifying in which RoundState (only
     * BEGINNING,MOVED_DANGEROUS,MOVED_SECURE) that Action could be valid.
     * 
     * @param clientId
     * @param attackItem
     */
    public ActivateAttackAction(String clientId, Card attackItem) {
        super(clientId, attackItem);
        this.addValidState(RoundState.BEGINNING);
        this.addValidState(RoundState.MOVED_DANGEROUS);
        this.addValidState(RoundState.MOVED_SECURE);
    }

    /**
     * This method defines if a certain Player could activate a AttackItem or
     * not.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     * @return true (if the Action is a valid Action for a certain Player in a
     *         certain Match) false (otherwise).
     */
    @Override
    public boolean isValid(Match currentMatch) {
        return validPlayerAndValidActivationItemCard(currentMatch);
    }

    /**
     * This method does a validated ActivateAttackAction done by a certain
     * Player in a certain Match, activating the AggressiveHuman strategy on
     * this Player.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     */
    @Override
    public void doAction(Match currentMatch) {
        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());
        currentPlayer.getCharacter().activateStrategy(new AggressiveHuman());

        this.publishInfo(this.getClientId() + " used an Attack Item Card.");
    }

}
