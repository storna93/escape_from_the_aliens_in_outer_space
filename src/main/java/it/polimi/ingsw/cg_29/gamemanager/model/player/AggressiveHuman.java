package it.polimi.ingsw.cg_29.gamemanager.model.player;

/**
 * This Human Strategy allows to manage the Attack ability of a Human Player.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class AggressiveHuman implements HumanStrategy {
    /**
     * Creates a Strategy AggressiveHuman.
     */
    public AggressiveHuman() {
        super();
    }

    /**
     * addBehaviour allows to add the AggressiveHuman Strategy to a certain
     * Human setting true the canAttack attribute (now this Human could do an
     * AttackAction).
     * 
     * @param character
     *            that is the Human on which will be activated the
     *            AggressiveHuman Strategy.
     */
    @Override
    public void addBehaviour(Character character) {
        character.setCanAttack(true);
    }

    /**
     * removeBehaviour allows to remove the AggressiveHuman Strategy to a
     * certain Human setting false the canAttack attribute (now this Human could
     * not do any AttackAction).
     * 
     * @param character
     *            that is the Human on which will be deactivated the
     *            AggressiveHuman Strategy.
     */
    @Override
    public void removeBehaviour(Character character) {
        character.setCanAttack(false);
    }

}
