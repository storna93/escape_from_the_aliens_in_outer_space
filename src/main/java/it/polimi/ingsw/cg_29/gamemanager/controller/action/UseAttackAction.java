package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.AttackItem;
import it.polimi.ingsw.cg_29.gamemanager.model.player.AggressiveHuman;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;

/**
 * This Action allows Human Player to do an Attack like an Alien Player.
 * 
 * @author Fulvio
 * @version 1.0
 *
 */
public class UseAttackAction extends Action {
    /**
     * creates an UseAttackAction assigning that to a clientId who wants to do
     * Attack like an Alien Attack, specifying in which RoundState (only
     * MOVED_SECURE, MOVED_DANGEROUS) that Action could be valid.
     * 
     * @param clientId
     */
    public UseAttackAction(String clientId) {
        super(clientId);

        this.addValidState(RoundState.MOVED_DANGEROUS);
        this.addValidState(RoundState.MOVED_SECURE);
    }

    /**
     * This method defines if a certain Player could useAttack in a certain
     * Match or not.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     * @return true (if the Action is a valid Action for a certain Player in a
     *         certain Match) false (otherwise).
     */
    @Override
    public boolean isValid(Match currentMatch) {
        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());
        if (!validPlayer(currentPlayer))
            return false;
        if (!(currentPlayer.getCharacter() instanceof Human))
            return false;
        if (!currentPlayer.getCharacter().getCanAttack())
            return false;

        return validSectorWhereAttack(currentMatch);
    }

    /**
     * This method does a validated UseAttackAction done by a certain Player in
     * a certain Match, allowing this Player to do an Attack like an Alien
     * Attack.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     */
    @Override
    public void doAction(Match currentMatch) {
        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());

        this.publishInfo(currentPlayer.getClientId() + " attacked in " + currentPlayer.getCurrentPosition().printToString() + " because he has activated Attack Item Card before.");

        this.doAttack(currentMatch, currentPlayer);
        this.discardUsedItemCard(currentMatch, new AttackItem());
        currentPlayer.getCharacter().deactivateStrategy(new AggressiveHuman());

    }

}
