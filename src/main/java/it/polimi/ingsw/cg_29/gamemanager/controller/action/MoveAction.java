package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.AdrenalineItem;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Card;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.EscapeGreen;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.EscapeRed;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Alien;
import it.polimi.ingsw.cg_29.gamemanager.model.player.FastHuman;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.PlayerState;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.AlienSector;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.DangerousSector;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.EscapeHatchSector;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.HumanSector;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Sector;

/**
 * This Action allows every Player move onto a specified Sector.
 * 
 * @author Fulvio
 * @version 1.0
 *
 */
public class MoveAction extends Action {

    /**
     * destination is Sector onto a Player wants to move with this.
     */
    private Coordinates destination;

    /**
     * creates a MoveAction assigning that to a clientId who want to move and to
     * the destination of the MoveAction, specifying in which RoundState (only
     * BEGINNING) that Action could be valid.
     * 
     * @param clientId
     * @param destination
     */
    public MoveAction(String clientId, Coordinates destination) {
        super(clientId);

        this.destination = destination;
        this.addValidState(RoundState.BEGINNING);
    }

    /**
     * This method defines if a certain Player could move onto a certain Sector
     * or not in a certain moment of the currentMatch.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     * @return true (if the Action is a valid Action for a certain Player in a
     *         certain Match) false (otherwise).
     */
    @Override
    public boolean isValid(Match currentMatch) {

        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());
        if (!validPlayer(currentPlayer))
            return false;

        Coordinates currentPosition = currentPlayer.getCurrentPosition();
        int range = currentPlayer.getCharacter().getRangeOfMove();

        Sector currentSector = currentMatch.getCurrentZone().getSectorByCoordinates(currentPosition);
        Sector destinationSector = currentMatch.getCurrentZone().getSectorByCoordinates(destination);
        if (destination == null)
            return false;

        if (currentPlayer.getCharacter() instanceof Alien && destinationSector instanceof EscapeHatchSector) {
            return false;
        }

        if (destinationSector instanceof EscapeHatchSector && !((EscapeHatchSector) destinationSector).isUsable()) {
            return false;
        }

        if (destinationSector instanceof AlienSector) {
            return false;
        }

        if (destinationSector instanceof HumanSector) {
            return false;
        }

        if (!currentSector.getNearSectors(range).contains(this.destination)) {
            return false;
        }

        return true;
    }

    /**
     * This method does a validated MoveAction done by a certain Player in a
     * certain Match, moving onto the Sector chosen by this Player and then
     * activating the effects of this Sector in order to Sector type.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     */
    @Override
    public void doAction(Match currentMatch) {
        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());

        currentPlayer.setCurrentPosition(this.destination);

        this.publishInfo(this.getClientId() + " moved.");

        if (currentPlayer.getCharacter() instanceof Human && currentPlayer.getCharacter().getRangeOfMove() == 2) {
            currentPlayer.getCharacter().deactivateStrategy(new FastHuman());
            this.discardUsedItemCard(currentMatch, new AdrenalineItem());
        }

        Sector currentPlayerSector = currentMatch.getCurrentZone().getSectorByCoordinates(currentPlayer.getCurrentPosition());

        if (currentPlayerSector instanceof EscapeHatchSector) {

            this.publishInfo(this.getClientId() + " reached Escape Hatch in " + this.destination.printToString());

            Card escapeCard = currentMatch.getEscapeHatchDeck().drawCard();
            currentMatch.getEscapeHatchDeck().discardCard(escapeCard);

            this.publishInfo(this.getClientId() + " has drawn an " + escapeCard.getClass().getSimpleName() + "Card.");

            if (escapeCard instanceof EscapeRed) {
                this.publishInfo("The Escape Hatch in " + this.destination.printToString() + " has been damaged and cannot be used.");
                currentPlayer.setRoundState(RoundState.ENDING);
            }
            if (escapeCard instanceof EscapeGreen) {
                this.publishInfo(this.getClientId() + " has successfully escaped from the spaceship.");
                this.publishInfo("The Escape Hatch in " + this.destination.printToString() + " has been blocked.");
                currentPlayer.setPlayerState(PlayerState.WINNER);
                this.endOfTurn(currentMatch, currentPlayer);
            }

            ((EscapeHatchSector) currentPlayerSector).block();

        } else if (currentPlayerSector instanceof DangerousSector) {
            currentPlayer.setRoundState(RoundState.MOVED_DANGEROUS);
        } else {
            currentPlayer.setRoundState(RoundState.MOVED_SECURE);
        }

    }

}
