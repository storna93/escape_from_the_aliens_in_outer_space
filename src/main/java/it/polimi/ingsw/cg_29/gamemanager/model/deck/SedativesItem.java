package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * When used, this Card allows the Human Player to not draw any
 * DangerousSectorCard after moved onto a DangerousSector .
 * 
 * @author Fulvio
 * @version 1.0
 */

public class SedativesItem extends ItemCard {
    /**
     * Creates a Card SedativesItem.
     */
    public SedativesItem() {
        super();
    }

}
