package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * This is a Deck composed only of ItemCards.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class ItemDeck extends Deck {

    /**
     * Construct a ItemDeck without initializing any attributes. Then a
     * ItemDeckCreator will fill this ItemDeck (that implements a factoryMethod)
     */
    protected ItemDeck() {
        super();

    }

}
