package it.polimi.ingsw.cg_29.gamemanager.model.zone;

/**
 * Implementation of AbstractFactory.
 * 
 * @author Luca
 * @version 1.0
 *
 */
public abstract class ZoneCreator {

    /**
     * Implementation of constructor
     */
    public ZoneCreator() {
    }

    /**
     * Create a specific zone and pass it to the caller.
     * 
     * @return specific zone.
     */
    public abstract Zone createZone();

}
