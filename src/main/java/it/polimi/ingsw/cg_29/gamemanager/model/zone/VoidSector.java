package it.polimi.ingsw.cg_29.gamemanager.model.zone;

/**
 * Implementation of VoidSector that represents the holes in zone.
 * 
 * @author Luca
 * @version 1.0
 */
public class VoidSector extends Sector {

    /**
     * Return a VoidSector with specific coordinates and reference to zone.
     * 
     * @param row
     *            reference to row of the zone by integer number.
     * @param col
     *            reference to column of the zone by integer number.
     * @param zone
     *            reference to the zone.
     * @throws NullPointerException
     */
    protected VoidSector(int row, int col, Zone zone) {
        super(row, col, zone);
    }

    /**
     * Return a VoidSector with specific coordinates and reference to zone.
     * 
     * @param col
     *            reference to column of the zone by string.
     * @param row
     *            reference to row of the zone by string.
     * @param zone
     *            reference to the zone.
     * @throws NullPointerException
     *             , NumberFormatException
     */
    public VoidSector(String col, String row, Zone zone) {
        super(col, row, zone);
    }

    /**
     * @return a "pretty print" string representing this Sector.
     */
    @Override
    public String printToString() {
        return "        ";
    }

}
