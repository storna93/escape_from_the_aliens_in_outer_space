package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * This is a Deck composed only of CharacterCards.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class CharacterDeck extends Deck {

    /**
     * Construct a CharacterDeck without initializing any attributes. Then a
     * CharacterDeckCreator will fill this CharacterDeck (that implements a
     * factoryMethod)
     */
    protected CharacterDeck() {
        super();
    }

}
