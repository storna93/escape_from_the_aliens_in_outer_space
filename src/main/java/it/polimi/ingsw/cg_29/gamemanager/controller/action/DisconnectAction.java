package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Human;
import it.polimi.ingsw.cg_29.gamemanager.model.player.NormalHuman;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.PlayerState;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;

/**
 * This Action allows every Player to disconnect from the currentMatch.
 * 
 * @author Fulvio
 * @version 1.0
 *
 */
public class DisconnectAction extends Action {
    /**
     * creates a DisconnectAction assigning that to a clientId. specifying in
     * which RoundState (only
     * STANDBY,BEGINNING,MOVED_SECURE,MOVED_DANGEROUS,FAKE_NOISE,ENDING) that
     * Action could be valid.
     * 
     * @param clientId
     */
    public DisconnectAction(String clientId) {
        super(clientId);
        this.addValidState(RoundState.STANDBY);
        this.addValidState(RoundState.BEGINNING);
        this.addValidState(RoundState.MOVED_SECURE);
        this.addValidState(RoundState.MOVED_DANGEROUS);
        this.addValidState(RoundState.FAKE_NOISE);
        this.addValidState(RoundState.ENDING);

    }

    /**
     * This method defines if a certain Player could disconnect from a certain
     * Match or not.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     * @return true (if the Action is a valid Action for a certain Player in a
     *         certain Match) false (otherwise).
     */
    @Override
    public boolean isValid(Match currentMatch) {
        return validConnectionCommand(currentMatch);
    }

    /**
     * This method does a validated DisconnectAction done by a certain Player in
     * a certain Match, making this Player a not Playing Player.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     */
    @Override
    public void doAction(Match currentMatch) {

        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());

        if (currentPlayer.getCharacter() instanceof Human) {
            currentPlayer.getCharacter().activateStrategy(new NormalHuman());
        }

        this.publishInfo(this.getClientId() + " has disconnected from the match.");

        if (currentPlayer.getPlayerState() == PlayerState.PLAYING)
            currentPlayer.setPlayerState(PlayerState.DISCONNECTED);

        if (currentPlayer.getRoundState() != RoundState.STANDBY)
            this.endOfTurn(currentMatch, currentPlayer);

        currentPlayer.setRoundState(RoundState.DISCONNECTED);
    }
}
