package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * It is the super type to manage all CharacterCards: HumanCard and AlienCard.
 * 
 * @author Fulvio
 * @version 1.0
 */

public abstract class CharacterCard extends Card {
    /**
     * Creates a generic CharacterCard.
     */
    protected CharacterCard() {
        super();
    }
}
