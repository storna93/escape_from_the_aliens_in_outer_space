package it.polimi.ingsw.cg_29.gamemanager.model.player;

/**
 * Alien class allows to activate or deactivate an Alien Strategy on a certain
 * Alien
 * 
 * @author Fulvio
 * @version 1.0
 */

public class Alien extends Character {

    /**
     * creates an Alien Character.
     *
     * @param behaviour
     *            , that is the first Alien Strategy activated on a certain
     *            Alien
     */
    public Alien(AlienStrategy behaviour) {
        super(behaviour);
    }

}
