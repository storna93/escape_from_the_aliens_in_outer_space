package it.polimi.ingsw.cg_29.gamemanager.publisher;

import it.polimi.ingsw.cg_29.gamemanager.commons.BrokerObjectInterface;
import it.polimi.ingsw.cg_29.gamemanager.commons.ClientRMIObjectInterface;

/**
 * Defines the remote object Broker, that offers the method to subscribe to the
 * real unique Broker.
 * 
 * @author Fulvio
 * @version 1.0
 */
public class BrokerObject implements BrokerObjectInterface {
    /**
     * broker is the reference that associates Broker object to the real unique
     * Server.
     */
    private Broker broker;

    /**
     * creates a remote object BrokerObject that is necessary to the RMI
     * communication between Broker and Client (subscriber).
     * 
     * @param broker
     * @param server
     */
    public BrokerObject(Broker broker) {
        this.broker = broker;
    }

    /**
     * This method allows client to subscribe to the broker of the match in
     * which him is involved. The method also updates the list of subscriber
     * interfaces that are subscribed to the broker.
     * 
     * @param clientToSubscribe
     *            is the Subcriber's remote interface that the broker can use to
     *            publish message.
     */
    @Override
    public void subscribe(ClientRMIObjectInterface clientToSubscribe, String controllerId) {

        SubscriberServerRMIThread newClientSubscriber = new SubscriberServerRMIThread(clientToSubscribe);

        broker.addSubscriber(controllerId, newClientSubscriber);
    }
}
