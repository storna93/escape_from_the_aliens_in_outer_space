package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * This is a Deck composed only of DangerousSectorCards.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class DangerousSectorDeck extends Deck {

    /**
     * Construct a DangerousSectorDeck without initializing any attributes. Then
     * a DangerousSectorDeckCreator will fill this DangerousSectorDeck (that
     * implements a factoryMethod)
     */
    protected DangerousSectorDeck() {
        super();
    }

}
