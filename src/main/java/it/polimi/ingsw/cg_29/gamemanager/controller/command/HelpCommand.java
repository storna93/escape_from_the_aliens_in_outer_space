package it.polimi.ingsw.cg_29.gamemanager.controller.command;

import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;
import it.polimi.ingsw.cg_29.gamemanager.view.View;

/**
 * A command to show well formed command.
 *
 * @author Luca
 * @version 1.0
 *
 */
public class HelpCommand extends Command {

    /**
     * Create a new command
     * 
     * @param controller
     *            the controller that manages current match
     * @param currentClient
     *            the id of client who sent command
     */
    public HelpCommand(Controller controller, View currentClient) {
        super(controller, currentClient);
    }

    /**
     * Launch this command.
     */
    @Override
    public void execute() {

        String helpList = "COMMAND:\n";

        helpList += "\t chat(message): send message to other players;\n";
        helpList += "\t set_zone(NUMBER_OF_ZONE): set zone before starting (1 = Galilei; 2 = Fermi; 3 = Galvani);\n";
        helpList += "\t show_zone: view zone;\n";
        helpList += "\t show_state: view player state;\n";
        helpList += "\t get_log: show a complete log of this match;\n";
        helpList += "\t move(SECTOR_COORDINATES): move to sector with specific sector_coordinates;\n";
        helpList += "\t draw: draw a card if you're in Dangerous Sector;\n";
        helpList += "\t activate_item(NUMBER_OF_CARD): activate a specific item card you have in hand (1; 2; 3);\n";
        helpList += "\t activate_spotlight(SECTOR_COORDINATES): activate spotlight specifing sector coordinates;\n";
        helpList += "\t attack: use attack if you're an Alien;\n";
        helpList += "\t use_attack: use attack if you're an Human and you have activated AttackItem;\n";
        helpList += "\t use_sedatives: avoid draw if you're an Human and you have activated SedativesItem;\n";
        helpList += "\t fake_noise(SECTOR_COORDINATES): say to other player a fake noise specifing sector coordinates;\n";
        helpList += "\t pass: end round;\n";
        helpList += "\t disconnect: disconnect from the match;\n";
        helpList += "\t reconnect: reconnect to the match;\n";

        this.getCurrentClient().send(helpList);

    }

}
