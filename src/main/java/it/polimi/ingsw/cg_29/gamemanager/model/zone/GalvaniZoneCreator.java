package it.polimi.ingsw.cg_29.gamemanager.model.zone;

/**
 * Implementation of AbstractFactory. This is a ConcreteFactory of Galvani Zone.
 * 
 * @author Luca
 * @version 1.0
 */
public class GalvaniZoneCreator extends ZoneCreator {

    /**
     *
     */
    public GalvaniZoneCreator() {
        super();
    }

    /**
     * @return GalvaniZoneCreator representation, created by parsing a String
     *         matrix
     */
    @Override
    public GalvaniZone createZone() {
        GalvaniZone zone = new GalvaniZone();

        String[][] mapString = { { "V", "V", "V", "D", "D", "E", "V", "V", "V", "D", "D", "D", "D", "D", "V", "E", "D", "D", "D", "D", "D", "S", "V" }, { "V", "D", "D", "V", "V", "D", "S", "D", "D", "V", "V", "V", "D", "S", "D", "D", "S", "V", "D", "D", "V", "D", "V" }, { "D", "V", "V", "D", "D", "V", "V", "D", "D", "D", "S", "S", "V", "V", "S", "D", "D", "D", "V", "V", "D", "D", "D" }, { "D", "S", "D", "V", "V", "D", "D", "V", "V", "D", "D", "V", "D", "D", "V", "V", "D", "D", "D", "V", "D", "D", "D" }, { "D", "V", "D", "V", "D", "V", "V", "S", "D", "V", "D", "D", "S", "V", "S", "D", "V", "S", "D", "V", "V", "V", "D" }, { "D", "V", "D", "V", "D", "V", "D", "V", "V", "D", "D", "A", "D", "D", "V", "V", "D", "V", "D", "V", "V", "V", "D" },
                { "D", "V", "D", "S", "D", "V", "D", "S", "D", "V", "V", "V", "V", "V", "D", "V", "D", "V", "D", "V", "V", "S", "D" }, { "S", "V", "D", "V", "D", "V", "D", "V", "D", "D", "V", "H", "V", "D", "D", "V", "D", "V", "D", "V", "S", "D", "D" }, { "D", "V", "D", "V", "D", "V", "D", "D", "V", "V", "S", "D", "S", "V", "S", "D", "D", "V", "D", "S", "V", "V", "D" }, { "D", "E", "D", "V", "D", "D", "S", "V", "D", "D", "S", "V", "V", "D", "D", "V", "V", "D", "D", "V", "D", "V", "D" }, { "D", "S", "D", "V", "V", "D", "D", "D", "V", "V", "D", "D", "D", "S", "V", "D", "D", "V", "V", "D", "D", "E", "D" }, { "D", "D", "V", "D", "D", "V", "V", "S", "D", "V", "V", "S", "V", "D", "D", "V", "V", "V", "V", "D", "D", "V", "D" },
                { "V", "V", "D", "V", "V", "V", "D", "D", "V", "V", "D", "D", "V", "V", "V", "V", "V", "D", "D", "V", "V", "D", "D" }, { "V", "V", "V", "V", "V", "V", "V", "V", "D", "D", "S", "V", "S", "D", "S", "D", "S", "D", "D", "D", "D", "V", "V" } };

        for (int r = 0; r < mapString.length; r++) {
            for (int c = 0; c < mapString[0].length; c++) {
                zone.addSectorByString(r, c, mapString[r][c]);
            }
        }

        return zone;
    }

}
