package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * It is the super type to manage all DangerousSectorCards: NoiseInYourSector,
 * NoiseInYourSectorIcon, NoiseInAnySector, NoiseInAnySectorIcon and Silence.
 * 
 * @author Fulvio
 * @version 1.0
 */

public abstract class DangerousSectorCard extends Card {
    /**
     * Creates a generic DangerousSectorCard.
     */
    protected DangerousSectorCard() {
        super();
    }

}
