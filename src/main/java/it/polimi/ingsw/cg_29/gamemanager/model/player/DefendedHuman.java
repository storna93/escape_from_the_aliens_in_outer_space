package it.polimi.ingsw.cg_29.gamemanager.model.player;

/**
 * This Human Strategy allows to manage the Defense ability of a Human Player.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class DefendedHuman implements HumanStrategy {
    /**
     * Creates a Strategy DefendedHuman.
     */
    public DefendedHuman() {
        super();
    }

    /**
     * addBehaviour allows to add the DefendedHuman Strategy to a certain Human
     * setting true the canDefense attribute (now this Human could receive an
     * AttackAction without lose).
     * 
     * @param character
     *            that is the Human on which will be activated the DefendedHuman
     *            Strategy.
     */
    @Override
    public void addBehaviour(Character character) {
        character.setCanDefense(true);
    }

    /**
     * removeBehaviour allows to remove the DefendedHuman Strategy to a certain
     * Human setting false the canDefense attribute (now this Human if receive
     * an AttackAction will be lost).
     * 
     * @param character
     *            that is the Human on which will be deactivated the
     *            DefendedHuman Strategy.
     */
    @Override
    public void removeBehaviour(Character character) {
        character.setCanDefense(false);

    }

}
