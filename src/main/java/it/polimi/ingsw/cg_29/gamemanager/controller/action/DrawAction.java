package it.polimi.ingsw.cg_29.gamemanager.controller.action;

import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Card;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Deck;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.NoiseInAnySectorIcon;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.NoiseInYourSector;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.NoiseInYourSectorIcon;
import it.polimi.ingsw.cg_29.gamemanager.model.deck.Silence;
import it.polimi.ingsw.cg_29.gamemanager.model.player.Player;
import it.polimi.ingsw.cg_29.gamemanager.model.player.RoundState;
import it.polimi.ingsw.cg_29.gamemanager.model.zone.Coordinates;

import java.util.List;

/**
 * This Action allows every Player to draw a DangerousSectorCard.
 * 
 * @author Fulvio
 * @version 1.0
 *
 */
public class DrawAction extends Action {

    /**
     * NUM_ITEM_CARD is the maximum number of itemCard in the Hand of a certain
     * Player during the Match.
     */
    public static final int NUM_ITEM_CARD = 3;
    /**
     * drawResultForTesting is the result of a DrawAction done and it is used
     * only for doing tests.
     * 
     */
    private String drawResultForTesting = "";
    /**
     * drawnCardForTesting is the DangerousSectorCard drawn on a DrawAction done
     * and it is used only for doing tests.
     * 
     */
    private Card drawnCardForTesting;

    /**
     * create a DrawAction assigning that to a clientId, specifying in which
     * RoundState (only MOVED_DANGEROUS) that Action could be valid.
     * 
     * @param clientId
     * @param teleportItem
     */
    public DrawAction(String clientId) {
        super(clientId);
        this.addValidState(RoundState.MOVED_DANGEROUS);

    }

    /**
     * This method defines if a certain Player could draw a DangerousSectorCard
     * or not.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     * @return true (if the Action is a valid Action for a certain Player in a
     *         certain Match) false (otherwise).
     */
    @Override
    public boolean isValid(Match currentMatch) {

        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());
        if (!validPlayer(currentPlayer))
            return false;
        if (currentPlayer.getCharacter().getBeSedate())
            return false;

        return true;

    }

    /**
     * This method does a validated DrawAction done by a certain Player in a
     * certain Match, drawing a DangerousSectorCard and it has an Icon then
     * drawing a ItemCard.
     * 
     * @param currentMatch
     *            that is the Match in which a certain Player wants to do a
     *            certain Action.
     */
    @Override
    public void doAction(Match currentMatch) {

        if (currentMatch.getDangerousSectorDeck().getCardList().isEmpty())
            currentMatch.getDangerousSectorDeck().reloadDeck();

        Card drawnDangerCard = currentMatch.getDangerousSectorDeck().drawCard();
        drawnCardForTesting = drawnDangerCard;
        currentMatch.getDangerousSectorDeck().discardCard(drawnDangerCard);
        Player currentPlayer = currentMatch.getSpecificPlayerById(this.getClientId());
        Coordinates currentPosition = currentPlayer.getCurrentPosition();

        currentPlayer.setRoundState(RoundState.FAKE_NOISE);

        String noiseStringIndicator = "[NOISE] ";

        if (drawnDangerCard instanceof NoiseInYourSector || drawnDangerCard instanceof NoiseInYourSectorIcon) {
            this.publishInfo(noiseStringIndicator + currentPlayer.getClientId() + ": NOISE IN SECTOR " + currentPosition.printToString());
            this.drawResultForTesting += noiseStringIndicator + currentPlayer.getClientId() + ": NOISE IN SECTOR " + currentPosition.printToString();
            currentPlayer.setRoundState(RoundState.ENDING);
        }

        if (drawnDangerCard instanceof Silence) {
            this.publishInfo(noiseStringIndicator + currentPlayer.getClientId() + ": SILENCE");
            this.drawResultForTesting += noiseStringIndicator + currentPlayer.getClientId() + ": SILENCE";
            currentPlayer.setRoundState(RoundState.ENDING);
        }

        Deck itemDeck = currentMatch.getItemDeck();
        List<Card> itemCardList = currentPlayer.getItemCardList();

        if (drawnDangerCard instanceof NoiseInYourSectorIcon || drawnDangerCard instanceof NoiseInAnySectorIcon) {

            if (itemCardList.size() == NUM_ITEM_CARD && (!itemDeck.isEmpty() || !itemDeck.getDiscardPile().isEmpty())) {
                this.discardUsedItemCard(currentMatch, itemCardList.get(0));
                itemCardList.remove(0);
            }

            if (!itemDeck.isEmpty()) {
                Card drawnCard = currentMatch.getItemDeck().drawCard();
                currentPlayer.addItemCard(drawnCard);
            } else if (!itemDeck.getDiscardPile().isEmpty()) {
                itemDeck.reloadDeck();
                Card drawnCard = currentMatch.getItemDeck().drawCard();
                currentPlayer.addItemCard(drawnCard);
            }
        }

    }

    /**
     * @return the drawResultForTesting
     */
    public String getDrawResultForTesting() {
        return drawResultForTesting;
    }

    /**
     * @return the drawnCardForTesting
     */
    public Card getDrawnCardForTesting() {
        return drawnCardForTesting;
    }

}
