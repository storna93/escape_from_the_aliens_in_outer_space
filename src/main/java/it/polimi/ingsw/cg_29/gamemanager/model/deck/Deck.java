package it.polimi.ingsw.cg_29.gamemanager.model.deck;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * It is the super type to manage several Deck type: CharacterDeck,
 * DangerousSectorDeck, EscapeHatchDeck and ItemDeck.
 * 
 * @author Fulvio
 * @version 1.0
 */
public abstract class Deck {

    /**
     * cardList is the list of card whence a Player can draw a Card; cardList
     * related to a certain Deck.
     */
    private List<Card> cardList;

    /**
     * discardPile is the list of card where a drawn and used Card could be put;
     * discardPile related to a certain Deck.
     */
    private List<Card> discardPile;

    /**
     * Construct a Deck without initializing any attributes.
     */
    protected Deck() {
        cardList = new ArrayList<Card>();
        discardPile = new ArrayList<Card>();
    }

    /**
     * addCard allows to add a Card to the cardList of a certain Deck.
     * 
     * @param card
     */
    protected void addCard(Card card) {
        this.cardList.add(card);
    }

    /**
     * deckSuffle allows to shuffle the cardList of a certain Deck.
     */
    public void deckShuffle() {

        Collections.shuffle(this.cardList);
    }

    /**
     * Getter to obtain the cardList of a Deck.
     * 
     * @return cardList whence a Player can draw a Card.
     */
    public List<Card> getCardList() {
        return cardList;
    }

    /**
     * Getter to obtain the discardPile of a Deck.
     * 
     * @return cardList where a Player can discard a used Card.
     */
    public List<Card> getDiscardPile() {
        return discardPile;
    }

    /**
     * isEmpty allows to know if the cardList has size()==0.
     * 
     * @return true (if a list of Card is empty), false (otherwise).
     */
    public boolean isEmpty() {
        if (this.cardList.isEmpty())
            return true;
        return false;
    }

    /**
     * drawCard allows to draw a Card from a cardList of a certain Deck.
     * 
     * @return drawnCard that is the Card has just drawn.
     */
    public Card drawCard() {

        if (this.getCardList().isEmpty())
            return null;

        return this.getCardList().remove(this.getCardList().size() - 1);

    }

    /**
     * discardCard allows to put a used Card onto the discardPile of a certain
     * Deck.
     * 
     * @param card
     *            that is the Card has just discarded.
     */
    public void discardCard(Card card) {

        this.getDiscardPile().add(card);
    }

    /**
     * reloadDeck allows to add all the Cards of the discardPile to the cardList
     * of a certain Deck. Then it shuffles cardList.
     */
    public void reloadDeck() {

        for (Card currentCard : this.getDiscardPile()) {

            this.getCardList().add(currentCard);

        }
        this.getDiscardPile().clear();
        this.deckShuffle();

    }

}
