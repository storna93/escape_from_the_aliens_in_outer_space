package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * When used, this Card allows every Players to see the Players that are onto a
 * specific Sector and the six adjacent Sectors.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class SpotlightItem extends ItemCard {
    /**
     * Creates a Card SpotlightItem.
     */
    public SpotlightItem() {
        super();
    }

}
