package it.polimi.ingsw.cg_29.gamemanager.commons;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Defines the interface of the ServerRMIView remote object. Every clients that
 * wants to remain connected to the ServerRMI has a reference to a ServerRMIView
 * object remote, that is the object that receive from the Server the message
 * response sends to the client who put the request. Every client could send a
 * command to the ServerRMI calling the remote method sendCommand() on this.
 * 
 * @author Fulvio
 * @version 1.0
 */
public interface ServerRMIViewObjectInterface extends Remote {
    /**
     * This method could be remotely call by the respective client for send a
     * command message to the Server
     * 
     * @param msg
     * @throws RemoteException
     */
    public void sendCommand(String msg) throws RemoteException;
}
