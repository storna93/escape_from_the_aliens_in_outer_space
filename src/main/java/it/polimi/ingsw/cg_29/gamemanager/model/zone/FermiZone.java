package it.polimi.ingsw.cg_29.gamemanager.model.zone;

/**
 * This class represent Fermi Zone.
 * 
 * @author Luca
 * @version 1.0
 *
 */
public class FermiZone extends Zone {

    /**
     * Create zone.
     */
    public FermiZone() {
        super();
    }

}
