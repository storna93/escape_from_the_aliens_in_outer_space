package it.polimi.ingsw.cg_29.gamemanager.controller;

import java.util.Observable;
import java.util.Observer;

/**
 * This class observes actions to start a new TimerToEndRound or to send
 * information about the result of an action.
 * 
 * @author Luca
 * @version 1.0
 */
public class ActionObserver implements Observer {

    /**
     * Reference to the controller of the match.
     */
    private Controller controller;

    /**
     * Creates a new ActionObserver.
     * 
     * @param controller
     *            reference to the controller of the match.
     */
    public ActionObserver(Controller controller) {
        this.controller = controller;
    }

    /**
     * When an action notifies this class starts a new TimerToEndRound or sends
     * information about the result of the action.
     */
    @Override
    public void update(Observable o, Object arg) {
        String s = (String) arg;
        if (s.matches("\\[TIMER START\\].*")) {
            String idClient = s.substring("[TIMER START]".length());
            this.controller.setNewTimer(idClient);
            return;
        }
        if (s.matches("\\[TIMER STOP\\].*")) {
            this.controller.interruptTimer();
            return;
        }
        this.controller.sendToAll(s);
    }

}
