package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * This ItemDeckCreator realizes a factoryMethod on the object ItemDeck using
 * the method createDeck().
 * 
 * @author Fulvio
 * @version 1.0
 */

public class ItemDeckCreator extends DeckCreator {
    /**
     * Creates a creator for a ItemDeckCreator.
     */
    public ItemDeckCreator() {
        super();
    }

    /**
     * This method allows to create a new object ItemDeck and then to populate
     * it with: 2 AdrenalineItem; 3 TeleportItem; 1 DefenseItem; 2 AttackItem; 2
     * SedativesItem.
     * 
     * @return d that is the new shuffled ItemDeck that has been realized using
     *         the factoryMethod.
     */
    @Override
    public ItemDeck createDeck() {

        ItemDeck d = new ItemDeck();

        for (int i = 0; i < 2; i++) {
            ItemCard c = new AdrenalineItem();
            d.addCard(c);
        }
        for (int i = 0; i < 3; i++) {
            ItemCard c = new TeleportItem();
            d.addCard(c);
        }
        for (int i = 0; i < 1; i++) {
            ItemCard c = new DefenseItem();
            d.addCard(c);
        }
        for (int i = 0; i < 2; i++) {
            ItemCard c = new AttackItem();
            d.addCard(c);
        }
        for (int i = 0; i < 2; i++) {
            ItemCard c = new SpotlightItem();
            d.addCard(c);
        }
        for (int i = 0; i < 2; i++) {
            ItemCard c = new SedativesItem();
            d.addCard(c);
        }

        d.deckShuffle();

        return d;
    }

}
