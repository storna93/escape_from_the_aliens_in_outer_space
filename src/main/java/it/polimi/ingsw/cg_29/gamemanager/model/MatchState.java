package it.polimi.ingsw.cg_29.gamemanager.model;

/**
 * Contains the possible states of a Match.
 * 
 * @author Luca
 * @version 1.0
 */
public enum MatchState {
    RUNNING, FINISHED
}
