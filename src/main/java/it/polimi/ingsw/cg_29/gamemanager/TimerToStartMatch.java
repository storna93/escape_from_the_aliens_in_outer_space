package it.polimi.ingsw.cg_29.gamemanager;

import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;

/**
 * Thread that launch match after selected time.
 * 
 * @author Luca
 * @version 1.0
 */
public class TimerToStartMatch extends Thread {

    /**
     * Time in second to end round.
     */
    public static final int SECONDS_TO_START_MATCH = 15;

    /**
     * Reference to current server
     */
    private final Server server;

    /**
     * 
     * Return a new TimerToStartMatch whit the reference to the server.
     * 
     * @param server
     */
    public TimerToStartMatch(Server server) {
        this.server = server;
    }

    /**
     * Starts Match after selected time.
     */
    @Override
    public void run() {
        Controller controller = this.server.getCurrentController();
        try {
            controller.sendToAll("Match starts in " + SECONDS_TO_START_MATCH + " seconds.");
            Thread.sleep((long) SECONDS_TO_START_MATCH * 1000);
            if (controller.getNumberOfPlayer() < Server.MIN_PLAYERS)
                controller.sendToAll("Some player has disconnected. Waiting for new players ...");
            else {
                this.server.setNewController();
                controller.startMatch();
            }
        } catch (InterruptedException e) {
            LoggerClass.getLogger().warn(e);
            controller.showOutputControllerMessage("Reached max number of player.");
        }
    }

}
