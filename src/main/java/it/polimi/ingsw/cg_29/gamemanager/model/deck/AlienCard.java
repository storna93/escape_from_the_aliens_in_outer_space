package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * When used, this Card defines the Character Alien for a certain Player.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class AlienCard extends CharacterCard {
    /**
     * Creates a character Card Alien.
     */
    protected AlienCard() {
        super();
    }

}
