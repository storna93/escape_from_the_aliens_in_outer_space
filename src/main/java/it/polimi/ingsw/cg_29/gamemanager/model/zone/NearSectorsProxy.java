package it.polimi.ingsw.cg_29.gamemanager.model.zone;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Implementation of a Proxy that calculates and caches the coordinates of
 * sectors at specific distance from one.
 * 
 * @author Luca
 * @version 1.0
 */
public class NearSectorsProxy {

    /**
     * reference to the Zone to verify near sectors are valid.
     */
    private final Zone zone;
    /**
     * contains lists of coordinates (for example: the second list contains the
     * coordinates of sectors (valid) at distance 2)
     */
    private List<Set<Coordinates>> nearSectorsCoordinatesList;

    /**
     * Create a new NearSectorProxy and set current sector coordinates in the
     * list at index 0
     * 
     * @param currentSectorCoordinates
     *            coordinates of current sector
     * @param zone
     *            reference to zone
     * @throws NullPointerException
     */
    protected NearSectorsProxy(Coordinates currentSectorCoordinates, Zone zone) {
        if (zone == null)
            throw new NullPointerException("A NearSectorsProxy can't have zone null");

        this.nearSectorsCoordinatesList = new ArrayList<Set<Coordinates>>();
        Set<Coordinates> currentSector = new HashSet<Coordinates>();
        currentSector.add(currentSectorCoordinates);
        this.nearSectorsCoordinatesList.add(0, currentSector);
        this.zone = zone;
    }

    /**
     * Implementation of hashCode
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((nearSectorsCoordinatesList == null) ? 0 : nearSectorsCoordinatesList.hashCode());
        result = prime * result + ((zone == null) ? 0 : zone.hashCode());
        return result;
    }

    /**
     * Implementation of equals
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        NearSectorsProxy other = (NearSectorsProxy) obj;
        if (nearSectorsCoordinatesList == null) {
            if (other.nearSectorsCoordinatesList != null)
                return false;
        } else if (!nearSectorsCoordinatesList.equals(other.nearSectorsCoordinatesList))
            return false;
        if (zone == null) {
            if (other.zone != null)
                return false;
        } else if (!zone.equals(other.zone))
            return false;
        return true;
    }

    /**
     * Implementation of toString Intended only for debugging.
     */
    @Override
    public String toString() {
        return "NearSectorsProxy [zone=" + zone.getClass() + ", nearSectorsCoordinatesList=" + nearSectorsCoordinatesList + "]";
    }

    /**
     * Calculate the sectors at range 1
     * 
     * @param row
     *            represent starting row
     * @param col
     *            represent starting column
     * @return a set with coordinates of sectors at range 1
     */
    private Set<Coordinates> getCloserSectors(int row, int col) {

        Set<Coordinates> closerSectors = new HashSet<Coordinates>();

        Coordinates currentC;

        currentC = new Coordinates(row - 1, col);
        if (this.zone.isWalkable(currentC))
            closerSectors.add(currentC);
        currentC = new Coordinates(row + 1, col);
        if (this.zone.isWalkable(currentC))
            closerSectors.add(currentC);
        currentC = new Coordinates(row, col - 1);
        if (this.zone.isWalkable(currentC))
            closerSectors.add(currentC);
        currentC = new Coordinates(row, col + 1);
        if (this.zone.isWalkable(currentC))
            closerSectors.add(currentC);

        if (col % 2 == 0) {
            currentC = new Coordinates(row - 1, col - 1);
            if (this.zone.isWalkable(currentC))
                closerSectors.add(currentC);
            currentC = new Coordinates(row - 1, col + 1);
            if (this.zone.isWalkable(currentC))
                closerSectors.add(currentC);
        } else {
            currentC = new Coordinates(row + 1, col - 1);
            if (this.zone.isWalkable(currentC))
                closerSectors.add(currentC);
            currentC = new Coordinates(row + 1, col + 1);
            if (this.zone.isWalkable(currentC))
                closerSectors.add(currentC);
        }

        return closerSectors;
    }

    /**
     * 
     * Calculate the sectors at specific range
     * 
     * @param range
     *            distance of sectors
     * @param row
     *            represent starting row
     * @param col
     *            represent starting column
     * @return a set with coordinates of sectors at specific range
     */
    protected Set<Coordinates> getNearSectors(int range, int row, int col) {

        Set<Coordinates> nearSector = new HashSet<Coordinates>();

        if (range < 1) {
            return nearSector;
        }

        if (range < this.nearSectorsCoordinatesList.size()) {
            nearSector.addAll(this.getNearSectors(range - 1, row, col));
            nearSector.addAll(this.nearSectorsCoordinatesList.get(range));
        } else {
            if (range == 1) {
                nearSector.addAll(this.getCloserSectors(row, col));
                if (!nearSector.isEmpty())
                    this.nearSectorsCoordinatesList.add(1, nearSector);
            } else {
                Set<Coordinates> sectorsAtSmallerRange = new HashSet<Coordinates>();
                sectorsAtSmallerRange = this.getNearSectors(range - 1, row, col);

                Set<Coordinates> sectorOnlyAtThisRange = new HashSet<Coordinates>();
                for (Coordinates currentC : sectorsAtSmallerRange) {
                    sectorOnlyAtThisRange.addAll(this.zone.getSectorByCoordinates(currentC).getNearSectors(1));
                }
                sectorOnlyAtThisRange.removeAll(sectorsAtSmallerRange);
                sectorOnlyAtThisRange.removeAll(this.nearSectorsCoordinatesList.get(0));

                if (!sectorOnlyAtThisRange.isEmpty())
                    this.nearSectorsCoordinatesList.add(range, sectorOnlyAtThisRange);
                nearSector.addAll(sectorsAtSmallerRange);
                nearSector.addAll(sectorOnlyAtThisRange);
            }
        }

        return nearSector;
    }

}
