package it.polimi.ingsw.cg_29.gamemanager.model.zone;

import it.polimi.ingsw.cg_29.gamemanager.model.exception.NotWellFormedZoneException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Implementation of Zone.
 * 
 * @author Luca
 * @version 1.0
 *
 */
public abstract class Zone {

    /**
     * Representation of Zone by Map of sectors
     */
    private final Map<Coordinates, Sector> sectorMap;

    /**
     * Create new void Zone.
     */
    protected Zone() {
        this.sectorMap = new HashMap<Coordinates, Sector>();
    }

    /**
     * Implementation of toString Intended only for debugging.
     */
    @Override
    public String toString() {

        List<String> sectorsOrderedByCoordinates = new ArrayList<String>();
        String returnString = "";

        for (Entry<Coordinates, Sector> entry : this.sectorMap.entrySet()) {
            Sector value = entry.getValue();
            if (!(value instanceof VoidSector))
                sectorsOrderedByCoordinates.add(value.toString() + "\n");
        }

        Collections.sort(sectorsOrderedByCoordinates);
        for (String currentSector : sectorsOrderedByCoordinates)
            returnString += currentSector;

        return returnString;
    }

    /**
     * Create a sector with specific coordinates parsing string
     * 
     * @param row
     *            representing row of sector in this zone.
     * @param col
     *            representing column of sector in this zone.
     * @param string
     *            representing a type of sector
     */
    public void addSectorByString(int row, int col, String string) {
        switch (string) {

        case "A":
            this.sectorMap.put(new Coordinates(row, col), new AlienSector(row, col, this));
            break;

        case "D":
            this.sectorMap.put(new Coordinates(row, col), new DangerousSector(row, col, this));
            break;

        case "E":
            this.sectorMap.put(new Coordinates(row, col), new EscapeHatchSector(row, col, this));
            break;

        case "H":
            this.sectorMap.put(new Coordinates(row, col), new HumanSector(row, col, this));
            break;

        case "S":
            this.sectorMap.put(new Coordinates(row, col), new SecureSector(row, col, this));
            break;

        case "V":
            this.sectorMap.put(new Coordinates(row, col), new VoidSector(row, col, this));
            break;

        default:
            break;

        }

    }

    /**
     * @param coordinates
     *            of the sector
     * @return true if a sector with specific coordinates exists in this zone.
     */
    public boolean sectorExist(Coordinates coordinates) {
        if (this.sectorMap.containsKey(coordinates))
            return true;
        return false;
    }

    /**
     * @param coordinates
     * @return true if the sector with specific coordinates is walkable in this
     *         zone
     */
    protected boolean isWalkable(Coordinates coordinates) {
        if (!this.sectorExist(coordinates))
            return false;
        if (this.getSectorByCoordinates(coordinates) instanceof VoidSector)
            return false;
        if (this.getSectorByCoordinates(coordinates) instanceof HumanSector)
            return false;
        if (this.getSectorByCoordinates(coordinates) instanceof AlienSector)
            return false;

        return true;
    }

    /**
     * Return specific Zone Sector
     * 
     * @param coordinates
     *            Coordinates of needed sector
     * @return needed sector
     * @throws IllegalArgumentException
     */
    public Sector getSectorByCoordinates(Coordinates coordinates) {
        if (this.sectorExist(coordinates))
            return this.sectorMap.get(coordinates);
        else
            return null;

    }

    /**
     * @param col
     *            column of required sector in this zone.
     * @param row
     *            row of required sector in this zone.
     * @return required sector.
     */
    protected Sector getSectorByString(String col, String row) {
        return this.getSectorByCoordinates(new Coordinates(Integer.parseInt(row) - 1, ((int) col.charAt(0)) - 65));
    }

    /**
     * auxiliary method to printToString
     */
    private String getPrettyPrint(List<String> zoneOrderedByRow, int lenghtRow, int lenghtRowCol) {

        String zoneString = "";
        String firstHalfRow = "";
        String secondHalfRow = "";
        String currentRowString = "";
        String separator = "";
        int count = 0;

        for (String currentSectorString : zoneOrderedByRow) {
            if (!currentRowString.equals(currentSectorString.substring(0, lenghtRow))) {
                zoneString += firstHalfRow + "\n" + secondHalfRow + "\n";
                count = 0;
                firstHalfRow = "";
                secondHalfRow = "";
                separator = "";
            }
            currentRowString = currentSectorString.substring(0, lenghtRow);
            if (count % 2 == 0) {
                firstHalfRow += currentSectorString.substring(lenghtRowCol);
                for (int i = 0; i < currentSectorString.substring(lenghtRowCol).length(); i++)
                    secondHalfRow += " ";
            } else {
                secondHalfRow += currentSectorString.substring(lenghtRowCol);
                for (int i = 0; i < currentSectorString.substring(lenghtRowCol).length(); i++)
                    firstHalfRow += " ";
            }

            for (int i = 0; i < currentSectorString.substring(lenghtRowCol).length(); i++)
                separator += "=";

            count++;
        }
        zoneString += firstHalfRow + "\n" + secondHalfRow + "\n";
        zoneString = separator + zoneString.substring(1) + separator;

        return zoneString;
    }

    /**
     * @return a "pretty print" string representing this Zone.
     */
    public String printToString() {

        List<String> zoneOrderedByRow = new ArrayList<String>();
        int row;
        int col;
        String stringRowCol = "";
        int lenghtRow = 0;
        int lenghtRowCol = 0;

        for (Entry<Coordinates, Sector> currentElm : this.sectorMap.entrySet()) {
            Sector currentSector = currentElm.getValue();

            row = currentSector.getRow();
            col = currentSector.getCol();
            if (row > -10 && row < 10) {
                stringRowCol = "0" + row;
            } else {
                stringRowCol = "" + row;
            }
            lenghtRow = stringRowCol.length();
            if (col > -10 && col < 10) {
                stringRowCol += "0" + col;
            } else {
                stringRowCol += "" + col;
            }
            lenghtRowCol = stringRowCol.length();

            zoneOrderedByRow.add(stringRowCol + currentSector.printToString());
        }

        Collections.sort(zoneOrderedByRow);

        return getPrettyPrint(zoneOrderedByRow, lenghtRow, lenghtRowCol);

    }

    /**
     * Find and return coordinates of HumanSector in this zone.
     * 
     * @return coordinates of human spawn sector.
     * @throws NotWellFormedZoneException
     */
    public Coordinates getHumanSectorCoordinates() throws NotWellFormedZoneException {

        for (Entry<Coordinates, Sector> currentElm : this.sectorMap.entrySet()) {
            Coordinates currentCoordinates = currentElm.getKey();
            Sector currentSector = currentElm.getValue();

            if (currentSector instanceof HumanSector) {
                return currentCoordinates;
            }
        }

        throw new NotWellFormedZoneException("There isn't human sector in this zone.");

    }

    /**
     * Find and return coordinates of AlienSector in this zone.
     * 
     * @return coordinates of alien spawn sector.
     * @throws NotWellFormedZoneException
     */
    public Coordinates getAlienSectorCoordinates() throws NotWellFormedZoneException {

        for (Entry<Coordinates, Sector> currentElm : this.sectorMap.entrySet()) {
            Coordinates currentCoordinates = currentElm.getKey();
            Sector currentSector = currentElm.getValue();

            if (currentSector instanceof AlienSector) {
                return currentCoordinates;
            }
        }

        throw new NotWellFormedZoneException("There isn't Alien sector in this zone.");
    }

}
