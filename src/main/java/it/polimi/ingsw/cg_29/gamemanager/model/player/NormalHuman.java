package it.polimi.ingsw.cg_29.gamemanager.model.player;

/**
 * This Human Strategy allows to reset all the abilities that a Human Player has
 * into Normal Human abilities.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class NormalHuman implements HumanStrategy {
    /**
     * Creates a Strategy NormalHuman.
     */
    public NormalHuman() {
        super();
    }

    /**
     * addBehaviour allows to add the NormalHuman Strategy to a certain Human
     * resetting to false the abilities attributes and to 1 the rangeOfMove
     * attribute (now this Human could not: - cross two Sectors during a
     * MoveAction but only one; - draw any DangerousSectorCard if him move onto
     * a DangerousSector; - do any AttackAction; - receive an AttackAction
     * without lose).
     * 
     * @param character
     *            that is the Human on which will be activated the NormalHuman
     *            Strategy.
     */
    @Override
    public void addBehaviour(Character character) {
        character.setBeSedate(false);
        character.setCanDefense(false);
        character.setRangeOfMove(1);
        character.setCanAttack(false);
    }

    /**
     * removeBehaviour allows to add the NormalHuman Strategy to a certain Human
     * resetting to false the abilities attributes and to 1 the rangeOfMove
     * attribute (now this Human could not: - cross two Sectors during a
     * MoveAction but only one; - draw any DangerousSectorCard if him move onto
     * a DangerousSector; - do any AttackAction; - receive an AttackAction
     * without lose).
     * 
     * @param character
     *            that is the Human on which will be deactivated the NormalHuman
     *            Strategy.
     */
    @Override
    public void removeBehaviour(Character character) {
        character.setBeSedate(false);
        character.setCanDefense(false);
        character.setRangeOfMove(1);
        character.setCanAttack(false);
    }

}
