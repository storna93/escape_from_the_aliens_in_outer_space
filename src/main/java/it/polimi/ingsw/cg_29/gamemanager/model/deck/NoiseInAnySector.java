package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * When drawn, this Card binds to say any Sector to all Player, for the Player
 * that drawn this Card.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class NoiseInAnySector extends DangerousSectorCard {
    /**
     * Creates a DanegerousSectorCard NoiseInAnySector.
     */
    protected NoiseInAnySector() {
        super();
    }

}
