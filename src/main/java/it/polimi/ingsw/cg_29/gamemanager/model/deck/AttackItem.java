package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * When used, this Card allows the Human Player to attack like an Alien Player.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class AttackItem extends ItemCard {
    /**
     * Creates a Card AttackItem.
     */
    public AttackItem() {
        super();
    }

}
