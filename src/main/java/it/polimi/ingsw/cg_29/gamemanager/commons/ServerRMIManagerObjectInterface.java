package it.polimi.ingsw.cg_29.gamemanager.commons;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Defines the interface of the ServerRMIManager remote object. Every clients
 * that wants to connect to this ServerRMI for receiving response from it as a
 * result of some requests from the client, could call the remote method
 * firstConnection() offered by this.
 * 
 * @author Fulvio
 * @version 1.0
 */
public interface ServerRMIManagerObjectInterface extends Remote {

    /**
     * This method could be remotely call by client for being connected to the
     * ServerRMI.
     * 
     * @param client
     *            , that is the interface of the client remote object that wants
     *            to be connected to this.
     * @param controllerId
     * @param clientId
     * 
     * @throws RemoteException
     */
    public void firstConnection(ClientRMIObjectInterface client, String clientId, String controllerId) throws RemoteException;
}
