package it.polimi.ingsw.cg_29.gamemanager.controller.command;

import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.Action;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.view.View;

/**
 * Command is an abstract class contains different methods to get elements to
 * modify when a specific command has executed. The function of a generic
 * command is to create an action to modify model and then send message at
 * specific client.
 *
 * @author Luca
 * @version 1.0
 *
 */
public abstract class Command {

    /**
     * Reference to controller that receives a command from clientView.
     */
    private Controller controller;

    /**
     * Reference to view that receives a command from client.
     */
    private View currentClient;

    /**
     * Creates new command and initializes the attributes.
     * 
     * @param controller
     *            reference to controller that receives a command from
     *            clientView.
     * @param currentClient
     *            reference to view that receives a command from client.
     */
    public Command(Controller controller, View currentClient) {
        this.controller = controller;
        this.currentClient = currentClient;
    }

    /**
     * @return the controller
     */
    protected Controller getController() {
        return this.controller;
    }

    /**
     * @return the currentClientId
     */
    protected String getCurrentClientId() {
        return this.currentClient.getClientId();
    }

    /**
     * @return the CurrentMatch
     */
    protected Match getCurrentMatch() {
        return this.controller.getMatch();
    }

    /**
     * @return if match is start
     */
    protected boolean matchIsRunning() {
        if (this.getCurrentMatch() == null)
            return false;
        return true;
    }

    /**
     * @return the CurrentClient
     */
    protected View getCurrentClient() {
        return this.currentClient;
    }

    /**
     * Add a specific action to the action observer of the controller that
     * receives a command from clientView.
     * 
     * @param action
     */
    protected void addActionToObserver(Action action) {
        action.addObserver(controller.getActionObserver());
    }

    /**
     * Launch this command.
     */
    public abstract void execute();

}
