package it.polimi.ingsw.cg_29.gamemanager.model.deck;

/**
 * This is a Deck composed only of EscapeHatchCards.
 * 
 * @author Fulvio
 * @version 1.0
 */

public class EscapeHatchDeck extends Deck {

    /**
     * Construct a EscapeHatchDeck without initializing any attributes. Then a
     * EscapeHatchDeckCreator will fill this EscapeHatchDeck (that implements a
     * factoryMethod)
     */
    protected EscapeHatchDeck() {
        super();

    }

}
