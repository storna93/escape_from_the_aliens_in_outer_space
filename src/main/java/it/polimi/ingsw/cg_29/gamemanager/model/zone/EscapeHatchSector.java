package it.polimi.ingsw.cg_29.gamemanager.model.zone;

/**
 * Implementation of Sector representing EscapeHatchSector.
 * 
 * @author Luca
 * @version 1.0
 *
 */
public class EscapeHatchSector extends Sector {

    /**
     * this boolean represents the state of EscapeHatch after his use by a
     * player.
     */
    private boolean usable;

    /**
     * Return an EscapeHatchSector with specific coordinates and reference to
     * zone and initializes the state.
     * 
     * @param row
     *            reference to row of the zone by integer number.
     * @param col
     *            reference to column of the zone by integer number.
     * @param zone
     *            reference to the zone.
     * @throws NullPointerException
     */
    protected EscapeHatchSector(int row, int col, Zone zone) {
        super(row, col, zone);
        this.usable = true;
    }

    /**
     * Return an EscapeHatchSector with specific coordinates and reference to
     * zone and initializes the state.
     * 
     * @param col
     *            reference to column of the zone by string.
     * @param row
     *            reference to row of the zone by string.
     * @param zone
     *            reference to the zone.
     * @throws NullPointerException
     *             , NumberFormatException
     */
    public EscapeHatchSector(String col, String row, Zone zone) {
        super(col, row, zone);
        this.usable = true;
    }

    /**
     * Sets the state to this EscapeHatch to damaged.
     */
    public void block() {
        this.usable = false;
    }

    /**
     * @return true if the EscapeHatch is not damaged.
     */
    public boolean isUsable() {
        return this.usable;
    }

    /**
     * @return a "pretty print" string representing this Sector.
     */
    @Override
    public String printToString() {
        if (!this.usable)
            return "<" + super.printToString() + "(#)>";

        return "<" + super.printToString() + "(E)>";
    }

    /**
     * Implementation of toString Intended only for debugging.
     */
    @Override
    public String toString() {
        return super.toString() + "(usable=" + usable + ")";
    }

    /**
     * Implementation of equals
     */
    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj))
            return false;
        if (this.usable != ((EscapeHatchSector) obj).isUsable())
            return false;

        return true;
    }

    /**
     * Implementation of hashCode
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result += ((usable) ? 13 : 11);
        return result;
    }

}
