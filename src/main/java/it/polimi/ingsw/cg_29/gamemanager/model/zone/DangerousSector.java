package it.polimi.ingsw.cg_29.gamemanager.model.zone;

/**
 * Implementation of Sector representing DangerousSector.
 * 
 * @author Luca
 * @version 1.0
 */
public class DangerousSector extends Sector {

    /**
     * Return a DangerousSector with specific coordinates and reference to zone.
     * 
     * @param row
     *            reference to row of the zone by integer number.
     * @param col
     *            reference to column of the zone by integer number.
     * @param zone
     *            reference to the zone.
     * @throws NullPointerException
     */
    protected DangerousSector(int row, int col, Zone zone) {
        super(row, col, zone);
    }

    /**
     * Return a DangerousSector with specific coordinates and reference to zone.
     * 
     * @param col
     *            reference to column of the zone by string.
     * @param row
     *            reference to row of the zone by string.
     * @param zone
     *            reference to the zone.
     * @throws NullPointerException
     *             , NumberFormatException
     */
    public DangerousSector(String col, String row, Zone zone) {
        super(col, row, zone);
    }

    /**
     * @return a "pretty print" string representing this Sector.
     */
    @Override
    public String printToString() {
        return "<" + super.printToString() + "(D)>";
    }

}
