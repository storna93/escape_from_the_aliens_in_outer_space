package it.polimi.ingsw.cg_29.gamemanager.controller.command;

import it.polimi.ingsw.cg_29.gamemanager.controller.Controller;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.Action;
import it.polimi.ingsw.cg_29.gamemanager.controller.action.PassAction;
import it.polimi.ingsw.cg_29.gamemanager.model.Match;
import it.polimi.ingsw.cg_29.gamemanager.view.View;

/**
 * A command to end round.
 *
 * @author Luca
 * @version 1.0
 *
 */
public class PassCommand extends Command {

    /**
     * Counter to send special message.
     */
    private static int counter = 0;

    /**
     * Create a new command
     * 
     * @param controller
     *            the controller that manages current match
     * @param currentClient
     *            the id of client who sent command
     */
    public PassCommand(Controller controller, View currentClient) {
        super(controller, currentClient);
    }

    /**
     * Launch this command.
     */
    @Override
    public void execute() {

        if (this.matchIsRunning()) {

            Match match = this.getCurrentMatch();
            Action pass = new PassAction(this.getCurrentClientId());

            if (pass.isValid(match)) {
                counter = 0;
                this.getCurrentClient().send("Round end.");
                this.addActionToObserver(pass);
                pass.doAction(match);
            } else {
                counter++;

                if (counter > 2) {
                    counter = 0;
                    this.getCurrentClient()
                            .send("\n                        YOU SHALL NOT PASS!          `.         \n                                                     ,;         \n                               .,                    ,i         \n      `+`                      ;i                    .i         \n      ;x#                      +#                    .*         \n      +xx.                     nx`                   .*         \n      ixx`                    ,xx:                   `+         \n       #+                     ixx*                   `+         \n       .*                     #xxz                    +         \n        +                    `xxxx`                   #         \n        +                    :xxxx;                   #         \n        +`                 ``*xxxx+```                #         \n        i,           :+#nxxxxxxxxxxxxxxnz+:           #         \n        :;           ,zxxxxxxxxxxxxxxxxxxz:           #         \n        .*             ,+xxxxxxxxxxxxxx+,          .,:z;:,      \n         #               `:#xxxxxxxx#:`            .,:n:.`      \n        `n:                ixxxxxxxx*                :x+        \n        ;xx:zn*.           +xxxxxxxx#           .izn;nx#        \n        ;xx*xxxx#;`        :xxMWWMxx;        `:#xxxx+nx:        \n        ,xx,xxxxxxx#i:,.`.:#xx@##Wxx#:.`.,:i#xxxxxxx;`*.        \n         ;# nxxxxxxxxxxxxxxxxM####Mxxxxxxxxxxxxxxxxx            \n          # #xxxxxxxxxxxxxxxxW@WW@Wxxxxxxxxxxxxxxxxz            \n          z *xxxxxxxxxxxxxxxx@####@xxxxxxxxxxxxxxxx+            \n          +.:xxxxxxxxxxxxxxxx@####@xxxxxxxxxxxxxxxx;            \n          i:`xxxxxxxxxxxxxxxx@####@xxxxxxxxxxxxxxxx.            \n          :i #xxxxxxxn##zxxxx@####@xxxxz##nxxxxxxxz             \n          .+ :xxx#i,`    .xxx@####@xxx,    `,i#xxx;             \n          `z  ,.          nxx@####@xxx          .:              \n           z              nxxW####Wxxx                          \n           #.             xxxM####Mxxx`                         \n           *:            .xxxx@##@xxxx,                         \n           ;i            :xxxxMWWMxxxx;                         \n           ,+            ixxxxxxxxxxxx*                         \n           `z            +xxxxxxxxxxxx#                         \n            n            nxxxxxxxxxxxxx                         \n            #.          .xxxxxxxxxxxxxx,                        \n            *:          ;xxxxxxxxxxxxxxi                        \n            ;i          +xxxxxxxxxxxxxx#                        \n            ,+          nxxxxxxxxxxxxxxx`                       \n            `n         .xxxxxxxxxxxxxxxx:                       \n             n`        ixxxxxxxxxxxxxxxx*                       \n             #.        #xxxxxxxxxxxxxxxxz                       \n             *;       `xxxxxxxxxxxxxxxxxx.                      \n             ;*       ;xxxxxxxxxxxxxxxxxxi                      \n             ,#       +xxxxxxxxxxxxxxxxxx#                      \n             `n      `xxxxxxxxxxxxxxxxxxxx`                     \n              n`     :xxxxxxxxxxxxxxxxxxxx;                     \n              #,     +xxxxxxxxxxxxxxxxxxxx#                     \n              `      nxxxxxxxxxxxxxxxxxxxxx`                    \n                    :xxxxxxxxxxxxxxxxxxxxxx;                    \n                    ,iiiiiiiiiiiiiiiiiiiiii:                    \n\n");

                } else
                    this.getCurrentClient().send("You cannot pass now.");
            }
        } else
            this.getCurrentClient().send("The match is not running.");

    }

}
