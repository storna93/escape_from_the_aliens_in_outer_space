package it.polimi.ingsw.cg_29.gamemanager.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Class that saves every public information of current match.
 * 
 * @author Luca
 * @version 1.0
 *
 */
public class CustomLog {

    /**
     * List contains public informations of match.
     */
    private List<String> matchInformationList;

    /**
     * Reference to controller of the match.
     */
    private Controller controller;

    /**
     * Creates a new Log and initializes matchInformationList as ArrayList.
     * 
     * @param controller
     *            reference to controller of the match.
     */
    protected CustomLog(Controller controller) {
        this.controller = controller;
        this.matchInformationList = new ArrayList<String>();
    }

    /**
     * Add new information with current time and reference of round.
     * 
     * @param eventNotification
     *            information of match.
     */
    protected void addEventNotification(String eventNotification) {
        if (this.controller.getMatch() != null) {
            Calendar cal = Calendar.getInstance();
            cal.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            this.matchInformationList.add("[LOG " + sdf.format(cal.getTime()) + "] [ROUND: " + this.controller.getMatch().getCurrentRound() + "] " + eventNotification);
        }
    }

    /**
     * Converts list of informations to string.
     * 
     * @return string representing the list of informations
     */
    public String getCompleteLog() {
        String completeLog = "";
        for (String eventNotification : this.matchInformationList) {
            completeLog += "\n" + eventNotification;
        }
        return completeLog;
    }

}
