package it.polimi.ingsw.cg_29.gamemanager.model.player;

/**
 * This Enumeration defines all and only the possible states that a Player Round
 * could be during the game.
 * 
 * @author Fulvio
 * @version 1.0
 */

public enum RoundState {

    DISCONNECTED, STANDBY, BEGINNING, MOVED_SECURE, MOVED_DANGEROUS, FAKE_NOISE, ENDING;
}
