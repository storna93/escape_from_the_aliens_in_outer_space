package it.polimi.ingsw.cg_29.gamemanager.commons;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Defines the interface of the ClientRMI remote object, that is the client who
 * uses RMI for communication. Every Broker that wants to publish messages to
 * this Client, could call the method dispatchMessage() offered by this.
 * 
 * @author Fulvio
 * @version 1.0
 */
public interface ClientRMIObjectInterface extends Remote {

    /**
     * This method could be remotely call for publishing message by the Broker
     * to the client.
     * 
     * @param msg
     * @throws RemoteException
     */
    public void dispatchMessage(String msg) throws RemoteException;

    /**
     * This method could be remotely call the ServerRMIView to setting the
     * identification of the controller and the ServerView connected to this and
     * to give the identification client to this.
     * 
     * @param controllerId
     * @param clientId
     * @param serverRMIViewObjectInterface
     * @throws RemoteException
     */
    public void setId(String controllerId, String clientId, ServerRMIViewObjectInterface serverRMIViewObjectInterface) throws RemoteException;

}
