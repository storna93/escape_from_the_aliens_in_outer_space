package it.polimi.ingsw.cg_29.client.socket;

import it.polimi.ingsw.cg_29.client.Client;

import java.util.Scanner;

/**
 * manages the incoming connection of the client, that is the messages sent from
 * the server to the client
 * 
 * @author Luca
 * @version 1.0
 */
public class ClientInHandler extends Thread {

    private Client client;

    /**
     * contains the Scanner which is used to read messages from the server
     */
    private Scanner socketIn;

    /**
     * creates a new Handler for the incoming connections
     * 
     * @param clientSocket
     * 
     * @param socketIn
     *            is the scanner which is used to read messages that are sent
     *            from the server
     */
    public ClientInHandler(Client client, Scanner socketIn) {
        this.client = client;
        this.socketIn = socketIn;
    }

    /**
     * executes the client handler
     */
    @Override
    public void run() {
        while (true) {
            String line = this.socketIn.nextLine();
            line = line.replaceAll("@new_line", "\n");
            if (line.matches("close_socket")) {
                return;
            }
            this.client.showMessage(line);
        }
    }
}
