package it.polimi.ingsw.cg_29.client.gui.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;

/**
 * Contains the implementation of StatePanel that displays informations of
 * current player's state his cards.
 * 
 * @author Luca
 * @version 1.0
 */
public class StatePanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Contain a list of icons of characters.
     */
    private final transient List<ImageIcon> characterIcons;

    /**
     * Reference to a specific color that changes according player's character
     */
    private Color characterTextColor;

    /**
     * Reference to a panel that displays the icon of player's character
     */
    private JLabel characterIcon;

    /**
     * Reference to a panel that displays the player's clientId
     */
    private JLabel clientId;

    /**
     * Reference to a panel that displays the player's character
     */
    private JLabel characterName;

    /**
     * Reference to a panel that displays the current player's state
     */
    private JLabel roundState;

    /**
     * Reference to a panel that displays the current player's round state
     */
    private JLabel playerState;

    /**
     * 
     * Create a new StatePanel that contains panels of the state information.
     */
    public StatePanel() {
        this.characterIcons = new ArrayList<ImageIcon>();
        this.characterIcons.add(new ImageIcon(new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "player" + File.separatorChar + "human_1.png").getImage().getScaledInstance(30, -1, Image.SCALE_SMOOTH)));
        this.characterIcons.add(new ImageIcon(new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "player" + File.separatorChar + "human_2.png").getImage().getScaledInstance(30, -1, Image.SCALE_SMOOTH)));
        this.characterIcons.add(new ImageIcon(new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "player" + File.separatorChar + "human_3.png").getImage().getScaledInstance(30, -1, Image.SCALE_SMOOTH)));
        this.characterIcons.add(new ImageIcon(new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "player" + File.separatorChar + "human_4.png").getImage().getScaledInstance(30, -1, Image.SCALE_SMOOTH)));
        this.characterIcons.add(new ImageIcon(new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "player" + File.separatorChar + "Alien_1.png").getImage().getScaledInstance(30, -1, Image.SCALE_SMOOTH)));
        this.characterIcons.add(new ImageIcon(new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "player" + File.separatorChar + "Alien_2.png").getImage().getScaledInstance(30, -1, Image.SCALE_SMOOTH)));
        this.characterIcons.add(new ImageIcon(new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "player" + File.separatorChar + "Alien_3.png").getImage().getScaledInstance(30, -1, Image.SCALE_SMOOTH)));
        this.characterIcons.add(new ImageIcon(new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "player" + File.separatorChar + "Alien_4.png").getImage().getScaledInstance(30, -1, Image.SCALE_SMOOTH)));

        this.characterIcon = new JLabel(characterIcons.get(0));
        this.characterTextColor = new Color(6, 167, 225);

        this.clientId = new JLabel("", SwingConstants.CENTER);
        this.characterName = new JLabel("");
        this.roundState = new JLabel("");
        this.playerState = new JLabel("");

        this.setBackground(Color.black);
        this.clientId.setOpaque(true);
        this.characterName.setForeground(Color.white);
        this.characterName.setFont(this.characterName.getFont().deriveFont(Font.BOLD));
        this.roundState.setForeground(Color.white);
        this.roundState.setFont(this.roundState.getFont().deriveFont(Font.BOLD));
        this.playerState.setForeground(Color.white);
        this.playerState.setFont(this.playerState.getFont().deriveFont(Font.BOLD));

        this.setLayout(new BorderLayout());

        JPanel stringStatePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        stringStatePanel.setOpaque(false);
        stringStatePanel.add(this.characterName);
        stringStatePanel.add(this.playerState);
        stringStatePanel.add(this.roundState);

        this.clientId.setBorder(new MatteBorder(1, 1, 1, 1, new Color(0, 0, 0, 0)));
        this.characterIcon.setBorder(new MatteBorder(10, 20, 10, 10, Color.black));
        stringStatePanel.setBorder(new MatteBorder(10, 0, 10, 10, Color.black));

        this.add(this.clientId, BorderLayout.NORTH);
        this.add(this.characterIcon, BorderLayout.WEST);
        this.add(stringStatePanel, BorderLayout.CENTER);
    }

    /**
     * Update clientId panel.
     * 
     * @param clientId
     *            string to set
     */
    public void updateClientId(String clientId) {
        this.clientId.setText(clientId);
    }

    /**
     * Update characterIcon panel, characterName panel and the
     * characterTextColor.
     * 
     * @param numberOfIcon
     *            string to set.
     */
    public void updateCharacter(int numberOfIcon) {
        if (0 <= numberOfIcon && numberOfIcon < 4) {
            this.characterIcon.setIcon(characterIcons.get(numberOfIcon));
            this.characterName.setText("HUMAN");
            this.characterTextColor = new Color(6, 167, 225);
        }
        if (4 <= numberOfIcon && numberOfIcon < 8) {
            this.characterIcon.setIcon(characterIcons.get(numberOfIcon));
            this.characterName.setText("ALIEN");
            this.characterTextColor = new Color(252, 75, 12);
        }
    }

    /**
     * Update playerState panel.
     * 
     * @param playerState
     *            string to set.
     */
    public void updatePlayerState(String playerState) {
        this.playerState.setText("[ " + playerState + " ]");
        if ("PLAYING".equals(playerState)) {
            this.playerState.setVisible(false);
            this.roundState.setVisible(true);
        } else {
            this.playerState.setVisible(true);
            this.roundState.setVisible(false);
            this.playerState.setForeground(this.characterTextColor);
        }
    }

    /**
     * Update roundState panel.
     * 
     * @param roundState
     *            string to set.
     */
    public void updateRoundState(String roundState) {
        this.roundState.setText("( " + roundState + " )");
        if ("STANDBY".equals(roundState)) {
            this.roundState.setForeground(new Color(187, 187, 187));
            this.clientId.setForeground(Color.black);
            this.clientId.setBackground(new JLabel().getBackground());
            this.clientId.setFont(this.characterName.getFont().deriveFont(Font.PLAIN));
        } else {
            this.roundState.setForeground(this.characterTextColor);
            this.clientId.setForeground(Color.white);
            this.clientId.setBackground(this.characterTextColor);
            this.clientId.setFont(this.characterName.getFont().deriveFont(Font.BOLD));
        }
    }
}
