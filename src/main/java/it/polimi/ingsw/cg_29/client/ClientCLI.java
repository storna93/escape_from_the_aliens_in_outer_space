package it.polimi.ingsw.cg_29.client;

import it.polimi.ingsw.cg_29.LoggerClass;

import java.util.Scanner;
import java.util.concurrent.Executors;

/**
 * ClientCLI defines the client executable associated to the user that wants to
 * play using a CLI interface.
 * 
 * @author Fulvio
 * @version 1.0
 */
public class ClientCLI extends Client {

    /**
     * Create an instance of ClientCli
     */
    public ClientCLI() {
        super();
    }

    /**
     * This method allows the user to establishing a match in RMI connection or
     * in Socket connection and allows the ClientCLI thread to catch all
     * messages sends by user and to put them into the Server.
     */
    @Override
    public void run() {

        Scanner scanner = new Scanner(System.in);
        String choice = "";

        while (!(choice.matches("1") || choice.matches("2"))) {
            showMessage("Choose what type of connection you want to use:");
            showMessage("\t1 = Socket");
            showMessage("\t2 = RMI");
            choice = scanner.nextLine();
        }

        this.establishConnection(choice);

        String messageToSend;

        while (true) {
            messageToSend = scanner.nextLine();
            if ("@scanner_close".equals(messageToSend))
                break;
            this.sendMessage(messageToSend);
        }

        scanner.close();
    }

    /**
     * This method, when called, shows a response message (send by Server) or a
     * published message (send by Broker) to the user.
     * 
     * @param message
     */
    @Override
    public void showMessage(String message) {
        System.out.println(message);
        LoggerClass.getLogger().info(message);
    }

    /**
     * This method starts a CLI interface useful for the user.
     * 
     * @param args
     */
    public static void main(String[] args) {
        Executors.newCachedThreadPool().submit(new ClientCLI());
    }

}
