package it.polimi.ingsw.cg_29.client.gui.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

/**
 * Contains the implementation of InfoPanel that displays public informations of
 * current match.
 * 
 * @author Luca
 * @version 1.0
 */
public class InfoPanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Text Area in which displays public informations of current match.
     */
    private JTextArea infoTextDisplay;

    /**
     * 
     * Create a new InfoPanel that contains infoTextDisplay.
     */
    public InfoPanel() {

        this.infoTextDisplay = new JTextArea(0, 27);
        this.infoTextDisplay.setLineWrap(true);
        this.infoTextDisplay.setWrapStyleWord(true);
        this.infoTextDisplay.setEditable(false);
        this.infoTextDisplay.setBackground(Color.black);
        this.infoTextDisplay.setForeground(Color.white);
        this.infoTextDisplay.setFont(this.infoTextDisplay.getFont().deriveFont(Font.BOLD, 13f));

        JScrollPane scrollable = new JScrollPane(infoTextDisplay);
        scrollable.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        this.setLayout(new BorderLayout());

        this.add(new JLabel("INFO", SwingConstants.CENTER), BorderLayout.NORTH);
        this.add(scrollable, BorderLayout.CENTER);

    }

    /**
     * Add a message to infoTextDisplay.
     * 
     * @param message
     *            message to add.
     */
    protected void addInfoMessage(String message) {

        Calendar cal = Calendar.getInstance();
        cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

        this.infoTextDisplay.append("[" + sdf.format(cal.getTime()) + "] " + message + "\n");
        this.infoTextDisplay.setCaretPosition(this.infoTextDisplay.getDocument().getLength());
    }
}
