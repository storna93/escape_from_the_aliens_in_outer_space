package it.polimi.ingsw.cg_29.client.socket;

import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.client.Client;

import java.util.Scanner;

/**
 * manages the incoming connection of the client, that is the messages sent from
 * the publisher
 * 
 * @author Luca
 * @version 1.0
 */
public class SubscriberThread extends Thread {

    private Client client;

    /**
     * contains the Scanner which is used to read messages from the publisher
     */
    private Scanner socketIn;

    /**
     * creates a new SubscriberThread for the incoming connections
     * 
     * @param socketIn
     *            is the scanner which is used to read messages that are sent
     *            from the publisher
     */
    public SubscriberThread(Client client, Scanner socketIn) {
        this.client = client;
        this.socketIn = socketIn;
    }

    /**
     * executes the SubscriberThread
     */
    @Override
    public void run() {
        while (true) {
            try {
                String line = this.socketIn.nextLine();
                line = line.replaceAll("@new_line", "\n");
                this.client.showMessage(line);
            } catch (Exception e) {
                LoggerClass.getLogger().warn(e);
                break;
            }
        }
        this.socketIn.close();
    }
}
