package it.polimi.ingsw.cg_29.client.gui.panel;

import it.polimi.ingsw.cg_29.client.gui.FrameGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

/**
 * Contains the implementation of the panel that displays the public
 * informations before starting match and allow the administrator to change
 * zone.
 * 
 * @author Luca
 * @version 1.0
 */
public class WaitingForPlayersPanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Reference to frame GUI to call method to send command.
     */
    private FrameGUI frameGUI;

    /**
     * Reference to InfoPanel that displays public informations.
     */
    private InfoPanel infoPanel;

    /**
     * Create a new WaitingForPlayersPanel with command buttons and InfoPanel
     * 
     * @param frameGUI
     *            reference to frame GUI to call method to send command.
     */
    public WaitingForPlayersPanel(FrameGUI frameGUI) {

        this.frameGUI = frameGUI;

        this.infoPanel = new InfoPanel();

        JButton setZoneButton = new JButton("SET ZONE");
        setZoneButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object[] options = { "GALILEI", "FERMI", "GALVANI" };
                int choice = JOptionPane.showOptionDialog(new JFrame(), "Choose zone:", "Set Zone", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
                setZone(choice);
            }
        });

        JButton disconnectButton = new JButton("EXIT");
        disconnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object[] options = { "YES", "NO" };
                int choice = JOptionPane.showOptionDialog(new JFrame(), "Are you sure to exit and leave the game before starting?", "Exit", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[1]);
                if (choice == 0)
                    disconnectAndExit();
            }
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(setZoneButton);
        buttonPanel.add(disconnectButton);
        buttonPanel.setOpaque(false);
        buttonPanel.setBorder(new CompoundBorder(buttonPanel.getBorder(), new EmptyBorder(0, 10, 10, 10)));

        this.infoPanel.setBorder(new LineBorder(Color.black, 20));
        this.setLayout(new BorderLayout());
        this.add(buttonPanel, BorderLayout.NORTH);
        this.add(this.infoPanel, BorderLayout.CENTER);

    }

    /**
     * Send a command to set specific zone.
     * 
     * @param choice
     *            represent specific zone.
     */
    private void setZone(int choice) {
        if (choice != -1)
            this.frameGUI.sendCommand("set_zone(" + (choice + 1) + ")");
    }

    /**
     * Method to add new info message to InfoPanel.
     * 
     * @param message
     *            message to add.
     */
    public void showInfo(String message) {
        this.infoPanel.addInfoMessage(message);
    }

    /**
     * If a client want to quit disconnects him and close window.
     */
    private void disconnectAndExit() {
        this.frameGUI.sendCommand("disconnect");
        this.frameGUI.dispatchEvent(new WindowEvent(this.frameGUI, WindowEvent.WINDOW_CLOSING));
    }

}
