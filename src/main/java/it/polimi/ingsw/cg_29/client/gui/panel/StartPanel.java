package it.polimi.ingsw.cg_29.client.gui.panel;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 * Contains the implementation of StartPanel that displays a specific panel with
 * the logo and the images of game.
 * 
 * @author Luca
 * @version 1.0
 *
 */
public class StartPanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Specific image of the game.
     */
    private final transient Image backgroundImage;

    /**
     * Creates a new StartPanel that displays a specific panel with the logo and
     * the images of game.
     * 
     * @param customPanel
     *            panel to display.
     * @param customBackgroundNumber
     *            represent image to display.
     */
    public StartPanel(JPanel customPanel, int customBackgroundNumber) {

        String backgroundImagePath = "." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "image" + File.separatorChar + "back.png";

        if (customBackgroundNumber == 2)
            backgroundImagePath = "." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "image" + File.separatorChar + "back2.png";

        this.backgroundImage = new ImageIcon(new ImageIcon(backgroundImagePath).getImage().getScaledInstance(1000, -1, Image.SCALE_SMOOTH)).getImage();

        this.setBackground(Color.black);

        this.setLayout(new GridLayout(1, 2));

        JPanel titlePanel = new JPanel() {

            private static final long serialVersionUID = 1L;

            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                Image gameLogo = new ImageIcon(new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "image" + File.separatorChar + "title.png").getImage().getScaledInstance(500, -1, Image.SCALE_SMOOTH)).getImage();
                int newWidth = this.getWidth();
                int newHeight = this.getHeight();

                if ((float) this.getWidth() / gameLogo.getWidth(null) > (float) this.getHeight() / gameLogo.getHeight(null))
                    newWidth = newHeight * gameLogo.getWidth(null) / gameLogo.getHeight(null);

                if ((float) this.getWidth() / gameLogo.getWidth(null) < (float) this.getHeight() / gameLogo.getHeight(null))
                    newHeight = newWidth * gameLogo.getHeight(null) / gameLogo.getWidth(null);

                newWidth /= 2;
                newHeight /= 2;

                int x = (this.getWidth() - newWidth) / 2;
                int y = (this.getHeight() - newHeight) / 2;
                g.drawImage(gameLogo, x, y, newWidth, newHeight, null);
            }
        };

        JPanel charPanel = new JPanel();

        JPanel rightPanel = new JPanel(new GridLayout(2, 1));
        rightPanel.add(titlePanel);
        rightPanel.add(customPanel);

        customPanel.setOpaque(false);
        titlePanel.setOpaque(false);
        charPanel.setOpaque(false);
        rightPanel.setOpaque(false);

        this.add(charPanel);
        this.add(rightPanel);
    }

    /**
     * Paint this panel with the game image (sets "contain" size and central
     * position).
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        int newWidth = this.getWidth();
        int newHeight = this.getHeight();

        if ((float) this.getWidth() / this.backgroundImage.getWidth(null) > (float) this.getHeight() / this.backgroundImage.getHeight(null))
            newWidth = newHeight * this.backgroundImage.getWidth(null) / this.backgroundImage.getHeight(null);

        if ((float) this.getWidth() / this.backgroundImage.getWidth(null) < (float) this.getHeight() / this.backgroundImage.getHeight(null))
            newHeight = newWidth * this.backgroundImage.getHeight(null) / this.backgroundImage.getWidth(null);

        int x = (this.getWidth() - newWidth) / 2;
        int y = (this.getHeight() - newHeight) / 2;
        g.drawImage(this.backgroundImage, x, y, newWidth, newHeight, null);
    }
}
