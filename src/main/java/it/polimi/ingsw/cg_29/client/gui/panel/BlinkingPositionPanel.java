package it.polimi.ingsw.cg_29.client.gui.panel;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * Contains the implementation of a blinking panel that display a blinking icon.
 * 
 * @author Luca
 * @version 1.0
 */
public class BlinkingPositionPanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Not scaled icon image.
     */
    private ImageIcon background;

    /**
     * Scaled icon image.
     */
    private ImageIcon scaledBackground;

    /**
     * If this attribute is true resize the image of icon at each change of
     * frame size.
     */
    private boolean hd = false;

    /**
     * Reference to current visibility (to blink).
     */
    private boolean visible = true;

    /**
     * Reference to current width of icon.
     */
    private int currentWidth = 0;

    /**
     * Reference to current height of icon.
     */
    private int currentHeight = 0;

    /**
     * Creates new panel that starts blinking.
     */
    public BlinkingPositionPanel() {
        this.background = null;
        this.scaledBackground = this.background;

        Timer timer = new Timer(600, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (visible) {
                    visible = false;
                    setVisible(visible);
                } else {
                    visible = true;
                    setVisible(visible);
                }
            }
        });
        timer.setInitialDelay(0);
        timer.start();
    }

    /**
     * Paint this panel with the icon (sets "contain" size and central
     * position).
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        int newWidth = this.getWidth();
        int newHeight = this.getHeight();

        if ((float) this.getWidth() / this.background.getImage().getWidth(null) > (float) this.getHeight() / this.background.getImage().getHeight(null))
            newWidth = newHeight * this.background.getImage().getWidth(null) / this.background.getImage().getHeight(null);

        if ((float) this.getWidth() / this.background.getImage().getWidth(null) < (float) this.getHeight() / this.background.getImage().getHeight(null))
            newHeight = newWidth * this.background.getImage().getHeight(null) / this.background.getImage().getWidth(null);

        int x = (this.getWidth() - newWidth) / 2;
        int y = (this.getHeight() - newHeight) / 2;

        if (hd && this.currentWidth != newWidth && this.currentHeight != newHeight)
            this.scaledBackground = new ImageIcon(this.background.getImage().getScaledInstance(newWidth, -1, Image.SCALE_SMOOTH));

        this.currentWidth = newWidth;
        this.currentHeight = newHeight;

        g.drawImage(this.scaledBackground.getImage(), x, y, newWidth, newHeight, null);
    }

    /**
     * To allow a forced repaint reset current size.
     */
    public void resetCurrentIconSize() {
        this.currentWidth = 0;
        this.currentHeight = 0;
    }

    /**
     * Change the image icon with an alien icon
     */
    public void setAlienIcon() {
        this.setIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "player" + File.separatorChar + "alien_position.png");

    }

    /**
     * Change the image icon with an human icon
     */
    public void setHumanIcon() {
        this.setIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "player" + File.separatorChar + "human_position.png");
    }

    /**
     * Change the image icon with an noise icon
     */
    public void setNoiseIcon() {
        this.setIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "card" + File.separatorChar + "noise_in_any_sector.png");
    }

    /**
     * Change the image icon with an HumanAttack icon
     */
    public void setHumanAttackIcon() {
        this.setIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "card" + File.separatorChar + "attack.png");
    }

    /**
     * Change the image icon with an AlienAttack icon
     */
    public void setAlienAttackIcon() {
        this.setIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "card" + File.separatorChar + "attack.png");
    }

    /**
     * Change the image icon with an invisible icon
     */
    public void setInvisibleIcon() {
        this.setIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "image" + File.separatorChar + "Trasparente.png");
    }

    /**
     * Change the image icon with a spotlight icon
     */
    public void setSpotlightIcon() {
        this.setIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "card" + File.separatorChar + "spotlight.png");
    }

    /**
     * Change the image icon with a teleport icon
     */
    public void setTeleportIcon() {
        this.setIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "card" + File.separatorChar + "teleport.png");
    }

    /**
     * Change the image icon with a escape hatch red icon
     */
    public void setEscapeRedIcon() {
        this.setIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "card" + File.separatorChar + "escape_red.png");
    }

    /**
     * Change the image icon with a escape hatch green icon
     */
    public void setEscapeGreenIcon() {
        this.setIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "card" + File.separatorChar + "escape_green.png");
    }

    /**
     * Change the image icon with a new image icon in order to the path
     * received.
     * 
     * @param newImageIcon
     */
    public void setIcon(String newImageIcon) {
        this.background = new ImageIcon(newImageIcon);
        this.scaledBackground = this.background;
        resetCurrentIconSize();
        revalidate();
        repaint();
    }

    /**
     * Set the hd
     * 
     * @param hd
     *            if is false set the zone image in low definition.
     */
    public void setHD(boolean hd) {
        this.hd = hd;
        if (!hd) {
            this.scaledBackground = this.background;
        }
        repaint();
    }

}
