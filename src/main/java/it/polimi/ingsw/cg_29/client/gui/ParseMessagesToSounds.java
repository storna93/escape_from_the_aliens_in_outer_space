package it.polimi.ingsw.cg_29.client.gui;

import it.polimi.ingsw.cg_29.LoggerClass;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * ParseMessagesToSounds allows to play specific sound in order to the specific
 * String receive as a parameter.
 * 
 * @author Fulvio
 * @version 1.0
 */
public class ParseMessagesToSounds {

    /**
     * RES contains the name of the older "src" + File.separatorChar + "main" +
     * File.separatorChar + "resources"
     */
    private static final String RES = "src" + File.separatorChar + "main" + File.separatorChar + "resources";

    /**
     * Representation of matching between the information String that has a
     * sound effect and his respective File of sound effect.
     */
    private final Map<String, String> fileSoundEffectMap;

    /**
     * SOUND contains the name of the older "sound"
     */
    private static final String SOUND = "sound";

    /**
     * counter is used for not repeat Winner and Loser sound effect.
     */
    private int counter = 0;

    /**
     * muteAllSoundEffects lets play sound effects during the match or not.
     */
    private Boolean muteAllSoundEffects = false;

    /**
     * creates a ParseMessagesToSounds. This object allows to play a certain
     * sound effect in order to a specific message String that it received.
     */
    public ParseMessagesToSounds() {
        super();
        this.fileSoundEffectMap = new HashMap<String, String>();

        fileSoundEffectMap.put("NOISE IN SECTOR", "HelloNoiseEcho.wav");
        fileSoundEffectMap.put("SILENCE", "WindSilence.wav");
        fileSoundEffectMap.put("moved.", "FastWalk.wav");
        fileSoundEffectMap.put("passed.", "PassGong.wav");
        fileSoundEffectMap.put("(Alien) attacked", "WookieChewbaccaNoise.wav");
        fileSoundEffectMap.put("because he has activated Attack Item Card before", "MP5.wav");
        fileSoundEffectMap.put("avoids drawing a card because he has activated Sedatives Item Card before.", "EwokNoise.wav");
        fileSoundEffectMap.put("(Human) died; ", "HumanDie.wav");
        fileSoundEffectMap.put("(Alien) died; ", "AlienDie.wav");
        fileSoundEffectMap.put("used a Teleport Item", "Teleport1.wav");
        fileSoundEffectMap.put("used an Adrenaline Item", "YahooAdrenalineItem.wav");
        fileSoundEffectMap.put("used a Sedatives Item", "SwooshSedativesItem.wav");
        fileSoundEffectMap.put("used a Spotlight Item", "SaberdownSpotlight.wav");
        fileSoundEffectMap.put("used an Attack Item", "AttackItem.wav");
        fileSoundEffectMap.put("DefenseItem", "Jab.wav");
        fileSoundEffectMap.put("40", "HumanDefense.wav");
        fileSoundEffectMap.put("has disconnected from the match", "MissleLaunchDisconnected.wav");
        fileSoundEffectMap.put("has reconnected to the match", "R2D2Reconnected.wav");
        fileSoundEffectMap.put("has drawn an EscapeGreenCard", "EscapeHatchGreen.wav");
        fileSoundEffectMap.put("has drawn an EscapeRedCard", "EscapeHatchRed.wav");

    }

    /**
     * This method allows to start a certain sound effect using the respective
     * path file.
     * 
     * @param pathSoundFile
     *            that is the path of the sound effect file.
     */
    public void startSoundEffect(String pathSoundFile) {
        if (!muteAllSoundEffects) {
            File soundFile = new File(pathSoundFile);
            try {
                AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);
                Clip clip = AudioSystem.getClip();
                clip.open(audioIn);
                clip.start();
            } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
                LoggerClass.getLogger().warn(e);
            }
        }
    }

    /**
     * @return the muteAllSoundEffects
     */
    public Boolean getMuteAllSoundEffects() {
        return muteAllSoundEffects;
    }

    /**
     * @param muteAllSoundEffects
     *            the muteAllSoundEffects to set
     */
    public void setMuteAllSoundEffects(Boolean muteAllSoundEffects) {
        this.muteAllSoundEffects = muteAllSoundEffects;
    }

    /**
     * This method allows to start a certain sound effect using the respective
     * path file in order to the specific InfoPanel messageToShow received.
     * 
     * @param messageToShow
     */
    public void doInfoSound(String messageToShow) {
        for (Entry<String, String> sound : fileSoundEffectMap.entrySet())
            if (messageToShow.contains(sound.getKey()))
                this.startSoundEffect("." + File.separatorChar + RES + File.separatorChar + SOUND + File.separatorChar + sound.getValue());

    }

    /**
     * This method allows to start a certain sound effect using the respective
     * path file in order to the specific ChatPanel messageToShow received.
     * 
     * @param messageToShow
     */
    public void doChatSound(String messageToShow) {
        if (messageToShow.contains("I'm a pirate!"))

            this.startSoundEffect("." + File.separatorChar + RES + File.separatorChar + SOUND + File.separatorChar + "pirate.wav");

        else

            this.startSoundEffect("." + File.separatorChar + RES + File.separatorChar + SOUND + File.separatorChar + "ChatEnter.wav");

    }

    /**
     * This method allows to start a certain sound effect using the respective
     * path file in order to the specific special message received.
     * 
     * @param messageToShow
     */
    public void doSpecialSound(String message) {
        if (message.contains("YOU SHALL NOT PASS!"))

            this.startSoundEffect("." + File.separatorChar + RES + File.separatorChar + SOUND + File.separatorChar + "Gandalf.wav");

        if (message.contains("OAK:  <<NOW IS NOT THE TIME TO USE THAT!>>"))

            this.startSoundEffect("." + File.separatorChar + RES + File.separatorChar + SOUND + File.separatorChar + "Oak.wav");

    }

    /**
     * This method allows to start a certain sound effect using the respective
     * path file in order to the specific StatePanel messageToShow received.
     * 
     * @param messageToShow
     */
    public void doStateSound(String messageToShow) {
        if (messageToShow.contains("WINNER") && counter < 1) {

            this.startSoundEffect("." + File.separatorChar + RES + File.separatorChar + SOUND + File.separatorChar + "Winner.wav");

            counter++;
        }

        if (messageToShow.contains("LOSER") && counter < 1) {

            this.startSoundEffect("." + File.separatorChar + RES + File.separatorChar + SOUND + File.separatorChar + "SuperMarioLoser.wav");

            counter++;
        }

    }

}
