package it.polimi.ingsw.cg_29.client.rmi;

import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.client.Client;
import it.polimi.ingsw.cg_29.client.Connection;
import it.polimi.ingsw.cg_29.client.ReconnectionThread;
import it.polimi.ingsw.cg_29.gamemanager.commons.ClientRMIObjectInterface;
import it.polimi.ingsw.cg_29.gamemanager.commons.ServerRMIManagerObjectInterface;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * ClientRMI defines the client thread associated to the user that wants to play
 * using a RMI connection in the communication to the game Server and the game
 * Broker.
 * 
 * @author Fulvio
 * @version 1.0
 */
public class ClientRMI extends Connection {
    /**
     * clientRMIObject that is the reference to the respective clientRMI remote
     * object.
     */
    private ClientRMIObject clientRMIObject;

    /**
     * creates the ClientRMI connection associated to the main Client.
     * 
     * @param client
     *            that is the client for which it's create the RMIconnection.
     */
    public ClientRMI(Client client) {
        super(client);
    }

    /**
     * This method creates the connection between Server and Client.
     *
     * @throws IOException
     * @throws InterruptedException
     * @throws NotBoundException
     */
    private void startConnectionServerClient() throws IOException, InterruptedException, NotBoundException {

        this.clientRMIObject = new ClientRMIObject(this.getClient());

        Registry registry = LocateRegistry.getRegistry(7777);
        ServerRMIManagerObjectInterface server = (ServerRMIManagerObjectInterface) registry.lookup("Server");

        if (this.getClient().getClientId() == null || this.getClient().getControllerId() == null)
            server.firstConnection((ClientRMIObjectInterface) UnicastRemoteObject.exportObject(this.clientRMIObject, 0), "", "");
        else
            server.firstConnection((ClientRMIObjectInterface) UnicastRemoteObject.exportObject(this.clientRMIObject, 0), this.getClient().getClientId(), this.getClient().getControllerId());

    }

    @Override
    /**
     * This method manages some connection exceptions and start the connection between
     * Server and Client.
     */
    public void run() {
        this.tryToStart();
    }

    @Override
    /**
     * This method sends a Client Command (message), done by user, to the Server.
     * 
     * @param message
     */
    public void sendMessageToServer(String message) {

        try {
            if (this.clientRMIObject.getServerView() != null)
                this.clientRMIObject.getServerView().sendCommand(message);
            else
                this.getClient().showMessage("You have not been connected yet.");
        } catch (RemoteException e) {
            LoggerClass.getLogger().warn(e);
            this.tryToStart();
        }
    }

    /**
     * If Server is not found try reconnect launching ReconnectionThread
     */
    @Override
    public void tryToStart() {
        try {
            this.startConnectionServerClient();
        } catch (Exception e) {
            LoggerClass.getLogger().warn(e);
            this.getClient().showMessage("Server not found.");
            new ReconnectionThread(this).start();
        }
    }
}
