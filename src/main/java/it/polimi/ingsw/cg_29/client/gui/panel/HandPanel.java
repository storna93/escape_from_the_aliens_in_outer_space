package it.polimi.ingsw.cg_29.client.gui.panel;

import it.polimi.ingsw.cg_29.client.gui.FrameGUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

/**
 * Contains the implementation of interactive panel that displays player's hand
 * with his cards.
 * 
 * @author Luca
 * @version 1.0
 */
public class HandPanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Reference to frame GUI to call method to send command.
     */
    private FrameGUI frameGUI;

    /**
     * Contain a map of icons of cards.
     */
    private final transient Map<String, ImageIcon> itemCardImages;

    /**
     * The panel that represents the first card in player's hand.
     */
    private final JButton card1 = new JButton();

    /**
     * The panel that represents the second card in player's hand.
     */
    private final JButton card2 = new JButton();

    /**
     * The panel that represents the third card in player's hand.
     */
    private final JButton card3 = new JButton();

    /**
     * If this attribute is true then the cards in player's hand cannot be used.
     */
    private boolean alienHand = false;

    /**
     * Create a new hand panel that contains panels of the three cards (empty at
     * start).
     * 
     * @param frameGUI
     *            reference to frame GUI
     */
    public HandPanel(FrameGUI frameGUI) {

        this.frameGUI = frameGUI;

        this.itemCardImages = new HashMap<String, ImageIcon>();

        this.itemCardImages.put("AdrenalineItem", new ImageIcon(new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "card" + File.separatorChar + "adrenaline.png").getImage().getScaledInstance(50, -1, Image.SCALE_SMOOTH)));
        this.itemCardImages.put("AttackItem", new ImageIcon(new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "card" + File.separatorChar + "attack.png").getImage().getScaledInstance(50, -1, Image.SCALE_SMOOTH)));
        this.itemCardImages.put("DefenseItem", new ImageIcon(new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "card" + File.separatorChar + "defense.png").getImage().getScaledInstance(50, -1, Image.SCALE_SMOOTH)));
        this.itemCardImages.put("SedativesItem", new ImageIcon(new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "card" + File.separatorChar + "sedatives.png").getImage().getScaledInstance(50, -1, Image.SCALE_SMOOTH)));
        this.itemCardImages.put("SpotlightItem", new ImageIcon(new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "card" + File.separatorChar + "spotlight.png").getImage().getScaledInstance(50, -1, Image.SCALE_SMOOTH)));
        this.itemCardImages.put("TeleportItem", new ImageIcon(new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "card" + File.separatorChar + "teleport.png").getImage().getScaledInstance(50, -1, Image.SCALE_SMOOTH)));

        this.card1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendCommand(card1, 1);
            }
        });

        this.card2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendCommand(card2, 2);
            }
        });

        this.card3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendCommand(card3, 3);
            }
        });

        this.card1.setLayout(new BorderLayout());
        this.card2.setLayout(new BorderLayout());
        this.card3.setLayout(new BorderLayout());
        this.card1.setBorder(new CompoundBorder(this.card1.getBorder(), new EmptyBorder(5, 5, 5, 5)));
        this.card2.setBorder(new CompoundBorder(this.card2.getBorder(), new EmptyBorder(5, 5, 5, 5)));
        this.card3.setBorder(new CompoundBorder(this.card3.getBorder(), new EmptyBorder(5, 5, 5, 5)));

        JPanel cardsPanel = new JPanel(new GridLayout(1, 3));
        cardsPanel.add(this.card1);
        cardsPanel.add(this.card2);
        cardsPanel.add(this.card3);

        this.setLayout(new BorderLayout());
        this.add(new JLabel("HAND", SwingConstants.CENTER), BorderLayout.NORTH);
        this.add(cardsPanel, BorderLayout.CENTER);
        this.setPreferredSize(new Dimension(0, 110));

        List<String> cards = new ArrayList<String>();
        this.updateHand(cards);

    }

    /**
     * @param alienHand
     *            the alienHand to set
     */
    public void setAlienHand(boolean alienHand) {
        this.alienHand = alienHand;
    }

    /**
     * Parse a list of strings representing cards, to update panels of cards in
     * this hand panel.
     * 
     * @param cards
     *            list of strings representing cards
     */
    public void updateHand(List<String> cards) {

        this.card1.removeAll();
        this.card2.removeAll();
        this.card3.removeAll();
        this.card1.setEnabled(false);
        this.card2.setEnabled(false);
        this.card3.setEnabled(false);

        int numberOfCard = 0;

        for (String card : cards) {

            numberOfCard++;

            if (numberOfCard == 1) {
                if (!this.alienHand)
                    this.card1.setEnabled(true);
                this.card1.add(new JLabel(this.itemCardImages.get(card)), BorderLayout.CENTER);
                this.card1.add(new JLabel(card.substring(0, card.indexOf("Item")), SwingConstants.CENTER), BorderLayout.SOUTH);
            }
            if (numberOfCard == 2) {
                if (!this.alienHand)
                    this.card2.setEnabled(true);
                this.card2.add(new JLabel(this.itemCardImages.get(card)), BorderLayout.CENTER);
                this.card2.add(new JLabel(card.substring(0, card.indexOf("Item")), SwingConstants.CENTER), BorderLayout.SOUTH);
            }
            if (numberOfCard == 3) {
                if (!this.alienHand)
                    this.card3.setEnabled(true);
                this.card3.add(new JLabel(this.itemCardImages.get(card)), BorderLayout.CENTER);
                this.card3.add(new JLabel(card.substring(0, card.indexOf("Item")), SwingConstants.CENTER), BorderLayout.SOUTH);
            }
        }

        this.card1.revalidate();
        this.card1.repaint();
        this.card2.revalidate();
        this.card2.repaint();
        this.card3.revalidate();
        this.card3.repaint();

    }

    /**
     * Send command to frameGUI to activate card in a specific position of hand.
     * 
     * @param cardPanel
     *            reference to analyze what kind of chosen card
     * @param numberOfCard
     *            position of chosen card in hand
     */
    private void sendCommand(JButton cardPanel, int numberOfCard) {
        if (cardPanel.getComponentCount() == 2) {
            String card = ((JLabel) cardPanel.getComponent(1)).getText();
            if ("Defense".equals(card)) {
                JOptionPane.showMessageDialog(new JFrame(), "The Defense has a passive effect.", "MESSAGE", JOptionPane.PLAIN_MESSAGE);
                return;
            }
            if ("Spotlight".equals(card)) {
                JOptionPane.showMessageDialog(new JFrame(), "To use Spotlight you have to select a sector.", "MESSAGE", JOptionPane.PLAIN_MESSAGE);
                return;
            }
            frameGUI.sendCommand("activate_item(" + numberOfCard + ")");
        }
    }
}
