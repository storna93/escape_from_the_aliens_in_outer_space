package it.polimi.ingsw.cg_29.client.socket;

import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.client.Client;
import it.polimi.ingsw.cg_29.client.Connection;
import it.polimi.ingsw.cg_29.client.ReconnectionThread;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * Initializes connection of client with server and broker using socket and
 * creates the client handler to communicate.
 *
 * @author Luca
 * @version 1.0
 */
public class ClientSocket extends Connection {

    /**
     * Reference of port to connect with server by socket
     */
    private static final int PORT_SERVER = 29999;

    /**
     * Reference of port to connect with broker by socket
     */
    private static final int PORT_BROKER = 29998;

    /**
     * Reference to ip to connect by socket
     */
    private String ip;

    /**
     * Create an instance of ClientSocket for specific client.
     * 
     * @param client
     *            represent specific client.
     */
    public ClientSocket(Client client, String ip) {
        super(client);
        this.ip = ip;
    }

    /**
     * Send a message to server using socket.
     */
    @Override
    public void sendMessageToServer(String message) {
        try {
            Socket socket = new Socket(ip, PORT_SERVER);

            Thread outThread = new ClientOutHandler(new PrintWriter(socket.getOutputStream()), message);
            Thread inThread = new ClientInHandler(this.getClient(), new Scanner(socket.getInputStream()));

            outThread.start();
            inThread.start();

            outThread.join();
            inThread.join();
            socket.close();

        } catch (IOException e) {
            LoggerClass.getLogger().warn(e);
            this.getClient().showMessage("Server not found.");
            new ReconnectionThread(this).start();
        } catch (InterruptedException e) {
            LoggerClass.getLogger().warn(e);
            this.getClient().showMessage("Error occurred in Socket Communication Thread.");
            this.tryToStart();
        }
    }

    /**
     * Starts the ClientSocket connects to server and subscribes to broker.
     *
     * @throws IOException
     * @throws InterruptedException
     */
    private void startClient() throws IOException, InterruptedException {

        if (this.getClient().getClientId() == null || this.getClient().getControllerId() == null) {
            Socket firstServerSocket = new Socket(ip, PORT_SERVER);
            this.initClient(firstServerSocket);
        } else
            this.getClient().showMessage("Reconnecting established.");

        Socket brokerSocket = new Socket(ip, PORT_BROKER);
        this.subscribeToBroker(brokerSocket);
    }

    /**
     * Send a command to server to receive the ids of client and match
     * 
     * @param socketIn
     *            input stream of first socket connection
     * @param socketOut
     *            output stream of first socket connection
     * @throws IOException
     */
    private void getIdsFromServer(Scanner socketIn, PrintWriter socketOut) throws IOException {

        socketOut.println("first_connection");
        socketOut.flush();

        boolean idClientReceived = false;
        boolean idControllerReceived = false;
        while (!(idClientReceived && idControllerReceived)) {
            String line = socketIn.nextLine();
            if (line.matches("set_client_id\\(.*\\)")) {
                this.getClient().setClientId(line.substring("set_client_id(".length(), line.length() - 1));
                idClientReceived = true;
            }
            if (line.matches("set_controller_id\\(.*\\)")) {
                this.getClient().setControllerId(line.substring("set_controller_id(".length(), line.length() - 1));
                idControllerReceived = true;
            }
        }
    }

    /**
     * Subscribe to broker
     * 
     * @param brokerSocket
     *            socket connected with broker
     * @throws IOException
     */
    private void subscribeToBroker(Socket brokerSocket) throws IOException {

        Scanner socketIn = new Scanner(brokerSocket.getInputStream());
        PrintWriter socketOut = new PrintWriter(brokerSocket.getOutputStream());

        socketOut.println("subscribe_topic#" + this.getClient().getControllerId());
        socketOut.flush();

        new SubscriberThread(this.getClient(), socketIn).start();

    }

    /**
     * Establish first connection with server to get id of client and match
     * 
     * @param firstSocket
     *            socket connected with server
     * @throws IOException
     */
    private void initClient(Socket firstSocket) throws IOException {

        Scanner socketIn = new Scanner(firstSocket.getInputStream());
        PrintWriter socketOut = new PrintWriter(firstSocket.getOutputStream());

        this.getIdsFromServer(socketIn, socketOut);

        this.getClient().showMessage("Connection Established! (MATCH_ID: " + this.getClient().getControllerId() + " CLIENT_ID: " + this.getClient().getClientId() + ")");

    }

    /**
     * If Server is not found try reconnect launching ReconnectionThread
     */
    @Override
    public void tryToStart() {
        try {
            this.startClient();
        } catch (IOException e) {
            LoggerClass.getLogger().warn(e);
            this.getClient().showMessage("Server not found.");
            new ReconnectionThread(this).start();
        } catch (InterruptedException e) {
            LoggerClass.getLogger().warn(e);
            this.getClient().showMessage("Error occurred in Socket Communication Thread.");
            this.tryToStart();
        }
    }

    /**
     * Run this thread.
     */
    @Override
    public void run() {
        this.tryToStart();
    }
}
