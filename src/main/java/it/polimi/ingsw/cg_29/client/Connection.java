package it.polimi.ingsw.cg_29.client;

/**
 * Connection defines the abstract client thread associated to the user that
 * wants to play using a RMI connection or a Socket connection in the
 * communication to the game Server and the game Broker.
 * 
 * @author Fulvio
 * @version 1.0
 */
public abstract class Connection implements Runnable {
    private Client client;

    /**
     * creates the Client connection associated to the main Client.
     * 
     * @param client
     *            that is the client for which it's create the RMIconnection or
     *            the SocketConnection
     */
    public Connection(Client client) {
        this.client = client;
    }

    /**
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * This method sends a Client Command (message), done by user, to the
     * Server.
     * 
     * @param message
     */
    public abstract void sendMessageToServer(String message);

    /**
     * If Server is not found try reconnect after few time.
     */
    public abstract void tryToStart();
}
