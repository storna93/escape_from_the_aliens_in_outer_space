package it.polimi.ingsw.cg_29.client.gui.panel;

import it.polimi.ingsw.cg_29.client.gui.FrameGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.LayoutManager;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;

/**
 * Contains the implementation of GamePanel which includes all interactive
 * panels to play a match
 * 
 * @author Luca
 * @version 1.0
 */
public class GamePanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Random integer to choose icon that represents human player.
     */
    private final int randomHumanNumber;

    /**
     * Random integer to choose icon that represents alien player.
     */
    private final int randomAlienNumber;

    /**
     * Reference to frame GUI
     */
    private FrameGUI frameGUI;

    /**
     * Saves the current state of player in this variable to upload informations
     * only if the state changes.
     */
    private String currentState = "";

    /**
     * Reference to panel in which display public informations.
     */
    private InfoPanel infoPanel;

    /**
     * Reference to panel in which display chat messages.
     */
    private ChatPanel chatPanel;

    /**
     * Reference to panel in which display interactive image of zone.
     */
    private ZonePanel zonePanel;

    /**
     * Reference to panel in which display player state.
     */
    private StatePanel statePanel;

    /**
     * Reference to panel in which display interactive buttons.
     */
    private ActionPanel actionPanel;

    /**
     * Reference to panel in which display player's cards.
     */
    private HandPanel handPanel;

    /**
     * Creates a GamePanel with all interactive panels to play a match
     * 
     * @param frameGUI
     *            reference to frameGUI
     */
    public GamePanel(FrameGUI frameGUI) {

        this.frameGUI = frameGUI;

        Random rand = new Random();
        this.randomHumanNumber = rand.nextInt(4);
        this.randomAlienNumber = rand.nextInt(4) + 4;

        ImageIcon zoneImage = this.frameGUI.getZoneImage();
        List<String> voidSectorsZone = this.frameGUI.getVoidSectorsZone();

        this.zonePanel = new ZonePanel(this.frameGUI, zoneImage, voidSectorsZone);
        this.infoPanel = new InfoPanel();
        this.statePanel = new StatePanel();
        this.actionPanel = new ActionPanel(this.frameGUI);
        this.handPanel = new HandPanel(this.frameGUI);
        this.chatPanel = new ChatPanel(this.frameGUI);

        JPanel zoneBackground = new ZoneBackground(new BorderLayout());
        zoneBackground.add(this.zonePanel, BorderLayout.CENTER);

        JPanel letftPanelTop = new JPanel(new BorderLayout());
        letftPanelTop.add(this.statePanel, BorderLayout.NORTH);
        letftPanelTop.add(this.handPanel, BorderLayout.CENTER);

        JPanel letftPanelBottom = new JPanel(new GridLayout(0, 1));
        letftPanelBottom.add(this.infoPanel);
        letftPanelBottom.add(this.chatPanel);

        JPanel letftPanel = new JPanel(new BorderLayout());
        letftPanel.add(letftPanelTop, BorderLayout.NORTH);
        letftPanel.add(letftPanelBottom, BorderLayout.CENTER);

        JPanel bottomPanel = new JPanel(new BorderLayout());
        bottomPanel.add(zoneBackground, BorderLayout.CENTER);
        bottomPanel.add(letftPanel, BorderLayout.WEST);

        this.setLayout(new BorderLayout());

        this.add(this.actionPanel, BorderLayout.NORTH);
        this.add(bottomPanel, BorderLayout.CENTER);
    }

    /**
     * @return the zonePanel
     */
    public ZonePanel getZonePanel() {
        return zonePanel;
    }

    /**
     * Show a message in the info panel and show on the zone the last info that
     * refers to a certain Sector.
     * 
     * @param message
     *            message to show
     */
    public void showInfo(String message) {
        this.infoPanel.addInfoMessage(message);

    }

    /**
     * Show a message in the chat panel
     * 
     * @param message
     *            message to show
     */
    public void showChat(String message) {
        this.chatPanel.addChatMessage(message);

    }

    /**
     * Change the informations of panels by player's character.
     * 
     * @param character
     *            the player's character ("Human", "Alien")
     */
    private void updateByCharacter(String character) {
        if ("Human".equals(character) || "Alien".equals(character)) {
            Color color1;
            Color color2;
            int numberOfIcon;

            if ("Alien".equals(character)) {
                color1 = new Color(252, 75, 12);
                color2 = new Color(96, 33, 11);
                numberOfIcon = this.randomAlienNumber;
                this.actionPanel.setAlienButtons();
                this.handPanel.setAlienHand(true);
                this.zonePanel.getPositionIcon().setAlienIcon();
            } else {
                color1 = new Color(6, 167, 225);
                color2 = new Color(5, 84, 113);
                numberOfIcon = this.randomHumanNumber;
                this.actionPanel.setHumanButtons();
                this.handPanel.setAlienHand(false);
                this.zonePanel.getPositionIcon().setHumanIcon();
            }

            this.statePanel.updateCharacter(numberOfIcon);

            this.setBorder(new EtchedBorder(SoftBevelBorder.LOWERED, color1, color2));
            this.zonePanel.setBorder(new EtchedBorder(SoftBevelBorder.LOWERED, color1, color2));
            this.actionPanel.setBorder(new EtchedBorder(SoftBevelBorder.LOWERED, color1, color2));
            this.statePanel.setBorder(new EtchedBorder(SoftBevelBorder.LOWERED, color1, color2));
            this.handPanel.setBorder(new EtchedBorder(SoftBevelBorder.LOWERED, color1, color2));
            this.infoPanel.setBorder(new EtchedBorder(SoftBevelBorder.LOWERED, color1, color2));
            this.chatPanel.setBorder(new EtchedBorder(SoftBevelBorder.LOWERED, color1, color2));
            this.handPanel.setBorder(new CompoundBorder(handPanel.getBorder(), new EmptyBorder(0, 5, 5, 5)));
        }
    }

    /**
     * Change the informations of panels by current player's state.
     * 
     * @param state
     *            string that represents current player's state
     */
    public void updateState(String state) {

        if (this.currentState.equals(state))
            return;

        this.currentState = state;

        String controlString = "";

        controlString = "character: ";
        if (state.contains(controlString)) {
            String character = state.substring(state.indexOf(controlString) + controlString.length());
            character = character.substring(0, character.indexOf("\n"));
            this.updateByCharacter(character);
        }

        controlString = "CLIENT_ID: ";
        if (state.contains(controlString)) {
            String clientId = state.substring(state.indexOf(controlString) + controlString.length());
            clientId = clientId.substring(0, clientId.indexOf("\n"));
            this.statePanel.updateClientId(clientId);
        }

        controlString = "[state: ";
        if (state.contains(controlString)) {
            String completeState = state.substring(state.indexOf(controlString) + controlString.length());
            completeState = completeState.substring(0, completeState.indexOf("\n"));

            controlString = " (roundState: ";
            if (state.contains(controlString)) {
                String playerState = completeState.substring(0, completeState.indexOf(controlString));
                String roundState = completeState.substring(completeState.indexOf(controlString) + controlString.length(), completeState.indexOf(")]"));
                this.statePanel.updatePlayerState(playerState);
                this.statePanel.updateRoundState(roundState);
            }
        }

        controlString = "hand: [";
        if (state.contains(controlString)) {
            String completeHand = state.substring(state.indexOf(controlString) + controlString.length());
            completeHand = completeHand.substring(0, completeHand.indexOf("]"));

            updateHandPanel(completeHand);
        }

        controlString = "currentPosition: ";
        if (state.contains(controlString)) {
            String sector = state.substring(state.indexOf(controlString) + controlString.length());
            sector = sector.substring(0, sector.indexOf("\n"));
            this.zonePanel.setCurrentPosition(sector);
        }
    }

    /**
     * Change the informations of hand panel by current player's state.
     * 
     * @param hand
     *            string that represents the cards of player.
     */
    private void updateHandPanel(String hand) {

        String cardsString = hand;
        String controlString;
        List<String> cards = new ArrayList<String>();

        controlString = "(1) ";
        if (cardsString.contains(controlString)) {
            String card1 = cardsString.substring(cardsString.indexOf(controlString) + controlString.length());
            if (card1.contains(",")) {
                card1 = card1.substring(0, card1.indexOf(","));
                cardsString = cardsString.substring(cardsString.indexOf(",") + 1);
            }
            cards.add(card1);
        }
        controlString = "(2) ";
        if (cardsString.contains(controlString)) {
            String card2 = cardsString.substring(cardsString.indexOf(controlString) + controlString.length());
            if (card2.contains(",")) {
                card2 = card2.substring(0, card2.indexOf(","));
                cardsString = cardsString.substring(cardsString.indexOf(",") + 1);
            }
            cards.add(card2);
        }
        controlString = "(3) ";
        if (cardsString.contains(controlString)) {
            String card3 = cardsString.substring(cardsString.indexOf(controlString) + controlString.length());
            if (card3.contains(",")) {
                card3 = card3.substring(0, card3.indexOf(","));
            }
            cards.add(card3);
        }

        this.handPanel.updateHand(cards);
    }

    /**
     * Panel that contains a background that fills it. Used as background for
     * zonePanel.
     * 
     * @author Luca
     *
     */
    private class ZoneBackground extends JPanel {

        /**
         * 
         */
        private static final long serialVersionUID = 1L;

        /**
         * Background image
         */
        private ImageIcon background = new ImageIcon(new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "image" + File.separatorChar + "space2.jpg").getImage().getScaledInstance(900, -1, Image.SCALE_SMOOTH));

        /**
         * Return a ZoneBackground with specific LayoutManager
         * 
         * @param layoutManager
         *            specific LayoutManager
         */
        private ZoneBackground(LayoutManager layoutManager) {
            super(layoutManager);
        }

        /**
         * Fill this panel with the background image.
         */
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);

            int newWidth = this.getWidth();
            int newHeight = this.getHeight();

            if ((float) this.getWidth() / this.background.getImage().getWidth(null) < (float) this.getHeight() / this.background.getImage().getHeight(null))
                newWidth = newHeight * this.background.getImage().getWidth(null) / this.background.getImage().getHeight(null);

            if ((float) this.getWidth() / this.background.getImage().getWidth(null) > (float) this.getHeight() / this.background.getImage().getHeight(null))
                newHeight = newWidth * this.background.getImage().getHeight(null) / this.background.getImage().getWidth(null);

            int x = (this.getWidth() - newWidth) / 2;
            int y = (this.getHeight() - newHeight) / 2;
            g.drawImage(this.background.getImage(), x, y, newWidth, newHeight, null);
        }
    }

}
