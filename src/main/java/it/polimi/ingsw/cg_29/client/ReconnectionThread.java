package it.polimi.ingsw.cg_29.client;

import it.polimi.ingsw.cg_29.LoggerClass;

/**
 * Thread that try to reconnect to Server after few seconds.
 * 
 * @author Luca
 * @version 1.0
 */
public class ReconnectionThread extends Thread {

    /**
     * Time in second to reconnect.
     */
    public static final int SECONDS_TO_RECONNECT = 60;

    /**
     * reference to current client
     */
    private final Connection currentClientConnection;

    /**
     * return a new ReconnectionThread whit the reference to the client that
     * create it
     * 
     * @param currentClient
     */
    public ReconnectionThread(Connection currentClientConnection) {
        this.currentClientConnection = currentClientConnection;
    }

    /**
     * Try to reconnect to the server in few seconds
     */
    @Override
    public void run() {
        try {
            this.currentClientConnection.getClient().showMessage("I try to reconnect to the server in " + SECONDS_TO_RECONNECT + " seconds ...");
            Thread.sleep((long) SECONDS_TO_RECONNECT * 1000);
            this.currentClientConnection.tryToStart();
        } catch (InterruptedException e) {
            LoggerClass.getLogger().warn(e);
            this.currentClientConnection.getClient().showMessage("Timer to reconnect has been interrupted.");
        }
    }

}
