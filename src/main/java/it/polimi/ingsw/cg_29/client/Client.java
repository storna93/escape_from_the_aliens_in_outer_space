package it.polimi.ingsw.cg_29.client;

import it.polimi.ingsw.cg_29.client.rmi.ClientRMI;
import it.polimi.ingsw.cg_29.client.socket.ClientSocket;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Client defines the abstract client executable associated to the user that
 * wants to play a match. ClientCLI and ClientGUI extend Client. The user could
 * use CLI interface or GUI interface for playing a match launching one of those
 * executables.
 * 
 * @author Fulvio
 * @version 1.0
 */
public abstract class Client implements Runnable {

    /**
     * clientId is the reference to the Client executable launching by user that
     * want to play a match.
     */
    private String clientId = null;

    /**
     * controllerId is the reference to the certain controller that manage the
     * match in which this Client is involved.
     */
    private String controllerId = null;

    /**
     * connection is the reference to the way to communicate with Broker and
     * Server chosen by the Client.
     */
    private Connection connection = null;

    /**
     * @return the clientId
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * @param clientId
     *            the clientId to set
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    /**
     * @return the controllerId
     */
    public String getControllerId() {
        return controllerId;
    }

    /**
     * @param controllerId
     *            the controllerId to set
     */
    public void setControllerId(String controllerId) {
        this.controllerId = controllerId;
    }

    /**
     * @return the connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * This method establish the Socket Connection or the RMI Connection in
     * order to the choice done by user (1 or 2).
     * 
     * @param choice
     */
    public void establishConnection(String choice) {

        if ("1".equals(choice) || "2".equals(choice)) {
            ExecutorService executor = Executors.newCachedThreadPool();

            if ("1".equals(choice)) {
                this.showMessage("Connection with Socket.");
                this.connection = new ClientSocket(this, "127.0.0.1");
            } else if ("2".equals(choice)) {
                this.showMessage("Connection with RMI.");
                this.connection = new ClientRMI(this);
            }

            executor.submit(this.getConnection());

        }
    }

    /**
     * This method send a command (a request) to the Server.
     * 
     * @param message
     *            that is the command requested to the Server by a certain
     *            client.
     */
    public void sendMessage(String message) {
        if (this.clientId != null && this.controllerId != null && this.connection != null) {
            connection.sendMessageToServer(this.getControllerId() + "#" + this.getClientId() + "#" + message);
        }
    }

    /**
     * This method, when called, shows a response message (send by Server) or a
     * published message (send by Broker) to the user.
     * 
     * @param message
     */
    public abstract void showMessage(String message);

}
