package it.polimi.ingsw.cg_29.client.gui.panel;

import it.polimi.ingsw.cg_29.client.gui.FrameGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.border.MatteBorder;

/**
 * Contains the implementation of interactive panel that displays buttons of
 * player's action.
 * 
 * @author Luca
 * @version 1.0
 */
public class ActionPanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Reference to frame GUI to call method to send command.
     */
    private FrameGUI frameGUI;

    /**
     * Reference to attackButton that is hidden when the player character is a
     * human.
     */
    private JButton attackButton;

    /**
     * Reference to useAttackButton that is hidden when the player character is
     * a alien.
     */
    private JButton useAttackButton;

    /**
     * Reference to useSedativesButton that is hidden when the player character
     * is a alien.
     */
    private JButton useSedativesButton;

    /**
     * Create a new ActionPanel that contains buttons of player's action.
     * 
     * @param frameGUI
     *            reference to frame GUI
     */
    public ActionPanel(FrameGUI frameGUI) {
        this.frameGUI = frameGUI;

        JMenuBar menuBar = new JMenuBar();
        JMenu menuOption = new JMenu();
        JRadioButtonMenuItem hdButton = new JRadioButtonMenuItem("HD");
        JRadioButtonMenuItem muteButton = new JRadioButtonMenuItem("MUTE");
        JMenuItem exitButton = new JMenuItem("EXIT");

        menuBar.add(menuOption);
        menuOption.add(hdButton);
        menuOption.add(muteButton);
        menuOption.add(exitButton);

        menuBar.setBorder(new MatteBorder(0, 0, 0, 1, Color.gray));
        Icon menuIcon = new ImageIcon(new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "image" + File.separatorChar + "menu.png").getImage().getScaledInstance(-1, 25, Image.SCALE_SMOOTH));
        menuOption.setIcon(menuIcon);

        hdButton.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    setHD(true);
                } else {
                    setHD(false);
                }
            }
        });

        muteButton.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    setMute(true);
                } else {
                    setMute(false);
                }
            }
        });

        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object[] options = { "YES", "NO" };
                int choice = JOptionPane.showOptionDialog(new JFrame(), "Are you sure to exit and leave the game?", "Exit", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[1]);
                if (choice == 0)
                    disconnectAndExit();
            }
        });

        JButton drawButton = new JButton("DRAW");
        drawButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendCommand("draw");
            }
        });

        this.attackButton = new JButton("ATTACK");
        this.attackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendCommand("attack");
            }
        });

        this.useAttackButton = new JButton("USE ATTACK");
        useAttackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendCommand("use_attack");
            }
        });

        this.useSedativesButton = new JButton("USE SEDATIVES");
        useSedativesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendCommand("use_sedatives");
            }
        });

        JButton passButton = new JButton("PASS");
        passButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendCommand("pass");
            }
        });

        JButton disconnectButton = new JButton("DISCONNECT");
        disconnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendCommand("disconnect");
            }
        });

        JButton reconnectButton = new JButton("RECONNECT");
        reconnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendCommand("reconnect");
            }
        });

        JPanel leftPanel = new JPanel();
        JPanel rightPanel = new JPanel();

        leftPanel.add(drawButton);
        leftPanel.add(this.attackButton);
        leftPanel.add(this.useAttackButton);
        leftPanel.add(this.useSedativesButton);
        leftPanel.add(passButton);

        rightPanel.add(disconnectButton);
        rightPanel.add(reconnectButton);

        this.setLayout(new BorderLayout());
        this.add(menuBar, BorderLayout.WEST);
        this.add(leftPanel, BorderLayout.CENTER);
        this.add(rightPanel, BorderLayout.EAST);
    }

    /**
     * When hd button is pressed activates or deactivates hd in frameGUI
     * 
     * @param hd
     *            true to activate
     */
    private void setHD(boolean hd) {
        this.frameGUI.setHD(hd);
    }

    /**
     * 
     * Send command to frameGUI to do specific action.
     * 
     * @param command
     *            specific action to do.
     */
    private void sendCommand(String command) {
        this.frameGUI.sendCommand(command);
    }

    /**
     * Hides attackButton.
     */
    public void setHumanButtons() {
        this.useAttackButton.setVisible(true);
        this.useSedativesButton.setVisible(true);
        this.attackButton.setVisible(false);
    }

    /**
     * Hides useAttackButton and useSedativesButton.
     */
    public void setAlienButtons() {
        this.useAttackButton.setVisible(false);
        this.useSedativesButton.setVisible(false);
        this.attackButton.setVisible(true);
    }

    /**
     * If a client want to quit disconnects him and close window.
     */
    private void disconnectAndExit() {
        this.frameGUI.sendCommand("disconnect");
        this.frameGUI.dispatchEvent(new WindowEvent(this.frameGUI, WindowEvent.WINDOW_CLOSING));
    }

    /**
     * Set the attribute muteAllSoundEffects of ParseMessagesToSounds
     * 
     * @param mute
     *            state to set
     */
    private void setMute(boolean mute) {
        this.frameGUI.getParserSound().setMuteAllSoundEffects(mute);
    }
}
