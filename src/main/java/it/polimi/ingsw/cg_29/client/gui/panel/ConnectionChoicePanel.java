package it.polimi.ingsw.cg_29.client.gui.panel;

import it.polimi.ingsw.cg_29.client.gui.FrameGUI;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

/**
 * Contains the implementation of the panel that is displayed before starting
 * match to choose the type of connection.
 * 
 * @author Luca
 * @version 1.0
 */
public class ConnectionChoicePanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Reference to frameGUI to launch game after choice.
     */
    private FrameGUI frameGUI;

    /**
     * Creates a new panel that displays the type of connection to choose.
     * 
     * @param frameGUI
     *            reference to frameGUI to launch game after choice.
     */
    public ConnectionChoicePanel(FrameGUI frameGUI) {

        this.frameGUI = frameGUI;

        JButton startSocketButton = new JButton();
        startSocketButton.setLayout(new BorderLayout());
        JLabel socketTopLabel = new JLabel("START GAME with", SwingConstants.CENTER);
        JLabel socketBottomLabel = new JLabel("SOCKET Connection", SwingConstants.CENTER);
        startSocketButton.add(BorderLayout.NORTH, socketTopLabel);
        startSocketButton.add(BorderLayout.SOUTH, socketBottomLabel);
        startSocketButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                launchGame("1");
            }
        });

        JButton startRMIButton = new JButton();
        startRMIButton.setLayout(new BorderLayout());
        JLabel rmiTopLabel = new JLabel("START GAME with", SwingConstants.CENTER);
        JLabel rmiBottomLabel = new JLabel("RMI Connection", SwingConstants.CENTER);
        startRMIButton.add(BorderLayout.NORTH, rmiTopLabel);
        startRMIButton.add(BorderLayout.SOUTH, rmiBottomLabel);
        startRMIButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                launchGame("2");
            }
        });

        this.setBorder(new CompoundBorder(this.getBorder(), new EmptyBorder(50, 0, 0, 0)));

        this.add(startSocketButton);
        this.add(new JLabel("          "));
        this.add(startRMIButton);

    }

    /**
     * Launch game in frameGUI with a specific type of connection.
     * 
     * @param choise
     *            represents type of connection
     */
    private void launchGame(String choise) {
        this.frameGUI.waitForStartGame(choise);
    }

}
