package it.polimi.ingsw.cg_29.client;

import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.client.gui.FrameGUI;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * ClientGUI defines the client executable associated to the user that wants to
 * play using a GUI interface.
 * 
 * @author Fulvio
 * @version 1.0
 */
public class ClientGUI extends Client {

    /**
     * frameGUI is the first display showed to a certain user.
     */
    FrameGUI frameGUI;

    /**
     * Create an instance of ClientGUI
     */
    public ClientGUI() {
        super();
        this.frameGUI = new FrameGUI(this);
    }

    @Override
    /**
     * This method allows the user to establish a connection with a GUI interface, 
     * that shows him the first display in which user could choice to establishing a match 
     * in RMI connection or in Socket connection
     */
    public void run() {
        ExecutorService executor = Executors.newCachedThreadPool();
        executor.submit(this.frameGUI);
    }

    @Override
    /**
     * This method, when called, shows a response message (send by Server)
     * or a published message (send by Broker) to the user.  
     * 
     * @param message
     */
    public void showMessage(String message) {
        if (this.frameGUI != null)
            this.frameGUI.parseMessage(message);
        LoggerClass.getLogger().info(message);
    }

    /**
     * This method starts a GUI interface useful for the user.
     * 
     * @param args
     */
    public static void main(String[] args) {
        Executors.newCachedThreadPool().submit(new ClientGUI());
    }
}
