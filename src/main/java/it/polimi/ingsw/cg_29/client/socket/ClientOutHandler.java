package it.polimi.ingsw.cg_29.client.socket;

import java.io.PrintWriter;

/**
 * contains the Handler which is responsible of sending specific command to the
 * server
 *
 * @author Luca
 * @version 1.0
 */
public class ClientOutHandler extends Thread {
    /**
     * contains the writer which is used to send messages to the server
     */
    private PrintWriter socketOut;
    private String messageToSend;

    /**
     * creates a new Handler responsible of sending specific command to the
     * server
     *
     * @param socketOut
     */
    public ClientOutHandler(PrintWriter socketOut, String messageToSend) {
        this.socketOut = socketOut;
        this.messageToSend = messageToSend;
    }

    /**
     * executes the client handler
     */
    @Override
    public void run() {
        socketOut.println(this.messageToSend);
        socketOut.flush();
        return;
    }
}
