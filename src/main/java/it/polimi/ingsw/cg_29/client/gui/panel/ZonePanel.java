package it.polimi.ingsw.cg_29.client.gui.panel;

import it.polimi.ingsw.cg_29.client.gui.FrameGUI;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * Contains the implementation of interactive panel that displays zone images
 * with clickable sectors and the information about player's position
 * 
 * @author Luca
 * @version 1.0
 */
public class ZonePanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Reference to frame GUI to call method to send command.
     */
    private FrameGUI frameGUI;

    /**
     * The image of zone of current match.
     */
    private ImageIcon zoneImage;

    /**
     * The image of zone of current match that is scaled according to the size
     * of frame.
     */
    private ImageIcon zoneBackground;

    /**
     * Current width of zone background.
     */
    private float zoneBackgroundWidth;

    /**
     * Current height of zone background.
     */
    private float zoneBackgroundHeight;

    /**
     * If this attribute is true resize the image of zone at each change of
     * frame size.
     */
    private boolean hd = false;

    /**
     * A list of non-clickable sectors.
     */
    private transient List<String> voidSectorsZone;

    /**
     * Zoom indicator
     */
    private int zoom = 1;

    /**
     * X position of mouse from center when zoom.
     */
    private int mouseXZoomCenter = 0;

    /**
     * Y position of mouse from center when zoom.
     */
    private int mouseYZoomCenter = 0;

    /**
     * Maximum width of zone when zoom or resize.
     */
    private int maxZoneWidth = 2500;

    /**
     * Reference to current position of player.
     */
    private String currentPosition = null;
    /**
     * Reference to last notified position of one of the rival players.
     */
    private String lastNotifiedPosition = "A00";

    /**
     * Reference to blinking panel that displays the player's icon on the zone.
     */
    private BlinkingPositionPanel positionIcon;

    /**
     * Reference to blinking panel that displays the last rival players position
     * icon notified on the zone.
     */
    private BlinkingPositionPanel lastNotifiedPositionIcon;

    /**
     * Creates and initializes a new zone panel.
     * 
     * @param frameGUI
     *            reference to frameGUI.
     * @param img
     *            reference to the image of zone of current match.
     * @param voidSectorsZone
     *            list of non-clickable sectors.
     */
    public ZonePanel(FrameGUI frameGUI, ImageIcon img, List<String> voidSectorsZone) {
        this.frameGUI = frameGUI;
        this.zoneImage = img;
        this.voidSectorsZone = voidSectorsZone;
        this.positionIcon = new BlinkingPositionPanel();

        this.lastNotifiedPositionIcon = new BlinkingPositionPanel();
        this.lastNotifiedPositionIcon.setInvisibleIcon();

        this.setHD(false);
        this.setOpaque(false);
        this.setLayout(null);
        this.addListener();
    }

    /**
     * Paint this panel with the zone image (sets "contain" size, specific zoom
     * and central position).
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        calculatesContainBackgroundSize();

        int x = mouseXZoomCenter + (int) (this.getWidth() - this.zoneBackgroundWidth) / 2;
        int y = mouseYZoomCenter + (int) (this.getHeight() - this.zoneBackgroundHeight) / 2;

        if (hd) {
            this.zoneBackground = new ImageIcon(this.zoneImage.getImage().getScaledInstance((int) this.zoneBackgroundWidth, -1, Image.SCALE_SMOOTH));
        }

        g.drawImage(this.zoneBackground.getImage(), x, y, (int) this.zoneBackgroundWidth, (int) this.zoneBackgroundHeight, null);

        if (this.currentPosition != null) {
            drawCurrentPosition();
            drawLastNotifiedPosition();
        }
    }

    /**
     * Calculates size of background image of zone to contain this panel.
     */
    private void calculatesContainBackgroundSize() {
        this.zoneBackgroundWidth = this.getWidth();
        this.zoneBackgroundHeight = this.getHeight();

        if ((float) this.getWidth() / this.zoneBackground.getImage().getWidth(null) > (float) this.getHeight() / this.zoneBackground.getImage().getHeight(null))
            this.zoneBackgroundWidth = this.zoneBackgroundHeight * this.zoneBackground.getImage().getWidth(null) / this.zoneBackground.getImage().getHeight(null);

        if ((float) this.getWidth() / this.zoneBackground.getImage().getWidth(null) < (float) this.getHeight() / this.zoneBackground.getImage().getHeight(null))
            this.zoneBackgroundHeight = this.zoneBackgroundWidth * this.zoneBackground.getImage().getHeight(null) / this.zoneBackground.getImage().getWidth(null);

        if (this.zoneBackgroundWidth * this.zoom > this.maxZoneWidth)
            this.zoom = (int) (this.maxZoneWidth / this.zoneBackgroundWidth) - 1;

        this.zoneBackgroundWidth *= this.zoom;
        this.zoneBackgroundHeight *= this.zoom;
    }

    /**
     * Add the positionIcon in this panel at current player position.
     */
    public void drawCurrentPosition() {
        int row = Integer.parseInt(this.currentPosition.substring(1)) - 1;
        int col = ((int) this.currentPosition.charAt(0)) - 65;

        this.add(this.positionIcon);
        this.positionIcon.setOpaque(false);
        this.positionIcon.setLocation(getColLocationOfSector(col), getRowLocationOfSector(row, col));
    }

    /**
     * Add the lastNotifiedPositionIcon in this panel at last notified rival
     * player position.
     */
    public void drawLastNotifiedPosition() {
        int row = Integer.parseInt(this.lastNotifiedPosition.substring(1)) - 1;
        int col = ((int) this.lastNotifiedPosition.charAt(0)) - 65;

        this.add(this.lastNotifiedPositionIcon);
        this.lastNotifiedPositionIcon.setOpaque(false);
        this.lastNotifiedPositionIcon.setLocation(getColLocationOfSector(col), getRowLocationOfSector(row, col));

    }

    /**
     * Calculates the position in pixel of specific column.
     * 
     * @param col
     *            specific number of column
     * @return the position in pixel of specific column
     */
    private int getColLocationOfSector(int col) {
        int colLocation = 0;

        int panelWidth = getWidth();

        // To center the mouse click in column
        float colCenterBorder = zoneBackgroundWidth / 147;
        float borderWidth = colCenterBorder + mouseXZoomCenter + (panelWidth - zoneBackgroundWidth) / 2;

        float numCol = 23.4f;
        float colWidth = zoneBackgroundWidth / numCol;

        colLocation = (int) ((colWidth * col) + borderWidth);

        return colLocation;
    }

    /**
     * Calculates the position in pixel of specific row.
     * 
     * @param row
     *            specific number of row
     * @param col
     *            specific number of column to choose correct half row
     * @return the position in pixel of specific row
     */
    private int getRowLocationOfSector(int row, int col) {
        int rowLocation = 0;

        int panelHeight = getHeight();

        float borderHeight = mouseYZoomCenter + (panelHeight - zoneBackgroundHeight) / 2;

        float numHalfRow = 29;
        float rowHeight = zoneBackgroundHeight / numHalfRow;

        if (col % 2 == 0)
            rowLocation = (int) ((rowHeight * 2 * row) + borderHeight);
        else
            rowLocation = (int) ((rowHeight * 2 * (row + 0.5)) + borderHeight);

        this.positionIcon.setSize((int) (zoneBackgroundWidth / 21.8f), (int) (zoneBackgroundHeight / 13.8f));

        this.lastNotifiedPositionIcon.setSize((int) (zoneBackgroundWidth / 21.8f), (int) (zoneBackgroundHeight / 13.8f));

        return rowLocation;
    }

    /**
     * Set the hd
     * 
     * @param hd
     *            if is false set the zone image in low definition.
     */
    public void setHD(boolean hd) {
        this.hd = hd;
        this.positionIcon.setHD(hd);
        if (!hd) {
            this.zoneBackground = new ImageIcon(this.zoneImage.getImage().getScaledInstance(1870, -1, Image.SCALE_SMOOTH));
        } else {
            this.positionIcon.resetCurrentIconSize();
        }
        repaint();
    }

    /**
     * @return the positionIcon
     */
    public BlinkingPositionPanel getPositionIcon() {
        return positionIcon;
    }

    /**
     * @return the lastNotifiedPositionIcon
     */
    public BlinkingPositionPanel getLastNotifiedPositionIcon() {
        return lastNotifiedPositionIcon;
    }

    /**
     * @param currentPosition
     *            the currentPosition to set
     */
    public void setCurrentPosition(String currentPosition) {
        this.currentPosition = currentPosition;
        repaint();
    }

    /**
     * @param lastNotifiedPosition
     *            the lastNotifiedPosition to set
     */
    public void setLastNotifiedPosition(String lastNotifiedPosition) {
        this.lastNotifiedPosition = lastNotifiedPosition;
        repaint();
    }

    /**
     * Add listener to zoom with mouse wheel and to click sectors of zone.
     */
    private void addListener() {

        this.addMouseWheelListener(new java.awt.event.MouseAdapter() {

            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                int steps = e.getWheelRotation();
                if (steps == -1) {
                    mouseXZoomCenter += (getWidth() / 2) - e.getX();
                    mouseYZoomCenter += (getHeight() / 2) - e.getY();
                } else {
                    mouseXZoomCenter = 0;
                    mouseYZoomCenter = 0;
                    zoom = 1;
                }

                zoom -= steps;

                if (zoom < 1)
                    zoom = 1;

                if (zoom == 1) {
                    mouseXZoomCenter = 0;
                    mouseYZoomCenter = 0;
                }

                if (getWidth() * zoom > maxZoneWidth) {
                    zoom = maxZoneWidth / getWidth();
                    mouseXZoomCenter -= (getWidth() / 2) - e.getX();
                    mouseYZoomCenter -= (getHeight() / 2) - e.getY();
                    return;
                }

                repaint();
            }
        });

        this.addMouseListener(new java.awt.event.MouseAdapter() {

            @Override
            public void mouseReleased(java.awt.event.MouseEvent e) {

                if (e.getButton() == MouseEvent.BUTTON2) {
                    if (zoom == 1)
                        return;

                    mouseXZoomCenter += (getWidth() / 2) - e.getX();
                    mouseYZoomCenter += (getHeight() / 2) - e.getY();

                    repaint();
                    return;
                }

                int panelWidth = getWidth();
                int panelHeight = getHeight();

                // To center the mouse click in column
                float colCenterBorder = zoneBackgroundWidth / 147;
                float borderWidth = colCenterBorder + mouseXZoomCenter + (panelWidth - zoneBackgroundWidth) / 2;
                float borderHeight = mouseYZoomCenter + (panelHeight - zoneBackgroundHeight) / 2;

                int mouseX = e.getX();
                int mouseY = e.getY();

                if (mouseX < borderWidth || mouseX > zoneBackgroundWidth + borderWidth || mouseY < borderHeight || mouseY > zoneBackgroundHeight + borderHeight)
                    return;

                float numCol = 23.4f;
                int currentCol = 0;
                float colWidth = zoneBackgroundWidth / numCol;
                for (int col = 0; col < numCol; col++)
                    if ((borderWidth + (col * colWidth)) < mouseX && mouseX <= (borderWidth + ((col + 1) * colWidth)))
                        currentCol = col;

                float numHalfRow = 29;
                int currentHalfRow = 0;
                float rowHeight = zoneBackgroundHeight / numHalfRow;
                for (int row = 0; row < numHalfRow; row++)
                    if ((borderHeight + (row * rowHeight)) < mouseY && mouseY <= (borderHeight + ((row + 1) * rowHeight)))
                        currentHalfRow = row;

                int currentRow = 0;

                if (currentCol % 2 == 0)
                    currentRow = (currentHalfRow / 2) + 1;
                else
                    currentRow = (currentHalfRow + 1) / 2;

                if (currentRow < 1 || currentRow > 14 || currentCol < 0 || currentCol > 22)
                    return;

                String rowString = currentRow + "";
                if (currentRow < 10)
                    rowString = "0" + rowString;
                String colString = (char) (currentCol + 65) + "";
                String sectorCoordinates = colString + rowString;

                if (voidSectorsZone.contains(sectorCoordinates))
                    return;

                Object[] options = { "MOVE", "FAKE NOISE", "SPOTLIGHT" };
                int choice = JOptionPane.showOptionDialog(new JFrame(), "You have selected the sector " + sectorCoordinates + "\n Choose option:", "Sector " + sectorCoordinates, JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);

                if (choice == 0)
                    frameGUI.sendCommand("move(" + sectorCoordinates + ")");
                if (choice == 1)
                    frameGUI.sendCommand("fake_noise(" + sectorCoordinates + ")");
                if (choice == 2)
                    frameGUI.sendCommand("activate_spotlight(" + sectorCoordinates + ")");

            }
        });

    }

    /**
     * This method shows to user the position subject of a certain action done
     * by a player, action that notified to all players a specific sector.
     * 
     * @param messageToShow
     */
    public void updateLastNotifiedPositionIcon(String messageToShow) {
        String controlString = "";
        String sectorTypeString = "";
        int messageLength = messageToShow.length();

        controlString = "NOISE IN SECTOR ";
        sectorTypeString = "M08";
        if (messageToShow.contains(controlString)) {
            this.setLastNotifiedPosition(messageToShow.substring(messageLength - sectorTypeString.length()));
            this.lastNotifiedPositionIcon.setNoiseIcon();
        }

        controlString = " because he has activated Attack Item Card before.";
        sectorTypeString = "M08";
        if (messageToShow.contains(controlString)) {
            this.setLastNotifiedPosition(messageToShow.substring(messageLength - controlString.length() - sectorTypeString.length(), messageLength - controlString.length()));
            this.lastNotifiedPositionIcon.setHumanAttackIcon();
        }

        controlString = "used a Spotlight Item Card in";
        sectorTypeString = "M08.";
        if (messageToShow.contains(controlString)) {
            this.setLastNotifiedPosition(messageToShow.substring(messageLength - sectorTypeString.length(), messageLength - 1));
            this.lastNotifiedPositionIcon.setSpotlightIcon();
        }

        controlString = "(Alien) attacked in";
        sectorTypeString = "M08.";
        if (messageToShow.contains(controlString)) {
            this.setLastNotifiedPosition(messageToShow.substring(messageLength - sectorTypeString.length(), messageLength - 1));
            this.lastNotifiedPositionIcon.setAlienAttackIcon();
        }

        controlString = "used a Teleport Item Card";
        sectorTypeString = "M08";
        if (messageToShow.contains(controlString)) {

            this.setLastNotifiedPosition(messageToShow.substring(messageLength - sectorTypeString.length()));
            this.lastNotifiedPositionIcon.setTeleportIcon();
        }

        controlString = "reached Escape Hatch in";
        sectorTypeString = "M08";
        if (messageToShow.contains(controlString)) {
            this.setLastNotifiedPosition(messageToShow.substring(messageLength - sectorTypeString.length()));
            this.lastNotifiedPositionIcon.setInvisibleIcon();
        }

        controlString = "has drawn an EscapeRed";
        if (messageToShow.contains(controlString)) {
            this.lastNotifiedPositionIcon.setEscapeRedIcon();
        }
        controlString = "has drawn an EscapeGreen";
        if (messageToShow.contains(controlString)) {
            this.lastNotifiedPositionIcon.setEscapeGreenIcon();
        }
    }

}
