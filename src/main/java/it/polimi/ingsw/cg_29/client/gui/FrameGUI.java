package it.polimi.ingsw.cg_29.client.gui;

import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.client.Client;
import it.polimi.ingsw.cg_29.client.gui.panel.ConnectionChoicePanel;
import it.polimi.ingsw.cg_29.client.gui.panel.GamePanel;
import it.polimi.ingsw.cg_29.client.gui.panel.StartPanel;
import it.polimi.ingsw.cg_29.client.gui.panel.WaitingForPlayersPanel;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

/**
 * Contains the implementation of the GUI JFrame.
 *
 * @author Luca
 * @version 1.0
 */
public class FrameGUI extends JFrame implements Runnable {

    /**
     * parseSound is the object used for stars a sound effect in order to the
     * type of notification to a user.
     */
    private transient ParseMessagesToSounds parserSound = new ParseMessagesToSounds();

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * The image of zone of the current match.
     */
    private ImageIcon zoneImage;

    /**
     * The list of void sectors of zone of the current match.
     */
    private transient List<String> voidSectorsZone;

    /**
     * The reference of the client.
     */
    private transient Client client;

    /**
     * The reference of panel show before starting match.
     */
    private WaitingForPlayersPanel waitingForPlayersPanel = null;

    /**
     * The reference of panel that shows informations of match and other
     * interactive panels.
     */
    private GamePanel gamePanel = null;

    /**
     * Create a new frame with reference of a client.
     * 
     * @param clientGUI
     *            reference of the client.
     */
    public FrameGUI(Client clientGUI) {
        this.client = clientGUI;

        try {
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            LoggerClass.getLogger().warn(e);
        }

        this.setTitle("Escape From The Aliens In Outer Space");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                sendCommand("disconnect");
                e.getWindow().dispose();
            }
        });
        this.setSize(1000, 600);
        this.setResizable(true);
        this.setMinimumSize(new Dimension(680, 500));

        this.getContentPane().add(new StartPanel(new ConnectionChoicePanel(this), 1));
    }

    /**
     * @return the parserSound
     */
    public ParseMessagesToSounds getParserSound() {
        return parserSound;
    }

    /**
     * @return the zoneImage
     */
    public ImageIcon getZoneImage() {
        return zoneImage;
    }

    /**
     * @return the voidSectorsZone
     */
    public List<String> getVoidSectorsZone() {
        return voidSectorsZone;
    }

    /**
     * Set specific zone before starting match.
     * 
     * @param num
     *            number of zone to set.
     */
    public void setZone(int num) {
        if (num == 1) {
            this.zoneImage = new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "zone" + File.separatorChar + "galilei.png");
            this.voidSectorsZone = new ArrayList<String>(Arrays.asList("W01", "E14", "E07", "W08", "W07", "F13", "F14", "F12", "V07", "D01", "T01", "D06", "D04", "D07", "E01", "T10", "T09", "T04", "T03", "I12", "J07", "J12", "K07", "H05", "H10", "H11", "I06", "N07", "O03", "O04", "O01", "L07", "K13", "M07", "R14", "R11", "R10", "S03", "S14", "S10", "S11", "S01", "B07", "A08", "A01", "A07"));
        }
        if (num == 2) {
            this.zoneImage = new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "zone" + File.separatorChar + "fermi.png");
            this.voidSectorsZone = new ArrayList<String>(Arrays.asList("W01", "E12", "W04", "E13", "W05", "E14", "W02", "W03", "E08", "E09", "E10", "E11", "E04", "E05", "E06", "E07", "F02", "F01", "F04", "W14", "F03", "W09", "W08", "W07", "W06", "W13", "W12", "W11", "W10", "V01", "V02", "F13", "V03", "F14", "V04", "F11", "F12", "F09", "F10", "F07", "F08", "F05", "F06", "V14", "G05", "V13", "G04", "G03", "G02", "G01", "V06", "V05", "V08", "V07", "V10", "V09", "V12", "V11", "C10", "U02", "C11", "U03", "C12", "C13", "U01", "C14", "C02", "C03", "C04", "C05", "C06", "C07", "C08", "C09", "U14", "U13", "D02", "U12", "D01", "U11", "U10", "U09", "U08", "U07", "U06", "U05", "U04", "D13", "D14", "D11", "T01", "D12", "T02", "D05", "D06", "D03", "D04", "D09", "D10", "D07", "D08", "T12", "E03", "T11",
                    "E02", "T14", "E01", "T13", "T08", "T07", "T10", "T09", "T04", "T03", "T06", "T05", "I13", "I14", "I08", "I11", "J06", "J02", "J14", "J13", "J09", "K01", "K09", "K06", "K07", "K05", "G07", "G06", "G09", "G08", "G11", "G10", "G13", "G12", "G14", "H01", "H02", "H03", "H04", "H05", "H06", "H09", "H08", "H07", "H14", "H13", "H12", "I02", "I03", "I01", "I04", "I05", "N09", "N06", "N02", "M14", "M12", "O11", "O13", "O08", "O03", "O02", "O05", "O04", "O01", "N13", "N14", "L02", "L01", "K12", "K10", "K14", "M05", "M07", "M06", "M09", "M10", "M01", "L13", "L14", "R07", "R08", "R05", "R06", "R03", "R04", "R01", "R02", "R13", "R14", "R11", "R12", "R09", "R10", "S06", "S07", "S08", "S09", "S02", "S03", "S04", "S05", "S14", "S10", "S11", "S12", "S13", "S01", "P01", "P02", "P05",
                    "P06", "P03", "P04", "C01", "P09", "P07", "P08", "P13", "P14", "P12", "B10", "B09", "B12", "O14", "B11", "B14", "B13", "B02", "B01", "B04", "B03", "B06", "B05", "B08", "B07", "Q01", "Q02", "Q03", "Q04", "Q05", "Q06", "Q07", "Q08", "Q09", "Q10", "Q11", "Q12", "Q13", "Q14", "A11", "A10", "A09", "A08", "A14", "A13", "A12", "A03", "A02", "A01", "A07", "A06", "A05", "A04"));
        }
        if (num == 3) {
            this.zoneImage = new ImageIcon("." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar + "zone" + File.separatorChar + "galvani.png");
            this.voidSectorsZone = new ArrayList<String>(Arrays.asList("W01", "E13", "E14", "W02", "E11", "E04", "W14", "F03", "F13", "F14", "F12", "F09", "F07", "F08", "F05", "F06", "V14", "G05", "G03", "G01", "V06", "V05", "V10", "V09", "V12", "U02", "C12", "C14", "C03", "U13", "D02", "U09", "U07", "U06", "U05", "D13", "D14", "D11", "D05", "D06", "D04", "D09", "D10", "D08", "E02", "T13", "T08", "T07", "T10", "T04", "T03", "T06", "T05", "I13", "I09", "I11", "J05", "J07", "J02", "J13", "J12", "J11", "J09", "K08", "K07", "K02", "G12", "G14", "H01", "H04", "H06", "H10", "H08", "H14", "I01", "I06", "I04", "N09", "N07", "N05", "N03", "M12", "M13", "O11", "O13", "O06", "O04", "O01", "N13", "L04", "L10", "L07", "L02", "K12", "M07", "M08", "M10", "M03", "L14", "R07", "R08", "R06", "R02", "R11",
                    "R12", "R09", "S03", "S11", "S12", "P06", "P04", "C01", "P10", "P07", "P08", "P13", "P12", "B09", "B14", "B13", "B01", "B03", "B06", "B05", "B08", "B07", "Q05", "Q10", "Q12", "Q13", "A14", "A13", "A02", "A01"));
        }
    }

    /**
     * Establish connection with server; initializes and shows the panel with
     * informations of starting match.
     * 
     * @param choise
     *            represent type of connection.
     */
    public void waitForStartGame(String choise) {

        this.getContentPane().removeAll();
        this.waitingForPlayersPanel = new WaitingForPlayersPanel(this);
        this.getContentPane().add(new StartPanel(this.waitingForPlayersPanel, 2));
        this.revalidate();
        this.repaint();

        this.client.establishConnection(choise);
    }

    /**
     * Initialize and show gamePanel
     */
    public void startGame() {
        this.getContentPane().removeAll();
        this.gamePanel = new GamePanel(this);
        this.getContentPane().add(gamePanel);
        this.revalidate();
        this.repaint();
    }

    /**
     * Send command to server.
     * 
     * @param command
     *            command to send.
     */
    public void sendCommand(String command) {
        this.client.sendMessage(command);
    }

    /**
     * Set zone image to resize without loss definition.
     * 
     * @param hd
     *            definition to set
     */
    public void setHD(boolean hd) {
        this.gamePanel.getZonePanel().setHD(hd);
    }

    /**
     * Show this frame.
     */
    @Override
    public void run() {
        this.setVisible(true);
    }

    /**
     * Parse received message from server or broker to show informations in
     * panels and play sounds in order to informations shown and show notified
     * rivals position on the zone.
     * 
     * @param message
     *            received from server or broker
     */
    public void parseMessage(String message) {

        String messageToShow = message;

        String controlString = "";

        if (messageToShow == null || messageToShow.equals(controlString))
            return;

        if (this.waitingForPlayersPanel != null) {

            controlString = "[INFO] ";
            if (messageToShow.length() >= controlString.length() && messageToShow.substring(0, controlString.length()).equals(controlString)) {
                messageToShow = messageToShow.substring(controlString.length());
            }

            controlString = "[START] [ZONE ";
            if (messageToShow.length() >= controlString.length() && messageToShow.substring(0, controlString.length()).equals(controlString)) {

                this.setZone(2);

                if (messageToShow.length() >= controlString.length() + 4) {
                    String zoneChars = messageToShow.substring(controlString.length(), controlString.length() + 4);
                    if ("Gali".equals(zoneChars))
                        this.setZone(1);
                    if ("Galv".equals(zoneChars))
                        this.setZone(3);
                }

                this.waitingForPlayersPanel = null;
                this.startGame();
                return;
            }

            this.waitingForPlayersPanel.showInfo(messageToShow);

        }

        if (this.gamePanel != null) {

            if (messageToShow.contains("Connection Established!") || messageToShow.contains("Send \"help\" to see valid commands."))
                return;

            controlString = "[START] ";
            if (messageToShow.length() >= controlString.length() && messageToShow.substring(0, controlString.length()).equals(controlString)) {
                return;
            }

            controlString = "[STATE] ";
            if (messageToShow.length() >= controlString.length() && messageToShow.substring(0, controlString.length()).equals(controlString)) {
                this.gamePanel.updateState(messageToShow);
                parserSound.doStateSound(messageToShow);
                return;
            }

            controlString = "[INFO] ";
            if (messageToShow.length() >= controlString.length() && messageToShow.substring(0, controlString.length()).equals(controlString)) {
                messageToShow = messageToShow.substring(controlString.length());
                this.gamePanel.showInfo(messageToShow);
                this.gamePanel.getZonePanel().updateLastNotifiedPositionIcon(messageToShow);
                parserSound.doInfoSound(messageToShow);
                this.sendCommand("show_state");
                return;
            }

            controlString = "[CHAT] ";
            if (messageToShow.length() >= controlString.length() && messageToShow.substring(0, controlString.length()).equals(controlString)) {
                messageToShow = messageToShow.substring(controlString.length());
                this.gamePanel.showChat(messageToShow);
                parserSound.doChatSound(messageToShow);
                return;
            }

            showDialog(messageToShow);

        }

    }

    /**
     * Show message in a pop-up
     * 
     * @param message
     *            to show
     */
    private void showDialog(final String message) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                String dialogTitle = "MESSAGE";
                String resourcesFolderPath = "." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar;

                if (message.contains("Send \"fake_noise(SECTOR_COORDINATES)\".")) {
                    sendCommand("show_state");
                    JOptionPane.showMessageDialog(null, "You draw a noise in any sector card.\nClick a sector to fake noise!", dialogTitle, JOptionPane.PLAIN_MESSAGE, new ImageIcon(new ImageIcon(resourcesFolderPath + "card" + File.separatorChar + "noise_in_any_sector.png").getImage().getScaledInstance(50, -1, Image.SCALE_SMOOTH)));
                    return;
                }
                if (message.contains("YOU SHALL NOT PASS!")) {
                    parserSound.doSpecialSound(message);
                    JOptionPane.showMessageDialog(null, "YOU SHALL NOT PASS!", dialogTitle, JOptionPane.PLAIN_MESSAGE, new ImageIcon(new ImageIcon(resourcesFolderPath + "image" + File.separatorChar + "shallNotPass.png").getImage().getScaledInstance(-1, 300, Image.SCALE_SMOOTH)));
                    return;
                }
                if (message.contains("OAK:  <<NOW IS NOT THE TIME TO USE THAT!>>")) {
                    parserSound.doSpecialSound(message);
                    JOptionPane.showMessageDialog(null, "OAK:  <<NOW IS NOT THE TIME TO USE THAT!>>", dialogTitle, JOptionPane.PLAIN_MESSAGE, new ImageIcon(new ImageIcon(resourcesFolderPath + "image" + File.separatorChar + "professorOak.png").getImage().getScaledInstance(-1, 300, Image.SCALE_SMOOTH)));
                    return;
                }
                if (message.contains("Reconnecting established."))
                    sendCommand("show_state");
                JOptionPane.showMessageDialog(null, message, dialogTitle, JOptionPane.PLAIN_MESSAGE);
            }
        });
    }

}
