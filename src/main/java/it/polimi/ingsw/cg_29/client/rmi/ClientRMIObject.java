package it.polimi.ingsw.cg_29.client.rmi;

import it.polimi.ingsw.cg_29.LoggerClass;
import it.polimi.ingsw.cg_29.client.Client;
import it.polimi.ingsw.cg_29.gamemanager.commons.BrokerObjectInterface;
import it.polimi.ingsw.cg_29.gamemanager.commons.ClientRMIObjectInterface;
import it.polimi.ingsw.cg_29.gamemanager.commons.ServerRMIViewObjectInterface;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Defines the interface of the Broker remote object. Every clients that wants
 * to subscribe to this Broker for receiving messages from it, could call the
 * method subscribe() offered by this.
 * 
 * @author Fulvio
 * @version 1.0
 */
public class ClientRMIObject implements ClientRMIObjectInterface {
    /**
     * client is the reference that associates a client to the specific remote
     * object client.
     */
    private Client client;
    /**
     * serverView is the reference to the remote object that is used to set it
     * on the specific client.
     */
    private ServerRMIViewObjectInterface serverView;

    /**
     * creates a remote object client that is necessary to the RMI communication
     * between Server and Client, and between Broker and Client (subscriber).
     * 
     * @param client
     */
    public ClientRMIObject(Client client) {
        super();
        this.client = client;
    }

    @Override
    /**
     * This method is used in remote for sends to client a message by Server or Broker and
     * for show it to the user.
     * 
     * @param msg that is the message to send by the Server or by the Broker to the Client.
     */
    public void dispatchMessage(String msg) {
        this.client.showMessage(msg);
    }

    @Override
    /**
     * This method could be remotely call the ServerRMIView to setting the identification
     * of the controller and the ServerView connected to this and to give the identification
     * client to this. 
     * This method even subscribe this to the remote object broker called "Broker" registered
     * on registry on port 7777.
     * 
     * @param controllerId
     * @param clientId
     * @param serverRMIViewObjectInterface
     */
    public void setId(String controllerId, String clientId, ServerRMIViewObjectInterface serverView) {

        boolean reconnecting = false;

        if (this.client.getClientId() == null || this.client.getControllerId() == null) {
            this.client.setControllerId(controllerId);
            this.client.setClientId(clientId);
            this.client.showMessage("Connection Established! (MATCH_ID: " + controllerId + " CLIENT_ID: " + clientId + ")");
        } else
            reconnecting = true;

        this.serverView = serverView;

        try {
            Registry registry = LocateRegistry.getRegistry(7777);
            BrokerObjectInterface brokerRMI = (BrokerObjectInterface) registry.lookup("Broker");
            brokerRMI.subscribe((ClientRMIObjectInterface) this, this.client.getControllerId());
        } catch (NotBoundException | RemoteException e) {
            LoggerClass.getLogger().warn(e);
            this.client.showMessage("Not found the remote object Broker that this is going to subcribe on.");
        }

        if (reconnecting)
            this.client.showMessage("Reconnecting established.");

    }

    /**
     * @return the serverRMIView of the remote object serverRMIView.
     */
    public ServerRMIViewObjectInterface getServerView() {
        return serverView;
    }

}
