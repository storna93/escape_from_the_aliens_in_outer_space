package it.polimi.ingsw.cg_29.client.gui.panel;

import it.polimi.ingsw.cg_29.client.gui.FrameGUI;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * Contains the implementation of interactive panel that displays public chat
 * messages and allows the player to send a new message.
 * 
 * @author Luca
 * @version 1.0
 */
public class ChatPanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Reference to frame GUI to call method to send command.
     */
    private FrameGUI frameGUI;

    /**
     * Text Field in which write a new message text.
     */
    private JTextField textField;

    /**
     * Text Area in which displays chat messages.
     */
    private JTextArea chatTextDisplay;

    /**
     * 
     * Create a new ChatPanel that contains textField, chatTextDisplay and a
     * button to send message.
     */
    public ChatPanel(FrameGUI frameGUI) {
        this.frameGUI = frameGUI;
        this.chatTextDisplay = new JTextArea(0, 27);
        this.chatTextDisplay.setLineWrap(true);
        this.chatTextDisplay.setWrapStyleWord(true);
        this.chatTextDisplay.setEditable(false);
        this.chatTextDisplay.setFont(this.chatTextDisplay.getFont().deriveFont(Font.BOLD, 13f));

        this.textField = new JTextField();
        this.textField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                send();
            }
        });

        JScrollPane scrollable = new JScrollPane(chatTextDisplay);
        scrollable.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        this.setLayout(new BorderLayout());

        JPanel sendPanel = new JPanel(new BorderLayout());
        JButton sendButton = new JButton("SEND");
        sendPanel.add(textField, BorderLayout.CENTER);
        sendPanel.add(sendButton, BorderLayout.EAST);
        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                send();
            }
        });

        this.add(new JLabel("CHAT", SwingConstants.CENTER), BorderLayout.NORTH);
        this.add(scrollable, BorderLayout.CENTER);
        this.add(sendPanel, BorderLayout.SOUTH);
    }

    /**
     * Send the text in textField as message.
     */
    private void send() {
        if (!"".equals(textField.getText())) {
            String message = textField.getText();
            textField.setText("");
            frameGUI.sendCommand("chat(" + message + ")");
        }
    }

    /**
     * Add a message to chatTextDisplay.
     * 
     * @param message
     *            message to add.
     */
    protected void addChatMessage(String message) {

        Calendar cal = Calendar.getInstance();
        cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

        this.chatTextDisplay.append("[" + sdf.format(cal.getTime()) + "] client_1: " + message + "\n");
        this.chatTextDisplay.setCaretPosition(this.chatTextDisplay.getDocument().getLength());
    }
}
