package it.polimi.ingsw.cg_29;

import org.apache.log4j.Logger;

/**
 * Contains the reference to the Logger
 * 
 * @author Luca
 * @version 1.0
 */
public class LoggerClass {

    /**
     * Reference to Logger
     */
    private static final Logger LOGGER = Logger.getLogger(LoggerClass.class);

    /**
     * 
     */
    private LoggerClass() {
    }

    /**
     * @return the logger
     */
    public static Logger getLogger() {
        return LOGGER;
    }

}
